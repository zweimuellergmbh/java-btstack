package ch.zweimuellergmbh.btstack.command;

import java.util.Map;

public class GattResponse {
    public final static int ATT_ERROR_SUCCESS = 0x0;
    public final static int ATT_ERROR_INSUFFICIENT_ENCRYPTION = 0x0F;

    final static Map<Integer, String> attErrorCodes = Map.of(
            ATT_ERROR_INSUFFICIENT_ENCRYPTION, "ATT_ERROR_INSUFFICIENT_ENCRYPTION"
    );

    private final boolean requestOk;
    private final int connectionHandle;
    private int statusCode;

    public GattResponse(boolean requestOk, int connectionHandle, int statusCode) {
        this.requestOk = requestOk;
        this.connectionHandle = connectionHandle;
        this.statusCode = statusCode;
    }

    public boolean isRequestOk() {
        return requestOk;
    }

    public int getConnectionHandle() {
        return connectionHandle;
    }

    public int getStatusCode() {
        return statusCode;}
}
