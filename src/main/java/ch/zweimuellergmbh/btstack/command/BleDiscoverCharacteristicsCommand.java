package ch.zweimuellergmbh.btstack.command;

import ch.zweimuellergmbh.btstack.BleCharacteristic;
import ch.zweimuellergmbh.btstack.BleService;
import com.bluekitchen.btstack.GATTCharacteristic;
import com.bluekitchen.btstack.Packet;
import com.bluekitchen.btstack.event.GATTEventCharacteristicQueryResult;
import com.bluekitchen.btstack.event.GATTEventQueryComplete;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class BleDiscoverCharacteristicsCommand extends BleCommand {
    private static final Logger logger = LoggerFactory.getLogger(BleDiscoverCharacteristicsCommand.class);
    private final BleService bleService;
    private final int connectionHandle;

    private final List<BleCharacteristic> discoveredCharacteristics = new ArrayList<>();

    public BleDiscoverCharacteristicsCommand(final BleService bleService, int connectionHandle) {
        this.bleService = bleService;
        this.connectionHandle = connectionHandle;
    }

    @Override
    public void bleRequest() {
        logger.info("[DiscoverCharacteristics] Discover characteristics for service: {}", bleService.getInternalGattServiceReference().getUUID());
        bleService.getBleDevice().getBleAdapter().getBtstack().GATTDiscoverCharacteristicsForService(connectionHandle, bleService.getInternalGattServiceReference());
    }

    @Override
    public void handleResponsePackets(Packet packet) {
        logger.debug("[DiscoverCharacteristics] {}", packet);

        if (packet instanceof GATTEventCharacteristicQueryResult) {
            final GATTCharacteristic characteristic = ((GATTEventCharacteristicQueryResult) packet).getCharacteristic();
            discoveredCharacteristics.add(new BleCharacteristic(bleService, characteristic));
            logger.info("[DiscoverCharacteristics] Discovered characteristic: {}", characteristic.getUUID());
        }
        
        if (packet instanceof GATTEventQueryComplete) {
            // query finished
            if (((GATTEventQueryComplete) packet).getATTStatus() == GattResponse.ATT_ERROR_SUCCESS) {
                getGattResponseCallback().accept(new GattResponse(true,
                        ((GATTEventQueryComplete) packet).getHandle(),
                        ((GATTEventQueryComplete) packet).getATTStatus()));
            }
            else {
                // fail
                getGattResponseCallback().accept(new GattResponse(false,
                        ((GATTEventQueryComplete) packet).getHandle(),
                        ((GATTEventQueryComplete) packet).getATTStatus()));
            }
        }
    }

    public List<BleCharacteristic> getDiscoveredCharacteristics() {
        return discoveredCharacteristics;
    }

}
