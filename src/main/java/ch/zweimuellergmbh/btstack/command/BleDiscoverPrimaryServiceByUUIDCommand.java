package ch.zweimuellergmbh.btstack.command;

import ch.zweimuellergmbh.btstack.BleDevice;
import ch.zweimuellergmbh.btstack.BleService;
import com.bluekitchen.btstack.BT_UUID;
import com.bluekitchen.btstack.GATTService;
import com.bluekitchen.btstack.Packet;
import com.bluekitchen.btstack.event.GATTEventQueryComplete;
import com.bluekitchen.btstack.event.GATTEventServiceQueryResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BleDiscoverPrimaryServiceByUUIDCommand extends BleCommand {
    private static final Logger logger = LoggerFactory.getLogger(BleDiscoverPrimaryServiceByUUIDCommand.class);
    private final BleDevice bleDevice;
    private final BT_UUID uuid;

    private BleService discoveredService = null;

    public BleDiscoverPrimaryServiceByUUIDCommand(final BleDevice bleDevice, final BT_UUID uuid) {
        this.bleDevice = bleDevice;
        this.uuid = uuid;
    }

    @Override
    public void bleRequest() {
        logger.info("[DiscoverPrimaryServiceUUID] Discover Primary services by UUID: {}", uuid.toString());
        bleDevice.getBleAdapter().getBtstack().GATTDiscoverPrimaryServicesByUUID128(bleDevice.getConnectionHandle(), uuid);
    }

    @Override
    public void handleResponsePackets(Packet packet) {
        logger.debug("[DiscoverPrimaryServiceUUID] {}", packet);

        if (packet instanceof GATTEventServiceQueryResult) {
            final GATTService service = ((GATTEventServiceQueryResult) packet).getService();
            discoveredService = new BleService(bleDevice, service);
            logger.info("[DiscoverPrimaryServiceUUID] Discovered Service: {} on device: {}", service.getUUID(), bleDevice.getBleAddress().getAddress());
        }

        if (packet instanceof GATTEventQueryComplete) {
            // query finished
            if (((GATTEventQueryComplete) packet).getATTStatus() == GattResponse.ATT_ERROR_SUCCESS) {
                getGattResponseCallback().accept(new GattResponse(true,
                        ((GATTEventQueryComplete) packet).getHandle(),
                        ((GATTEventQueryComplete) packet).getATTStatus()));
            }
            else {
                // fail
                getGattResponseCallback().accept(new GattResponse(false,
                        ((GATTEventQueryComplete) packet).getHandle(),
                        ((GATTEventQueryComplete) packet).getATTStatus()));
            }
        }
    }

    public BleService getDiscoveredService() {
        return discoveredService;
    }

}
