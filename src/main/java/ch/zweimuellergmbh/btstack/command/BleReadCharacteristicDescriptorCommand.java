package ch.zweimuellergmbh.btstack.command;

import ch.zweimuellergmbh.btstack.BleCharacteristicDescriptor;
import ch.zweimuellergmbh.btstack.BleDevice;
import com.bluekitchen.btstack.Packet;
import com.bluekitchen.btstack.event.GATTEventCharacteristicDescriptorQueryResult;
import com.bluekitchen.btstack.event.GATTEventQueryComplete;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BleReadCharacteristicDescriptorCommand extends BleCommand {
    private static final Logger logger = LoggerFactory.getLogger(BleReadCharacteristicDescriptorCommand.class);

    private final BleCharacteristicDescriptor bleCharacteristicDescriptor;

    private byte[] readDescriptorValue;

    public BleReadCharacteristicDescriptorCommand(final BleCharacteristicDescriptor bleCharacteristicDescriptor) {
        this.bleCharacteristicDescriptor = bleCharacteristicDescriptor;
    }

    @Override
    public void bleRequest() {
        logger.info("[ReadCharacteristicDescriptor] Read descriptor: {}", bleCharacteristicDescriptor);
        final BleDevice bleDevice = bleCharacteristicDescriptor.getBleCharacteristic().getService().getBleDevice();
        bleDevice.getBleAdapter().getBtstack().GATTReadCharacteristicDescriptor(
                bleDevice.getConnectionHandle(), bleCharacteristicDescriptor.getInternalDescriptor());
    }

    @Override
    public void handleResponsePackets(Packet packet) {
        logger.debug("[ReadCharacteristicDescriptor] {}", packet);

        if (packet instanceof GATTEventCharacteristicDescriptorQueryResult) {
            GATTEventCharacteristicDescriptorQueryResult packet1 = (GATTEventCharacteristicDescriptorQueryResult) packet;
            readDescriptorValue = packet1.getDescriptor();
        }

        if (packet instanceof GATTEventQueryComplete) {
            // query finished
            if (((GATTEventQueryComplete) packet).getATTStatus() == GattResponse.ATT_ERROR_SUCCESS) {
                getGattResponseCallback().accept(new GattResponse(true,
                        ((GATTEventQueryComplete) packet).getHandle(),
                        ((GATTEventQueryComplete) packet).getATTStatus()));
            }
            else {
                // fail
                getGattResponseCallback().accept(new GattResponse(false,
                        ((GATTEventQueryComplete) packet).getHandle(),
                        ((GATTEventQueryComplete) packet).getATTStatus()));
            }
        }
    }

    public byte[] getReadValue() {
        return readDescriptorValue;
    }
}
