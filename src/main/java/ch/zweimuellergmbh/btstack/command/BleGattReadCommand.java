package ch.zweimuellergmbh.btstack.command;

import ch.zweimuellergmbh.btstack.BleCharacteristic;
import ch.zweimuellergmbh.btstack.BleDevice;
import com.bluekitchen.btstack.Packet;
import com.bluekitchen.btstack.event.GATTEventCharacteristicValueQueryResult;
import com.bluekitchen.btstack.event.GATTEventQueryComplete;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BleGattReadCommand extends BleCommand {
    private static final Logger logger = LoggerFactory.getLogger(BleGattReadCommand.class);

    private final BleCharacteristic bleCharacteristic;
    
    private byte[] readGattValue;

    public BleGattReadCommand(BleCharacteristic bleCharacteristic) {
        this.bleCharacteristic = bleCharacteristic;
    }

    @Override
    public void bleRequest() {
        logger.info("[GattRead] Read gatt characteristic: {}", bleCharacteristic.getInternalCharacteristicReference().getUUID());
        final BleDevice bleDevice = bleCharacteristic.getService().getBleDevice();
        bleDevice.getBleAdapter().getBtstack().GATTReadValueOfCharacteristic(
                bleDevice.getConnectionHandle(), bleCharacteristic.getInternalCharacteristicReference());
    }

    @Override
    public void handleResponsePackets(Packet packet) {
        logger.debug("[GattRead] {}", packet);

        if (packet instanceof GATTEventCharacteristicValueQueryResult) {
            readGattValue = ((GATTEventCharacteristicValueQueryResult) packet).getValue();
        }

        if (packet instanceof GATTEventQueryComplete) {
            // query finished
            if (((GATTEventQueryComplete) packet).getATTStatus() == GattResponse.ATT_ERROR_SUCCESS) {
                getGattResponseCallback().accept(new GattResponse(true,
                        ((GATTEventQueryComplete) packet).getHandle(),
                        ((GATTEventQueryComplete) packet).getATTStatus()));
            }
            else {
                // fail
                getGattResponseCallback().accept(new GattResponse(false,
                        ((GATTEventQueryComplete) packet).getHandle(),
                        ((GATTEventQueryComplete) packet).getATTStatus()));
            }
        }
    }

    public byte[] getReadValue() {
        return readGattValue;
    }
}
