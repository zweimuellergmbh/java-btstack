package ch.zweimuellergmbh.btstack.command;

import ch.zweimuellergmbh.btstack.BleDevice;
import com.bluekitchen.btstack.Packet;
import com.bluekitchen.btstack.event.HCIEventDisconnectionComplete;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BleDisconnectCommand extends BleCommand {

    private static final Logger logger = LoggerFactory.getLogger(BleDisconnectCommand.class);

    private final BleDevice bleDevice;

    public BleDisconnectCommand(BleDevice bleDevice) {
        this.bleDevice = bleDevice;
    }

    @Override
    public void bleRequest() {
        logger.info("[Disconnect] Disconnect from handle: {} with address: {}", bleDevice.getConnectionHandle(),
                bleDevice.getBleAddress().getAddress());
        bleDevice.getBleAdapter().getBtstack().GAPDisconnect(bleDevice.getConnectionHandle());
    }

    @Override
    public void handleResponsePackets(Packet packet) {
        logger.debug("[Disconnect] {}", packet);

        if (packet instanceof HCIEventDisconnectionComplete) {
            final HCIEventDisconnectionComplete event = (HCIEventDisconnectionComplete) packet;
            logger.info("[Disconnect] from: {}", bleDevice.getBleAddress().getAddress());
            getGattResponseCallback().accept(new GattResponse(true,
                    ((HCIEventDisconnectionComplete) packet).getConnectionHandle(),
                    ((HCIEventDisconnectionComplete) packet).getStatus()));
        }
    }
}
