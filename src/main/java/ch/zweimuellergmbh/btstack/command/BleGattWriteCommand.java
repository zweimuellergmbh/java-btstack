package ch.zweimuellergmbh.btstack.command;

import ch.zweimuellergmbh.btstack.BleCharacteristic;
import ch.zweimuellergmbh.btstack.BleDevice;
import com.bluekitchen.btstack.Packet;
import com.bluekitchen.btstack.event.GATTEventQueryComplete;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BleGattWriteCommand extends BleCommand {
    private static final Logger logger = LoggerFactory.getLogger(BleGattWriteCommand.class);

    private final BleCharacteristic bleCharacteristic;
    private final byte[] data;

    public BleGattWriteCommand(BleCharacteristic bleCharacteristic, byte[] data) {
        this.bleCharacteristic = bleCharacteristic;
        this.data = data;
    }

    @Override
    public void bleRequest() {
        logger.info("[GattWrite] Write gatt characteristic: {}", bleCharacteristic.getInternalCharacteristicReference().getUUID());
        final BleDevice bleDevice = bleCharacteristic.getService().getBleDevice();
        bleDevice.getBleAdapter().getBtstack().GATTWriteValueOfCharacteristic(
                bleDevice.getConnectionHandle(), bleCharacteristic.getInternalCharacteristicReference(),
                data.length, data);
    }

    @Override
    public void handleResponsePackets(Packet packet) {
        logger.debug("[GattWrite] {}", packet);

        if (packet instanceof GATTEventQueryComplete) {
            // query finished
            if (((GATTEventQueryComplete) packet).getATTStatus() == GattResponse.ATT_ERROR_SUCCESS) {
                getGattResponseCallback().accept(new GattResponse(true,
                        ((GATTEventQueryComplete) packet).getHandle(),
                        ((GATTEventQueryComplete) packet).getATTStatus()));
            }
            else {
                // fail
                getGattResponseCallback().accept(new GattResponse(false,
                        ((GATTEventQueryComplete) packet).getHandle(),
                        ((GATTEventQueryComplete) packet).getATTStatus()));
            }
        }
    }
}
