package ch.zweimuellergmbh.btstack.command;

import ch.zweimuellergmbh.btstack.BleCharacteristic;
import ch.zweimuellergmbh.btstack.BleService;
import com.bluekitchen.btstack.BT_UUID;
import com.bluekitchen.btstack.GATTCharacteristic;
import com.bluekitchen.btstack.Packet;
import com.bluekitchen.btstack.event.GATTEventCharacteristicQueryResult;
import com.bluekitchen.btstack.event.GATTEventQueryComplete;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BleDiscoverCharacteristicByUUIDCommand extends BleCommand {
    private static final Logger logger = LoggerFactory.getLogger(BleDiscoverCharacteristicByUUIDCommand.class);
    private final BleService bleService;

    private BleCharacteristic discoveredCharacteristic = null;

    private final BT_UUID uuid;

    public BleDiscoverCharacteristicByUUIDCommand(final BleService bleService, BT_UUID uuid) {
        this.uuid = uuid;
        this.bleService = bleService;
    }

    @Override
    public void bleRequest() {
        logger.info("[DiscoverCharacteristicByUUID] Discover characteristic by UUID: {}", uuid);
        bleService.getBleDevice().getBleAdapter().getBtstack().GATTDiscoverCharacteristicsForServiceByUUID128(
                bleService.getBleDevice().getConnectionHandle(), bleService.getInternalGattServiceReference(), uuid);
    }

    @Override
    public void handleResponsePackets(Packet packet) {
        logger.debug("[DiscoverCharacteristicByUUID] {}", packet);

        if (packet instanceof GATTEventCharacteristicQueryResult) {
            final GATTCharacteristic characteristic = ((GATTEventCharacteristicQueryResult) packet).getCharacteristic();
            discoveredCharacteristic = new BleCharacteristic(bleService, characteristic);
            logger.info("[DiscoverCharacteristicByUUID] Discovered characteristic: {}", characteristic.getUUID());
        }

        if (packet instanceof GATTEventQueryComplete) {
            // query finished
            if (((GATTEventQueryComplete) packet).getATTStatus() == GattResponse.ATT_ERROR_SUCCESS) {
                getGattResponseCallback().accept(new GattResponse(true,
                        ((GATTEventQueryComplete) packet).getHandle(),
                        ((GATTEventQueryComplete) packet).getATTStatus()));
            }
            else {
                // fail
                getGattResponseCallback().accept(new GattResponse(false,
                        ((GATTEventQueryComplete) packet).getHandle(),
                        ((GATTEventQueryComplete) packet).getATTStatus()));
            }
        }
    }

    public BleCharacteristic getDiscoveredCharacteristic() {
        return discoveredCharacteristic;
    }

}
