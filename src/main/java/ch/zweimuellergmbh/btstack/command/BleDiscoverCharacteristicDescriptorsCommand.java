package ch.zweimuellergmbh.btstack.command;

import ch.zweimuellergmbh.btstack.BleCharacteristic;
import ch.zweimuellergmbh.btstack.BleCharacteristicDescriptor;
import ch.zweimuellergmbh.btstack.BleDevice;
import com.bluekitchen.btstack.GATTCharacteristicDescriptor;
import com.bluekitchen.btstack.Packet;
import com.bluekitchen.btstack.event.GATTEventAllCharacteristicDescriptorsQueryResult;
import com.bluekitchen.btstack.event.GATTEventQueryComplete;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class BleDiscoverCharacteristicDescriptorsCommand extends BleCommand {
    private static final Logger logger = LoggerFactory.getLogger(BleDiscoverCharacteristicDescriptorsCommand.class);

    private final BleCharacteristic bleCharacteristic;

    private final List<BleCharacteristicDescriptor> discoveredDescriptors = new ArrayList<>();

    public BleDiscoverCharacteristicDescriptorsCommand(final BleCharacteristic bleCharacteristic) {
        this.bleCharacteristic = bleCharacteristic;
    }

    @Override
    public void bleRequest() {
        logger.info("[DiscoverCharacteristicDescriptors] Discover characteristic descriptors for: {}", bleCharacteristic.getInternalCharacteristicReference().getUUID());
        final BleDevice bleDevice = bleCharacteristic.getService().getBleDevice();
        bleDevice.getBleAdapter().getBtstack().GATTDiscoverCharacteristicDescriptors(
                bleDevice.getConnectionHandle(), bleCharacteristic.getInternalCharacteristicReference());
    }

    @Override
    public void handleResponsePackets(Packet packet) {
        logger.info("[DiscoverCharacteristicDescriptors] {}", packet);


        if (packet instanceof GATTEventAllCharacteristicDescriptorsQueryResult) {
            final GATTCharacteristicDescriptor discovered = ((GATTEventAllCharacteristicDescriptorsQueryResult) packet).getCharacteristicDescriptor();
            logger.info("[DiscoverCharacteristicDescriptors] Discovered descriptor: {}", discovered);
            discoveredDescriptors.add(new BleCharacteristicDescriptor(bleCharacteristic, discovered));
        }

        if (packet instanceof GATTEventQueryComplete) {
            // query finished
            if (((GATTEventQueryComplete) packet).getATTStatus() == GattResponse.ATT_ERROR_SUCCESS) {
                getGattResponseCallback().accept(new GattResponse(true,
                        ((GATTEventQueryComplete) packet).getHandle(),
                        ((GATTEventQueryComplete) packet).getATTStatus()));
            }
            else {
                // fail
                getGattResponseCallback().accept(new GattResponse(false,
                        ((GATTEventQueryComplete) packet).getHandle(),
                        ((GATTEventQueryComplete) packet).getATTStatus()));
            }
        }
    }

    public List<BleCharacteristicDescriptor> getDiscoveredDescriptors() {
        return discoveredDescriptors;
    }
}
