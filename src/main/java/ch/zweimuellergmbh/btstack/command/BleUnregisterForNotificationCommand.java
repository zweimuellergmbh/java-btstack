package ch.zweimuellergmbh.btstack.command;

import ch.zweimuellergmbh.btstack.BleCharacteristic;
import ch.zweimuellergmbh.btstack.BleDevice;
import com.bluekitchen.btstack.Packet;
import com.bluekitchen.btstack.event.GATTEventQueryComplete;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BleUnregisterForNotificationCommand extends BleCommand {
    private static final int NOTIFICATION_OFF = 0x0;

    private static final Logger logger = LoggerFactory.getLogger(BleUnregisterForNotificationCommand.class);

    private final BleCharacteristic bleCharacteristic;

    public BleUnregisterForNotificationCommand(final BleCharacteristic bleCharacteristic) {
        this.bleCharacteristic = bleCharacteristic;
    }

    @Override
    public void bleRequest() {
        logger.info("[UnregisterForNotification] Unregister for notifications: {}", bleCharacteristic.getInternalCharacteristicReference().getUUID());
        final BleDevice bleDevice = bleCharacteristic.getService().getBleDevice();

        bleDevice.getBleAdapter().getBtstack().GATTWriteClientCharacteristicConfiguration(
                bleDevice.getConnectionHandle(), bleCharacteristic.getInternalCharacteristicReference(), NOTIFICATION_OFF);
    }

    @Override
    public void handleResponsePackets(Packet packet) {
        logger.debug("[UnregisterForNotification] {}", packet);

        if (packet instanceof GATTEventQueryComplete) {
            // query finished
            if (((GATTEventQueryComplete) packet).getATTStatus() == GattResponse.ATT_ERROR_SUCCESS) {

                // TODO refactor notification registration
                bleCharacteristic.getService().getBleDevice().getBleAdapter().getNotificationRegistry().unregisterNotificationCallback(
                        ((GATTEventQueryComplete) packet).getHandle(),
                        bleCharacteristic.getInternalCharacteristicReference().getValueHandle());

                getGattResponseCallback().accept(new GattResponse(true,
                        ((GATTEventQueryComplete) packet).getHandle(),
                        ((GATTEventQueryComplete) packet).getATTStatus()));
            }
            else {
                // fail
                getGattResponseCallback().accept(new GattResponse(false,
                        ((GATTEventQueryComplete) packet).getHandle(),
                        ((GATTEventQueryComplete) packet).getATTStatus()));
            }
        }
    }
}
