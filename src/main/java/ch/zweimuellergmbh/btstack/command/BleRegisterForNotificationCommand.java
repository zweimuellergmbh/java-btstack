package ch.zweimuellergmbh.btstack.command;

import ch.zweimuellergmbh.btstack.BleCharacteristic;
import ch.zweimuellergmbh.btstack.BleDevice;
import com.bluekitchen.btstack.Packet;
import com.bluekitchen.btstack.event.GATTEventQueryComplete;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.function.Consumer;

public class BleRegisterForNotificationCommand extends BleCommand {
    private static final int NOTIFICATION_ON = 0x1;

    private final Consumer<byte[]> notificationCallback;

    private static final Logger logger = LoggerFactory.getLogger(BleRegisterForNotificationCommand.class);

    private final BleCharacteristic bleCharacteristic;

    public BleRegisterForNotificationCommand(final BleCharacteristic bleCharacteristic, final Consumer<byte[]> notificationCallback) {
        this.bleCharacteristic = bleCharacteristic;
        this.notificationCallback = notificationCallback;
    }

    @Override
    public void bleRequest() {
        logger.info("[RegisterForNotification] Register for notifications: {}", bleCharacteristic.getInternalCharacteristicReference().getUUID());
        final BleDevice bleDevice = bleCharacteristic.getService().getBleDevice();

        bleDevice.getBleAdapter().getBtstack().GATTWriteClientCharacteristicConfiguration(
                bleDevice.getConnectionHandle(), bleCharacteristic.getInternalCharacteristicReference(), NOTIFICATION_ON);
    }

    @Override
    public void handleResponsePackets(Packet packet) {
        logger.debug("[RegisterForNotification] {}", packet);

        if (packet instanceof GATTEventQueryComplete) {
            // query finished
            if (((GATTEventQueryComplete) packet).getATTStatus() == GattResponse.ATT_ERROR_SUCCESS) {

                try {
                    bleCharacteristic.getService().getBleDevice().getBleAdapter().getNotificationRegistry().registerNotificationCallback(
                            ((GATTEventQueryComplete) packet).getHandle(),
                            bleCharacteristic.getInternalCharacteristicReference().getValueHandle(),
                            notificationCallback
                    );
                } catch (IllegalArgumentException illegalArgumentException) {
                    logger.error("Notification callback must not be null:", illegalArgumentException);
                    throw new RuntimeException(illegalArgumentException);
                }

                getGattResponseCallback().accept(new GattResponse(true,
                        ((GATTEventQueryComplete) packet).getHandle(),
                        ((GATTEventQueryComplete) packet).getATTStatus()));
            }
            else {
                // fail
                getGattResponseCallback().accept(new GattResponse(false,
                        ((GATTEventQueryComplete) packet).getHandle(),
                        ((GATTEventQueryComplete) packet).getATTStatus()));
            }
        }
    }
}
