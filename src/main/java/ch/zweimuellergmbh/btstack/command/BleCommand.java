package ch.zweimuellergmbh.btstack.command;

import com.bluekitchen.btstack.Packet;

import java.util.function.Consumer;

public abstract class BleCommand {

    private Consumer<GattResponse> gattResponseCallback;

    /**
     * The user of this command can register a callback which will be called when
     * the gatt request has finished.
     *
     * If a command uses a special response type (e.g. connect) the corresponding
     * register methods have to be written manually.
     *
     * @param gattResponseCallback  The gatt callback to be called, when the request has finished.
     */
    public void registerResponseCallback(final Consumer<GattResponse> gattResponseCallback) {
        this.gattResponseCallback = gattResponseCallback;
    }

    /**
     * @return Return the registered response callback.
     */
    public Consumer<GattResponse> getGattResponseCallback() {
        return this.gattResponseCallback;
    }

    /**
     * Send the BLE request for the current command.
     *
     * The different implementations of this interface method build up the individual
     * BLE commands to be sent.
     */
    public abstract void bleRequest();

    /**
     * This method is called by the
     *
     * @see ch.zweimuellergmbh.btstack.BleAdapter class
     *
     * whenever an incoming packet for the current request arrives.
     *
     * The different implementations of this interface method then decide what to do
     * with it.
     *
     * @param packet    Incoming packet.
     */
    public abstract void handleResponsePackets(final Packet packet);
}
