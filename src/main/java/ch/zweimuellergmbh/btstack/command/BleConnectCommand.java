package ch.zweimuellergmbh.btstack.command;

import ch.zweimuellergmbh.btstack.BleAdapter;
import ch.zweimuellergmbh.btstack.BleAddress;
import com.bluekitchen.btstack.Packet;
import com.bluekitchen.btstack.event.HCIEventLEConnectionComplete;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BleConnectCommand extends BleCommand {
    private static final Logger logger = LoggerFactory.getLogger(BleConnectCommand.class);

    private final BleAdapter bleAdapter;
    private final BleAddress bleAddress;

    public BleConnectCommand(BleAdapter bleAdapter, BleAddress bleAddress) {
        this.bleAdapter = bleAdapter;
        this.bleAddress = bleAddress;
    }

    @Override
    public void bleRequest() {
        logger.info("[Connect] Connect to: {}", bleAddress.getAddress());
        bleAdapter.getBtstack().GAPLEConnect(bleAddress.getAddressType().getValue(), bleAddress.getAddress());
    }

    @Override
    public void handleResponsePackets(Packet packet) {
        logger.debug("[Connect] {}", packet);

        if (packet instanceof HCIEventLEConnectionComplete) {
            final HCIEventLEConnectionComplete event = (HCIEventLEConnectionComplete) packet;

            if (event.getStatus() != 0) {
                logger.error("[Connect] Connection failed. {}", event.getStatus());
                getGattResponseCallback().accept(new GattResponse(false, -1,
                        ((HCIEventLEConnectionComplete) packet).getStatus()));
            }
            else {
                int connectionHandle = ((HCIEventLEConnectionComplete) packet).getConnectionHandle();
                getGattResponseCallback().accept(new GattResponse(true, connectionHandle,
                        ((HCIEventLEConnectionComplete) packet).getStatus()));
            }
        }
    }
}
