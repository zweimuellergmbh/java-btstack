package ch.zweimuellergmbh.btstack.command;

import ch.zweimuellergmbh.btstack.BleDevice;
import ch.zweimuellergmbh.btstack.BleService;
import com.bluekitchen.btstack.GATTService;
import com.bluekitchen.btstack.Packet;
import com.bluekitchen.btstack.event.GATTEventQueryComplete;
import com.bluekitchen.btstack.event.GATTEventServiceQueryResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class BleDiscoverPrimaryServicesCommand extends BleCommand {

    private static final Logger logger = LoggerFactory.getLogger(BleDiscoverPrimaryServicesCommand.class);

    private final List<BleService> discoveredServices = new ArrayList<>();

    private final BleDevice bleDevice;

    public BleDiscoverPrimaryServicesCommand(final BleDevice bleDevice) {
        this.bleDevice = bleDevice;
    }

    @Override
    public void bleRequest() {
        logger.info("[DiscoverPrimaryServices] Discover Primary services for: {}", bleDevice.getBleAddress());
        bleDevice.getBleAdapter().getBtstack().GATTDiscoverPrimaryServices(bleDevice.getConnectionHandle());
    }

    @Override
    public void handleResponsePackets(Packet packet) {
        logger.debug("[DiscoverPrimaryServices] {}", packet);

        if (packet instanceof GATTEventServiceQueryResult) {
            final GATTService service = ((GATTEventServiceQueryResult) packet).getService();
            discoveredServices.add(new BleService(bleDevice, service));
            logger.info("[DiscoverPrimaryServices] Discovered Service: {} on device: {}", service.getUUID(), bleDevice.getBleAddress().getAddress());
        }

        if (packet instanceof GATTEventQueryComplete) {
            // query finished
            if (((GATTEventQueryComplete) packet).getATTStatus() == GattResponse.ATT_ERROR_SUCCESS) {
                getGattResponseCallback().accept(new GattResponse(true,
                        packet.getChannel(), ((GATTEventQueryComplete) packet).getATTStatus()));
            }
            else {
                // fail
                getGattResponseCallback().accept(new GattResponse(false,
                        ((GATTEventQueryComplete) packet).getHandle(),
                        ((GATTEventQueryComplete) packet).getATTStatus()));
            }
        }
    }

    public List<BleService> getDiscoveredServices() {
        return discoveredServices;
    }
}
