package ch.zweimuellergmbh.btstack.jna;

import com.sun.jna.Library;
import com.sun.jna.Native;
import org.apache.commons.lang3.SystemUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class BtStackNativeLoader {

    private static final Logger logger = LoggerFactory.getLogger(BtStackNativeLoader.class);

    private static final String LIB_BTSTACK_SERVER_OSX = "/libBTstackServer.dylib";
    private static final String LIB_BTSTACK_SERVER_32_DLL = "/libBTstackServer32.dll";
    private static final String LIB_BTSTACK_SERVER_64_DLL = "/libBTstackServer64.dll";
    private static final String LIB_BTSTACK_SERVER_LINUX_64 = "/libBTstackServer64.so";

    public static final String DEFAULT_TEMP_FOLDER_PREFIX = "java-btstack";
    public static final String ARCH_64 = "64";

    private static CLibrary dynamicLibrary = null;

    public interface CLibrary extends Library {
        int btstack_server_run_tcp();

        void btstack_server_set_storage_path(String path);
    }

    public static void loadAndInitializeLibrary(final String btStackDir) {

        String libraryName;

        logger.info("Architecture: {}", SystemUtils.OS_ARCH);

        if (SystemUtils.IS_OS_WINDOWS) {
            if (SystemUtils.OS_ARCH.contains(ARCH_64)) {
                libraryName = LIB_BTSTACK_SERVER_64_DLL;
                logger.info("Running on a 64Bit JVM - load the 64Bit BTstack DLLs");
            } else {
                libraryName = LIB_BTSTACK_SERVER_32_DLL;
                logger.info("Running on a 32Bit JVM - load the 32Bit BTstack DLLs");
            }
        } else if (SystemUtils.IS_OS_MAC) {
            throwIfNot64BitOS();
            libraryName = LIB_BTSTACK_SERVER_OSX;
        } else if (SystemUtils.IS_OS_LINUX) {
            throwIfNot64BitOS();
            libraryName = LIB_BTSTACK_SERVER_LINUX_64;
        } else {
            logger.error("OS not supported by this library");

            throw new RuntimeException("OS not supported by this library.");
        }

        logger.info("Use BTstack library: {}", libraryName);

        try {
            final File libraryFile = Native.extractFromResourcePath(libraryName);

            logger.info("Loading library: {}", libraryFile.getCanonicalPath());
            dynamicLibrary = Native.load(libraryFile.getCanonicalPath(), CLibrary.class);

            if (btStackDir != null && (new File(btStackDir).exists())) {
                logger.info("Setting BTStack data path to: " + btStackDir);
                dynamicLibrary.btstack_server_set_storage_path(btStackDir);
            }
            else {
                logger.warn("Invalid BTstack data path specified {} - use default", btStackDir);
                logger.info("Setting BTstack data path to: " + getDefaultBtStackDataDir());
                dynamicLibrary.btstack_server_set_storage_path(getDefaultBtStackDataDir());
            }

            logger.info("Calling: btstack_server_run_tcp()");
            new Thread(() -> dynamicLibrary.btstack_server_run_tcp(), "btstack_server_run_tcp").start();

        } catch (Exception e) {
            throw new RuntimeException("Error initializing BTstack library: " + e.getMessage());
        }
    }

    /**
     * Throws a RuntimeException, if current architecture is not 64bit.
     */
    private static void throwIfNot64BitOS() {
        if (!SystemUtils.OS_ARCH.contains(ARCH_64)) {
            throw new RuntimeException("This library only supports 64bit on os: " + SystemUtils.OS_NAME);
        }
    }

    private static String getDefaultBtStackDataDir() throws IOException {
        final Path tempDirWithPrefix = Files.createTempDirectory(DEFAULT_TEMP_FOLDER_PREFIX);
        return tempDirWithPrefix.toAbsolutePath().toString();
    }
}
