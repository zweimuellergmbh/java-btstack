package ch.zweimuellergmbh.btstack;

public class AdvertisingData {
    private final BleAddress bleAddress;
    private final byte[] data;

    // TODO improve data - not just pass array
    public AdvertisingData(final BleAddress bleAddress, final byte[] data) {
        this.bleAddress = bleAddress;
        this.data = data;
    }

    public BleAddress getBleAddress() {
        return bleAddress;
    }

    public byte[] getData() {
        return data;
    }
}
