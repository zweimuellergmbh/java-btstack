package ch.zweimuellergmbh.btstack.utils;

import ch.zweimuellergmbh.btstack.BleAddress;

import javax.swing.*;
import java.util.function.BiFunction;
import java.util.function.Function;

public class SMHelper {
    public static final BiFunction<BleAddress, Long, Boolean> ncYesNoSupplier = (address, passkey) -> {
        int okCxl = JOptionPane.showConfirmDialog(null, "Pairing with: " + address.getAddress() + "\n\n" + "Confirm number: " + passkey, "NC Pairing", JOptionPane.OK_CANCEL_OPTION);
        return okCxl == JOptionPane.OK_OPTION;
    };

    public static final Function<BleAddress, Long> passkeySupplier = (address) -> {
        final JPasswordField pf = new JPasswordField();
        int okCxl = JOptionPane.showConfirmDialog(null, pf, "Enter passkey to pair with: " + address.getAddress(), JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);

        if (okCxl == JOptionPane.OK_OPTION) {
            final String password = new String(pf.getPassword());
            System.err.println("You entered: " + password);
            return Long.valueOf(password);
        }

        // simply return wrong pin on cancel
        return (long) 0;
    };

    public static Function<BleAddress, Boolean> justWorksSupplier = (address) -> {
        int okCxl = JOptionPane.showConfirmDialog(null, "Do you want to pair with: " + address.getAddress() , "Just Works pairing", JOptionPane.OK_CANCEL_OPTION);
        return okCxl == JOptionPane.OK_OPTION;
    };

    public static BiFunction<BleAddress, Long, Boolean> showPasskeyConsumer = (address, passkey) -> {
        System.out.println("Device: " + address.getAddress() + " wants to pair with you. Enter the following pin on it: " + passkey);
        return true;
        // TODO implement option to cancel pairing in this scenario. with custom dialog.
    };
}
