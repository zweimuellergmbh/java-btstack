package ch.zweimuellergmbh.btstack;

public class BleResourceException extends RuntimeException {
    public BleResourceException(String message) {
        super(message);
    }
}
