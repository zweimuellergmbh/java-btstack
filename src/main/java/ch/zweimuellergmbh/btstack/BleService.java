package ch.zweimuellergmbh.btstack;

import ch.zweimuellergmbh.btstack.command.BleCommand;
import ch.zweimuellergmbh.btstack.command.BleDiscoverCharacteristicByUUIDCommand;
import ch.zweimuellergmbh.btstack.command.BleDiscoverCharacteristicsCommand;
import com.bluekitchen.btstack.BT_UUID;
import com.bluekitchen.btstack.GATTService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicBoolean;

public class BleService {

    private static final Logger logger = LoggerFactory.getLogger(BleService.class);

    private final BleDevice bleDevice;
    private final GATTService service;

    // TODO make own types
    public BleService(final BleDevice bleDevice, final GATTService service) {
        this.bleDevice = bleDevice;
        this.service = service;
    }

    public List<BleCharacteristic> discoverCharacteristics() throws BleGattException, TimeoutException {
        try {
            return discoverCharacteristicsAsync().toCompletableFuture().get();
        } catch (InterruptedException | ExecutionException e) {
            if (e.getCause() instanceof TimeoutException) {
                throw new TimeoutException("Timeout discover characteristics of: " + getInternalGattServiceReference().getUUID());
            }
            throw new BleGattException(e.getCause().getMessage());
        }
    }

    public BleCharacteristic getCharacteristicByUUID(final BT_UUID uuid) throws BleGattException, TimeoutException {
        try {
            return getCharacteristicByUUIDAsync(uuid).toCompletableFuture().get();
        } catch (InterruptedException | ExecutionException e) {
            if (e.getCause() instanceof TimeoutException) {
                throw new TimeoutException("Timeout get characteristic by uuid: " + uuid);
            }
            throw new BleGattException(e.getCause().getMessage());
        }
    }

    public CompletionStage<List<BleCharacteristic>> discoverCharacteristicsAsync() {
        final CompletableFuture<List<BleCharacteristic>> promise = new CompletableFuture<>();
        CompletableFuture.supplyAsync(() -> {
            if (!bleDevice.isConnected()) {
                promise.completeExceptionally(new BleGattException("GATT Request not possible. Device is not connected."));
            }

            final BleDiscoverCharacteristicsCommand bleCommand = new BleDiscoverCharacteristicsCommand(
                    this, bleDevice.getConnectionHandle());

            try {
                if (!waitForResponse(bleCommand)) {
                    promise.completeExceptionally(new BleGattException("Discover characteristics failed: " + getInternalGattServiceReference().getUUID()));
                }
            } catch (BleGattException | TimeoutException e) {
                promise.completeExceptionally(e);
            }

            return bleCommand.getDiscoveredCharacteristics();
        })
        .whenComplete((characteristics, err) -> {
            if (err != null) {
                logger.error("Error discovering characteristics", err);
                promise.completeExceptionally(new BleGattException("Discover characteristics failed: " + getInternalGattServiceReference().getUUID()));
            }
            promise.complete(characteristics);
        });

        return promise.minimalCompletionStage();
    }

    public CompletionStage<BleCharacteristic> getCharacteristicByUUIDAsync(final BT_UUID uuid) {
        final CompletableFuture<BleCharacteristic> promise = new CompletableFuture<>();
        CompletableFuture.supplyAsync(() -> {
            if (!bleDevice.isConnected()) {
                promise.completeExceptionally(new BleGattException("GATT Request not possible. Device is not connected."));
            }

            final BleDiscoverCharacteristicByUUIDCommand bleCommand = new BleDiscoverCharacteristicByUUIDCommand(
                    this, uuid);

            try {
                if (!waitForResponse(bleCommand)) {
                    promise.completeExceptionally(new BleGattException("Discover characteristic by UUID failed: " + uuid));
                }
            } catch (BleGattException | TimeoutException e) {
                promise.completeExceptionally(e);
            }

            return bleCommand.getDiscoveredCharacteristic();
        })
        .whenComplete((characteristic, err) -> {
            if (err != null) {
                logger.error("Error discovering characteristic by uuid", err);
                promise.completeExceptionally(new BleGattException("Discover characteristic by UUID failed: " + uuid));
            }
            promise.complete(characteristic);
        });

        return promise.minimalCompletionStage();
    }

    private boolean waitForResponse(final BleCommand bleCommand) throws BleGattException, TimeoutException {
        final CountDownLatch doneLatch = new CountDownLatch(1);
        final AtomicBoolean requestOk = new AtomicBoolean();
        bleCommand.registerResponseCallback(gattResponse -> {
            requestOk.set(gattResponse.isRequestOk());
            doneLatch.countDown();
        });

        bleDevice.getBleAdapter().executeBleCommandOnConnection(bleCommand, getBleDevice());

        try {
            if (!doneLatch.await(getBleDevice().getBleAdapter().getGattTimeoutMs(), TimeUnit.MILLISECONDS)) {
                logger.error("Timeout waiting for GATT response");
                throw new TimeoutException("Timeout on GATT request.");
            }
        } catch (InterruptedException ignored) { }

        return requestOk.get();
    }


    public BleDevice getBleDevice() {
        return bleDevice;
    }

    public GATTService getInternalGattServiceReference() {
        return service;
    }
}
