package ch.zweimuellergmbh.btstack;

import com.bluekitchen.btstack.BD_ADDR;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class BleScanner {
    private static final Logger logger = LoggerFactory.getLogger(BleScanner.class);

    private final BleAdapter bleAdapter;

    BleScanner(final BleAdapter bleAdapter) {
        this.bleAdapter = bleAdapter;
    }

    BleDevice scanForDeviceWithName(int timeoutMilliseconds, final String deviceName) throws BleScanTimeoutException {
        final CountDownLatch deviceFound = new CountDownLatch(1);
        final AtomicReference<BleAddress> address = new AtomicReference<>(null);

        bleAdapter.registerRawScanCallback(advertisingData -> {
            if (new String(advertisingData.getData()).contains(deviceName)) {
                logger.info("Found device: {} with address: {}", deviceName, advertisingData.getBleAddress());
                address.set(advertisingData.getBleAddress());
                deviceFound.countDown();
                bleAdapter.stopScanning();
            }
        });

        bleAdapter.startScanning();

        try {
            if (!deviceFound.await(timeoutMilliseconds, TimeUnit.MILLISECONDS)) {
                logger.info("Timeout scanning for device: {}", deviceName);
                throw new BleScanTimeoutException("Timeout scanning for device with name: " + deviceName);
            }
        } catch (InterruptedException ignored) {
            throw new BleResourceException("Error scanning for devices");
        }

        return new BleDevice(address.get(), bleAdapter);
    }

    public BleDevice scanForDeviceWithAddress(int timeoutMilliseconds, final String addressString) throws BleScanException, BleScanTimeoutException {

        if (!validate(addressString)) {
            logger.warn("Invalid BT address format provided");
            throw new BleScanException("Invalid Address format provided");
        }

        final CountDownLatch deviceFound = new CountDownLatch(1);
        final AtomicReference<BleAddress> address = new AtomicReference<>(null);

        bleAdapter.registerRawScanCallback(advertisingData -> {
            if (advertisingData.getBleAddress().getAddress().equals(new BD_ADDR(addressString))) {
                logger.info("Found device with address: {}", advertisingData.getBleAddress());
                address.set(advertisingData.getBleAddress());
                deviceFound.countDown();
                bleAdapter.stopScanning();
            }
        });

        bleAdapter.startScanning();

        try {
            if (!deviceFound.await(timeoutMilliseconds, TimeUnit.MILLISECONDS)) {
                logger.info("Timeout scanning for device with address: {}", addressString);
                throw new BleScanTimeoutException("Timeout scanning for device with address: " + addressString);
            }
        } catch (InterruptedException ignored) {
            throw new BleResourceException("Error scanning for devices");
        }

        return new BleDevice(address.get(), bleAdapter);
    }

    public List<BleDevice> scanForDevices(int durationMs) {
        final Map<BD_ADDR, BleDevice> scannedDevices = new HashMap();
        final CountDownLatch scanFinished = new CountDownLatch(1);


        bleAdapter.registerRawScanCallback(advertisingData -> {
            logger.info("Found device with address: {}", advertisingData.getBleAddress());
            scannedDevices.put(advertisingData.getBleAddress().getAddress(), new BleDevice(advertisingData.getBleAddress(), bleAdapter));
        });

        bleAdapter.startScanning();

        try {
            scanFinished.await(durationMs, TimeUnit.MILLISECONDS);
        } catch (InterruptedException ignored) {
            throw new BleResourceException("Error scanning for devices");
        }

        return new ArrayList<>(scannedDevices.values());
    }

    /**
     * Validates the format of a BT address.
     * <p>
     * Valid format:
     * <p>
     * - 11:22:33:44:55:66
     *
     * @param address   BT address in string format to be checked.
     * @return True or false, wether the given address is a valid BT address or not.
     */
    public boolean validate(final String address) {
        Pattern p = Pattern.compile("^([0-9A-Fa-f]{2}[:]){5}([0-9A-Fa-f]{2})$");
        Matcher m = p.matcher(address);
        return m.find();
    }

    // TODO 2
    // async variants
}
