package ch.zweimuellergmbh.btstack;

public class BleScanTimeoutException extends Exception {
    public BleScanTimeoutException(String message) {
        super(message);
    }
}
