package ch.zweimuellergmbh.btstack;

import ch.zweimuellergmbh.btstack.command.BleCommand;
import ch.zweimuellergmbh.btstack.command.BleReadCharacteristicDescriptorCommand;
import com.bluekitchen.btstack.GATTCharacteristicDescriptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicBoolean;

public class BleCharacteristicDescriptor {
    private static final Logger logger = LoggerFactory.getLogger(BleCharacteristicDescriptor.class);

    private final BleCharacteristic bleCharacteristic;
    private final GATTCharacteristicDescriptor descriptor;

    public BleCharacteristic getBleCharacteristic() {
        return bleCharacteristic;
    }

    public GATTCharacteristicDescriptor getInternalDescriptor() {
        return descriptor;
    }

    public BleCharacteristicDescriptor(final BleCharacteristic bleCharacteristic,
                                       final GATTCharacteristicDescriptor descriptor) {
        this.bleCharacteristic = bleCharacteristic;
        this.descriptor = descriptor;
    }

    public byte[] read() throws BleGattException, TimeoutException {
        try {
            return readAsync().toCompletableFuture().get();
        } catch (InterruptedException | ExecutionException e) {
            if (e.getCause() instanceof TimeoutException) {
                throw new TimeoutException("Timeout reading characteristic descriptor: " + descriptor.getUUID());
            }
            throw new BleGattException(e.getCause().getMessage());
        }
    }

    public CompletionStage<byte[]> readAsync() {
        final CompletableFuture<byte[]> promise = new CompletableFuture<>();
        CompletableFuture.supplyAsync(() -> {
            if (!bleCharacteristic.getService().getBleDevice().isConnected()) {
                promise.completeExceptionally(new BleGattException("GATT Request not possible. Device is not connected."));
            }

            final BleReadCharacteristicDescriptorCommand bleCommand = new BleReadCharacteristicDescriptorCommand(this);

            try {
                if (!waitForResponse(bleCommand)) {
                    promise.completeExceptionally(new BleGattException("Read descriptor failed."));
                }
            } catch (BleGattException | TimeoutException e) {
                promise.completeExceptionally(e);
            }

            return bleCommand.getReadValue();
        })
        .whenComplete((readData, err) -> {
            if (err != null) {
                promise.completeExceptionally(new BleGattException("Read descriptor failed."));
            }
            promise.complete(readData);
        });

        return promise.minimalCompletionStage();
    }

    private boolean waitForResponse(final BleCommand bleCommand) throws BleGattException, TimeoutException {
        final CountDownLatch doneLatch = new CountDownLatch(1);
        final AtomicBoolean requestOk = new AtomicBoolean();
        bleCommand.registerResponseCallback(gattResponse -> {
            requestOk.set(gattResponse.isRequestOk());
            doneLatch.countDown();
        });

        bleCharacteristic.getService().getBleDevice().getBleAdapter().executeBleCommandOnConnection(
                bleCommand, bleCharacteristic.getService().getBleDevice());

        try {
            if (!doneLatch.await(bleCharacteristic.getService().getBleDevice().getBleAdapter().getGattTimeoutMs(), TimeUnit.MILLISECONDS)) {
                logger.error("Timeout waiting for GATT response.");
                throw new TimeoutException("Timeout on GATT request.");
            }
        } catch (InterruptedException ignored) { }

        return requestOk.get();
    }
}
