package ch.zweimuellergmbh.btstack;

import ch.zweimuellergmbh.btstack.command.BleCommand;
import ch.zweimuellergmbh.btstack.command.BleConnectCommand;
import ch.zweimuellergmbh.btstack.command.BleDisconnectCommand;
import ch.zweimuellergmbh.btstack.command.GattResponse;
import ch.zweimuellergmbh.btstack.jna.BtStackNativeLoader;
import com.bluekitchen.btstack.*;
import com.bluekitchen.btstack.event.*;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.*;

import static java.lang.Thread.sleep;

public class BleAdapter implements PacketHandler {
    private static final Logger logger = LoggerFactory.getLogger(BleAdapter.class);

    public static final int BTSTACK_WORKING = 2;

    private final BTstack btstack;
    private final CountDownLatch btStackPoweredUpLatch = new CountDownLatch(1);

    private Consumer<AdvertisingData> scanCallback;

    private SMDispachter smDispachter;

    private final NotificationRegistry notificationRegistry = new NotificationRegistry();

    private int gattTimeoutMs;
    private int connectTimeoutMs;
    private IOCapabilities ioCapabilities;

    private final BleScanner bleScanner = new BleScanner(this);
    private boolean useBonding;

    private enum STATE {
        INIT,
        INITIALIZED,
    }

    private STATE state = STATE.INIT;

    private BleAdapter(final BleAdapterBuilder builder) {
        this(builder.bTstack, builder.useExternalBtStackServer, builder.btStackDir,
                builder.gattTimeoutMs, builder.connectTimeoutMs, builder.useBonding,
                builder.ioCapabilities, builder.smDispachter);
    }

    private BleAdapter(final BTstack btstack, final boolean useExternalBtStack, final String btStackDir,
                       int gattTimeoutMs, int connectTimeoutMs, boolean useBonding,
                       final IOCapabilities ioCapabilities, final SMDispachter smDispachter) {
        this.btstack = btstack;
        this.gattTimeoutMs = gattTimeoutMs;
        this.connectTimeoutMs = connectTimeoutMs;
        this.useBonding = useBonding;
        this.ioCapabilities = ioCapabilities;
        this.smDispachter = smDispachter;

        if (!useExternalBtStack) {
            // use internal BtStack
            BtStackNativeLoader.loadAndInitializeLibrary(btStackDir);
            try {
                sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }



    /**
     * Scan for a BLE device with the given name.
     * <p>
     * This method blocks until the desired device has been found or the timeout is reached.
     *
     * @param timeoutSeconds Max scanning time before timeout.
     * @param deviceName     The name to scan for.
     * @return The discovered BLE device.
     * @throws BleScanTimeoutException If the scan times out.
     */
    public BleDevice scanForDeviceWithName(int timeoutSeconds, final String deviceName)
            throws BleScanTimeoutException {
        return bleScanner.scanForDeviceWithName(timeoutSeconds * 1000, deviceName);
    }

    /**
     * Scan for a BLE device with the given address.
     * <p>
     * This method blocks until the desired device has been found or the timeout is reached.
     *
     * @param timeoutSeconds Max scanning time before timeout.
     * @param addressString  The address to scan for.
     * @return The discovered BLE device.
     * @throws BleScanException        If the specified addressString is invalid.
     * @throws BleScanTimeoutException If the scan times out.
     */
    public BleDevice scanForDeviceWithAddress(int timeoutSeconds, final String addressString)
            throws BleScanException, BleScanTimeoutException {
        return bleScanner.scanForDeviceWithAddress(timeoutSeconds * 1000, addressString);
    }

    /**
     * Scan for BLE devices for the given duration and return them as a list.
     * <p>
     * The devices are identified by there address. If a device uses a random ble address it
     * will be found multiple times, if it changes it's address during the duration of the scan.
     * <p>
     * This method blocks until the scan is finished.
     *
     * @param durationSeconds The duration of the scan in seconds.
     * @return A list of the discovered devices.
     */
    public List<BleDevice> scanForDevices(int durationSeconds) {
        return bleScanner.scanForDevices(durationSeconds * 1000);
    }

    public void registerRawScanCallback(@NonNull final Consumer<AdvertisingData> scanCallback) {
        this.scanCallback = scanCallback;
    }

    public void startScanning() {
        btstack.GAPLEScanStart();
    }

    public void stopScanning() {
        btstack.GAPLEScanStop();
    }


    public boolean isInitialized() {
        return state == STATE.INITIALIZED;
    }

    public BTstack getBtstack() {
        return this.btstack;
    }

    public NotificationRegistry getNotificationRegistry() {
        return this.notificationRegistry;
    }

    // TODO test command map
    private final Map<Integer, BleCommand> commandMap = new HashMap<>();

    private final AtomicReference<BleCommand> connectCommand = new AtomicReference<>(null);

    private final AtomicReference<BleDevice> deviceWithConnectionInProgress = new AtomicReference<>(null);

    private final Map<Integer, BleDevice> connectionHandleDeviceMap = new HashMap<>();

    synchronized void executeBleCommandOnConnection(final BleCommand bleCommand, final BleDevice bleDevice) throws BleGattException {
//        TODO: Ble command for sure is not a connectCommand
        if (!(bleCommand instanceof BleDisconnectCommand)) {
            BleCommand bleCommand1 = commandMap.get(bleDevice.getConnectionHandle());
            if (bleCommand1 != null) {
                logger.error("Concurrent GATT request");
                throw new BleGattException("GATT request in progress");
            }
        }
        logger.debug("Register command {} for handle: {}", bleCommand, bleDevice.getConnectionHandle());
        commandMap.put(bleDevice.getConnectionHandle(), bleCommand);

        bleCommand.bleRequest();
    }

    synchronized void executeBleConnectionCommand(final BleConnectCommand bleCommand, final BleDevice bleDevice) throws BleConnectException {
        if (connectCommand.get() != null) {
            logger.error("Connection in progress");
            throw new BleConnectException("Connection in progress");
        }
        this.connectCommand.set(bleCommand);
        this.deviceWithConnectionInProgress.set(bleDevice);

        bleCommand.bleRequest();
    }

    @Override
    public void handlePacket(Packet packet) {
        logger.debug("[{}] Incoming Packet: {}", state.name(), packet);

        switch (state) {
            case INIT: {
                if (packet instanceof BTstackEventState) {
                    final BTstackEventState event = (BTstackEventState) packet;

                    if (event.getState() == BTSTACK_WORKING) {
                        state = STATE.INITIALIZED;
                        logger.info("BtStack started up successfully.");
                        btStackPoweredUpLatch.countDown();
                    }
                }
                break;
            }

            case INITIALIZED: {
                if (packet instanceof SMEventPasskeyInputNumber) {
                    final SMEventPasskeyInputNumber request = (SMEventPasskeyInputNumber) packet;

                    logger.info("Pairing request: Passkey -> enter PIN");
                    smDispachter.passkeyRequest(request);
                    break;
                }

                if (packet instanceof SMEventNumericComparisonRequest) {
                    final SMEventNumericComparisonRequest request = (SMEventNumericComparisonRequest) packet;
                    logger.info("Pairing request: Numeric Comparison: {}", request.getPasskey());

                    smDispachter.ncRequest(request);
                    break;
                }

                if (packet instanceof SMEventJustWorksRequest) {
                    final SMEventJustWorksRequest request = (SMEventJustWorksRequest) packet;
                    logger.info("Pairing request: Just Works");

                    smDispachter.justWorksRequest(request);
                    break;
                }

                if (packet instanceof SMEventPasskeyDisplayNumber) {
                    final SMEventPasskeyDisplayNumber request = (SMEventPasskeyDisplayNumber)(packet);
                    logger.info("Pairing: Display Number");

                    smDispachter.passkeyDisplayRequest(request);
                    break;
                }

                if (packet instanceof SMEventPairingComplete) {
                    if (((SMEventPairingComplete) packet).getStatus() == GattResponse.ATT_ERROR_SUCCESS) {
                        logger.info("Pairing on connection: {} OK", ((SMEventPairingComplete) packet).getHandle());
                    } else {
                        logger.info("Pairing on connection: {} failed.", ((SMEventPairingComplete) packet).getHandle());
                        // this happens on:
                        // - pairing timeout
                        // - wrong passkey
                        // - just works or NC cancel/reject
                        smDispachter.resetPairingOnConnection(((SMEventPairingComplete) packet).getHandle());
                    }
                    break;
                }

                if (packet instanceof GAPEventAdvertisingReport) {
                    final GAPEventAdvertisingReport report = (GAPEventAdvertisingReport) packet;

                    final BleAddress bleAddress = new BleAddress(report.getAddress(), BleAddress.AddressType.fromInt(report.getAddressType()));

                    if (scanCallback != null) {
                        scanCallback.accept(new AdvertisingData(bleAddress, report.getData()));
                    }
                    break;
                }

                if (packet instanceof GATTEventNotification) {
                    final GATTEventNotification event = (GATTEventNotification) packet;

                    notificationRegistry.getRegisteredCallback(
                            ((GATTEventNotification) packet).getHandle(),
                            ((GATTEventNotification) packet).getValueHandle()
                    ).ifPresent(consumer -> consumer.accept(event.getValue()));
                    break;
                }

                BleCommand bleCommand = null;
                if (packet instanceof HCIEventLEConnectionComplete) {
                    if (connectCommand.get() != null) {
                        connectCommand.get().handleResponsePackets(packet);
                        connectCommand.set(null);
                    }
                    if (((HCIEventLEConnectionComplete) packet).getStatus() == GattResponse.ATT_ERROR_SUCCESS) {
                        logger.info("Register BleDevice for connection handle: {}", ((HCIEventLEConnectionComplete) packet).getConnectionHandle());
                        connectionHandleDeviceMap.put(((HCIEventLEConnectionComplete) packet).getConnectionHandle(), deviceWithConnectionInProgress.get());
                    }
                    deviceWithConnectionInProgress.set(null);
                }

                if (packet instanceof HCIEventDisconnectionComplete) {
                    bleCommand = commandMap.get(((HCIEventDisconnectionComplete) packet).getConnectionHandle());
                }
                if (packet instanceof GATTEventServiceQueryResult) {
                    bleCommand = commandMap.get(((GATTEventServiceQueryResult) packet).getHandle());
                }
                if (packet instanceof GATTEventQueryComplete) {
                    bleCommand = commandMap.get(((GATTEventQueryComplete) packet).getHandle());
                }
                if (packet instanceof GATTEventCharacteristicQueryResult) {
                    bleCommand = commandMap.get(((GATTEventCharacteristicQueryResult) packet).getHandle());
                }
                if (packet instanceof GATTEventCharacteristicValueQueryResult) {
                    bleCommand = commandMap.get(((GATTEventCharacteristicValueQueryResult) packet).getHandle());
                }
                if (packet instanceof GATTEventAllCharacteristicDescriptorsQueryResult) {
                    bleCommand = commandMap.get(((GATTEventAllCharacteristicDescriptorsQueryResult) packet).getHandle());
                }
                if (packet instanceof GATTEventCharacteristicDescriptorQueryResult) {
                    bleCommand = commandMap.get(((GATTEventCharacteristicDescriptorQueryResult) packet).getHandle());
                }

                if (bleCommand != null) {
                    // end of GATT reqeust -> remove command
                    if (packet instanceof GATTEventQueryComplete) {
                        logger.debug("Remove GATT command from commandMap: {}", bleCommand);

                        for (final Iterator<BleCommand> iterator = commandMap.values().iterator(); iterator.hasNext(); ) {
                            BleCommand value = iterator.next();
                            if (value == bleCommand) {
                                iterator.remove();
                            }
                        }
                    }

                    bleCommand.handleResponsePackets(packet);
                }

                if (packet instanceof HCIEventDisconnectionComplete) {
                    final int connectionHandle = ((HCIEventDisconnectionComplete) packet).getConnectionHandle();

                    commandMap.remove(connectionHandle);

                    final BleDevice toDisconnect = connectionHandleDeviceMap.get(connectionHandle);
                    if (toDisconnect != null) {
                        logger.debug("Remove BleDevice {} from connected devices list", toDisconnect.getBleAddress().getAddress());
                        toDisconnect.disconnected();
                    }
                    connectionHandleDeviceMap.remove(connectionHandle);

                    notificationRegistry.removeAllRegisteredCallbacksForConnection(connectionHandle);
                }
                break;
            }
        }
    }

    /**
     * Connects to the BTstack daemon and power it up.
     * <p>
     * This method is blocking by design.
     *
     * @throws BtStackException If stack can not be started within 1 seconds.
     */
    public void powerOnBtStack() throws BtStackException {
        // connect to BTstack Daemon via default port on localhost
        btstack.setTcpPort(BTstack.DEFAULT_TCP_PORT);
        btstack.registerPacketHandler(this);

        boolean ok = btstack.connect();

        if (!ok) {
            logger.error("Failed to connect to BTStack daemon.");
            throw new RuntimeException("Failed to connect to BTStack daemon");
        }

        if (useBonding) {
            // SM_AUTHREQ_SECURE_CONNECTION 8 | SM_AUTHREQ_MITM_PROTECTION 4 | BOND 1
            btstack.SMSetAuthenticationRequirements(13);
        } else {
            // SM_AUTHREQ_SECURE_CONNECTION 8 | SM_AUTHREQ_MITM_PROTECTION 4
            btstack.SMSetAuthenticationRequirements(12);
        }

        btstack.SMSetIoCapabilities(ioCapabilities.value);


        btstack.BTstackSetPowerMode(1);

        try {
            if (!btStackPoweredUpLatch.await(1, TimeUnit.SECONDS)) {
                throw new BtStackException("Timeout powering up BTstack");
            }
        } catch (InterruptedException e) {
            throw new BtStackException("Error powering up BTstack", e);
        }
    }

    public int getGattTimeoutMs() {
        return gattTimeoutMs;
    }

    public int getConnectTimeoutMs() {
        return connectTimeoutMs;
    }

    public static class BleAdapterBuilder {
        public static final boolean USE_BONDING_DEFAULT_FALSE = false;
        private static final int DEFAULT_GATT_TIMEOUT_MILLISECONDS = 60000;
        private static final int DEFAULT_CONNECT_TIMEOUT_MILLISECONDS = 60000;

        private final BTstack bTstack;
        private IOCapabilities ioCapabilities;
        private SMDispachter smDispachter;
        private boolean useExternalBtStackServer = false;
        private String btStackDir = null;
        private boolean useBonding = USE_BONDING_DEFAULT_FALSE;
        private int gattTimeoutMs = DEFAULT_GATT_TIMEOUT_MILLISECONDS;
        private int connectTimeoutMs = DEFAULT_CONNECT_TIMEOUT_MILLISECONDS;

        public BleAdapterBuilder(final BTstack bTstack, IOCapabilities ioCapabilities, SMDispachter smDispachter) {
            this.bTstack = bTstack;
            this.ioCapabilities = ioCapabilities;
            this.smDispachter = smDispachter;
        }

        public BleAdapterBuilder useExternalBtStackServer(final boolean useExternalBtStackServer) {
            this.useExternalBtStackServer = useExternalBtStackServer;
            return this;
        }

        public BleAdapterBuilder useBtStackTempDirectory(final String btStackDir) {
            this.btStackDir = btStackDir;
            return this;
        }

        public BleAdapterBuilder gattTimeoutMs(final int gattTimeoutMs) {
            this.gattTimeoutMs = gattTimeoutMs;
            return this;
        }

        public BleAdapterBuilder connectTimeoutMs(final int connectTimeoutMs) {
            this.connectTimeoutMs = connectTimeoutMs;
            return this;
        }

        public BleAdapterBuilder useBonding(final boolean useBonding) {
            this.useBonding = useBonding;
            return this;
        }

        public BleAdapter build() {
            if (btStackDir != null && useExternalBtStackServer) {
                logger.warn("Option 'btStackDir' is ignored when using external BTStack Server Daemon.");
            }
            return new BleAdapter(this);
        }
    }
}
