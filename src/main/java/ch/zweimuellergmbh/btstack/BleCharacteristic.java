package ch.zweimuellergmbh.btstack;

import ch.zweimuellergmbh.btstack.command.*;
import com.bluekitchen.btstack.BT_UUID;
import com.bluekitchen.btstack.GATTCharacteristic;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;

public class BleCharacteristic {
    private static final Logger logger = LoggerFactory.getLogger(BleCharacteristic.class);

    private final BleService bleService;
    private final GATTCharacteristic characteristic;
    private final CharacteristicProperties properties;

    public BleCharacteristic(final BleService bleService, final GATTCharacteristic characteristic) {
        this.bleService = bleService;
        this.characteristic = characteristic;
        this.properties = new CharacteristicProperties(characteristic.getProperties());
    }

    public BT_UUID getUUID() {
        return this.characteristic.getUUID();
    }

    public GATTCharacteristic getInternalCharacteristicReference() {
        return characteristic;
    }

    public BleService getService() {
        return bleService;
    }

    public byte[] read() throws BleGattException, TimeoutException {
        try {
            return readAsync().toCompletableFuture().get();
        } catch (InterruptedException | ExecutionException e) {
            if (e.getCause() instanceof TimeoutException) {
                throw new TimeoutException("Timeout reading characteristic: " + characteristic.getUUID());
            }
            throw new BleGattException(e.getCause().getMessage());
        }
    }

    public void write(byte[] data) throws BleGattException, TimeoutException {
        try {
            writeAsync(data).toCompletableFuture().get();
        } catch (InterruptedException | ExecutionException e) {
            if (e.getCause() instanceof TimeoutException) {
                throw new TimeoutException("Timeout writing characteristic: " + characteristic.getUUID());
            }
            throw new BleGattException(e.getCause().getMessage());
        }
    }

    public void registerForNotification(final Consumer<byte[]> notificationCallback) throws BleGattException, TimeoutException {
        try {
            registerForNotificationAsync(notificationCallback).toCompletableFuture().get();
        } catch (InterruptedException | ExecutionException e) {
            if (e.getCause() instanceof TimeoutException) {
                throw new TimeoutException("Timeout register for notifications: " + characteristic.getUUID());
            }
            throw new BleGattException(e.getCause().getMessage());
        }
    }

    public void unregisterForNotification() throws BleGattException, TimeoutException {
        try {
            unregisterForNotificationAsync().toCompletableFuture().get();
        } catch (InterruptedException | ExecutionException e) {
            if (e.getCause() instanceof TimeoutException) {
                throw new TimeoutException("Timeout unregister for notifications: " + getUUID());
            }
            throw new BleGattException(e.getCause().getMessage());
        }
    }

    public List<BleCharacteristicDescriptor> discoverDescriptors() throws BleGattException, TimeoutException {
        try {
            return discoverDescriptorsAsync().toCompletableFuture().get();
        } catch (InterruptedException | ExecutionException e) {
            if (e.getCause() instanceof TimeoutException) {
                throw new TimeoutException("Timeout discover characteristic descriptors of characteristic: " + getUUID());
            }
            throw new BleGattException(e.getCause().getMessage());
        }
    }

    public CompletionStage<byte[]> readAsync() {
        final CompletableFuture<byte[]> promise = new CompletableFuture<>();
        CompletableFuture.supplyAsync(() -> {
            if (!bleService.getBleDevice().isConnected()) {
                promise.completeExceptionally(new BleGattException("GATT Request not possible. Device is not connected."));
            }

            BleGattReadCommand bleCommand = new BleGattReadCommand(this);

            try {
                if (!waitForResponse(bleCommand)) {
                    promise.completeExceptionally(new BleGattException("GATT read failed"));
                }
            } catch (BleGattException | TimeoutException e) {
                promise.completeExceptionally(e);
            }

            return bleCommand.getReadValue();
        })
        .whenComplete((readData, err) -> {
            if (err != null) {
                promise.completeExceptionally(new BleGattException("GATT read failed"));
            }
            promise.complete(readData);
        });

        return promise.minimalCompletionStage();
    }

    public CompletionStage<Void> writeAsync(final byte[] data) {
        final CompletableFuture<Void> promise = new CompletableFuture<>();
        CompletableFuture.runAsync(() -> {
            if (!bleService.getBleDevice().isConnected()) {
                promise.completeExceptionally(new BleGattException("GATT Request not possible. Device is not connected."));
            }

            BleGattWriteCommand bleCommand = new BleGattWriteCommand(this, data);

            try {
                if (!waitForResponse(bleCommand)) {
                    promise.completeExceptionally(new BleGattException("GATT write failed"));
                }
            } catch (BleGattException | TimeoutException e) {
                promise.completeExceptionally(e);
            }
        })
        .whenComplete((result, err) -> {
            if (err != null) {
                promise.completeExceptionally(new BleGattException("GATT write failed"));
            }
            promise.complete(result);
        });

        return promise.minimalCompletionStage();
    }

    public CompletionStage<Void> registerForNotificationAsync(final Consumer<byte[]> notificationCallback) {
        final CompletableFuture<Void> promise = new CompletableFuture<>();
        CompletableFuture.runAsync(() -> {
            if (!bleService.getBleDevice().isConnected()) {
                promise.completeExceptionally(new BleGattException("GATT Request not possible. Device is not connected."));
            }

            BleRegisterForNotificationCommand bleCommand = new BleRegisterForNotificationCommand(this, notificationCallback);

            try {
                if (!waitForResponse(bleCommand)) {
                    promise.completeExceptionally(new BleGattException("GATT register for notification failed"));
                }
            } catch (BleGattException | TimeoutException e) {
                promise.completeExceptionally(e);
            }
        })
        .whenComplete((result, err) -> {
            if (err != null) {
                promise.completeExceptionally(new BleGattException("GATT register for notification failed"));
            }
            promise.complete(result);
        });

        return promise.minimalCompletionStage();
    }

    public CompletionStage<Void> unregisterForNotificationAsync() {
        final CompletableFuture<Void> promise = new CompletableFuture<>();
        CompletableFuture.runAsync(() -> {
            if (!bleService.getBleDevice().isConnected()) {
                promise.completeExceptionally(new BleGattException("GATT Request not possible. Device is not connected."));
            }

            BleUnregisterForNotificationCommand bleCommand = new BleUnregisterForNotificationCommand(this);

            try {
                if (!waitForResponse(bleCommand)) {
                    promise.completeExceptionally(new BleGattException("GATT unregister for notification failed"));
                }
            } catch (BleGattException | TimeoutException e) {
                promise.completeExceptionally(e);
            }
        })
        .whenComplete((result, err) -> {
            if (err != null) {
                promise.completeExceptionally(new BleGattException("GATT unregister for notification failed"));
            }
            promise.complete(result);
        });

        return promise.minimalCompletionStage();
    }

    public CompletionStage<List<BleCharacteristicDescriptor>> discoverDescriptorsAsync() {
        final CompletableFuture<List<BleCharacteristicDescriptor>> promise = new CompletableFuture<>();
        CompletableFuture.supplyAsync(() -> {
            if (!bleService.getBleDevice().isConnected()) {
                promise.completeExceptionally(new BleGattException("GATT Request not possible. Device is not connected."));
            }

            BleDiscoverCharacteristicDescriptorsCommand bleCommand = new BleDiscoverCharacteristicDescriptorsCommand(this);

            try {
                if (!waitForResponse(bleCommand)) {
                    promise.completeExceptionally(new BleGattException("Discover descriptors failed"));
                }
            } catch (BleGattException | TimeoutException e) {
                promise.completeExceptionally(e);
            }

            return bleCommand.getDiscoveredDescriptors();
        })
        .whenComplete((readData, err) -> {
            if (err != null) {
                promise.completeExceptionally(new BleGattException("Discover descriptors failed"));
            }
            promise.complete(readData);
        });

        return promise.minimalCompletionStage();
    }

    // TODO return whole gattResponse to caller to be able to have status in client code.
    private boolean waitForResponse(final BleCommand bleCommand) throws BleGattException, TimeoutException {
        final CountDownLatch doneLatch = new CountDownLatch(1);
        final AtomicBoolean requestOk = new AtomicBoolean();
        bleCommand.registerResponseCallback(gattResponse -> {
            requestOk.set(gattResponse.isRequestOk());
            doneLatch.countDown();
        });

        bleService.getBleDevice().getBleAdapter().executeBleCommandOnConnection(bleCommand, getService().getBleDevice());

        try {
            if (!doneLatch.await(getService().getBleDevice().getBleAdapter().getGattTimeoutMs(), TimeUnit.MILLISECONDS)) {
                logger.error("Timeout waiting for GATT response");
                throw new TimeoutException("Timeout on GATT request.");
            }
        } catch (InterruptedException ignored) { }

        return requestOk.get();
    }

    public CharacteristicProperties getProperties() {
        return properties;
    }
}
