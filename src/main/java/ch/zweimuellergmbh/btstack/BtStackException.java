package ch.zweimuellergmbh.btstack;

public class BtStackException extends Exception {
    public BtStackException(String message) {
        super(message);
    }

    public BtStackException(String message, Throwable cause) {
        super(message, cause);
    }
}
