package ch.zweimuellergmbh.btstack;

public class BleScanException extends Exception {
    public BleScanException(String message) {
        super(message);
    }
}
