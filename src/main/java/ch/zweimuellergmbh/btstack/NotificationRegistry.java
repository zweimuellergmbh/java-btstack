package ch.zweimuellergmbh.btstack;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.Consumer;

public class NotificationRegistry {
    private static final Logger logger = LoggerFactory.getLogger(NotificationRegistry.class);

    private final Map<Integer, Map<Integer, Consumer<byte[]>>> notificationRegistry = new HashMap<>();

    /**
     * Registers a notification callback for the given connectionHandle and the given valueHandle.
     *
     * @param connectionHandle      The connectionHandle where the notification callback is registered.
     * @param valueHandle           The valueHandle, for which the notification callback is registered.
     * @param notificationCallback  The callback to be called, when a notification for the given valueHandle
     *                              arrives on the given connectionHandle.
     *
     * @throws IllegalArgumentException      If the callback is <code>null</code>
     */
    public void registerNotificationCallback(final int connectionHandle, final int valueHandle,
                                             final Consumer<byte[]> notificationCallback) throws IllegalArgumentException {
        if (notificationCallback == null) {
            throw new IllegalArgumentException("Notification Callback must not be null");
        }

        Map<Integer, Consumer<byte[]>> callbackMap = notificationRegistry.get(connectionHandle);
        if (callbackMap == null) {
            // no mapping for the current connection
            callbackMap = new HashMap<>();
        }
        callbackMap.put(valueHandle, notificationCallback);
        notificationRegistry.put(connectionHandle, callbackMap);

        logger.debug("Register notification callback for connection: {}, value handle: {}.", connectionHandle, valueHandle);
    }

    /**
     * Unregisters the notification callback for the given valueHandle on the given connectionHandle if there is
     * one registered. If no callback is registered for the given arguments this method just returns.
     *
     * @param connectionHandle  The connectionHandle on which the callback has to be unregistered from.
     * @param valueHandle       The valueHandle, for which the callback has to be unregistered from.
     */
    public void unregisterNotificationCallback(int connectionHandle, int valueHandle) {
        Map<Integer, Consumer<byte[]>> callbackMap = notificationRegistry.get(connectionHandle);
        if (callbackMap != null) {
            callbackMap.remove(valueHandle);
            logger.debug("Remove notification callback for connection: {}, value handle: {}", connectionHandle, valueHandle);
        }
    }

    /**
     * Removes all registered callbacks on the given connectionHandle. This method is used for example
     * when a connection has been closed.
     *
     * @param connectionHandle  The connectionHandle to remove all callbacks from.
     */
    public void removeAllRegisteredCallbacksForConnection(final int connectionHandle) {
        logger.debug("Remove all notifications for connection: {}", connectionHandle);
        notificationRegistry.remove(connectionHandle);
    }

    /**
     * Returns the registered callback on the given connectionHandle for the given valueHandle. If there
     * is no callback registered <code>null</code> is returned.
     *
     * @param connectionHandle  The connectionHandle to get the callback for.
     * @param valueHandle       The valueHandle to get the callback for.
     *
     * @return The registered callback wrapped in a Optional. The optional is empty, if callback is not registered.
     */
    public Optional<Consumer<byte[]>> getRegisteredCallback(final int connectionHandle, final int valueHandle) {
        final Map<Integer, Consumer<byte[]>> callbackMap = notificationRegistry.get(connectionHandle);
        if (callbackMap != null) {
            // 'null' is never a valid value of the map and means always 'not found'
            return Optional.ofNullable(callbackMap.get(valueHandle));
        }
        return Optional.empty();
    }
}
