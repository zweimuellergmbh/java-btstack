package ch.zweimuellergmbh.btstack;

import com.bluekitchen.btstack.BTstack;
import com.bluekitchen.btstack.event.SMEventJustWorksRequest;
import com.bluekitchen.btstack.event.SMEventNumericComparisonRequest;
import com.bluekitchen.btstack.event.SMEventPasskeyDisplayNumber;
import com.bluekitchen.btstack.event.SMEventPasskeyInputNumber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Function;

public class SMDispachter {
    private static final Logger logger = LoggerFactory.getLogger(SMDispachter.class);

    private Function<BleAddress, Boolean> justWorksRequestCallback;
    private BiFunction<BleAddress, Long, Boolean> numericComparisonRequestCallback;
    private Function<BleAddress, Long> passkeyRequestCallback;
    private BiFunction<BleAddress, Long, Boolean> displayPasskeyRequestHandler;

    private BTstack btstack;
    private Thread thread;

    public SMDispachter(BTstack btstack) {
        this.btstack = btstack;
    }

    public void registerJustWorksRequestCallback(final Function<BleAddress, Boolean> justWorksRequestCallback) {
        this.justWorksRequestCallback = justWorksRequestCallback;
    }

    public void registerNumericComparisonRequestCallback(final BiFunction<BleAddress, Long, Boolean> numericComparisonRequestCallback) {
        this.numericComparisonRequestCallback = numericComparisonRequestCallback;
    }

    public void registerPasskeyRequestCallback(final Function<BleAddress, Long> passkeyRequestCallback) {
        this.passkeyRequestCallback = passkeyRequestCallback;
    }

    public void registerDisplayPasskeyRequestCallback(final BiFunction<BleAddress, Long, Boolean> displayPasskeyRequestHandler) {
        this.displayPasskeyRequestHandler = displayPasskeyRequestHandler;
    }

    public void justWorksRequest(final SMEventJustWorksRequest request) {
        if (justWorksRequestCallback == null) {
            throw new RuntimeException("No just-works handler is installed - please use SMDispachter.registerJustWorksRequestCallback()");
        }

        thread = new Thread(() -> {
            if (justWorksRequestCallback.apply(
                    new BleAddress(request.getAddress(), BleAddress.AddressType.fromInt(request.getAddrType())))) {
                logger.info("Confirm JustWorks pairing");
                this.btstack.SMJustWorksConfirm(request.getHandle());
            } else {
                logger.info("Reject JustWorks pairing");
                // SMBondingDecline cancels the pairing and any hanging/waiting GATT requests
                btstack.SMBondingDecline(request.getHandle());
            }
        });
        thread.start();
    }

    public void ncRequest(final SMEventNumericComparisonRequest request) {
        if (numericComparisonRequestCallback == null) {
            throw new RuntimeException("No numeric comparison handler is installed - please use SMDispachter.registerNumericComparisonRequestCallback()");
        }

        thread = new Thread(() -> {
            if (numericComparisonRequestCallback.apply(
                    new BleAddress(request.getAddress(), BleAddress.AddressType.fromInt(request.getAddrType())),
                    request.getPasskey())) {
                btstack.SMNumericComparisonConfirm(request.getHandle());
            } else {
                logger.info("Reject NC pairing");
                // SMBondingDecline cancels the pairing and any hanging/waiting GATT requests
                btstack.SMBondingDecline(request.getHandle());
            }
        });
        thread.start();
    }


    public void passkeyDisplayRequest(final SMEventPasskeyDisplayNumber request) {
        if (displayPasskeyRequestHandler == null) {
            throw new RuntimeException("No passkey display handler is installed - please use SMDispachter.registerDisplayPasskeyRequestCallback()");
        }

        thread = new Thread(() -> {
            if (displayPasskeyRequestHandler.apply(
                    new BleAddress(request.getAddress(), BleAddress.AddressType.fromInt(request.getAddrType())),
                    request.getPasskey())) {
                // stack simply waits for pin entry on peripheral
            } else {
                logger.info("Cancel pairing");
                // SMBondingDecline cancels the pairing and any hanging/waiting GATT requests
                btstack.SMBondingDecline(request.getHandle());
            }
        });
        thread.start();
    }


    // TODO support ative cancelling?
    public void passkeyRequest(final SMEventPasskeyInputNumber request) {
        if (passkeyRequestCallback == null) {
            throw new RuntimeException("No passkey handler is installed - please use bleDevice.registerPasskeyRequestCallback()");
        }

        thread = new Thread(() -> {
            long pin = passkeyRequestCallback.apply(new BleAddress(request.getAddress(), BleAddress.AddressType.fromInt(request.getAddrType())));
            btstack.SMPasskeyInput(request.getHandle(), pin);
        });
        thread.start();
    }

    // TODO mark pairing type per handle
    public void resetPairingOnConnection(int handle) {
        logger.debug("Reset pairing callback thread on connection: {}", handle);
        if (thread.isAlive()) {
            thread.interrupt(); // same behaviour as 'reject'
        }
    }
}
