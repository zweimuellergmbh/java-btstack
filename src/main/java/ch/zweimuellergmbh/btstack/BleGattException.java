package ch.zweimuellergmbh.btstack;

public class BleGattException extends Exception {
    public BleGattException(String message) {
        super(message);
    }

    public BleGattException(String message, Throwable cause) {
        super(message, cause);
    }
}
