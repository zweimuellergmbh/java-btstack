package ch.zweimuellergmbh.btstack;

import com.bluekitchen.btstack.BD_ADDR;

import java.util.Arrays;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class BleAddress {

    public enum AddressType {
        STATIC(0),
        RANDOM(1);

        private final int value;

        AddressType(final int value) {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }

        public static AddressType fromInt(final int id) {
            final AddressType addressType = reverseLookup.get(id);
            if (addressType == null) {
                throw new IllegalArgumentException("Invalid address type specified");
            }
            return addressType;
        }

        private static final Map<Integer, AddressType> reverseLookup =
                Arrays.stream(AddressType.values()).collect(Collectors.toMap(AddressType::getValue, Function.identity()));
    }

    private final BD_ADDR address;
    private final AddressType addressType;

    public BleAddress(final BD_ADDR address, final AddressType addressType) {
        this.address = address;
        this.addressType = addressType;
    }

    public BD_ADDR getAddress() {
        return address;
    }

    public AddressType getAddressType() {
        return addressType;
    }

    @Override
    public String toString() {
        return String.format("[BleAddress: address: %s type: %s]", address, addressType.name());
    }
}
