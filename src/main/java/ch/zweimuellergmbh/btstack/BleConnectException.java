package ch.zweimuellergmbh.btstack;

public class BleConnectException extends Exception {
    public BleConnectException(String message) {
        super(message);
    }
}
