package ch.zweimuellergmbh.btstack;

import ch.zweimuellergmbh.btstack.command.*;
import com.bluekitchen.btstack.BT_UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicBoolean;

public class BleDevice {
    private static final Logger logger = LoggerFactory.getLogger(BleDevice.class);
    private final BleAddress bleAddress;
    private final BleAdapter bleAdapter;
    protected boolean connected;
    protected int connectionHandle;

    public BleDevice(BleAddress bleAddress, BleAdapter bleAdapter) {
        this.bleAddress = bleAddress;
        this.bleAdapter = bleAdapter;
    }

    public boolean isConnected() {
        return connected;
    }

    public BleAdapter getBleAdapter() {
        return bleAdapter;
    }

    public int getConnectionHandle() {
        return connectionHandle;
    }

    public BleAddress getBleAddress() {
        return bleAddress;
    }

    public void disconnected() {
        logger.warn("Device: {} has received a disconnect event.",
                getBleAddress().getAddress());
        this.connected = false;
    }

    public void connect() throws BleConnectException, TimeoutException {
        try {
            connectAsync().toCompletableFuture().get();
        } catch (InterruptedException | ExecutionException e) {
            if (e.getCause() instanceof TimeoutException) {
                throw new TimeoutException("Timeout connecting to: " + getBleAddress().getAddress());
            }
            throw new BleConnectException(e.getCause().getMessage());
        }
    }

    public void disconnect() throws BleGattException, TimeoutException {
        try {
            disconnectAsync().toCompletableFuture().get();
        } catch (InterruptedException | ExecutionException e) {
            if (e.getCause() instanceof TimeoutException) {
                throw new TimeoutException("Timeout disconnecting from: " + getBleAddress().getAddress());
            }
            throw new BleGattException(e.getCause().getMessage());
        }
    }

    public List<BleService> discoverPrimaryServices() throws BleGattException, TimeoutException {
        try {
            return discoverPrimaryServicesAsync().toCompletableFuture().get();
        } catch (InterruptedException | ExecutionException e) {
            if (e.getCause() instanceof TimeoutException) {
                throw new TimeoutException("Timeout discover services of: " + getBleAddress().getAddress());
            }
            throw new BleGattException(e.getCause().getMessage());
        }
    }

    public BleService discoverPrimaryServiceByUUID(BT_UUID uuid) throws BleGattException, TimeoutException {
        try {
            return discoverPrimaryServiceByUUIDAsync(uuid).toCompletableFuture().get();
        } catch (InterruptedException | ExecutionException e) {
            if (e.getCause() instanceof TimeoutException) {
                throw new TimeoutException("Timeout discover service by UUID: " + getBleAddress().getAddress());
            }
            throw new BleGattException(e.getCause().getMessage());
        }
    }

    public CompletionStage<BleDevice> connectAsync() {
        CompletableFuture<BleDevice> promise = new CompletableFuture<>();
        CompletableFuture.supplyAsync(() -> {
            final CountDownLatch connectLatch = new CountDownLatch(1);
            final BleConnectCommand bleCommand = new BleConnectCommand(bleAdapter, bleAddress);

            bleCommand.registerResponseCallback(gattResponse -> {
                this.connected = gattResponse.isRequestOk();
                this.connectionHandle = gattResponse.getConnectionHandle();
                connectLatch.countDown();
            });

            try {
                bleAdapter.executeBleConnectionCommand(bleCommand, this);
            } catch (BleConnectException e) {
                promise.completeExceptionally(e);
            }

            try {
                if (!connectLatch.await(bleAdapter.getConnectTimeoutMs(), TimeUnit.MILLISECONDS)) {
                    // TODO call connect cancel on btstack??
                    promise.completeExceptionally(new TimeoutException("Timeout during connect."));
                }
            } catch (InterruptedException e) {
                this.connected = false; // correct?
            }

            return this.connected;
        })
        .whenComplete((connected, err) -> {
            if (!connected || err != null) {
                // TODO call connect cancel on btstack??
                logger.error("Error during connect", err);
                promise.completeExceptionally(new BleConnectException("Connect to address failed: " + bleAddress.getAddress()));
            }
            promise.complete(this);
        });

        return promise.minimalCompletionStage();
    }


    public CompletionStage<BleDevice> disconnectAsync() {
        final CompletableFuture<BleDevice> promise = new CompletableFuture<>();
        CompletableFuture.supplyAsync(() -> {
            if (!connected) {
                promise.completeExceptionally(new BleGattException("GATT Request not possible. Device is not connected."));
            }

            final CountDownLatch connectLatch = new CountDownLatch(1);
            final BleDisconnectCommand bleCommand = new BleDisconnectCommand(this);

            bleCommand.registerResponseCallback(gattResponse -> {
                this.connected = false; // always
                connectLatch.countDown();
            });

            try {
                bleAdapter.executeBleCommandOnConnection(bleCommand, this);
            } catch (BleGattException e) {
                promise.completeExceptionally(e);
            }

            try {
                if (!connectLatch.await(bleAdapter.getGattTimeoutMs(), TimeUnit.MILLISECONDS)) {
                    promise.completeExceptionally(new TimeoutException("Timeout during GATT disconnect."));
                }
            } catch (InterruptedException ignored) {
            }

            return this.connected;
        })
        .whenComplete((connected, err) -> {
            if (connected || (err != null)) {
                logger.error("Error during disconnect", err);
                promise.completeExceptionally(new BleGattException("Disconnecting from address failed: " + bleAddress.getAddress()));
            }
            promise.complete(this);
        });

        return promise.minimalCompletionStage();
    }


    public CompletionStage<List<BleService>> discoverPrimaryServicesAsync() {
        final CompletableFuture<List<BleService>> promise = new CompletableFuture<>();
        CompletableFuture.supplyAsync(() -> {
            if (!connected) {
                promise.completeExceptionally(new BleGattException("GATT Request not possible. Device is not connected."));
            }

            BleDiscoverPrimaryServicesCommand bleCommand = new BleDiscoverPrimaryServicesCommand(this);

            try {
                if (!waitForResponse(bleCommand)) {
                    promise.completeExceptionally(new BleGattException("Discover primary services failed: " + bleAddress.getAddress()));
                }
            } catch (BleGattException | TimeoutException e) {
                promise.completeExceptionally(e);
            }

            return bleCommand.getDiscoveredServices();
        })
        .whenComplete((services, err) -> {
            if (err != null) {
                logger.error("Error discover primary services", err);
                promise.completeExceptionally(new BleGattException("Discover primary services failed: " + bleAddress.getAddress()));
            }
            promise.complete(services);
        });

        return promise.minimalCompletionStage();
    }

    public CompletionStage<BleService> discoverPrimaryServiceByUUIDAsync(final BT_UUID uuid) {
        final CompletableFuture<BleService> promise = new CompletableFuture<>();
        CompletableFuture.supplyAsync(() -> {
            if (!connected) {
                promise.completeExceptionally(new BleGattException("GATT Request not possible. Device is not connected."));
            }

            BleDiscoverPrimaryServiceByUUIDCommand bleCommand = new BleDiscoverPrimaryServiceByUUIDCommand(this, uuid);

            try {
                if (!waitForResponse(bleCommand)) {
                    promise.completeExceptionally(new BleGattException("Discover primary services for UUID failed: " + uuid));
                }
            } catch (BleGattException | TimeoutException e) {
                promise.completeExceptionally(e);
            }

            return bleCommand.getDiscoveredService();
        })
        .whenComplete((services, err) -> {
            if (err != null) {
                logger.error("Error discover primary service by uuid", err);
                promise.completeExceptionally(new BleGattException("Discover primary service for UUID failed: " + uuid));
            }
            promise.complete(services);
        });

        return promise.minimalCompletionStage();
    }

    private boolean waitForResponse(BleCommand bleCommand) throws BleGattException, TimeoutException {
        final CountDownLatch doneLatch = new CountDownLatch(1);
        final AtomicBoolean requestOk = new AtomicBoolean();
        bleCommand.registerResponseCallback(gattResponse -> {
            requestOk.set(gattResponse.isRequestOk());
            doneLatch.countDown();
        });

        bleAdapter.executeBleCommandOnConnection(bleCommand, this);

        try {
            if (!doneLatch.await(bleAdapter.getGattTimeoutMs(), TimeUnit.MILLISECONDS)) {
                logger.error("Timeout waiting for GATT response");
                throw new TimeoutException("Timeout on GATT request.");
            }
        } catch (InterruptedException ignored) { }

        return requestOk.get();
    }
}
