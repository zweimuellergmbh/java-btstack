package ch.zweimuellergmbh.btstack;

public class CharacteristicProperties {
    private final boolean broadcastCharacteristicValueAllowed;
    private final boolean read;
    private final boolean writeWithoutResponse;
    private final boolean write;
    private final boolean notify;
    private final boolean indicate;
    private final boolean signedWrite;
    private final boolean queuedWrite;
    private final boolean writeableAuxiliaries;

    public CharacteristicProperties(int properties) {
        byte[] data = new byte[2];

        // explicit conversion for clarity
        data[0] = (byte) properties;
        data[1] = (byte) (properties >> 8);

        broadcastCharacteristicValueAllowed = isSetBit(data[0], 0);
        read = isSetBit(data[0], 1);
        writeWithoutResponse = isSetBit(data[0], 2);
        write = isSetBit(data[0], 3);
        notify = isSetBit(data[0], 4);
        indicate = isSetBit(data[0], 5);
        signedWrite = isSetBit(data[0], 6);
        queuedWrite = isSetBit(data[0], 7);
        writeableAuxiliaries = isSetBit(data[1], 0);
    }

    public boolean isBroadcastCharacteristicValueAllowed() {
        return broadcastCharacteristicValueAllowed;
    }

    public boolean isRead() {
        return read;
    }

    public boolean isWriteWithoutResponse() {
        return writeWithoutResponse;
    }

    public boolean isWrite() {
        return write;
    }

    public boolean isNotify() {
        return notify;
    }

    public boolean isIndicate() {
        return indicate;
    }

    public boolean isSignedWrite() {
        return signedWrite;
    }

    public boolean isQueuedWrite() {
        return queuedWrite;
    }

    public boolean isWriteableAuxiliaries() {
        return writeableAuxiliaries;
    }

    private boolean isSetBit(byte input, int position) {
        return ((input >> position) & 1) == 1;
    }

    @Override
    public String toString() {
        String tmp = "[" +
                ((broadcastCharacteristicValueAllowed) ? "BROADCAST_ALLOWED, " : "") +
                ((read) ? "READ, " : "") +
                ((writeWithoutResponse) ? "WRITE_W/O_RESPONSE, " : "") +
                ((write) ? "WRITE, " : "") +
                ((notify) ? "NOTIFY, " : "") +
                ((indicate) ? "INDICATE, " : "") +
                ((signedWrite) ? "SIGNED_WRITE, " : "") +
                ((queuedWrite) ? "QUEUED_WRITE, " : "") +
                ((writeableAuxiliaries) ? "WRITEABLE_AUXILIARIES, " : "") +
                "]";
        return tmp.replace(", ]", "]");
    }
}
