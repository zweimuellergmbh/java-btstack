package ch.zweimuellergmbh.btstack;

public enum IOCapabilities {
    IO_CAP_DISPLAY_ONLY(0x0),
    IO_CAP_DISPLAY_YES_NO(0x1),
    IO_CAP_KEYBOARD_ONLY(0x2),
    IO_CAP_NO_INPUT_NO_OUTPUT(0x3),
    IO_CAP_KEYBOARD_DISPLAY(0x4);

    public final int value;

    IOCapabilities(int value) {
        this.value = value;
    }
}
