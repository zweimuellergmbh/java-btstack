package ch.zweimuellergmbh.btstack;

import com.bluekitchen.btstack.BT_UUID;
import com.bluekitchen.btstack.BTstack;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.concurrent.*;

import static java.lang.System.exit;
import static java.lang.Thread.sleep;


// TODO create integration test project from this
class Application {

    private static final Logger logger = LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) throws BtStackException, BleScanTimeoutException, ExecutionException, InterruptedException {

        BTstack bTstack = new BTstack();
        BleAdapter bleAdapter = new BleAdapter.BleAdapterBuilder(bTstack, IOCapabilities.IO_CAP_KEYBOARD_ONLY, null)
                .useExternalBtStackServer(true)
                .useBonding(false)
                .build();

        bleAdapter.powerOnBtStack();


        final BleDevice bleDevice = bleAdapter.scanForDeviceWithName(10, "LE Counter");


        /**
         * connect
         */
        bleDevice.connectAsync().toCompletableFuture().get(); // TODO check, what happens if cancelled or timeout from outside.


        /**
         * Reject concurrent requests
         */
        CompletionStage<List<BleService>> request1 = bleDevice.discoverPrimaryServicesAsync();
        CompletionStage<List<BleService>> request2 = bleDevice.discoverPrimaryServicesAsync();
        // both futures start to run now.
        try {
            request1.toCompletableFuture().get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        request2.whenComplete((s, err) -> {
            if (err == null) {
                logger.error("Concurrent request should not be possible");
            }
            else {
                logger.info("TEST CONCURRENT REQUEST SUCCESS");
            }
        });



        logger.info("******************************");
        logger.info("******** ASYNC ***************");
        logger.info("******************************");

        // notifications
        BT_UUID serviceUuid = new BT_UUID("0000ff10-0000-1000-8000-00805f9b34fb");
        BT_UUID charUuid = new BT_UUID("0000ff11-0000-1000-8000-00805f9b34fb");

        bleDevice.connectAsync()
                .thenCompose(device -> bleDevice.discoverPrimaryServiceByUUIDAsync(serviceUuid))
                .thenCompose(service -> service.getCharacteristicByUUIDAsync(charUuid))
                .thenCompose(c -> c.registerForNotificationAsync(b -> System.out.println(new String(b))))
                .thenRun(() -> sleepSeconds(5)).thenRun(bleDevice::disconnectAsync);


        sleepSeconds(10);

        exit(0);
    }

    private static void sleepSeconds(int seconds) {
        try {
            Thread.sleep(seconds * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
