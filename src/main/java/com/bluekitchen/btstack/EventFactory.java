package com.bluekitchen.btstack;

import com.bluekitchen.btstack.event.*;

public class EventFactory {

    /** @brief event codes */

    public static final int A2DP_SUBEVENT_COMMAND_ACCEPTED = 0x0D;

    public static final int A2DP_SUBEVENT_COMMAND_REJECTED = 0x0E;

    public static final int A2DP_SUBEVENT_SIGNALING_CAPABILITIES_COMPLETE = 0x1B;

    public static final int A2DP_SUBEVENT_SIGNALING_CAPABILITIES_DONE = 0x1A;

    public static final int A2DP_SUBEVENT_SIGNALING_CONNECTION_ESTABLISHED = 0x0F;

    public static final int A2DP_SUBEVENT_SIGNALING_CONNECTION_RELEASED = 0x10;

    public static final int A2DP_SUBEVENT_SIGNALING_DELAY_REPORT = 0x19;

    public static final int A2DP_SUBEVENT_SIGNALING_DELAY_REPORTING_CAPABILITY = 0x18;

    public static final int A2DP_SUBEVENT_SIGNALING_MEDIA_CODEC_ATRAC_CAPABILITY = 0x16;

    public static final int A2DP_SUBEVENT_SIGNALING_MEDIA_CODEC_ATRAC_CONFIGURATION = 0x05;

    public static final int A2DP_SUBEVENT_SIGNALING_MEDIA_CODEC_MPEG_AAC_CAPABILITY = 0x15;

    public static final int A2DP_SUBEVENT_SIGNALING_MEDIA_CODEC_MPEG_AAC_CONFIGURATION = 0x04;

    public static final int A2DP_SUBEVENT_SIGNALING_MEDIA_CODEC_MPEG_AUDIO_CAPABILITY = 0x14;

    public static final int A2DP_SUBEVENT_SIGNALING_MEDIA_CODEC_MPEG_AUDIO_CONFIGURATION = 0x03;

    public static final int A2DP_SUBEVENT_SIGNALING_MEDIA_CODEC_OTHER_CAPABILITY = 0x17;

    public static final int A2DP_SUBEVENT_SIGNALING_MEDIA_CODEC_OTHER_CONFIGURATION = 0x06;

    public static final int A2DP_SUBEVENT_SIGNALING_MEDIA_CODEC_SBC_CAPABILITY = 0x13;

    public static final int A2DP_SUBEVENT_SIGNALING_MEDIA_CODEC_SBC_CONFIGURATION = 0x02;

    public static final int A2DP_SUBEVENT_START_STREAM_REQUESTED = 0x08;

    public static final int A2DP_SUBEVENT_STREAMING_CAN_SEND_MEDIA_PACKET_NOW = 0x01;

    public static final int A2DP_SUBEVENT_STREAM_ESTABLISHED = 0x07;

    public static final int A2DP_SUBEVENT_STREAM_RECONFIGURED = 0x12;

    public static final int A2DP_SUBEVENT_STREAM_RELEASED = 0x0C;

    public static final int A2DP_SUBEVENT_STREAM_STARTED = 0x09;

    public static final int A2DP_SUBEVENT_STREAM_STOPPED = 0x0B;

    public static final int A2DP_SUBEVENT_STREAM_SUSPENDED = 0x0A;

    public static final int ANCS_SUBEVENT_CLIENT_CONNECTED = 0xF0;

    public static final int ANCS_SUBEVENT_CLIENT_DISCONNECTED = 0xF2;

    public static final int ANCS_SUBEVENT_CLIENT_NOTIFICATION = 0xF1;

    public static final int ATT_EVENT_CAN_SEND_NOW = 0xB7;

    public static final int ATT_EVENT_CONNECTED = 0xB3;

    public static final int ATT_EVENT_DISCONNECTED = 0xB4;

    public static final int ATT_EVENT_HANDLE_VALUE_INDICATION_COMPLETE = 0xB6;

    public static final int ATT_EVENT_MTU_EXCHANGE_COMPLETE = 0xB5;

    public static final int AVDTP_SUBEVENT_SIGNALING_ACCEPT = 0x01;

    public static final int AVDTP_SUBEVENT_SIGNALING_CAPABILITIES_DONE = 0x13;

    public static final int AVDTP_SUBEVENT_SIGNALING_CONNECTION_ESTABLISHED = 0x04;

    public static final int AVDTP_SUBEVENT_SIGNALING_CONNECTION_RELEASED = 0x05;

    public static final int AVDTP_SUBEVENT_SIGNALING_CONTENT_PROTECTION_CAPABILITY = 0x0F;

    public static final int AVDTP_SUBEVENT_SIGNALING_DELAY_REPORT = 0x1D;

    public static final int AVDTP_SUBEVENT_SIGNALING_DELAY_REPORTING_CAPABILITY = 0x11;

    public static final int AVDTP_SUBEVENT_SIGNALING_GENERAL_REJECT = 0x03;

    public static final int AVDTP_SUBEVENT_SIGNALING_HEADER_COMPRESSION_CAPABILITY = 0x12;

    public static final int AVDTP_SUBEVENT_SIGNALING_MEDIA_CODEC_ATRAC_CAPABILITY = 0x0A;

    public static final int AVDTP_SUBEVENT_SIGNALING_MEDIA_CODEC_ATRAC_CONFIGURATION = 0x17;

    public static final int AVDTP_SUBEVENT_SIGNALING_MEDIA_CODEC_MPEG_AAC_CAPABILITY = 0x09;

    public static final int AVDTP_SUBEVENT_SIGNALING_MEDIA_CODEC_MPEG_AAC_CONFIGURATION = 0x16;

    public static final int AVDTP_SUBEVENT_SIGNALING_MEDIA_CODEC_MPEG_AUDIO_CAPABILITY = 0x08;

    public static final int AVDTP_SUBEVENT_SIGNALING_MEDIA_CODEC_MPEG_AUDIO_CONFIGURATION = 0x15;

    public static final int AVDTP_SUBEVENT_SIGNALING_MEDIA_CODEC_OTHER_CAPABILITY = 0x0B;

    public static final int AVDTP_SUBEVENT_SIGNALING_MEDIA_CODEC_OTHER_CONFIGURATION = 0x18;

    public static final int AVDTP_SUBEVENT_SIGNALING_MEDIA_CODEC_SBC_CAPABILITY = 0x07;

    public static final int AVDTP_SUBEVENT_SIGNALING_MEDIA_CODEC_SBC_CONFIGURATION = 0x14;

    public static final int AVDTP_SUBEVENT_SIGNALING_MEDIA_TRANSPORT_CAPABILITY = 0x0C;

    public static final int AVDTP_SUBEVENT_SIGNALING_MULTIPLEXING_CAPABILITY = 0x10;

    public static final int AVDTP_SUBEVENT_SIGNALING_RECOVERY_CAPABILITY = 0x0E;

    public static final int AVDTP_SUBEVENT_SIGNALING_REJECT = 0x02;

    public static final int AVDTP_SUBEVENT_SIGNALING_REPORTING_CAPABILITY = 0x0D;

    public static final int AVDTP_SUBEVENT_SIGNALING_SEP_DICOVERY_DONE = 0x1C;

    public static final int AVDTP_SUBEVENT_SIGNALING_SEP_FOUND = 0x06;

    public static final int AVDTP_SUBEVENT_STREAMING_CAN_SEND_MEDIA_PACKET_NOW = 0x1B;

    public static final int AVDTP_SUBEVENT_STREAMING_CONNECTION_ESTABLISHED = 0x19;

    public static final int AVDTP_SUBEVENT_STREAMING_CONNECTION_RELEASED = 0x1A;

    public static final int AVRCP_SUBEVENT_BROWSING_CONNECTION_ESTABLISHED = 0x31;

    public static final int AVRCP_SUBEVENT_BROWSING_CONNECTION_RELEASED = 0x32;

    public static final int AVRCP_SUBEVENT_BROWSING_DONE = 0x33;

    public static final int AVRCP_SUBEVENT_BROWSING_GET_FOLDER_ITEMS = 0x34;

    public static final int AVRCP_SUBEVENT_BROWSING_GET_TOTAL_NUM_ITEMS = 0x35;

    public static final int AVRCP_SUBEVENT_BROWSING_SET_BROWSED_PLAYER = 0x36;

    public static final int AVRCP_SUBEVENT_COMPANY_IDS_QUERY = 0x19;

    public static final int AVRCP_SUBEVENT_CONNECTION_ESTABLISHED = 0x12;

    public static final int AVRCP_SUBEVENT_CONNECTION_RELEASED = 0x13;

    public static final int AVRCP_SUBEVENT_ENABLE_NOTIFICATION_COMPLETE = 0x11;

    public static final int AVRCP_SUBEVENT_EVENT_IDS_QUERY = 0x1A;

    public static final int AVRCP_SUBEVENT_GET_CAPABILITY_COMPANY_ID = 0x28;

    public static final int AVRCP_SUBEVENT_GET_CAPABILITY_COMPANY_ID_DONE = 0x29;

    public static final int AVRCP_SUBEVENT_GET_CAPABILITY_EVENT_ID = 0x26;

    public static final int AVRCP_SUBEVENT_GET_CAPABILITY_EVENT_ID_DONE = 0x27;

    public static final int AVRCP_SUBEVENT_INCOMING_BROWSING_CONNECTION = 0x30;

    public static final int AVRCP_SUBEVENT_NOTIFICATION_AVAILABLE_PLAYERS_CHANGED = 0x0A;

    public static final int AVRCP_SUBEVENT_NOTIFICATION_EVENT_BATT_STATUS_CHANGED = 0x06;

    public static final int AVRCP_SUBEVENT_NOTIFICATION_EVENT_PLAYBACK_POS_CHANGED = 0x05;

    public static final int AVRCP_SUBEVENT_NOTIFICATION_EVENT_SYSTEM_STATUS_CHANGED = 0x07;

    public static final int AVRCP_SUBEVENT_NOTIFICATION_EVENT_TRACK_REACHED_END = 0x03;

    public static final int AVRCP_SUBEVENT_NOTIFICATION_EVENT_TRACK_REACHED_START = 0x04;

    public static final int AVRCP_SUBEVENT_NOTIFICATION_EVENT_UIDS_CHANGED = 0x0C;

    public static final int AVRCP_SUBEVENT_NOTIFICATION_NOW_PLAYING_CONTENT_CHANGED = 0x09;

    public static final int AVRCP_SUBEVENT_NOTIFICATION_PLAYBACK_POS_CHANGED = 0x25;

    public static final int AVRCP_SUBEVENT_NOTIFICATION_PLAYBACK_STATUS_CHANGED = 0x01;

    public static final int AVRCP_SUBEVENT_NOTIFICATION_TRACK_CHANGED = 0x02;

    public static final int AVRCP_SUBEVENT_NOTIFICATION_VOLUME_CHANGED = 0x0D;

    public static final int AVRCP_SUBEVENT_NOW_PLAYING_ALBUM_INFO = 0x22;

    public static final int AVRCP_SUBEVENT_NOW_PLAYING_ARTIST_INFO = 0x21;

    public static final int AVRCP_SUBEVENT_NOW_PLAYING_GENRE_INFO = 0x23;

    public static final int AVRCP_SUBEVENT_NOW_PLAYING_INFO_DONE = 0x24;

    public static final int AVRCP_SUBEVENT_NOW_PLAYING_SONG_LENGTH_MS_INFO = 0x1F;

    public static final int AVRCP_SUBEVENT_NOW_PLAYING_TITLE_INFO = 0x20;

    public static final int AVRCP_SUBEVENT_NOW_PLAYING_TOTAL_TRACKS_INFO = 0x1E;

    public static final int AVRCP_SUBEVENT_NOW_PLAYING_TRACK_INFO = 0x1D;

    public static final int AVRCP_SUBEVENT_OPERATION = 0x1C;

    public static final int AVRCP_SUBEVENT_OPERATION_COMPLETE = 0x17;

    public static final int AVRCP_SUBEVENT_OPERATION_START = 0x16;

    public static final int AVRCP_SUBEVENT_PLAYER_APPLICATION_VALUE_RESPONSE = 0x18;

    // defines[AVRCP_SUBEVENT_PLAY_STATUS] not set

    public static final int AVRCP_SUBEVENT_PLAY_STATUS_QUERY = 0x1B;

    public static final int AVRCP_SUBEVENT_SET_ABSOLUTE_VOLUME_RESPONSE = 0x10;

    public static final int AVRCP_SUBEVENT_SHUFFLE_AND_REPEAT_MODE = 0x14;

    // defines[BNEP_EVENT_CAN_SEND_NOW] not set

    // defines[BNEP_EVENT_CHANNEL_CLOSED] not set

    // defines[BNEP_EVENT_CHANNEL_OPENED] not set

    public static final int BNEP_EVENT_CHANNEL_TIMEOUT = 0xC3;

    // defines[BNEP_EVENT_SERVICE_REGISTERED] not set

    public static final int BTSTACK_EVENT_DISCOVERABLE_ENABLED = 0x66;

    public static final int BTSTACK_EVENT_NR_CONNECTIONS_CHANGED = 0x61;

    public static final int BTSTACK_EVENT_POWERON_FAILED = 0x62;

    public static final int BTSTACK_EVENT_STATE = 0x60;

    public static final int DAEMON_EVENT_L2CAP_SERVICE_REGISTERED = 0x75;

    public static final int DAEMON_EVENT_REMOTE_NAME_CACHED = 0x65;

    public static final int DAEMON_EVENT_RFCOMM_CREDITS = 0x84;

    public static final int DAEMON_EVENT_RFCOMM_PERSISTENT_CHANNEL = 0x86;

    public static final int DAEMON_EVENT_RFCOMM_SERVICE_REGISTERED = 0x85;

    public static final int DAEMON_EVENT_SDP_SERVICE_REGISTERED = 0x90;

    public static final int DAEMON_EVENT_SYSTEM_BLUETOOTH_ENABLED = 0x64;

    public static final int DAEMON_EVENT_VERSION = 0x63;

    public static final int GAP_EVENT_ADVERTISING_REPORT = 0xDA;

    public static final int GAP_EVENT_DEDICATED_BONDING_COMPLETED = 0xD9;

    public static final int GAP_EVENT_INQUIRY_COMPLETE = 0xDC;

    public static final int GAP_EVENT_INQUIRY_RESULT = 0xDB;

    public static final int GAP_EVENT_LOCAL_OOB_DATA = 0xDE;

    public static final int GAP_EVENT_PAIRING_COMPLETE = 0xE0;

    public static final int GAP_EVENT_PAIRING_STARTED = 0xDF;

    public static final int GAP_EVENT_RSSI_MEASUREMENT = 0xDD;

    public static final int GAP_EVENT_SECURITY_LEVEL = 0xD8;

    public static final int GATTSERVICE_SUBEVENT_BATTERY_SERVICE_CONNECTED = 0x04;

    public static final int GATTSERVICE_SUBEVENT_BATTERY_SERVICE_LEVEL = 0x05;

    public static final int GATTSERVICE_SUBEVENT_CYCLING_POWER_BROADCAST_START = 0x02;

    public static final int GATTSERVICE_SUBEVENT_CYCLING_POWER_BROADCAST_STOP = 0x03;

    public static final int GATTSERVICE_SUBEVENT_CYCLING_POWER_START_CALIBRATION = 0x01;

    public static final int GATTSERVICE_SUBEVENT_DEVICE_INFORMATION_DONE = 0x06;

    public static final int GATTSERVICE_SUBEVENT_DEVICE_INFORMATION_FIRMWARE_REVISION = 0x0B;

    public static final int GATTSERVICE_SUBEVENT_DEVICE_INFORMATION_HARDWARE_REVISION = 0x0A;

    public static final int GATTSERVICE_SUBEVENT_DEVICE_INFORMATION_IEEE_REGULATORY_CERTIFICATION = 0x0E;

    public static final int GATTSERVICE_SUBEVENT_DEVICE_INFORMATION_MANUFACTURER_NAME = 0x07;

    public static final int GATTSERVICE_SUBEVENT_DEVICE_INFORMATION_MODEL_NUMBER = 0x08;

    public static final int GATTSERVICE_SUBEVENT_DEVICE_INFORMATION_PNP_ID = 0x0F;

    public static final int GATTSERVICE_SUBEVENT_DEVICE_INFORMATION_SERIAL_NUMBER = 0x09;

    public static final int GATTSERVICE_SUBEVENT_DEVICE_INFORMATION_SOFTWARE_REVISION = 0x0C;

    public static final int GATTSERVICE_SUBEVENT_DEVICE_INFORMATION_SYSTEM_ID = 0x0D;

    public static final int GATTSERVICE_SUBEVENT_HID_INFORMATION = 0x15;

    public static final int GATTSERVICE_SUBEVENT_HID_PROTOCOL_MODE = 0x16;

    public static final int GATTSERVICE_SUBEVENT_HID_REPORT = 0x14;

    public static final int GATTSERVICE_SUBEVENT_HID_SERVICE_CONNECTED = 0x13;

    public static final int GATTSERVICE_SUBEVENT_HID_SERVICE_REPORTS_NOTIFICATION = 0x17;

    public static final int GATTSERVICE_SUBEVENT_MICROPHONE_CONTROL_SERVICE_CONNECTED = 0x19;

    public static final int GATTSERVICE_SUBEVENT_MICROPHONE_CONTROL_SERVICE_MUTE = 0x1A;

    public static final int GATTSERVICE_SUBEVENT_SCAN_PARAMETERS_SERVICE_CONNECTED = 0x10;

    public static final int GATTSERVICE_SUBEVENT_SCAN_PARAMETERS_SERVICE_SCAN_INTERVAL_UPDATE = 0x18;

    public static final int GATTSERVICE_SUBEVENT_SPP_SERVICE_CONNECTED = 0x11;

    public static final int GATTSERVICE_SUBEVENT_SPP_SERVICE_DISCONNECTED = 0x12;

    public static final int GATT_EVENT_ALL_CHARACTERISTIC_DESCRIPTORS_QUERY_RESULT = 0xA4;

    public static final int GATT_EVENT_CAN_WRITE_WITHOUT_RESPONSE = 0xAC;

    public static final int GATT_EVENT_CHARACTERISTIC_DESCRIPTOR_QUERY_RESULT = 0xA9;

    public static final int GATT_EVENT_CHARACTERISTIC_QUERY_RESULT = 0xA2;

    public static final int GATT_EVENT_CHARACTERISTIC_VALUE_QUERY_RESULT = 0xA5;

    public static final int GATT_EVENT_INCLUDED_SERVICE_QUERY_RESULT = 0xA3;

    public static final int GATT_EVENT_INDICATION = 0xA8;

    public static final int GATT_EVENT_LONG_CHARACTERISTIC_DESCRIPTOR_QUERY_RESULT = 0xAA;

    public static final int GATT_EVENT_LONG_CHARACTERISTIC_VALUE_QUERY_RESULT = 0xA6;

    public static final int GATT_EVENT_MTU = 0xAB;

    public static final int GATT_EVENT_NOTIFICATION = 0xA7;

    public static final int GATT_EVENT_QUERY_COMPLETE = 0xA0;

    public static final int GATT_EVENT_SERVICE_QUERY_RESULT = 0xA1;

    public static final int GOEP_SUBEVENT_CAN_SEND_NOW = 0x03;

    public static final int GOEP_SUBEVENT_CONNECTION_CLOSED = 0x02;

    public static final int GOEP_SUBEVENT_CONNECTION_OPENED = 0x01;

    public static final int HCI_EVENT_AUTHENTICATION_COMPLETE = 0x06;

    public static final int HCI_EVENT_CHANGE_CONNECTION_LINK_KEY_COMPLETE = 0x09;

    public static final int HCI_EVENT_COMMAND_COMPLETE = 0x0E;

    public static final int HCI_EVENT_COMMAND_STATUS = 0x0F;

    public static final int HCI_EVENT_CONNECTION_COMPLETE = 0x03;

    public static final int HCI_EVENT_CONNECTION_PACKET_TYPE_CHANGED = 0x1D;

    public static final int HCI_EVENT_CONNECTION_REQUEST = 0x04;

    public static final int HCI_EVENT_DATA_BUFFER_OVERFLOW = 0x1A;

    public static final int HCI_EVENT_DISCONNECTION_COMPLETE = 0x05;

    public static final int HCI_EVENT_ENCRYPTION_CHANGE = 0x08;

    public static final int HCI_EVENT_ENCRYPTION_KEY_REFRESH_COMPLETE = 0x30;

    public static final int HCI_EVENT_EXTENDED_INQUIRY_RESPONSE = 0x2F;

    public static final int HCI_EVENT_HARDWARE_ERROR = 0x10;

    public static final int HCI_EVENT_INQUIRY_COMPLETE = 0x01;

    public static final int HCI_EVENT_INQUIRY_RESULT = 0x02;

    public static final int HCI_EVENT_INQUIRY_RESULT_WITH_RSSI = 0x22;

    public static final int HCI_EVENT_IO_CAPABILITY_REQUEST = 0x31;

    public static final int HCI_EVENT_IO_CAPABILITY_RESPONSE = 0x32;

    public static final int HCI_EVENT_KEYPRESS_NOTIFICATION = 0x3C;

    public static final int HCI_EVENT_LINK_KEY_REQUEST = 0x17;

    public static final int HCI_EVENT_MASTER_LINK_KEY_COMPLETE = 0x0A;

    public static final int HCI_EVENT_MAX_SLOTS_CHANGED = 0x1B;

    public static final int HCI_EVENT_MODE_CHANGE = 0x14;

    public static final int HCI_EVENT_PIN_CODE_REQUEST = 0x16;

    public static final int HCI_EVENT_READ_CLOCK_OFFSET_COMPLETE = 0x1C;

    public static final int HCI_EVENT_READ_REMOTE_VERSION_INFORMATION_COMPLETE = 0x0C;

    public static final int HCI_EVENT_REMOTE_NAME_REQUEST_COMPLETE = 0x07;

    public static final int HCI_EVENT_REMOTE_OOB_DATA_REQUEST = 0x35;

    public static final int HCI_EVENT_ROLE_CHANGE = 0x12;

    public static final int HCI_EVENT_SCO_CAN_SEND_NOW = 0x6F;

    public static final int HCI_EVENT_SIMPLE_PAIRING_COMPLETE = 0x36;

    public static final int HCI_EVENT_SYNCHRONOUS_CONNECTION_COMPLETE = 0x2C;

    public static final int HCI_EVENT_TRANSPORT_SLEEP_MODE = 0x69;

    public static final int HCI_EVENT_USER_CONFIRMATION_REQUEST = 0x33;

    public static final int HCI_EVENT_USER_PASSKEY_NOTIFICATION = 0x3B;

    public static final int HCI_EVENT_USER_PASSKEY_REQUEST = 0x34;

    public static final int HCI_SUBEVENT_LE_CONNECTION_COMPLETE = 0x01;

    // defines[HCI_SUBEVENT_LE_CONNECTION_UPDATE_COMPLETE] not set

    public static final int HCI_SUBEVENT_LE_DATA_LENGTH_CHANGE = 0x07;

    public static final int HCI_SUBEVENT_LE_ENHANCED_CONNECTION_COMPLETE = 0x0A;

    public static final int HCI_SUBEVENT_LE_GENERATE_DHKEY_COMPLETE = 0x09;

    public static final int HCI_SUBEVENT_LE_LONG_TERM_KEY_REQUEST = 0x05;

    public static final int HCI_SUBEVENT_LE_READ_LOCAL_P256_PUBLIC_KEY_COMPLETE = 0x08;

    public static final int HCI_SUBEVENT_LE_READ_REMOTE_FEATURES_COMPLETE = 0x04;

    public static final int HCI_SUBEVENT_LE_REMOTE_CONNECTION_PARAMETER_REQUEST = 0x06;

    public static final int HFP_SUBEVENT_AG_INDICATOR_MAPPING = 0x06;

    public static final int HFP_SUBEVENT_AG_INDICATOR_STATUS_CHANGED = 0x07;

    public static final int HFP_SUBEVENT_ATTACH_NUMBER_TO_VOICE_TAG = 0x0E;

    public static final int HFP_SUBEVENT_AT_MESSAGE_RECEIVED = 0x1C;

    public static final int HFP_SUBEVENT_AT_MESSAGE_SENT = 0x1B;

    public static final int HFP_SUBEVENT_AUDIO_CONNECTION_ESTABLISHED = 0x03;

    public static final int HFP_SUBEVENT_AUDIO_CONNECTION_RELEASED = 0x04;

    public static final int HFP_SUBEVENT_CALLING_LINE_IDENTIFICATION_NOTIFICATION = 0x17;

    public static final int HFP_SUBEVENT_CALL_ANSWERED = 0x11;

    public static final int HFP_SUBEVENT_CALL_TERMINATED = 0x12;

    public static final int HFP_SUBEVENT_CALL_WAITING_NOTIFICATION = 0x16;

    public static final int HFP_SUBEVENT_COMPLETE = 0x05;

    public static final int HFP_SUBEVENT_CONFERENCE_CALL = 0x13;

    public static final int HFP_SUBEVENT_ECHO_CANCELING_AND_NOISE_REDUCTION_DEACTIVATE = 0x26;

    public static final int HFP_SUBEVENT_ENHANCED_CALL_STATUS = 0x18;

    public static final int HFP_SUBEVENT_ENHANCED_VOICE_RECOGNITION_AG_IS_PROCESSING_AUDIO_INPUT = 0x23;

    public static final int HFP_SUBEVENT_ENHANCED_VOICE_RECOGNITION_AG_IS_STARTING_SOUND = 0x22;

    public static final int HFP_SUBEVENT_ENHANCED_VOICE_RECOGNITION_AG_MESSAGE = 0x25;

    public static final int HFP_SUBEVENT_ENHANCED_VOICE_RECOGNITION_AG_MESSAGE_SENT = 0x24;

    public static final int HFP_SUBEVENT_ENHANCED_VOICE_RECOGNITION_AG_READY_TO_ACCEPT_AUDIO_INPUT = 0x21;

    public static final int HFP_SUBEVENT_ENHANCED_VOICE_RECOGNITION_HF_READY_FOR_AUDIO = 0x20;

    public static final int HFP_SUBEVENT_EXTENDED_AUDIO_GATEWAY_ERROR = 0x09;

    public static final int HFP_SUBEVENT_HF_INDICATOR = 0x27;

    public static final int HFP_SUBEVENT_IN_BAND_RING_TONE = 0x1D;

    public static final int HFP_SUBEVENT_MICROPHONE_VOLUME = 0x15;

    public static final int HFP_SUBEVENT_NETWORK_OPERATOR_CHANGED = 0x08;

    public static final int HFP_SUBEVENT_NUMBER_FOR_VOICE_TAG = 0x0F;

    public static final int HFP_SUBEVENT_PLACE_CALL_WITH_NUMBER = 0x0D;

    public static final int HFP_SUBEVENT_RESPONSE_AND_HOLD_STATUS = 0x1A;

    public static final int HFP_SUBEVENT_RING = 0x0B;

    public static final int HFP_SUBEVENT_SERVICE_LEVEL_CONNECTION_ESTABLISHED = 0x01;

    public static final int HFP_SUBEVENT_SERVICE_LEVEL_CONNECTION_RELEASED = 0x02;

    public static final int HFP_SUBEVENT_SPEAKER_VOLUME = 0x14;

    public static final int HFP_SUBEVENT_START_RINGING = 0x0A;

    public static final int HFP_SUBEVENT_STOP_RINGING = 0x0C;

    public static final int HFP_SUBEVENT_SUBSCRIBER_NUMBER_INFORMATION = 0x19;

    public static final int HFP_SUBEVENT_TRANSMIT_DTMF_CODES = 0x10;

    public static final int HFP_SUBEVENT_VOICE_RECOGNITION_ACTIVATED = 0x1E;

    public static final int HFP_SUBEVENT_VOICE_RECOGNITION_DEACTIVATED = 0x1F;

    public static final int HIDS_SUBEVENT_BOOT_KEYBOARD_INPUT_REPORT_ENABLE = 0x04;

    public static final int HIDS_SUBEVENT_BOOT_MOUSE_INPUT_REPORT_ENABLE = 0x03;

    public static final int HIDS_SUBEVENT_CAN_SEND_NOW = 0x01;

    public static final int HIDS_SUBEVENT_EXIT_SUSPEND = 0x09;

    public static final int HIDS_SUBEVENT_FEATURE_REPORT_ENABLE = 0x07;

    public static final int HIDS_SUBEVENT_INPUT_REPORT_ENABLE = 0x05;

    public static final int HIDS_SUBEVENT_OUTPUT_REPORT_ENABLE = 0x06;

    public static final int HIDS_SUBEVENT_PROTOCOL_MODE = 0x02;

    public static final int HIDS_SUBEVENT_SUSPEND = 0x08;

    public static final int HID_SUBEVENT_CAN_SEND_NOW = 0x04;

    public static final int HID_SUBEVENT_CONNECTION_CLOSED = 0x03;

    public static final int HID_SUBEVENT_CONNECTION_OPENED = 0x02;

    public static final int HID_SUBEVENT_DESCRIPTOR_AVAILABLE = 0x0D;

    public static final int HID_SUBEVENT_EXIT_SUSPEND = 0x06;

    public static final int HID_SUBEVENT_GET_PROTOCOL_RESPONSE = 0x0A;

    public static final int HID_SUBEVENT_GET_REPORT_RESPONSE = 0x08;

    public static final int HID_SUBEVENT_INCOMING_CONNECTION = 0x01;

    public static final int HID_SUBEVENT_REPORT = 0x0C;

    public static final int HID_SUBEVENT_SET_PROTOCOL_RESPONSE = 0x0B;

    public static final int HID_SUBEVENT_SET_REPORT_RESPONSE = 0x09;

    public static final int HID_SUBEVENT_SNIFF_SUBRATING_PARAMS = 0x0E;

    public static final int HID_SUBEVENT_SUSPEND = 0x05;

    public static final int HID_SUBEVENT_VIRTUAL_CABLE_UNPLUG = 0x07;

    public static final int HSP_SUBEVENT_AG_INDICATION = 0x09;

    public static final int HSP_SUBEVENT_AUDIO_CONNECTION_COMPLETE = 0x03;

    public static final int HSP_SUBEVENT_AUDIO_DISCONNECTION_COMPLETE = 0x04;

    public static final int HSP_SUBEVENT_BUTTON_PRESSED = 0x0a;

    public static final int HSP_SUBEVENT_HS_COMMAND = 0x08;

    public static final int HSP_SUBEVENT_MICROPHONE_GAIN_CHANGED = 0x06;

    public static final int HSP_SUBEVENT_RFCOMM_CONNECTION_COMPLETE = 0x01;

    public static final int HSP_SUBEVENT_RFCOMM_DISCONNECTION_COMPLETE = 0x02;

    public static final int HSP_SUBEVENT_RING = 0x05;

    public static final int HSP_SUBEVENT_SPEAKER_GAIN_CHANGED = 0x07;

    public static final int L2CAP_EVENT_CAN_SEND_NOW = 0x78;

    public static final int L2CAP_EVENT_CHANNEL_CLOSED = 0x71;

    public static final int L2CAP_EVENT_CHANNEL_OPENED = 0x70;

    public static final int L2CAP_EVENT_CONNECTION_PARAMETER_UPDATE_REQUEST = 0x76;

    public static final int L2CAP_EVENT_CONNECTION_PARAMETER_UPDATE_RESPONSE = 0x77;

    public static final int L2CAP_EVENT_ERTM_BUFFER_RELEASED = 0x7e;

    public static final int L2CAP_EVENT_INCOMING_CONNECTION = 0x72;

    public static final int L2CAP_EVENT_LE_CAN_SEND_NOW = 0x7c;

    public static final int L2CAP_EVENT_LE_CHANNEL_CLOSED = 0x7b;

    public static final int L2CAP_EVENT_LE_CHANNEL_OPENED = 0x7a;

    public static final int L2CAP_EVENT_LE_INCOMING_CONNECTION = 0x79;

    public static final int L2CAP_EVENT_LE_PACKET_SENT = 0x7d;

    public static final int L2CAP_EVENT_TRIGGER_RUN = 0x7f;

    public static final int MAP_SUBEVENT_CONNECTION_CLOSED = 0x02;

    public static final int MAP_SUBEVENT_CONNECTION_OPENED = 0x01;

    public static final int MAP_SUBEVENT_FOLDER_LISTING_ITEM = 0x04;

    public static final int MAP_SUBEVENT_MESSAGE_LISTING_ITEM = 0x05;

    public static final int MAP_SUBEVENT_OPERATION_COMPLETED = 0x03;

    public static final int MAP_SUBEVENT_PARSING_DONE = 0x06;

    public static final int MESH_SUBEVENT_ATTENTION_TIMER = 0x1e;

    public static final int MESH_SUBEVENT_CAN_SEND_NOW = 0x01;

    public static final int MESH_SUBEVENT_CONFIGURATION_APPKEY_INDEX = 0x46;

    // defines[MESH_SUBEVENT_CONFIGURATION_APPKEY_INDEX_LIST_ITEM] not set

    public static final int MESH_SUBEVENT_CONFIGURATION_BEACON = 0x36;

    public static final int MESH_SUBEVENT_CONFIGURATION_DEFAULT_TTL = 0x38;

    public static final int MESH_SUBEVENT_CONFIGURATION_FRIEND = 0x52;

    public static final int MESH_SUBEVENT_CONFIGURATION_GATT_PROXY = 0x39;

    public static final int MESH_SUBEVENT_CONFIGURATION_HEARTBEAT_PUBLICATION = 0x54;

    public static final int MESH_SUBEVENT_CONFIGURATION_HEARTBEAT_SUBSCRIPTION = 0x55;

    public static final int MESH_SUBEVENT_CONFIGURATION_KEY_REFRESH_PHASE = 0x53;

    public static final int MESH_SUBEVENT_CONFIGURATION_LOW_POWER_NODE_POLL_TIMEOUT = 0x56;

    public static final int MESH_SUBEVENT_CONFIGURATION_MODEL_APP = 0x49;

    public static final int MESH_SUBEVENT_CONFIGURATION_MODEL_APP_LIST_ITEM = 0x50;

    public static final int MESH_SUBEVENT_CONFIGURATION_MODEL_PUBLICATION = 0x41;

    public static final int MESH_SUBEVENT_CONFIGURATION_MODEL_SUBSCRIPTION = 0x42;

    public static final int MESH_SUBEVENT_CONFIGURATION_MODEL_SUBSCRIPTION_LIST_ITEM = 0x43;

    public static final int MESH_SUBEVENT_CONFIGURATION_NETKEY_INDEX = 0x44;

    public static final int MESH_SUBEVENT_CONFIGURATION_NETKEY_INDEX_LIST_ITEM = 0x45;

    public static final int MESH_SUBEVENT_CONFIGURATION_NETWORK_TRANSMIT = 0x57;

    public static final int MESH_SUBEVENT_CONFIGURATION_NODE_IDENTITY = 0x48;

    public static final int MESH_SUBEVENT_CONFIGURATION_NODE_RESET = 0x51;

    public static final int MESH_SUBEVENT_CONFIGURATION_RELAY = 0x40;

    public static final int MESH_SUBEVENT_GENERIC_DEFAULT_TRANSITION_TIME = 0x35;

    public static final int MESH_SUBEVENT_GENERIC_LEVEL = 0x32;

    public static final int MESH_SUBEVENT_GENERIC_ON_OFF = 0x31;

    public static final int MESH_SUBEVENT_HEALTH_ATTENTION_TIMER_CHANGED = 0x34;

    public static final int MESH_SUBEVENT_HEALTH_PERFORM_TEST = 0x33;

    public static final int MESH_SUBEVENT_MESSAGE_NOT_ACKNOWLEDGED = 0x30;

    public static final int MESH_SUBEVENT_MESSAGE_SENT = 0x23;

    public static final int MESH_SUBEVENT_PB_PROV_ATTENTION_TIMER = 0x10;

    public static final int MESH_SUBEVENT_PB_PROV_CAPABILITIES = 0x1c;

    public static final int MESH_SUBEVENT_PB_PROV_COMPLETE = 0x1d;

    public static final int MESH_SUBEVENT_PB_PROV_INPUT_OOB_REQUEST = 0x13;

    public static final int MESH_SUBEVENT_PB_PROV_OUTPUT_OOB_REQUEST = 0x19;

    public static final int MESH_SUBEVENT_PB_PROV_START_EMIT_INPUT_OOB = 0x1a;

    public static final int MESH_SUBEVENT_PB_PROV_START_EMIT_OUTPUT_OOB = 0x15;

    public static final int MESH_SUBEVENT_PB_PROV_START_EMIT_PUBLIC_KEY_OOB = 0x11;

    public static final int MESH_SUBEVENT_PB_PROV_START_RECEIVE_PUBLIC_KEY_OOB = 0x17;

    public static final int MESH_SUBEVENT_PB_PROV_STOP_EMIT_INPUT_OOB = 0x1b;

    public static final int MESH_SUBEVENT_PB_PROV_STOP_EMIT_OUTPUT_OOB = 0x16;

    public static final int MESH_SUBEVENT_PB_PROV_STOP_EMIT_PUBLIC_KEY_OOB = 0x12;

    public static final int MESH_SUBEVENT_PB_PROV_STOP_RECEIVE_PUBLIC_KEY_OOB = 0x18;

    public static final int MESH_SUBEVENT_PB_TRANSPORT_LINK_CLOSED = 0x04;

    public static final int MESH_SUBEVENT_PB_TRANSPORT_LINK_OPEN = 0x03;

    public static final int MESH_SUBEVENT_PB_TRANSPORT_PDU_SENT = 0x02;

    public static final int MESH_SUBEVENT_PROXY_CONNECTED = 0x20;

    public static final int MESH_SUBEVENT_PROXY_DISCONNECTED = 0x22;

    public static final int MESH_SUBEVENT_PROXY_PDU_SENT = 0x21;

    public static final int MESH_SUBEVENT_STATE_UPDATE_BOOL = 0x24;

    public static final int MESH_SUBEVENT_STATE_UPDATE_INT16 = 0x25;

    public static final int PBAP_SUBEVENT_AUTHENTICATION_REQUEST = 0x05;

    public static final int PBAP_SUBEVENT_CARD_RESULT = 0x06;

    public static final int PBAP_SUBEVENT_CONNECTION_CLOSED = 0x02;

    public static final int PBAP_SUBEVENT_CONNECTION_OPENED = 0x01;

    public static final int PBAP_SUBEVENT_OPERATION_COMPLETED = 0x03;

    public static final int PBAP_SUBEVENT_PHONEBOOK_SIZE = 0x04;

    public static final int RFCOMM_EVENT_CAN_SEND_NOW = 0x89;

    public static final int RFCOMM_EVENT_CHANNEL_CLOSED = 0x81;

    public static final int RFCOMM_EVENT_CHANNEL_OPENED = 0x80;

    public static final int RFCOMM_EVENT_INCOMING_CONNECTION = 0x82;

    public static final int RFCOMM_EVENT_REMOTE_LINE_STATUS = 0x83;

    public static final int RFCOMM_EVENT_REMOTE_MODEM_STATUS = 0x87;

    public static final int SDP_EVENT_QUERY_ATTRIBUTE_BYTE = 0x93;

    public static final int SDP_EVENT_QUERY_ATTRIBUTE_VALUE = 0x94;

    public static final int SDP_EVENT_QUERY_COMPLETE = 0x91;

    public static final int SDP_EVENT_QUERY_RFCOMM_SERVICE = 0x92;

    public static final int SDP_EVENT_QUERY_SERVICE_RECORD_HANDLE = 0x95;

    public static final int SM_EVENT_AUTHORIZATION_REQUEST = 0xD0;

    public static final int SM_EVENT_AUTHORIZATION_RESULT = 0xD1;

    public static final int SM_EVENT_IDENTITY_CREATED = 0xD3;

    public static final int SM_EVENT_IDENTITY_RESOLVING_FAILED = 0xCE;

    public static final int SM_EVENT_IDENTITY_RESOLVING_STARTED = 0xCD;

    public static final int SM_EVENT_IDENTITY_RESOLVING_SUCCEEDED = 0xCF;

    public static final int SM_EVENT_JUST_WORKS_REQUEST = 0xC8;

    public static final int SM_EVENT_KEYPRESS_NOTIFICATION = 0xD2;

    public static final int SM_EVENT_NUMERIC_COMPARISON_REQUEST = 0xCC;

    public static final int SM_EVENT_PAIRING_COMPLETE = 0xD5;

    public static final int SM_EVENT_PAIRING_STARTED = 0xD4;

    public static final int SM_EVENT_PASSKEY_DISPLAY_CANCEL = 0xCA;

    public static final int SM_EVENT_PASSKEY_DISPLAY_NUMBER = 0xC9;

    public static final int SM_EVENT_PASSKEY_INPUT_NUMBER = 0xCB;

    public static final int SM_EVENT_REENCRYPTION_COMPLETE = 0xD7;

    public static final int SM_EVENT_REENCRYPTION_STARTED = 0xD6;

    public static Event eventForPacket(Packet packet){
        int eventType = Util.readByte(packet.getBuffer(), 0);
        switch (eventType){

        case 0x01:
            return new HCIEventInquiryComplete(packet);

        case 0x02:
            return new HCIEventInquiryResult(packet);

        case 0x03:
            return new HCIEventConnectionComplete(packet);

        case 0x04:
            return new HCIEventConnectionRequest(packet);

        case 0x05:
            return new HCIEventDisconnectionComplete(packet);

        case 0x06:
            return new HCIEventAuthenticationComplete(packet);

        case 0x07:
            return new HCIEventRemoteNameRequestComplete(packet);

        case 0x08:
            return new HCIEventEncryptionChange(packet);

        case 0x09:
            return new HCIEventChangeConnectionLinkKeyComplete(packet);

        case 0x0A:
            return new HCIEventMasterLinkKeyComplete(packet);

        case 0x0C:
            return new HCIEventReadRemoteVersionInformationComplete(packet);

        case 0x0E:
            return new HCIEventCommandComplete(packet);

        case 0x0F:
            return new HCIEventCommandStatus(packet);

        case 0x10:
            return new HCIEventHardwareError(packet);

        case 0x12:
            return new HCIEventRoleChange(packet);

        case 0x14:
            return new HCIEventModeChange(packet);

        case 0x16:
            return new HCIEventPinCodeRequest(packet);

        case 0x17:
            return new HCIEventLinkKeyRequest(packet);

        case 0x1A:
            return new HCIEventDataBufferOverflow(packet);

        case 0x1B:
            return new HCIEventMaxSlotsChanged(packet);

        case 0x1C:
            return new HCIEventReadClockOffsetComplete(packet);

        case 0x1D:
            return new HCIEventConnectionPacketTypeChanged(packet);

        case 0x22:
            return new HCIEventInquiryResultWithRssi(packet);

        case 0x2C:
            return new HCIEventSynchronousConnectionComplete(packet);

        case 0x2F:
            return new HCIEventExtendedInquiryResponse(packet);

        case 0x30:
            return new HCIEventEncryptionKeyRefreshComplete(packet);

        case 0x31:
            return new HCIEventIoCapabilityRequest(packet);

        case 0x32:
            return new HCIEventIoCapabilityResponse(packet);

        case 0x33:
            return new HCIEventUserConfirmationRequest(packet);

        case 0x34:
            return new HCIEventUserPasskeyRequest(packet);

        case 0x35:
            return new HCIEventRemoteOobDataRequest(packet);

        case 0x36:
            return new HCIEventSimplePairingComplete(packet);

        case 0x3B:
            return new HCIEventUserPasskeyNotification(packet);

        case 0x3C:
            return new HCIEventKeypressNotification(packet);

        case 0x60:
            return new BTstackEventState(packet);

        case 0x61:
            return new BTstackEventNrConnectionsChanged(packet);

        case 0x62:
            return new BTstackEventPoweronFailed(packet);

        case 0x66:
            return new BTstackEventDiscoverableEnabled(packet);

        case 0x63:
            return new DaemonEventVersion(packet);

        case 0x64:
            return new DaemonEventSystemBluetoothEnabled(packet);

        case 0x65:
            return new DaemonEventRemoteNameCached(packet);

        case 0x75:
            return new DaemonEventL2CAPServiceRegistered(packet);

        case 0x84:
            return new DaemonEventRFCOMMCredits(packet);

        case 0x85:
            return new DaemonEventRFCOMMServiceRegistered(packet);

        case 0x86:
            return new DaemonEventRFCOMMPersistentChannel(packet);

        case 0x90:
            return new DaemonEventSDPServiceRegistered(packet);

        case 0x69:
            return new HCIEventTransportSleepMode(packet);

        case 0x6F:
            return new HCIEventScoCanSendNow(packet);

        case 0x70:
            return new L2CAPEventChannelOpened(packet);

        case 0x71:
            return new L2CAPEventChannelClosed(packet);

        case 0x72:
            return new L2CAPEventIncomingConnection(packet);

        case 0x76:
            return new L2CAPEventConnectionParameterUpdateRequest(packet);

        case 0x77:
            return new L2CAPEventConnectionParameterUpdateResponse(packet);

        case 0x78:
            return new L2CAPEventCanSendNow(packet);

        case 0x79:
            return new L2CAPEventLEIncomingConnection(packet);

        case 0x7a:
            return new L2CAPEventLEChannelOpened(packet);

        case 0x7b:
            return new L2CAPEventLEChannelClosed(packet);

        case 0x7c:
            return new L2CAPEventLECanSendNow(packet);

        case 0x7d:
            return new L2CAPEventLEPacketSent(packet);

        case 0x7e:
            return new L2CAPEventErtmBufferReleased(packet);

        case 0x7f:
            return new L2CAPEventTriggerRun(packet);

        case 0x80:
            return new RFCOMMEventChannelOpened(packet);

        case 0x81:
            return new RFCOMMEventChannelClosed(packet);

        case 0x82:
            return new RFCOMMEventIncomingConnection(packet);

        case 0x83:
            return new RFCOMMEventRemoteLineStatus(packet);

        case 0x87:
            return new RFCOMMEventRemoteModemStatus(packet);

        case 0x89:
            return new RFCOMMEventCanSendNow(packet);

        case 0x91:
            return new SDPEventQueryComplete(packet);

        case 0x92:
            return new SDPEventQueryRFCOMMService(packet);

        case 0x93:
            return new SDPEventQueryAttributeByte(packet);

        case 0x94:
            return new SDPEventQueryAttributeValue(packet);

        case 0x95:
            return new SDPEventQueryServiceRecordHandle(packet);

        case 0xA0:
            return new GATTEventQueryComplete(packet);

        case 0xA1:
            return new GATTEventServiceQueryResult(packet);

        case 0xA2:
            return new GATTEventCharacteristicQueryResult(packet);

        case 0xA3:
            return new GATTEventIncludedServiceQueryResult(packet);

        case 0xA4:
            return new GATTEventAllCharacteristicDescriptorsQueryResult(packet);

        case 0xA5:
            return new GATTEventCharacteristicValueQueryResult(packet);

        case 0xA6:
            return new GATTEventLongCharacteristicValueQueryResult(packet);

        case 0xA7:
            return new GATTEventNotification(packet);

        case 0xA8:
            return new GATTEventIndication(packet);

        case 0xA9:
            return new GATTEventCharacteristicDescriptorQueryResult(packet);

        case 0xAA:
            return new GATTEventLongCharacteristicDescriptorQueryResult(packet);

        case 0xAB:
            return new GATTEventMtu(packet);

        case 0xAC:
            return new GATTEventCanWriteWithoutResponse(packet);

        case 0xB3:
            return new ATTEventConnected(packet);

        case 0xB4:
            return new ATTEventDisconnected(packet);

        case 0xB5:
            return new ATTEventMtuExchangeComplete(packet);

        case 0xB6:
            return new ATTEventHandleValueIndicationComplete(packet);

        case 0xB7:
            return new ATTEventCanSendNow(packet);

        case 0xC0:
            return new BnepEventServiceRegistered(packet);

        case 0xC1:
            return new BnepEventChannelOpened(packet);

        case 0xC2:
            return new BnepEventChannelClosed(packet);

        case 0xC3:
            return new BnepEventChannelTimeout(packet);

        case 0xC4:
            return new BnepEventCanSendNow(packet);

        case 0xC8:
            return new SMEventJustWorksRequest(packet);

        case 0xC9:
            return new SMEventPasskeyDisplayNumber(packet);

        case 0xCA:
            return new SMEventPasskeyDisplayCancel(packet);

        case 0xCB:
            return new SMEventPasskeyInputNumber(packet);

        case 0xCC:
            return new SMEventNumericComparisonRequest(packet);

        case 0xCD:
            return new SMEventIdentityResolvingStarted(packet);

        case 0xCE:
            return new SMEventIdentityResolvingFailed(packet);

        case 0xCF:
            return new SMEventIdentityResolvingSucceeded(packet);

        case 0xD0:
            return new SMEventAuthorizationRequest(packet);

        case 0xD1:
            return new SMEventAuthorizationResult(packet);

        case 0xD2:
            return new SMEventKeypressNotification(packet);

        case 0xD3:
            return new SMEventIdentityCreated(packet);

        case 0xD4:
            return new SMEventPairingStarted(packet);

        case 0xD5:
            return new SMEventPairingComplete(packet);

        case 0xD6:
            return new SMEventReencryptionStarted(packet);

        case 0xD7:
            return new SMEventReencryptionComplete(packet);

        case 0xD8:
            return new GAPEventSecurityLevel(packet);

        case 0xD9:
            return new GAPEventDedicatedBondingCompleted(packet);

        case 0xDA:
            return new GAPEventAdvertisingReport(packet);

        case 0xDB:
            return new GAPEventInquiryResult(packet);

        case 0xDC:
            return new GAPEventInquiryComplete(packet);

        case 0xDD:
            return new GAPEventRssiMeasurement(packet);

        case 0xDE:
            return new GAPEventLocalOobData(packet);

        case 0xDF:
            return new GAPEventPairingStarted(packet);

        case 0xE0:
            return new GAPEventPairingComplete(packet);
        
        case 0x3e:  // LE_META_EVENT
            int subEventType = Util.readByte(packet.getBuffer(), 2);
            switch (subEventType){

            case 0x01:
                return new HCIEventLEConnectionComplete(packet);

            case 0x03:
                return new HCIEventLEConnectionUpdateComplete(packet);

            case 0x04:
                return new HCIEventLEReadRemoteFeaturesComplete(packet);

            case 0x05:
                return new HCIEventLELongTermKeyRequest(packet);

            case 0x06:
                return new HCIEventLERemoteConnectionParameterRequest(packet);

            case 0x07:
                return new HCIEventLEDataLengthChange(packet);

            case 0x08:
                return new HCIEventLEReadLocalP256PublicKeyComplete(packet);

            case 0x09:
                return new HCIEventLEGenerateDhkeyComplete(packet);

            case 0x0A:
                return new HCIEventLEEnhancedConnectionComplete(packet);
            
            default:
                return new Event(packet);
            }

        default:
            return new Event(packet);
        }
    }
}
