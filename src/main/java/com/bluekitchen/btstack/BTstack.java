/** 
 * BTstack Client Library
 */

package com.bluekitchen.btstack;

public class BTstack extends BTstackClient {
    

    public boolean BTstackGetState(){
        // 
        int command_len = 3;
        byte[] command = new byte[command_len];
        Util.storeBt16(command, 0, Util.opcode(OGF_BTSTACK, BTSTACK_GET_STATE));
        int offset = 2;
        Util.storeByte(command, offset, command_len - 3);
        offset++;

        Packet packet = new Packet(Packet.HCI_COMMAND_PACKET, 0, command, command.length);
        return sendPacket(packet);
    }

    public boolean BTstackSetPowerMode(int powerMode){
        // 1
        int command_len = 4;
        byte[] command = new byte[command_len];
        Util.storeBt16(command, 0, Util.opcode(OGF_BTSTACK, BTSTACK_SET_POWER_MODE));
        int offset = 2;
        Util.storeByte(command, offset, command_len - 3);
        offset++;
        Util.storeByte(command, offset, powerMode);
        offset += 1;

        Packet packet = new Packet(Packet.HCI_COMMAND_PACKET, 0, command, command.length);
        return sendPacket(packet);
    }

    public boolean BTstackSetAclCaptureMode(int aclCaptureMode){
        // 1
        int command_len = 4;
        byte[] command = new byte[command_len];
        Util.storeBt16(command, 0, Util.opcode(OGF_BTSTACK, BTSTACK_SET_ACL_CAPTURE_MODE));
        int offset = 2;
        Util.storeByte(command, offset, command_len - 3);
        offset++;
        Util.storeByte(command, offset, aclCaptureMode);
        offset += 1;

        Packet packet = new Packet(Packet.HCI_COMMAND_PACKET, 0, command, command.length);
        return sendPacket(packet);
    }

    public boolean BTstackGetVersion(){
        // 
        int command_len = 3;
        byte[] command = new byte[command_len];
        Util.storeBt16(command, 0, Util.opcode(OGF_BTSTACK, BTSTACK_GET_VERSION));
        int offset = 2;
        Util.storeByte(command, offset, command_len - 3);
        offset++;

        Packet packet = new Packet(Packet.HCI_COMMAND_PACKET, 0, command, command.length);
        return sendPacket(packet);
    }

    public boolean BTstackGetSystemBluetoothEnabled(){
        // 
        int command_len = 3;
        byte[] command = new byte[command_len];
        Util.storeBt16(command, 0, Util.opcode(OGF_BTSTACK, BTSTACK_GET_SYSTEM_BLUETOOTH_ENABLED));
        int offset = 2;
        Util.storeByte(command, offset, command_len - 3);
        offset++;

        Packet packet = new Packet(Packet.HCI_COMMAND_PACKET, 0, command, command.length);
        return sendPacket(packet);
    }

    public boolean BTstackSetSystemBluetoothEnabled(int bluetoothEnabledFlag){
        // 1
        int command_len = 4;
        byte[] command = new byte[command_len];
        Util.storeBt16(command, 0, Util.opcode(OGF_BTSTACK, BTSTACK_SET_SYSTEM_BLUETOOTH_ENABLED));
        int offset = 2;
        Util.storeByte(command, offset, command_len - 3);
        offset++;
        Util.storeByte(command, offset, bluetoothEnabledFlag);
        offset += 1;

        Packet packet = new Packet(Packet.HCI_COMMAND_PACKET, 0, command, command.length);
        return sendPacket(packet);
    }

    public boolean BTstackSetDiscoverable(int discoverableFlag){
        // 1
        int command_len = 4;
        byte[] command = new byte[command_len];
        Util.storeBt16(command, 0, Util.opcode(OGF_BTSTACK, BTSTACK_SET_DISCOVERABLE));
        int offset = 2;
        Util.storeByte(command, offset, command_len - 3);
        offset++;
        Util.storeByte(command, offset, discoverableFlag);
        offset += 1;

        Packet packet = new Packet(Packet.HCI_COMMAND_PACKET, 0, command, command.length);
        return sendPacket(packet);
    }

    public boolean BTstackSetBluetoothEnabled(int bluetoothEnabledFlag){
        // 1
        int command_len = 4;
        byte[] command = new byte[command_len];
        Util.storeBt16(command, 0, Util.opcode(OGF_BTSTACK, BTSTACK_SET_BLUETOOTH_ENABLED));
        int offset = 2;
        Util.storeByte(command, offset, command_len - 3);
        offset++;
        Util.storeByte(command, offset, bluetoothEnabledFlag);
        offset += 1;

        Packet packet = new Packet(Packet.HCI_COMMAND_PACKET, 0, command, command.length);
        return sendPacket(packet);
    }

    public boolean L2CAPCreateChannel(BD_ADDR bdAddr, int psm){
        // B2
        int command_len = 11;
        byte[] command = new byte[command_len];
        Util.storeBt16(command, 0, Util.opcode(OGF_BTSTACK, L2CAP_CREATE_CHANNEL));
        int offset = 2;
        Util.storeByte(command, offset, command_len - 3);
        offset++;
        Util.storeBytes(command, offset, bdAddr.getBytes());
        offset += 6;
        Util.storeBt16(command, offset, psm);
        offset += 2;

        Packet packet = new Packet(Packet.HCI_COMMAND_PACKET, 0, command, command.length);
        return sendPacket(packet);
    }

    public boolean L2CAPCreateChannelMtu(BD_ADDR bdAddr, int psm, int mtu){
        // B22
        int command_len = 13;
        byte[] command = new byte[command_len];
        Util.storeBt16(command, 0, Util.opcode(OGF_BTSTACK, L2CAP_CREATE_CHANNEL_MTU));
        int offset = 2;
        Util.storeByte(command, offset, command_len - 3);
        offset++;
        Util.storeBytes(command, offset, bdAddr.getBytes());
        offset += 6;
        Util.storeBt16(command, offset, psm);
        offset += 2;
        Util.storeBt16(command, offset, mtu);
        offset += 2;

        Packet packet = new Packet(Packet.HCI_COMMAND_PACKET, 0, command, command.length);
        return sendPacket(packet);
    }

    public boolean L2CAPDisconnect(int arg1, int arg2){
        // 21
        int command_len = 6;
        byte[] command = new byte[command_len];
        Util.storeBt16(command, 0, Util.opcode(OGF_BTSTACK, L2CAP_DISCONNECT));
        int offset = 2;
        Util.storeByte(command, offset, command_len - 3);
        offset++;
        Util.storeBt16(command, offset, arg1);
        offset += 2;
        Util.storeByte(command, offset, arg2);
        offset += 1;

        Packet packet = new Packet(Packet.HCI_COMMAND_PACKET, 0, command, command.length);
        return sendPacket(packet);
    }

    public boolean L2CAPRegisterService(int psm, int mtu){
        // 22
        int command_len = 7;
        byte[] command = new byte[command_len];
        Util.storeBt16(command, 0, Util.opcode(OGF_BTSTACK, L2CAP_REGISTER_SERVICE));
        int offset = 2;
        Util.storeByte(command, offset, command_len - 3);
        offset++;
        Util.storeBt16(command, offset, psm);
        offset += 2;
        Util.storeBt16(command, offset, mtu);
        offset += 2;

        Packet packet = new Packet(Packet.HCI_COMMAND_PACKET, 0, command, command.length);
        return sendPacket(packet);
    }

    public boolean L2CAPUnregisterService(int psm){
        // 2
        int command_len = 5;
        byte[] command = new byte[command_len];
        Util.storeBt16(command, 0, Util.opcode(OGF_BTSTACK, L2CAP_UNREGISTER_SERVICE));
        int offset = 2;
        Util.storeByte(command, offset, command_len - 3);
        offset++;
        Util.storeBt16(command, offset, psm);
        offset += 2;

        Packet packet = new Packet(Packet.HCI_COMMAND_PACKET, 0, command, command.length);
        return sendPacket(packet);
    }

    public boolean L2CAPAcceptConnection(int sourceCid){
        // 2
        int command_len = 5;
        byte[] command = new byte[command_len];
        Util.storeBt16(command, 0, Util.opcode(OGF_BTSTACK, L2CAP_ACCEPT_CONNECTION));
        int offset = 2;
        Util.storeByte(command, offset, command_len - 3);
        offset++;
        Util.storeBt16(command, offset, sourceCid);
        offset += 2;

        Packet packet = new Packet(Packet.HCI_COMMAND_PACKET, 0, command, command.length);
        return sendPacket(packet);
    }

    public boolean L2CAPDeclineConnection(int sourceCid, int reason){
        // 21
        int command_len = 6;
        byte[] command = new byte[command_len];
        Util.storeBt16(command, 0, Util.opcode(OGF_BTSTACK, L2CAP_DECLINE_CONNECTION));
        int offset = 2;
        Util.storeByte(command, offset, command_len - 3);
        offset++;
        Util.storeBt16(command, offset, sourceCid);
        offset += 2;
        Util.storeByte(command, offset, reason);
        offset += 1;

        Packet packet = new Packet(Packet.HCI_COMMAND_PACKET, 0, command, command.length);
        return sendPacket(packet);
    }

    public boolean SDPRegisterServiceRecord(byte [] serviceRecord){
        // S
        int command_len = 3 + serviceRecord.length;
        byte[] command = new byte[command_len];
        Util.storeBt16(command, 0, Util.opcode(OGF_BTSTACK, SDP_REGISTER_SERVICE_RECORD));
        int offset = 2;
        Util.storeByte(command, offset, command_len - 3);
        offset++;
        Util.storeBytes(command, offset, serviceRecord);
        offset += serviceRecord.length;

        Packet packet = new Packet(Packet.HCI_COMMAND_PACKET, 0, command, command.length);
        return sendPacket(packet);
    }

    public boolean SDPUnregisterServiceRecord(long serviceRecordHandle){
        // 4
        int command_len = 7;
        byte[] command = new byte[command_len];
        Util.storeBt16(command, 0, Util.opcode(OGF_BTSTACK, SDP_UNREGISTER_SERVICE_RECORD));
        int offset = 2;
        Util.storeByte(command, offset, command_len - 3);
        offset++;
        Util.storeBt32(command, offset, serviceRecordHandle);
        offset += 4;

        Packet packet = new Packet(Packet.HCI_COMMAND_PACKET, 0, command, command.length);
        return sendPacket(packet);
    }

    public boolean SDPClientQueryRFCOMMServices(BD_ADDR bdAddr, byte [] serviceSearchPattern){
        // BS
        int command_len = 9 + serviceSearchPattern.length;
        byte[] command = new byte[command_len];
        Util.storeBt16(command, 0, Util.opcode(OGF_BTSTACK, SDP_CLIENT_QUERY_RFCOMM_SERVICES));
        int offset = 2;
        Util.storeByte(command, offset, command_len - 3);
        offset++;
        Util.storeBytes(command, offset, bdAddr.getBytes());
        offset += 6;
        Util.storeBytes(command, offset, serviceSearchPattern);
        offset += serviceSearchPattern.length;

        Packet packet = new Packet(Packet.HCI_COMMAND_PACKET, 0, command, command.length);
        return sendPacket(packet);
    }

    public boolean SDPClientQueryServices(BD_ADDR bdAddr, byte [] serviceSearchPattern, byte [] attributeIdList){
        // BSS
        int command_len = 9 + serviceSearchPattern.length + attributeIdList.length;
        byte[] command = new byte[command_len];
        Util.storeBt16(command, 0, Util.opcode(OGF_BTSTACK, SDP_CLIENT_QUERY_SERVICES));
        int offset = 2;
        Util.storeByte(command, offset, command_len - 3);
        offset++;
        Util.storeBytes(command, offset, bdAddr.getBytes());
        offset += 6;
        Util.storeBytes(command, offset, serviceSearchPattern);
        offset += serviceSearchPattern.length;
        Util.storeBytes(command, offset, attributeIdList);
        offset += attributeIdList.length;

        Packet packet = new Packet(Packet.HCI_COMMAND_PACKET, 0, command, command.length);
        return sendPacket(packet);
    }

    public boolean RFCOMMCreateChannel(BD_ADDR bdAddr, int serverChannel){
        // B1
        int command_len = 10;
        byte[] command = new byte[command_len];
        Util.storeBt16(command, 0, Util.opcode(OGF_BTSTACK, RFCOMM_CREATE_CHANNEL));
        int offset = 2;
        Util.storeByte(command, offset, command_len - 3);
        offset++;
        Util.storeBytes(command, offset, bdAddr.getBytes());
        offset += 6;
        Util.storeByte(command, offset, serverChannel);
        offset += 1;

        Packet packet = new Packet(Packet.HCI_COMMAND_PACKET, 0, command, command.length);
        return sendPacket(packet);
    }

    public boolean RFCOMMCreateChannelWithInitialCredits(BD_ADDR bdAddr, int serverChannel, int mtu, int credits){
        // B121
        int command_len = 13;
        byte[] command = new byte[command_len];
        Util.storeBt16(command, 0, Util.opcode(OGF_BTSTACK, RFCOMM_CREATE_CHANNEL_WITH_CREDITS));
        int offset = 2;
        Util.storeByte(command, offset, command_len - 3);
        offset++;
        Util.storeBytes(command, offset, bdAddr.getBytes());
        offset += 6;
        Util.storeByte(command, offset, serverChannel);
        offset += 1;
        Util.storeBt16(command, offset, mtu);
        offset += 2;
        Util.storeByte(command, offset, credits);
        offset += 1;

        Packet packet = new Packet(Packet.HCI_COMMAND_PACKET, 0, command, command.length);
        return sendPacket(packet);
    }

    public boolean RFCOMMGrantsCredits(int rFCOMMCid, int credits){
        // 21
        int command_len = 6;
        byte[] command = new byte[command_len];
        Util.storeBt16(command, 0, Util.opcode(OGF_BTSTACK, RFCOMM_GRANT_CREDITS));
        int offset = 2;
        Util.storeByte(command, offset, command_len - 3);
        offset++;
        Util.storeBt16(command, offset, rFCOMMCid);
        offset += 2;
        Util.storeByte(command, offset, credits);
        offset += 1;

        Packet packet = new Packet(Packet.HCI_COMMAND_PACKET, 0, command, command.length);
        return sendPacket(packet);
    }

    public boolean RFCOMMDisconnect(int rFCOMMCid, int reason){
        // 21
        int command_len = 6;
        byte[] command = new byte[command_len];
        Util.storeBt16(command, 0, Util.opcode(OGF_BTSTACK, RFCOMM_DISCONNECT));
        int offset = 2;
        Util.storeByte(command, offset, command_len - 3);
        offset++;
        Util.storeBt16(command, offset, rFCOMMCid);
        offset += 2;
        Util.storeByte(command, offset, reason);
        offset += 1;

        Packet packet = new Packet(Packet.HCI_COMMAND_PACKET, 0, command, command.length);
        return sendPacket(packet);
    }

    public boolean RFCOMMRegisterService(int serverChannel, int mtu){
        // 12
        int command_len = 6;
        byte[] command = new byte[command_len];
        Util.storeBt16(command, 0, Util.opcode(OGF_BTSTACK, RFCOMM_REGISTER_SERVICE));
        int offset = 2;
        Util.storeByte(command, offset, command_len - 3);
        offset++;
        Util.storeByte(command, offset, serverChannel);
        offset += 1;
        Util.storeBt16(command, offset, mtu);
        offset += 2;

        Packet packet = new Packet(Packet.HCI_COMMAND_PACKET, 0, command, command.length);
        return sendPacket(packet);
    }

    public boolean RFCOMMRegisterServiceWithInitialCredits(int serverChannel, int mtu, int initialCredits){
        // 121
        int command_len = 7;
        byte[] command = new byte[command_len];
        Util.storeBt16(command, 0, Util.opcode(OGF_BTSTACK, RFCOMM_REGISTER_SERVICE_WITH_CREDITS));
        int offset = 2;
        Util.storeByte(command, offset, command_len - 3);
        offset++;
        Util.storeByte(command, offset, serverChannel);
        offset += 1;
        Util.storeBt16(command, offset, mtu);
        offset += 2;
        Util.storeByte(command, offset, initialCredits);
        offset += 1;

        Packet packet = new Packet(Packet.HCI_COMMAND_PACKET, 0, command, command.length);
        return sendPacket(packet);
    }

    public boolean RFCOMMUnregisterService(int serviceChannel){
        // 2
        int command_len = 5;
        byte[] command = new byte[command_len];
        Util.storeBt16(command, 0, Util.opcode(OGF_BTSTACK, RFCOMM_UNREGISTER_SERVICE));
        int offset = 2;
        Util.storeByte(command, offset, command_len - 3);
        offset++;
        Util.storeBt16(command, offset, serviceChannel);
        offset += 2;

        Packet packet = new Packet(Packet.HCI_COMMAND_PACKET, 0, command, command.length);
        return sendPacket(packet);
    }

    public boolean RFCOMMAcceptConnection(int sourceCid){
        // 2
        int command_len = 5;
        byte[] command = new byte[command_len];
        Util.storeBt16(command, 0, Util.opcode(OGF_BTSTACK, RFCOMM_ACCEPT_CONNECTION));
        int offset = 2;
        Util.storeByte(command, offset, command_len - 3);
        offset++;
        Util.storeBt16(command, offset, sourceCid);
        offset += 2;

        Packet packet = new Packet(Packet.HCI_COMMAND_PACKET, 0, command, command.length);
        return sendPacket(packet);
    }

    public boolean RFCOMMDeclineConnection(int sourceCid, int reason){
        // 21
        int command_len = 6;
        byte[] command = new byte[command_len];
        Util.storeBt16(command, 0, Util.opcode(OGF_BTSTACK, RFCOMM_DECLINE_CONNECTION));
        int offset = 2;
        Util.storeByte(command, offset, command_len - 3);
        offset++;
        Util.storeBt16(command, offset, sourceCid);
        offset += 2;
        Util.storeByte(command, offset, reason);
        offset += 1;

        Packet packet = new Packet(Packet.HCI_COMMAND_PACKET, 0, command, command.length);
        return sendPacket(packet);
    }

    public boolean RFCOMMPersistentChannelForService(String namedService){
        // N
        int command_len = 251;
        byte[] command = new byte[command_len];
        Util.storeBt16(command, 0, Util.opcode(OGF_BTSTACK, RFCOMM_PERSISTENT_CHANNEL));
        int offset = 2;
        Util.storeByte(command, offset, command_len - 3);
        offset++;
        Util.storeString(command, offset, namedService, 248);
        offset += 248;

        Packet packet = new Packet(Packet.HCI_COMMAND_PACKET, 0, command, command.length);
        return sendPacket(packet);
    }

    public boolean GAPDisconnect(int handle){
        // H
        int command_len = 5;
        byte[] command = new byte[command_len];
        Util.storeBt16(command, 0, Util.opcode(OGF_BTSTACK, GAP_DISCONNECT));
        int offset = 2;
        Util.storeByte(command, offset, command_len - 3);
        offset++;
        Util.storeBt16(command, offset, handle);
        offset += 2;

        Packet packet = new Packet(Packet.HCI_COMMAND_PACKET, 0, command, command.length);
        return sendPacket(packet);
    }

    public boolean GAPInquiryStart(int duration){
        // 1
        int command_len = 4;
        byte[] command = new byte[command_len];
        Util.storeBt16(command, 0, Util.opcode(OGF_BTSTACK, GAP_INQUIRY_START));
        int offset = 2;
        Util.storeByte(command, offset, command_len - 3);
        offset++;
        Util.storeByte(command, offset, duration);
        offset += 1;

        Packet packet = new Packet(Packet.HCI_COMMAND_PACKET, 0, command, command.length);
        return sendPacket(packet);
    }

    public boolean GAPInquiryStop(){
        // 
        int command_len = 3;
        byte[] command = new byte[command_len];
        Util.storeBt16(command, 0, Util.opcode(OGF_BTSTACK, GAP_INQUIRY_STOP));
        int offset = 2;
        Util.storeByte(command, offset, command_len - 3);
        offset++;

        Packet packet = new Packet(Packet.HCI_COMMAND_PACKET, 0, command, command.length);
        return sendPacket(packet);
    }

    public boolean GAPRemoteNameRequest(BD_ADDR addr, int pageScanRepetitionMode, int clockOffset){
        // B12
        int command_len = 12;
        byte[] command = new byte[command_len];
        Util.storeBt16(command, 0, Util.opcode(OGF_BTSTACK, GAP_REMOTE_NAME_REQUEST));
        int offset = 2;
        Util.storeByte(command, offset, command_len - 3);
        offset++;
        Util.storeBytes(command, offset, addr.getBytes());
        offset += 6;
        Util.storeByte(command, offset, pageScanRepetitionMode);
        offset += 1;
        Util.storeBt16(command, offset, clockOffset);
        offset += 2;

        Packet packet = new Packet(Packet.HCI_COMMAND_PACKET, 0, command, command.length);
        return sendPacket(packet);
    }

    public boolean GAPDropLinkKey(BD_ADDR addr){
        // B
        int command_len = 9;
        byte[] command = new byte[command_len];
        Util.storeBt16(command, 0, Util.opcode(OGF_BTSTACK, GAP_DROP_LINK_KEY_FOR_BD_ADDR));
        int offset = 2;
        Util.storeByte(command, offset, command_len - 3);
        offset++;
        Util.storeBytes(command, offset, addr.getBytes());
        offset += 6;

        Packet packet = new Packet(Packet.HCI_COMMAND_PACKET, 0, command, command.length);
        return sendPacket(packet);
    }

    public boolean GAPDeleteAllLinkKeys(){
        // 
        int command_len = 3;
        byte[] command = new byte[command_len];
        Util.storeBt16(command, 0, Util.opcode(OGF_BTSTACK, GAP_DELETE_ALL_LINK_KEYS));
        int offset = 2;
        Util.storeByte(command, offset, command_len - 3);
        offset++;

        Packet packet = new Packet(Packet.HCI_COMMAND_PACKET, 0, command, command.length);
        return sendPacket(packet);
    }

    public boolean GAPPinCodeResponse(BD_ADDR bdAddr, int pinLength, byte [] pin){
        // B1P
        int command_len = 26;
        byte[] command = new byte[command_len];
        Util.storeBt16(command, 0, Util.opcode(OGF_BTSTACK, GAP_PIN_CODE_RESPONSE));
        int offset = 2;
        Util.storeByte(command, offset, command_len - 3);
        offset++;
        Util.storeBytes(command, offset, bdAddr.getBytes());
        offset += 6;
        Util.storeByte(command, offset, pinLength);
        offset += 1;
        Util.storeBytes(command, offset, pin, 16);
        offset += 16;

        Packet packet = new Packet(Packet.HCI_COMMAND_PACKET, 0, command, command.length);
        return sendPacket(packet);
    }

    public boolean GAPPinCodeNegative(BD_ADDR bdAddr){
        // B
        int command_len = 9;
        byte[] command = new byte[command_len];
        Util.storeBt16(command, 0, Util.opcode(OGF_BTSTACK, GAP_PIN_CODE_NEGATIVE));
        int offset = 2;
        Util.storeByte(command, offset, command_len - 3);
        offset++;
        Util.storeBytes(command, offset, bdAddr.getBytes());
        offset += 6;

        Packet packet = new Packet(Packet.HCI_COMMAND_PACKET, 0, command, command.length);
        return sendPacket(packet);
    }

    public boolean GAPLEScanStart(){
        // 
        int command_len = 3;
        byte[] command = new byte[command_len];
        Util.storeBt16(command, 0, Util.opcode(OGF_BTSTACK, GAP_LE_SCAN_START));
        int offset = 2;
        Util.storeByte(command, offset, command_len - 3);
        offset++;

        Packet packet = new Packet(Packet.HCI_COMMAND_PACKET, 0, command, command.length);
        return sendPacket(packet);
    }

    public boolean GAPLEScanStop(){
        // 
        int command_len = 3;
        byte[] command = new byte[command_len];
        Util.storeBt16(command, 0, Util.opcode(OGF_BTSTACK, GAP_LE_SCAN_STOP));
        int offset = 2;
        Util.storeByte(command, offset, command_len - 3);
        offset++;

        Packet packet = new Packet(Packet.HCI_COMMAND_PACKET, 0, command, command.length);
        return sendPacket(packet);
    }

    public boolean GAPLESetScanParameters(int scanType, int scanInterval, int scanWindow){
        // 122
        int command_len = 8;
        byte[] command = new byte[command_len];
        Util.storeBt16(command, 0, Util.opcode(OGF_BTSTACK, GAP_LE_SET_SCAN_PARAMETERS));
        int offset = 2;
        Util.storeByte(command, offset, command_len - 3);
        offset++;
        Util.storeByte(command, offset, scanType);
        offset += 1;
        Util.storeBt16(command, offset, scanInterval);
        offset += 2;
        Util.storeBt16(command, offset, scanWindow);
        offset += 2;

        Packet packet = new Packet(Packet.HCI_COMMAND_PACKET, 0, command, command.length);
        return sendPacket(packet);
    }

    public boolean GAPLEConnect(int peerAddressType, BD_ADDR peerAddress){
        // 1B
        int command_len = 10;
        byte[] command = new byte[command_len];
        Util.storeBt16(command, 0, Util.opcode(OGF_BTSTACK, GAP_LE_CONNECT));
        int offset = 2;
        Util.storeByte(command, offset, command_len - 3);
        offset++;
        Util.storeByte(command, offset, peerAddressType);
        offset += 1;
        Util.storeBytes(command, offset, peerAddress.getBytes());
        offset += 6;

        Packet packet = new Packet(Packet.HCI_COMMAND_PACKET, 0, command, command.length);
        return sendPacket(packet);
    }

    public boolean GAPLEConnectCancel(){
        // 
        int command_len = 3;
        byte[] command = new byte[command_len];
        Util.storeBt16(command, 0, Util.opcode(OGF_BTSTACK, GAP_LE_CONNECT_CANCEL));
        int offset = 2;
        Util.storeByte(command, offset, command_len - 3);
        offset++;

        Packet packet = new Packet(Packet.HCI_COMMAND_PACKET, 0, command, command.length);
        return sendPacket(packet);
    }

    public boolean GATTDiscoverPrimaryServices(int handle){
        // H
        int command_len = 5;
        byte[] command = new byte[command_len];
        Util.storeBt16(command, 0, Util.opcode(OGF_BTSTACK, GATT_DISCOVER_ALL_PRIMARY_SERVICES));
        int offset = 2;
        Util.storeByte(command, offset, command_len - 3);
        offset++;
        Util.storeBt16(command, offset, handle);
        offset += 2;

        Packet packet = new Packet(Packet.HCI_COMMAND_PACKET, 0, command, command.length);
        return sendPacket(packet);
    }

    public boolean GATTDiscoverPrimaryServicesByUUID16(int handle, int uuid16){
        // H2
        int command_len = 7;
        byte[] command = new byte[command_len];
        Util.storeBt16(command, 0, Util.opcode(OGF_BTSTACK, GATT_DISCOVER_PRIMARY_SERVICES_BY_UUID16));
        int offset = 2;
        Util.storeByte(command, offset, command_len - 3);
        offset++;
        Util.storeBt16(command, offset, handle);
        offset += 2;
        Util.storeBt16(command, offset, uuid16);
        offset += 2;

        Packet packet = new Packet(Packet.HCI_COMMAND_PACKET, 0, command, command.length);
        return sendPacket(packet);
    }

    public boolean GATTDiscoverPrimaryServicesByUUID128(int handle, BT_UUID uuid128){
        // HU
        int command_len = 21;
        byte[] command = new byte[command_len];
        Util.storeBt16(command, 0, Util.opcode(OGF_BTSTACK, GATT_DISCOVER_PRIMARY_SERVICES_BY_UUID128));
        int offset = 2;
        Util.storeByte(command, offset, command_len - 3);
        offset++;
        Util.storeBt16(command, offset, handle);
        offset += 2;
        Util.storeBytes(command, offset, uuid128.getBytes());
        offset += 16;

        Packet packet = new Packet(Packet.HCI_COMMAND_PACKET, 0, command, command.length);
        return sendPacket(packet);
    }

    public boolean GATTFindIncludedServicesForService(int handle, GATTService service){
        // HX
        int command_len = 25;
        byte[] command = new byte[command_len];
        Util.storeBt16(command, 0, Util.opcode(OGF_BTSTACK, GATT_FIND_INCLUDED_SERVICES_FOR_SERVICE));
        int offset = 2;
        Util.storeByte(command, offset, command_len - 3);
        offset++;
        Util.storeBt16(command, offset, handle);
        offset += 2;
        Util.storeBytes(command, offset, service.getBytes());
        offset += 20;

        Packet packet = new Packet(Packet.HCI_COMMAND_PACKET, 0, command, command.length);
        return sendPacket(packet);
    }

    public boolean GATTDiscoverCharacteristicsForService(int handle, GATTService service){
        // HX
        int command_len = 25;
        byte[] command = new byte[command_len];
        Util.storeBt16(command, 0, Util.opcode(OGF_BTSTACK, GATT_DISCOVER_CHARACTERISTICS_FOR_SERVICE));
        int offset = 2;
        Util.storeByte(command, offset, command_len - 3);
        offset++;
        Util.storeBt16(command, offset, handle);
        offset += 2;
        Util.storeBytes(command, offset, service.getBytes());
        offset += 20;

        Packet packet = new Packet(Packet.HCI_COMMAND_PACKET, 0, command, command.length);
        return sendPacket(packet);
    }

    public boolean GATTDiscoverCharacteristicsForServiceByUUID128(int handle, GATTService service, BT_UUID uuid128){
        // HXU
        int command_len = 41;
        byte[] command = new byte[command_len];
        Util.storeBt16(command, 0, Util.opcode(OGF_BTSTACK, GATT_DISCOVER_CHARACTERISTICS_FOR_SERVICE_BY_UUID128));
        int offset = 2;
        Util.storeByte(command, offset, command_len - 3);
        offset++;
        Util.storeBt16(command, offset, handle);
        offset += 2;
        Util.storeBytes(command, offset, service.getBytes());
        offset += 20;
        Util.storeBytes(command, offset, uuid128.getBytes());
        offset += 16;

        Packet packet = new Packet(Packet.HCI_COMMAND_PACKET, 0, command, command.length);
        return sendPacket(packet);
    }

    public boolean GATTDiscoverCharacteristicDescriptors(int handle, GATTCharacteristic characteristic){
        // HY
        int command_len = 29;
        byte[] command = new byte[command_len];
        Util.storeBt16(command, 0, Util.opcode(OGF_BTSTACK, GATT_DISCOVER_CHARACTERISTIC_DESCRIPTORS));
        int offset = 2;
        Util.storeByte(command, offset, command_len - 3);
        offset++;
        Util.storeBt16(command, offset, handle);
        offset += 2;
        Util.storeBytes(command, offset, characteristic.getBytes());
        offset += 24;

        Packet packet = new Packet(Packet.HCI_COMMAND_PACKET, 0, command, command.length);
        return sendPacket(packet);
    }

    public boolean GATTReadValueOfCharacteristic(int handle, GATTCharacteristic characteristic){
        // HY
        int command_len = 29;
        byte[] command = new byte[command_len];
        Util.storeBt16(command, 0, Util.opcode(OGF_BTSTACK, GATT_READ_VALUE_OF_CHARACTERISTIC));
        int offset = 2;
        Util.storeByte(command, offset, command_len - 3);
        offset++;
        Util.storeBt16(command, offset, handle);
        offset += 2;
        Util.storeBytes(command, offset, characteristic.getBytes());
        offset += 24;

        Packet packet = new Packet(Packet.HCI_COMMAND_PACKET, 0, command, command.length);
        return sendPacket(packet);
    }

    public boolean GATTReadLongValueOfCharacteristic(int handle, GATTCharacteristic characteristic){
        // HY
        int command_len = 29;
        byte[] command = new byte[command_len];
        Util.storeBt16(command, 0, Util.opcode(OGF_BTSTACK, GATT_READ_LONG_VALUE_OF_CHARACTERISTIC));
        int offset = 2;
        Util.storeByte(command, offset, command_len - 3);
        offset++;
        Util.storeBt16(command, offset, handle);
        offset += 2;
        Util.storeBytes(command, offset, characteristic.getBytes());
        offset += 24;

        Packet packet = new Packet(Packet.HCI_COMMAND_PACKET, 0, command, command.length);
        return sendPacket(packet);
    }

    public boolean GATTWriteValueOfCharacteristicWithoutResponse(int handle, GATTCharacteristic characteristic, int dataLength, byte [] data){
        // HYLV
        int command_len = 31 + data.length;
        byte[] command = new byte[command_len];
        Util.storeBt16(command, 0, Util.opcode(OGF_BTSTACK, GATT_WRITE_VALUE_OF_CHARACTERISTIC_WITHOUT_RESPONSE));
        int offset = 2;
        Util.storeByte(command, offset, command_len - 3);
        offset++;
        Util.storeBt16(command, offset, handle);
        offset += 2;
        Util.storeBytes(command, offset, characteristic.getBytes());
        offset += 24;
        Util.storeBt16(command, offset, dataLength);
        offset += 2;
        Util.storeBytes(command, offset, data, dataLength);
        offset += dataLength;

        Packet packet = new Packet(Packet.HCI_COMMAND_PACKET, 0, command, command.length);
        return sendPacket(packet);
    }

    public boolean GATTWriteValueOfCharacteristic(int handle, GATTCharacteristic characteristic, int dataLength, byte [] data){
        // HYLV
        int command_len = 31 + data.length;
        byte[] command = new byte[command_len];
        Util.storeBt16(command, 0, Util.opcode(OGF_BTSTACK, GATT_WRITE_VALUE_OF_CHARACTERISTIC));
        int offset = 2;
        Util.storeByte(command, offset, command_len - 3);
        offset++;
        Util.storeBt16(command, offset, handle);
        offset += 2;
        Util.storeBytes(command, offset, characteristic.getBytes());
        offset += 24;
        Util.storeBt16(command, offset, dataLength);
        offset += 2;
        Util.storeBytes(command, offset, data, dataLength);
        offset += dataLength;

        Packet packet = new Packet(Packet.HCI_COMMAND_PACKET, 0, command, command.length);
        return sendPacket(packet);
    }

    public boolean GATTWriteLongValueOfCharacteristic(int handle, GATTCharacteristic characteristic, int dataLength, byte [] data){
        // HYLV
        int command_len = 31 + data.length;
        byte[] command = new byte[command_len];
        Util.storeBt16(command, 0, Util.opcode(OGF_BTSTACK, GATT_WRITE_LONG_VALUE_OF_CHARACTERISTIC));
        int offset = 2;
        Util.storeByte(command, offset, command_len - 3);
        offset++;
        Util.storeBt16(command, offset, handle);
        offset += 2;
        Util.storeBytes(command, offset, characteristic.getBytes());
        offset += 24;
        Util.storeBt16(command, offset, dataLength);
        offset += 2;
        Util.storeBytes(command, offset, data, dataLength);
        offset += dataLength;

        Packet packet = new Packet(Packet.HCI_COMMAND_PACKET, 0, command, command.length);
        return sendPacket(packet);
    }

    public boolean GATTReliableWriteLongValueOfCharacteristic(int handle, GATTCharacteristic characteristic, int dataLength, byte [] data){
        // HYLV
        int command_len = 31 + data.length;
        byte[] command = new byte[command_len];
        Util.storeBt16(command, 0, Util.opcode(OGF_BTSTACK, GATT_RELIABLE_WRITE_LONG_VALUE_OF_CHARACTERISTIC));
        int offset = 2;
        Util.storeByte(command, offset, command_len - 3);
        offset++;
        Util.storeBt16(command, offset, handle);
        offset += 2;
        Util.storeBytes(command, offset, characteristic.getBytes());
        offset += 24;
        Util.storeBt16(command, offset, dataLength);
        offset += 2;
        Util.storeBytes(command, offset, data, dataLength);
        offset += dataLength;

        Packet packet = new Packet(Packet.HCI_COMMAND_PACKET, 0, command, command.length);
        return sendPacket(packet);
    }

    public boolean GATTReadCharacteristicDescriptor(int handle, GATTCharacteristicDescriptor descriptor){
        // HZ
        int command_len = 23;
        byte[] command = new byte[command_len];
        Util.storeBt16(command, 0, Util.opcode(OGF_BTSTACK, GATT_READ_CHARACTERISTIC_DESCRIPTOR));
        int offset = 2;
        Util.storeByte(command, offset, command_len - 3);
        offset++;
        Util.storeBt16(command, offset, handle);
        offset += 2;
        Util.storeBytes(command, offset, descriptor.getBytes());
        offset += 18;

        Packet packet = new Packet(Packet.HCI_COMMAND_PACKET, 0, command, command.length);
        return sendPacket(packet);
    }

    public boolean GATTReadLongCharacteristicDescriptor(int handle, GATTCharacteristicDescriptor descriptor){
        // HZ
        int command_len = 23;
        byte[] command = new byte[command_len];
        Util.storeBt16(command, 0, Util.opcode(OGF_BTSTACK, GATT_READ_LONG_CHARACTERISTIC_DESCRIPTOR));
        int offset = 2;
        Util.storeByte(command, offset, command_len - 3);
        offset++;
        Util.storeBt16(command, offset, handle);
        offset += 2;
        Util.storeBytes(command, offset, descriptor.getBytes());
        offset += 18;

        Packet packet = new Packet(Packet.HCI_COMMAND_PACKET, 0, command, command.length);
        return sendPacket(packet);
    }

    public boolean GATTWriteCharacteristicDescriptor(int handle, GATTCharacteristicDescriptor descriptor, int dataLength, byte [] data){
        // HZLV
        int command_len = 25 + data.length;
        byte[] command = new byte[command_len];
        Util.storeBt16(command, 0, Util.opcode(OGF_BTSTACK, GATT_WRITE_CHARACTERISTIC_DESCRIPTOR));
        int offset = 2;
        Util.storeByte(command, offset, command_len - 3);
        offset++;
        Util.storeBt16(command, offset, handle);
        offset += 2;
        Util.storeBytes(command, offset, descriptor.getBytes());
        offset += 18;
        Util.storeBt16(command, offset, dataLength);
        offset += 2;
        Util.storeBytes(command, offset, data, dataLength);
        offset += dataLength;

        Packet packet = new Packet(Packet.HCI_COMMAND_PACKET, 0, command, command.length);
        return sendPacket(packet);
    }

    public boolean GATTWriteLongCharacteristicDescriptor(int handle, GATTCharacteristicDescriptor descriptor, int dataLength, byte [] data){
        // HZLV
        int command_len = 25 + data.length;
        byte[] command = new byte[command_len];
        Util.storeBt16(command, 0, Util.opcode(OGF_BTSTACK, GATT_WRITE_LONG_CHARACTERISTIC_DESCRIPTOR));
        int offset = 2;
        Util.storeByte(command, offset, command_len - 3);
        offset++;
        Util.storeBt16(command, offset, handle);
        offset += 2;
        Util.storeBytes(command, offset, descriptor.getBytes());
        offset += 18;
        Util.storeBt16(command, offset, dataLength);
        offset += 2;
        Util.storeBytes(command, offset, data, dataLength);
        offset += dataLength;

        Packet packet = new Packet(Packet.HCI_COMMAND_PACKET, 0, command, command.length);
        return sendPacket(packet);
    }

    public boolean GATTWriteClientCharacteristicConfiguration(int handle, GATTCharacteristic characteristic, int configuration){
        // HY2
        int command_len = 31;
        byte[] command = new byte[command_len];
        Util.storeBt16(command, 0, Util.opcode(OGF_BTSTACK, GATT_WRITE_CLIENT_CHARACTERISTIC_CONFIGURATION));
        int offset = 2;
        Util.storeByte(command, offset, command_len - 3);
        offset++;
        Util.storeBt16(command, offset, handle);
        offset += 2;
        Util.storeBytes(command, offset, characteristic.getBytes());
        offset += 24;
        Util.storeBt16(command, offset, configuration);
        offset += 2;

        Packet packet = new Packet(Packet.HCI_COMMAND_PACKET, 0, command, command.length);
        return sendPacket(packet);
    }

    public boolean GATTGetMtu(int handle){
        // H
        int command_len = 5;
        byte[] command = new byte[command_len];
        Util.storeBt16(command, 0, Util.opcode(OGF_BTSTACK, GATT_GET_MTU));
        int offset = 2;
        Util.storeByte(command, offset, command_len - 3);
        offset++;
        Util.storeBt16(command, offset, handle);
        offset += 2;

        Packet packet = new Packet(Packet.HCI_COMMAND_PACKET, 0, command, command.length);
        return sendPacket(packet);
    }

    public boolean SMSetAuthenticationRequirements(int authReq){
        // 1
        int command_len = 4;
        byte[] command = new byte[command_len];
        Util.storeBt16(command, 0, Util.opcode(OGF_BTSTACK, SM_SET_AUTHENTICATION_REQUIREMENTS));
        int offset = 2;
        Util.storeByte(command, offset, command_len - 3);
        offset++;
        Util.storeByte(command, offset, authReq);
        offset += 1;

        Packet packet = new Packet(Packet.HCI_COMMAND_PACKET, 0, command, command.length);
        return sendPacket(packet);
    }

    public boolean SMSetIoCapabilities(int ioCapabilities){
        // 1
        int command_len = 4;
        byte[] command = new byte[command_len];
        Util.storeBt16(command, 0, Util.opcode(OGF_BTSTACK, SM_SET_IO_CAPABILITIES));
        int offset = 2;
        Util.storeByte(command, offset, command_len - 3);
        offset++;
        Util.storeByte(command, offset, ioCapabilities);
        offset += 1;

        Packet packet = new Packet(Packet.HCI_COMMAND_PACKET, 0, command, command.length);
        return sendPacket(packet);
    }

    public boolean SMBondingDecline(int conHandle){
        // H
        int command_len = 5;
        byte[] command = new byte[command_len];
        Util.storeBt16(command, 0, Util.opcode(OGF_BTSTACK, SM_BONDING_DECLINE));
        int offset = 2;
        Util.storeByte(command, offset, command_len - 3);
        offset++;
        Util.storeBt16(command, offset, conHandle);
        offset += 2;

        Packet packet = new Packet(Packet.HCI_COMMAND_PACKET, 0, command, command.length);
        return sendPacket(packet);
    }

    public boolean SMJustWorksConfirm(int conHandle){
        // H
        int command_len = 5;
        byte[] command = new byte[command_len];
        Util.storeBt16(command, 0, Util.opcode(OGF_BTSTACK, SM_JUST_WORKS_CONFIRM));
        int offset = 2;
        Util.storeByte(command, offset, command_len - 3);
        offset++;
        Util.storeBt16(command, offset, conHandle);
        offset += 2;

        Packet packet = new Packet(Packet.HCI_COMMAND_PACKET, 0, command, command.length);
        return sendPacket(packet);
    }

    public boolean SMNumericComparisonConfirm(int conHandle){
        // H
        int command_len = 5;
        byte[] command = new byte[command_len];
        Util.storeBt16(command, 0, Util.opcode(OGF_BTSTACK, SM_NUMERIC_COMPARISON_CONFIRM));
        int offset = 2;
        Util.storeByte(command, offset, command_len - 3);
        offset++;
        Util.storeBt16(command, offset, conHandle);
        offset += 2;

        Packet packet = new Packet(Packet.HCI_COMMAND_PACKET, 0, command, command.length);
        return sendPacket(packet);
    }

    public boolean SMPasskeyInput(int conHandle, long passkey){
        // H4
        int command_len = 9;
        byte[] command = new byte[command_len];
        Util.storeBt16(command, 0, Util.opcode(OGF_BTSTACK, SM_PASSKEY_INPUT));
        int offset = 2;
        Util.storeByte(command, offset, command_len - 3);
        offset++;
        Util.storeBt16(command, offset, conHandle);
        offset += 2;
        Util.storeBt32(command, offset, passkey);
        offset += 4;

        Packet packet = new Packet(Packet.HCI_COMMAND_PACKET, 0, command, command.length);
        return sendPacket(packet);
    }

    /** defines used */

    public static final int BTSTACK_GET_STATE = 0x01;
    public static final int BTSTACK_GET_SYSTEM_BLUETOOTH_ENABLED = 0x05;
    public static final int BTSTACK_GET_VERSION = 0x04;
    public static final int BTSTACK_SET_ACL_CAPTURE_MODE = 0x03;
    public static final int BTSTACK_SET_BLUETOOTH_ENABLED = 0x08;
    public static final int BTSTACK_SET_DISCOVERABLE = 0x07;
    public static final int BTSTACK_SET_POWER_MODE = 0x02;
    public static final int BTSTACK_SET_SYSTEM_BLUETOOTH_ENABLED = 0x06;
    public static final int GAP_DELETE_ALL_LINK_KEYS = 0x55;
    public static final int GAP_DISCONNECT = 0x50;
    public static final int GAP_DROP_LINK_KEY_FOR_BD_ADDR = 0x54;
    public static final int GAP_INQUIRY_START = 0x51;
    public static final int GAP_INQUIRY_STOP = 0x52;
    public static final int GAP_LE_CONNECT = 0x62;
    public static final int GAP_LE_CONNECT_CANCEL = 0x63;
    public static final int GAP_LE_SCAN_START = 0x60;
    public static final int GAP_LE_SCAN_STOP = 0x61;
    public static final int GAP_LE_SET_SCAN_PARAMETERS = 0x64;
    public static final int GAP_PIN_CODE_NEGATIVE = 0x57;
    public static final int GAP_PIN_CODE_RESPONSE = 0x56;
    public static final int GAP_REMOTE_NAME_REQUEST = 0x53;
    public static final int GATT_DISCOVER_ALL_PRIMARY_SERVICES = 0x70;
    public static final int GATT_DISCOVER_CHARACTERISTICS_FOR_SERVICE = 0x74;
    public static final int GATT_DISCOVER_CHARACTERISTICS_FOR_SERVICE_BY_UUID128 = 0x75;
    public static final int GATT_DISCOVER_CHARACTERISTIC_DESCRIPTORS = 0x76;
    public static final int GATT_DISCOVER_PRIMARY_SERVICES_BY_UUID128 = 0x72;
    public static final int GATT_DISCOVER_PRIMARY_SERVICES_BY_UUID16 = 0x71;
    public static final int GATT_FIND_INCLUDED_SERVICES_FOR_SERVICE = 0x73;
    public static final int GATT_GET_MTU = 0x82;
    public static final int GATT_READ_CHARACTERISTIC_DESCRIPTOR = 0X7D;
    public static final int GATT_READ_LONG_CHARACTERISTIC_DESCRIPTOR = 0X7E;
    public static final int GATT_READ_LONG_VALUE_OF_CHARACTERISTIC = 0x78;
    public static final int GATT_READ_VALUE_OF_CHARACTERISTIC = 0x77;
    public static final int GATT_RELIABLE_WRITE_LONG_VALUE_OF_CHARACTERISTIC = 0x7C;
    public static final int GATT_WRITE_CHARACTERISTIC_DESCRIPTOR = 0X7F;
    public static final int GATT_WRITE_CLIENT_CHARACTERISTIC_CONFIGURATION = 0X81;
    public static final int GATT_WRITE_LONG_CHARACTERISTIC_DESCRIPTOR = 0X80;
    public static final int GATT_WRITE_LONG_VALUE_OF_CHARACTERISTIC = 0x7B;
    public static final int GATT_WRITE_VALUE_OF_CHARACTERISTIC = 0x7A;
    public static final int GATT_WRITE_VALUE_OF_CHARACTERISTIC_WITHOUT_RESPONSE = 0x79;
    public static final int L2CAP_ACCEPT_CONNECTION = 0x24;
    public static final int L2CAP_CREATE_CHANNEL = 0x20;
    public static final int L2CAP_CREATE_CHANNEL_MTU = 0x26;
    public static final int L2CAP_DECLINE_CONNECTION = 0x25;
    public static final int L2CAP_DISCONNECT = 0x21;
    public static final int L2CAP_REGISTER_SERVICE = 0x22;
    public static final int L2CAP_UNREGISTER_SERVICE = 0x23;
    public static final int OGF_BTSTACK = 0x3d;
    public static final int RFCOMM_ACCEPT_CONNECTION = 0x44;
    public static final int RFCOMM_CREATE_CHANNEL = 0x40;
    public static final int RFCOMM_CREATE_CHANNEL_WITH_CREDITS = 0x47;
    public static final int RFCOMM_DECLINE_CONNECTION = 0x45;
    public static final int RFCOMM_DISCONNECT = 0x41;
    public static final int RFCOMM_GRANT_CREDITS = 0x49;
    public static final int RFCOMM_PERSISTENT_CHANNEL = 0x46;
    public static final int RFCOMM_REGISTER_SERVICE = 0x42;
    public static final int RFCOMM_REGISTER_SERVICE_WITH_CREDITS = 0x48;
    public static final int RFCOMM_UNREGISTER_SERVICE = 0x43;
    public static final int SDP_CLIENT_QUERY_RFCOMM_SERVICES = 0x32;
    public static final int SDP_CLIENT_QUERY_SERVICES = 0x33;
    public static final int SDP_REGISTER_SERVICE_RECORD = 0x30;
    public static final int SDP_UNREGISTER_SERVICE_RECORD = 0x31;
    public static final int SM_BONDING_DECLINE = 0x93;
    public static final int SM_JUST_WORKS_CONFIRM = 0x94;
    public static final int SM_NUMERIC_COMPARISON_CONFIRM = 0x95;
    public static final int SM_PASSKEY_INPUT = 0x96;
    public static final int SM_SET_AUTHENTICATION_REQUIREMENTS = 0x90;
    public static final int SM_SET_IO_CAPABILITIES = 0x92;

}
