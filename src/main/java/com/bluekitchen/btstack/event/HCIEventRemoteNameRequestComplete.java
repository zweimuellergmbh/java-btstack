package com.bluekitchen.btstack.event;

import com.bluekitchen.btstack.*;

public class HCIEventRemoteNameRequestComplete extends Event {

    public HCIEventRemoteNameRequestComplete(Packet packet) {
        super(packet);
    }
    
    /**
     * @return Status as int
     */
    public int getStatus(){
        return Util.readByte(data, 2);
    }

    /**
     * @return BdAddr as BD_ADDR
     */
    public BD_ADDR getBdAddr(){
        return Util.readBdAddr(data, 3);
    }

    /**
     * @return RemoteName as String
     */
    public String getRemoteName(){
        return Util.getText(data, 9, 248);
    }

    
    public String toString(){
        StringBuffer t = new StringBuffer();
        t.append("HCIEventRemoteNameRequestComplete < type = ");
        t.append(String.format("0x%02x, ", getEventType()));
        t.append(getEventType());
        t.append(", status = ");
        t.append(getStatus());
        t.append(", bd_addr = ");
        t.append(getBdAddr());
        t.append(", remote_name = ");
        t.append(getRemoteName());
        t.append(" >");
        return t.toString();
    }

}
