package com.bluekitchen.btstack.event;

import com.bluekitchen.btstack.*;

public class SDPEventQueryAttributeByte extends Event {

    public SDPEventQueryAttributeByte(Packet packet) {
        super(packet);
    }
    
    /**
     * @return RecordId as int
     */
    public int getRecordId(){
        return Util.readBt16(data, 2);
    }

    /**
     * @return AttributeId as int
     */
    public int getAttributeId(){
        return Util.readBt16(data, 4);
    }

    /**
     * @return AttributeLength as int
     */
    public int getAttributeLength(){
        return Util.readBt16(data, 6);
    }

    /**
     * @return DataOffset as int
     */
    public int getDataOffset(){
        return Util.readBt16(data, 8);
    }

    /**
     * @return Data as int
     */
    public int getData(){
        return Util.readByte(data, 10);
    }

    
    public String toString(){
        StringBuffer t = new StringBuffer();
        t.append("SDPEventQueryAttributeByte < type = ");
        t.append(String.format("0x%02x, ", getEventType()));
        t.append(getEventType());
        t.append(", record_id = ");
        t.append(getRecordId());
        t.append(", attribute_id = ");
        t.append(getAttributeId());
        t.append(", attribute_length = ");
        t.append(getAttributeLength());
        t.append(", data_offset = ");
        t.append(getDataOffset());
        t.append(", data = ");
        t.append(getData());
        t.append(" >");
        return t.toString();
    }

}
