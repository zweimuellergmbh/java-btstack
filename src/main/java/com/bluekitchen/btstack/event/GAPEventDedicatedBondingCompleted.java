package com.bluekitchen.btstack.event;

import com.bluekitchen.btstack.*;

public class GAPEventDedicatedBondingCompleted extends Event {

    public GAPEventDedicatedBondingCompleted(Packet packet) {
        super(packet);
    }
    
    /**
     * @return Status as int
     */
    public int getStatus(){
        return Util.readByte(data, 2);
    }

    /**
     * @return Address as BD_ADDR
     */
    public BD_ADDR getAddress(){
        return Util.readBdAddr(data, 3);
    }

    
    public String toString(){
        StringBuffer t = new StringBuffer();
        t.append("GAPEventDedicatedBondingCompleted < type = ");
        t.append(String.format("0x%02x, ", getEventType()));
        t.append(getEventType());
        t.append(", status = ");
        t.append(getStatus());
        t.append(", address = ");
        t.append(getAddress());
        t.append(" >");
        return t.toString();
    }

}
