package com.bluekitchen.btstack.event;

import com.bluekitchen.btstack.*;

public class BTstackEventPoweronFailed extends Event {

    public BTstackEventPoweronFailed(Packet packet) {
        super(packet);
    }
    
    
    public String toString(){
        StringBuffer t = new StringBuffer();
        t.append("BTstackEventPoweronFailed < type = ");
        t.append(String.format("0x%02x, ", getEventType()));
        t.append(getEventType());
        t.append(" >");
        return t.toString();
    }

}
