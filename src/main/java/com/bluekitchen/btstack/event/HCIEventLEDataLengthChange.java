package com.bluekitchen.btstack.event;

import com.bluekitchen.btstack.*;

public class HCIEventLEDataLengthChange extends Event {

    public HCIEventLEDataLengthChange(Packet packet) {
        super(packet);
    }
    
    /**
     * @return SubeventCode as int
     */
    public int getSubeventCode(){
        return Util.readByte(data, 2);
    }

    /**
     * @return ConnectionHandle as int
     */
    public int getConnectionHandle(){
        return Util.readBt16(data, 3);
    }

    /**
     * @return MaxTxOctets as int
     */
    public int getMaxTxOctets(){
        return Util.readBt16(data, 5);
    }

    /**
     * @return MaxTxTime as int
     */
    public int getMaxTxTime(){
        return Util.readBt16(data, 7);
    }

    /**
     * @return MaxRxOctets as int
     */
    public int getMaxRxOctets(){
        return Util.readBt16(data, 9);
    }

    /**
     * @return MaxRxTime as int
     */
    public int getMaxRxTime(){
        return Util.readBt16(data, 11);
    }

    
    public String toString(){
        StringBuffer t = new StringBuffer();
        t.append("HCIEventLEDataLengthChange < type = ");
        t.append(String.format("0x%02x, ", getEventType()));
        t.append(getEventType());
        t.append(", subevent_code = ");
        t.append(getSubeventCode());
        t.append(", connection_handle = ");
        t.append(getConnectionHandle());
        t.append(", max_tx_octets = ");
        t.append(getMaxTxOctets());
        t.append(", max_tx_time = ");
        t.append(getMaxTxTime());
        t.append(", max_rx_octets = ");
        t.append(getMaxRxOctets());
        t.append(", max_rx_time = ");
        t.append(getMaxRxTime());
        t.append(" >");
        return t.toString();
    }

}
