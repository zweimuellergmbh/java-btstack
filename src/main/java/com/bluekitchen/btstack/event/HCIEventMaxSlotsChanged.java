package com.bluekitchen.btstack.event;

import com.bluekitchen.btstack.*;

public class HCIEventMaxSlotsChanged extends Event {

    public HCIEventMaxSlotsChanged(Packet packet) {
        super(packet);
    }
    
    /**
     * @return Handle as int
     */
    public int getHandle(){
        return Util.readBt16(data, 2);
    }

    /**
     * @return LmpMaxSlots as int
     */
    public int getLmpMaxSlots(){
        return Util.readByte(data, 4);
    }

    
    public String toString(){
        StringBuffer t = new StringBuffer();
        t.append("HCIEventMaxSlotsChanged < type = ");
        t.append(String.format("0x%02x, ", getEventType()));
        t.append(getEventType());
        t.append(", handle = ");
        t.append(getHandle());
        t.append(", lmp_max_slots = ");
        t.append(getLmpMaxSlots());
        t.append(" >");
        return t.toString();
    }

}
