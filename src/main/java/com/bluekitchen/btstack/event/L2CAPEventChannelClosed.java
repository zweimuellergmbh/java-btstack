package com.bluekitchen.btstack.event;

import com.bluekitchen.btstack.*;

public class L2CAPEventChannelClosed extends Event {

    public L2CAPEventChannelClosed(Packet packet) {
        super(packet);
    }
    
    /**
     * @return LocalCid as int
     */
    public int getLocalCid(){
        return Util.readBt16(data, 2);
    }

    
    public String toString(){
        StringBuffer t = new StringBuffer();
        t.append("L2CAPEventChannelClosed < type = ");
        t.append(String.format("0x%02x, ", getEventType()));
        t.append(getEventType());
        t.append(", local_cid = ");
        t.append(getLocalCid());
        t.append(" >");
        return t.toString();
    }

}
