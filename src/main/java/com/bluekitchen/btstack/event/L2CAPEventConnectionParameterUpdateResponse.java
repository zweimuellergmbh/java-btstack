package com.bluekitchen.btstack.event;

import com.bluekitchen.btstack.*;

public class L2CAPEventConnectionParameterUpdateResponse extends Event {

    public L2CAPEventConnectionParameterUpdateResponse(Packet packet) {
        super(packet);
    }
    
    /**
     * @return Handle as int
     */
    public int getHandle(){
        return Util.readBt16(data, 2);
    }

    /**
     * @return Result as int
     */
    public int getResult(){
        return Util.readBt16(data, 4);
    }

    
    public String toString(){
        StringBuffer t = new StringBuffer();
        t.append("L2CAPEventConnectionParameterUpdateResponse < type = ");
        t.append(String.format("0x%02x, ", getEventType()));
        t.append(getEventType());
        t.append(", handle = ");
        t.append(getHandle());
        t.append(", result = ");
        t.append(getResult());
        t.append(" >");
        return t.toString();
    }

}
