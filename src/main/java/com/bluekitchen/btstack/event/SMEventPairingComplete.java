package com.bluekitchen.btstack.event;

import com.bluekitchen.btstack.*;

public class SMEventPairingComplete extends Event {

    public SMEventPairingComplete(Packet packet) {
        super(packet);
    }
    
    /**
     * @return Handle as int
     */
    public int getHandle(){
        return Util.readBt16(data, 2);
    }

    /**
     * @return AddrType as int
     */
    public int getAddrType(){
        return Util.readByte(data, 4);
    }

    /**
     * @return Address as BD_ADDR
     */
    public BD_ADDR getAddress(){
        return Util.readBdAddr(data, 5);
    }

    /**
     * @return Status as int
     */
    public int getStatus(){
        return Util.readByte(data, 11);
    }

    /**
     * @return Reason as int
     */
    public int getReason(){
        return Util.readByte(data, 12);
    }

    
    public String toString(){
        StringBuffer t = new StringBuffer();
        t.append("SMEventPairingComplete < type = ");
        t.append(String.format("0x%02x, ", getEventType()));
        t.append(getEventType());
        t.append(", handle = ");
        t.append(getHandle());
        t.append(", addr_type = ");
        t.append(getAddrType());
        t.append(", address = ");
        t.append(getAddress());
        t.append(", status = ");
        t.append(getStatus());
        t.append(", reason = ");
        t.append(getReason());
        t.append(" >");
        return t.toString();
    }

}
