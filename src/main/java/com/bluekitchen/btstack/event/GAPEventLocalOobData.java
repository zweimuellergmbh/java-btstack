package com.bluekitchen.btstack.event;

import com.bluekitchen.btstack.*;

public class GAPEventLocalOobData extends Event {

    public GAPEventLocalOobData(Packet packet) {
        super(packet);
    }
    
    /**
     * @return OobDataPresent as int
     */
    public int getOobDataPresent(){
        return Util.readByte(data, 2);
    }

    /**
     * @return C192 as byte []
     */
    public byte [] getC192(){
        int len = 16;
        byte[] result = new byte[len];
        System.arraycopy(data, 3, result, 0, len);
        return result;
    }

    /**
     * @return R192 as byte []
     */
    public byte [] getR192(){
        int len = 16;
        byte[] result = new byte[len];
        System.arraycopy(data, 19, result, 0, len);
        return result;
    }

    /**
     * @return C256 as byte []
     */
    public byte [] getC256(){
        int len = 16;
        byte[] result = new byte[len];
        System.arraycopy(data, 35, result, 0, len);
        return result;
    }

    /**
     * @return R256 as byte []
     */
    public byte [] getR256(){
        int len = 16;
        byte[] result = new byte[len];
        System.arraycopy(data, 51, result, 0, len);
        return result;
    }

    
    public String toString(){
        StringBuffer t = new StringBuffer();
        t.append("GAPEventLocalOobData < type = ");
        t.append(String.format("0x%02x, ", getEventType()));
        t.append(getEventType());
        t.append(", oob_data_present = ");
        t.append(getOobDataPresent());
        t.append(", c_192 = ");
        t.append(getC192());
        t.append(", r_192 = ");
        t.append(getR192());
        t.append(", c_256 = ");
        t.append(getC256());
        t.append(", r_256 = ");
        t.append(getR256());
        t.append(" >");
        return t.toString();
    }

}
