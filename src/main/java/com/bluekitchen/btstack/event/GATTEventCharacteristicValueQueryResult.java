package com.bluekitchen.btstack.event;

import com.bluekitchen.btstack.*;

public class GATTEventCharacteristicValueQueryResult extends Event {

    public GATTEventCharacteristicValueQueryResult(Packet packet) {
        super(packet);
    }
    
    /**
     * @return Handle as int
     */
    public int getHandle(){
        return Util.readBt16(data, 2);
    }

    /**
     * @return ValueHandle as int
     */
    public int getValueHandle(){
        return Util.readBt16(data, 4);
    }

    /**
     * @return ValueLength as int
     */
    public int getValueLength(){
        return Util.readBt16(data, 6);
    }

    /**
     * @return Value as byte []
     */
    public byte [] getValue(){
        int len = getValueLength();
        byte[] result = new byte[len];
        System.arraycopy(data, 8, result, 0, len);
        return result;
    }

    
    public String toString(){
        StringBuffer t = new StringBuffer();
        t.append("GATTEventCharacteristicValueQueryResult < type = ");
        t.append(String.format("0x%02x, ", getEventType()));
        t.append(getEventType());
        t.append(", handle = ");
        t.append(getHandle());
        t.append(", value_handle = ");
        t.append(getValueHandle());
        t.append(", value_length = ");
        t.append(getValueLength());
        t.append(", value = ");
        t.append(getValue());
        t.append(" >");
        return t.toString();
    }

}
