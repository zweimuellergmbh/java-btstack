package com.bluekitchen.btstack.event;

import com.bluekitchen.btstack.*;

public class HCIEventSynchronousConnectionComplete extends Event {

    public HCIEventSynchronousConnectionComplete(Packet packet) {
        super(packet);
    }
    
    /**
     * @return Status as int
     */
    public int getStatus(){
        return Util.readByte(data, 2);
    }

    /**
     * @return Handle as int
     */
    public int getHandle(){
        return Util.readBt16(data, 3);
    }

    /**
     * @return BdAddr as BD_ADDR
     */
    public BD_ADDR getBdAddr(){
        return Util.readBdAddr(data, 5);
    }

    /**
     * @return LinkType as int
     */
    public int getLinkType(){
        return Util.readByte(data, 11);
    }

    /**
     * @return TransmissionInterval as int
     */
    public int getTransmissionInterval(){
        return Util.readByte(data, 12);
    }

    /**
     * @return RetransmissionInterval as int
     */
    public int getRetransmissionInterval(){
        return Util.readByte(data, 13);
    }

    /**
     * @return RxPacketLength as int
     */
    public int getRxPacketLength(){
        return Util.readBt16(data, 14);
    }

    /**
     * @return TxPacketLength as int
     */
    public int getTxPacketLength(){
        return Util.readBt16(data, 16);
    }

    /**
     * @return AirMode as int
     */
    public int getAirMode(){
        return Util.readByte(data, 18);
    }

    
    public String toString(){
        StringBuffer t = new StringBuffer();
        t.append("HCIEventSynchronousConnectionComplete < type = ");
        t.append(String.format("0x%02x, ", getEventType()));
        t.append(getEventType());
        t.append(", status = ");
        t.append(getStatus());
        t.append(", handle = ");
        t.append(getHandle());
        t.append(", bd_addr = ");
        t.append(getBdAddr());
        t.append(", link_type = ");
        t.append(getLinkType());
        t.append(", transmission_interval = ");
        t.append(getTransmissionInterval());
        t.append(", retransmission_interval = ");
        t.append(getRetransmissionInterval());
        t.append(", rx_packet_length = ");
        t.append(getRxPacketLength());
        t.append(", tx_packet_length = ");
        t.append(getTxPacketLength());
        t.append(", air_mode = ");
        t.append(getAirMode());
        t.append(" >");
        return t.toString();
    }

}
