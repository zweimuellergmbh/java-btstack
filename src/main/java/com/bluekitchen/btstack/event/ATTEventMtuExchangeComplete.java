package com.bluekitchen.btstack.event;

import com.bluekitchen.btstack.*;

public class ATTEventMtuExchangeComplete extends Event {

    public ATTEventMtuExchangeComplete(Packet packet) {
        super(packet);
    }
    
    /**
     * @return Handle as int
     */
    public int getHandle(){
        return Util.readBt16(data, 2);
    }

    /**
     * @return Mtu as int
     */
    public int getMtu(){
        return Util.readBt16(data, 4);
    }

    
    public String toString(){
        StringBuffer t = new StringBuffer();
        t.append("ATTEventMtuExchangeComplete < type = ");
        t.append(String.format("0x%02x, ", getEventType()));
        t.append(getEventType());
        t.append(", handle = ");
        t.append(getHandle());
        t.append(", MTU = ");
        t.append(getMtu());
        t.append(" >");
        return t.toString();
    }

}
