package com.bluekitchen.btstack.event;

import com.bluekitchen.btstack.*;

public class HCIEventLEConnectionUpdateComplete extends Event {

    public HCIEventLEConnectionUpdateComplete(Packet packet) {
        super(packet);
    }
    
    /**
     * @return SubeventCode as int
     */
    public int getSubeventCode(){
        return Util.readByte(data, 2);
    }

    /**
     * @return Status as int
     */
    public int getStatus(){
        return Util.readByte(data, 3);
    }

    /**
     * @return ConnectionHandle as int
     */
    public int getConnectionHandle(){
        return Util.readBt16(data, 4);
    }

    /**
     * @return ConnInterval as int
     */
    public int getConnInterval(){
        return Util.readBt16(data, 6);
    }

    /**
     * @return ConnLatency as int
     */
    public int getConnLatency(){
        return Util.readBt16(data, 8);
    }

    /**
     * @return SupervisionTimeout as int
     */
    public int getSupervisionTimeout(){
        return Util.readBt16(data, 10);
    }

    
    public String toString(){
        StringBuffer t = new StringBuffer();
        t.append("HCIEventLEConnectionUpdateComplete < type = ");
        t.append(String.format("0x%02x, ", getEventType()));
        t.append(getEventType());
        t.append(", subevent_code = ");
        t.append(getSubeventCode());
        t.append(", status = ");
        t.append(getStatus());
        t.append(", connection_handle = ");
        t.append(getConnectionHandle());
        t.append(", conn_interval = ");
        t.append(getConnInterval());
        t.append(", conn_latency = ");
        t.append(getConnLatency());
        t.append(", supervision_timeout = ");
        t.append(getSupervisionTimeout());
        t.append(" >");
        return t.toString();
    }

}
