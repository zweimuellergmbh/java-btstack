package com.bluekitchen.btstack.event;

import com.bluekitchen.btstack.*;

public class L2CAPEventLEChannelOpened extends Event {

    public L2CAPEventLEChannelOpened(Packet packet) {
        super(packet);
    }
    
    /**
     * @return Status as int
     */
    public int getStatus(){
        return Util.readByte(data, 2);
    }

    /**
     * @return AddressType as int
     */
    public int getAddressType(){
        return Util.readByte(data, 3);
    }

    /**
     * @return Address as BD_ADDR
     */
    public BD_ADDR getAddress(){
        return Util.readBdAddr(data, 4);
    }

    /**
     * @return Handle as int
     */
    public int getHandle(){
        return Util.readBt16(data, 10);
    }

    /**
     * @return Incoming as int
     */
    public int getIncoming(){
        return Util.readByte(data, 12);
    }

    /**
     * @return Psm as int
     */
    public int getPsm(){
        return Util.readBt16(data, 13);
    }

    /**
     * @return LocalCid as int
     */
    public int getLocalCid(){
        return Util.readBt16(data, 15);
    }

    /**
     * @return RemoteCid as int
     */
    public int getRemoteCid(){
        return Util.readBt16(data, 17);
    }

    /**
     * @return LocalMtu as int
     */
    public int getLocalMtu(){
        return Util.readBt16(data, 19);
    }

    /**
     * @return RemoteMtu as int
     */
    public int getRemoteMtu(){
        return Util.readBt16(data, 21);
    }

    
    public String toString(){
        StringBuffer t = new StringBuffer();
        t.append("L2CAPEventLEChannelOpened < type = ");
        t.append(String.format("0x%02x, ", getEventType()));
        t.append(getEventType());
        t.append(", status = ");
        t.append(getStatus());
        t.append(", address_type = ");
        t.append(getAddressType());
        t.append(", address = ");
        t.append(getAddress());
        t.append(", handle = ");
        t.append(getHandle());
        t.append(", incoming = ");
        t.append(getIncoming());
        t.append(", psm = ");
        t.append(getPsm());
        t.append(", local_cid = ");
        t.append(getLocalCid());
        t.append(", remote_cid = ");
        t.append(getRemoteCid());
        t.append(", local_mtu = ");
        t.append(getLocalMtu());
        t.append(", remote_mtu = ");
        t.append(getRemoteMtu());
        t.append(" >");
        return t.toString();
    }

}
