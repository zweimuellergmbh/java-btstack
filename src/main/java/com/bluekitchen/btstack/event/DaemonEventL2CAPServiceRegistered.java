package com.bluekitchen.btstack.event;

import com.bluekitchen.btstack.*;

public class DaemonEventL2CAPServiceRegistered extends Event {

    public DaemonEventL2CAPServiceRegistered(Packet packet) {
        super(packet);
    }
    
    /**
     * @return Status as int
     */
    public int getStatus(){
        return Util.readByte(data, 2);
    }

    /**
     * @return Psm as int
     */
    public int getPsm(){
        return Util.readBt16(data, 3);
    }

    
    public String toString(){
        StringBuffer t = new StringBuffer();
        t.append("DaemonEventL2CAPServiceRegistered < type = ");
        t.append(String.format("0x%02x, ", getEventType()));
        t.append(getEventType());
        t.append(", status = ");
        t.append(getStatus());
        t.append(", psm = ");
        t.append(getPsm());
        t.append(" >");
        return t.toString();
    }

}
