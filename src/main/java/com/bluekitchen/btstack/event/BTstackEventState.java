package com.bluekitchen.btstack.event;

import com.bluekitchen.btstack.*;

public class BTstackEventState extends Event {

    public BTstackEventState(Packet packet) {
        super(packet);
    }
    
    /**
     * @return State as int
     */
    public int getState(){
        return Util.readByte(data, 2);
    }

    
    public String toString(){
        StringBuffer t = new StringBuffer();
        t.append("BTstackEventState < type = ");
        t.append(String.format("0x%02x, ", getEventType()));
        t.append(getEventType());
        t.append(", state = ");
        t.append(getState());
        t.append(" >");
        return t.toString();
    }

}
