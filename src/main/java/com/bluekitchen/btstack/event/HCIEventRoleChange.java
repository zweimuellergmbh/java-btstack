package com.bluekitchen.btstack.event;

import com.bluekitchen.btstack.*;

public class HCIEventRoleChange extends Event {

    public HCIEventRoleChange(Packet packet) {
        super(packet);
    }
    
    /**
     * @return Status as int
     */
    public int getStatus(){
        return Util.readByte(data, 2);
    }

    /**
     * @return BdAddr as BD_ADDR
     */
    public BD_ADDR getBdAddr(){
        return Util.readBdAddr(data, 3);
    }

    /**
     * @return Role as int
     */
    public int getRole(){
        return Util.readByte(data, 9);
    }

    
    public String toString(){
        StringBuffer t = new StringBuffer();
        t.append("HCIEventRoleChange < type = ");
        t.append(String.format("0x%02x, ", getEventType()));
        t.append(getEventType());
        t.append(", status = ");
        t.append(getStatus());
        t.append(", bd_addr = ");
        t.append(getBdAddr());
        t.append(", role = ");
        t.append(getRole());
        t.append(" >");
        return t.toString();
    }

}
