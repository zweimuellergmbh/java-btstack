package com.bluekitchen.btstack.event;

import com.bluekitchen.btstack.*;

public class DaemonEventSDPServiceRegistered extends Event {

    public DaemonEventSDPServiceRegistered(Packet packet) {
        super(packet);
    }
    
    /**
     * @return Status as int
     */
    public int getStatus(){
        return Util.readByte(data, 2);
    }

    /**
     * @return ServiceRecordHandle as long
     */
    public long getServiceRecordHandle(){
        return Util.readBt32(data, 3);
    }

    
    public String toString(){
        StringBuffer t = new StringBuffer();
        t.append("DaemonEventSDPServiceRegistered < type = ");
        t.append(String.format("0x%02x, ", getEventType()));
        t.append(getEventType());
        t.append(", status = ");
        t.append(getStatus());
        t.append(", service_record_handle = ");
        t.append(getServiceRecordHandle());
        t.append(" >");
        return t.toString();
    }

}
