package com.bluekitchen.btstack.event;

import com.bluekitchen.btstack.*;

public class SDPEventQueryComplete extends Event {

    public SDPEventQueryComplete(Packet packet) {
        super(packet);
    }
    
    /**
     * @return Status as int
     */
    public int getStatus(){
        return Util.readByte(data, 2);
    }

    
    public String toString(){
        StringBuffer t = new StringBuffer();
        t.append("SDPEventQueryComplete < type = ");
        t.append(String.format("0x%02x, ", getEventType()));
        t.append(getEventType());
        t.append(", status = ");
        t.append(getStatus());
        t.append(" >");
        return t.toString();
    }

}
