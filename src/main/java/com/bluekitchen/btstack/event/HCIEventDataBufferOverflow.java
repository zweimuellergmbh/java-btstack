package com.bluekitchen.btstack.event;

import com.bluekitchen.btstack.*;

public class HCIEventDataBufferOverflow extends Event {

    public HCIEventDataBufferOverflow(Packet packet) {
        super(packet);
    }
    
    /**
     * @return LinkType as int
     */
    public int getLinkType(){
        return Util.readByte(data, 2);
    }

    
    public String toString(){
        StringBuffer t = new StringBuffer();
        t.append("HCIEventDataBufferOverflow < type = ");
        t.append(String.format("0x%02x, ", getEventType()));
        t.append(getEventType());
        t.append(", link_type = ");
        t.append(getLinkType());
        t.append(" >");
        return t.toString();
    }

}
