package com.bluekitchen.btstack.event;

import com.bluekitchen.btstack.*;

public class HCIEventIoCapabilityResponse extends Event {

    public HCIEventIoCapabilityResponse(Packet packet) {
        super(packet);
    }
    
    /**
     * @return BdAddr as BD_ADDR
     */
    public BD_ADDR getBdAddr(){
        return Util.readBdAddr(data, 2);
    }

    /**
     * @return IoCapability as int
     */
    public int getIoCapability(){
        return Util.readByte(data, 8);
    }

    /**
     * @return OobDataPresent as int
     */
    public int getOobDataPresent(){
        return Util.readByte(data, 9);
    }

    /**
     * @return AuthenticationRequirements as int
     */
    public int getAuthenticationRequirements(){
        return Util.readByte(data, 10);
    }

    
    public String toString(){
        StringBuffer t = new StringBuffer();
        t.append("HCIEventIoCapabilityResponse < type = ");
        t.append(String.format("0x%02x, ", getEventType()));
        t.append(getEventType());
        t.append(", bd_addr = ");
        t.append(getBdAddr());
        t.append(", io_capability = ");
        t.append(getIoCapability());
        t.append(", oob_data_present = ");
        t.append(getOobDataPresent());
        t.append(", authentication_requirements = ");
        t.append(getAuthenticationRequirements());
        t.append(" >");
        return t.toString();
    }

}
