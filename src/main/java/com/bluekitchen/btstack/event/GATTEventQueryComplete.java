package com.bluekitchen.btstack.event;

import com.bluekitchen.btstack.*;

public class GATTEventQueryComplete extends Event {

    public GATTEventQueryComplete(Packet packet) {
        super(packet);
    }
    
    /**
     * @return Handle as int
     */
    public int getHandle(){
        return Util.readBt16(data, 2);
    }

    /**
     * @return ATTStatus as int
     */
    public int getATTStatus(){
        return Util.readByte(data, 4);
    }

    
    public String toString(){
        StringBuffer t = new StringBuffer();
        t.append("GATTEventQueryComplete < type = ");
        t.append(String.format("0x%02x, ", getEventType()));
        t.append(getEventType());
        t.append(", handle = ");
        t.append(getHandle());
        t.append(", att_status = ");
        t.append(getATTStatus());
        t.append(" >");
        return t.toString();
    }

}
