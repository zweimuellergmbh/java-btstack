package com.bluekitchen.btstack.event;

import com.bluekitchen.btstack.*;

public class ATTEventDisconnected extends Event {

    public ATTEventDisconnected(Packet packet) {
        super(packet);
    }
    
    /**
     * @return Handle as int
     */
    public int getHandle(){
        return Util.readBt16(data, 2);
    }

    
    public String toString(){
        StringBuffer t = new StringBuffer();
        t.append("ATTEventDisconnected < type = ");
        t.append(String.format("0x%02x, ", getEventType()));
        t.append(getEventType());
        t.append(", handle = ");
        t.append(getHandle());
        t.append(" >");
        return t.toString();
    }

}
