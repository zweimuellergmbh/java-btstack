package com.bluekitchen.btstack.event;

import com.bluekitchen.btstack.*;

public class BnepEventChannelOpened extends Event {

    public BnepEventChannelOpened(Packet packet) {
        super(packet);
    }
    
    /**
     * @return Status as int
     */
    public int getStatus(){
        return Util.readByte(data, 2);
    }

    /**
     * @return BnepCid as int
     */
    public int getBnepCid(){
        return Util.readBt16(data, 3);
    }

    /**
     * @return SourceUuid as int
     */
    public int getSourceUuid(){
        return Util.readBt16(data, 5);
    }

    /**
     * @return DestinationUuid as int
     */
    public int getDestinationUuid(){
        return Util.readBt16(data, 7);
    }

    /**
     * @return Mtu as int
     */
    public int getMtu(){
        return Util.readBt16(data, 9);
    }

    /**
     * @return RemoteAddress as BD_ADDR
     */
    public BD_ADDR getRemoteAddress(){
        return Util.readBdAddr(data, 11);
    }

    /**
     * @return ConHandle as int
     */
    public int getConHandle(){
        return Util.readBt16(data, 17);
    }

    
    public String toString(){
        StringBuffer t = new StringBuffer();
        t.append("BnepEventChannelOpened < type = ");
        t.append(String.format("0x%02x, ", getEventType()));
        t.append(getEventType());
        t.append(", status = ");
        t.append(getStatus());
        t.append(", bnep_cid = ");
        t.append(getBnepCid());
        t.append(", source_uuid = ");
        t.append(getSourceUuid());
        t.append(", destination_uuid = ");
        t.append(getDestinationUuid());
        t.append(", mtu = ");
        t.append(getMtu());
        t.append(", remote_address = ");
        t.append(getRemoteAddress());
        t.append(", con_handle = ");
        t.append(getConHandle());
        t.append(" >");
        return t.toString();
    }

}
