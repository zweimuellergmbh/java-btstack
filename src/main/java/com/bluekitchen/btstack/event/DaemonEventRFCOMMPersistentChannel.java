package com.bluekitchen.btstack.event;

import com.bluekitchen.btstack.*;

public class DaemonEventRFCOMMPersistentChannel extends Event {

    public DaemonEventRFCOMMPersistentChannel(Packet packet) {
        super(packet);
    }
    
    /**
     * @return Status as int
     */
    public int getStatus(){
        return Util.readByte(data, 2);
    }

    /**
     * @return ServerChannelId as int
     */
    public int getServerChannelId(){
        return Util.readByte(data, 3);
    }

    
    public String toString(){
        StringBuffer t = new StringBuffer();
        t.append("DaemonEventRFCOMMPersistentChannel < type = ");
        t.append(String.format("0x%02x, ", getEventType()));
        t.append(getEventType());
        t.append(", status = ");
        t.append(getStatus());
        t.append(", server_channel_id = ");
        t.append(getServerChannelId());
        t.append(" >");
        return t.toString();
    }

}
