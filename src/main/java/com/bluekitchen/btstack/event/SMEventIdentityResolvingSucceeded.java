package com.bluekitchen.btstack.event;

import com.bluekitchen.btstack.*;

public class SMEventIdentityResolvingSucceeded extends Event {

    public SMEventIdentityResolvingSucceeded(Packet packet) {
        super(packet);
    }
    
    /**
     * @return Handle as int
     */
    public int getHandle(){
        return Util.readBt16(data, 2);
    }

    /**
     * @return AddrType as int
     */
    public int getAddrType(){
        return Util.readByte(data, 4);
    }

    /**
     * @return Address as BD_ADDR
     */
    public BD_ADDR getAddress(){
        return Util.readBdAddr(data, 5);
    }

    /**
     * @return IdentityAddrType as int
     */
    public int getIdentityAddrType(){
        return Util.readByte(data, 11);
    }

    /**
     * @return IdentityAddress as BD_ADDR
     */
    public BD_ADDR getIdentityAddress(){
        return Util.readBdAddr(data, 12);
    }

    /**
     * @return Index as int
     */
    public int getIndex(){
        return Util.readBt16(data, 18);
    }

    
    public String toString(){
        StringBuffer t = new StringBuffer();
        t.append("SMEventIdentityResolvingSucceeded < type = ");
        t.append(String.format("0x%02x, ", getEventType()));
        t.append(getEventType());
        t.append(", handle = ");
        t.append(getHandle());
        t.append(", addr_type = ");
        t.append(getAddrType());
        t.append(", address = ");
        t.append(getAddress());
        t.append(", identity_addr_type = ");
        t.append(getIdentityAddrType());
        t.append(", identity_address = ");
        t.append(getIdentityAddress());
        t.append(", index = ");
        t.append(getIndex());
        t.append(" >");
        return t.toString();
    }

}
