package com.bluekitchen.btstack.event;

import com.bluekitchen.btstack.*;

public class HCIEventLERemoteConnectionParameterRequest extends Event {

    public HCIEventLERemoteConnectionParameterRequest(Packet packet) {
        super(packet);
    }
    
    /**
     * @return SubeventCode as int
     */
    public int getSubeventCode(){
        return Util.readByte(data, 2);
    }

    /**
     * @return ConnectionHandle as int
     */
    public int getConnectionHandle(){
        return Util.readBt16(data, 3);
    }

    /**
     * @return IntervalMin as int
     */
    public int getIntervalMin(){
        return Util.readBt16(data, 5);
    }

    /**
     * @return IntervalMax as int
     */
    public int getIntervalMax(){
        return Util.readBt16(data, 7);
    }

    /**
     * @return Latency as int
     */
    public int getLatency(){
        return Util.readBt16(data, 9);
    }

    /**
     * @return Timeout as int
     */
    public int getTimeout(){
        return Util.readBt16(data, 11);
    }

    
    public String toString(){
        StringBuffer t = new StringBuffer();
        t.append("HCIEventLERemoteConnectionParameterRequest < type = ");
        t.append(String.format("0x%02x, ", getEventType()));
        t.append(getEventType());
        t.append(", subevent_code = ");
        t.append(getSubeventCode());
        t.append(", connection_handle = ");
        t.append(getConnectionHandle());
        t.append(", interval_min = ");
        t.append(getIntervalMin());
        t.append(", interval_max = ");
        t.append(getIntervalMax());
        t.append(", latency = ");
        t.append(getLatency());
        t.append(", timeout = ");
        t.append(getTimeout());
        t.append(" >");
        return t.toString();
    }

}
