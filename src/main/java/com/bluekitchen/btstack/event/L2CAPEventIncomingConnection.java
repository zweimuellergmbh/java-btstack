package com.bluekitchen.btstack.event;

import com.bluekitchen.btstack.*;

public class L2CAPEventIncomingConnection extends Event {

    public L2CAPEventIncomingConnection(Packet packet) {
        super(packet);
    }
    
    /**
     * @return Address as BD_ADDR
     */
    public BD_ADDR getAddress(){
        return Util.readBdAddr(data, 2);
    }

    /**
     * @return Handle as int
     */
    public int getHandle(){
        return Util.readBt16(data, 8);
    }

    /**
     * @return Psm as int
     */
    public int getPsm(){
        return Util.readBt16(data, 10);
    }

    /**
     * @return LocalCid as int
     */
    public int getLocalCid(){
        return Util.readBt16(data, 12);
    }

    /**
     * @return RemoteCid as int
     */
    public int getRemoteCid(){
        return Util.readBt16(data, 14);
    }

    
    public String toString(){
        StringBuffer t = new StringBuffer();
        t.append("L2CAPEventIncomingConnection < type = ");
        t.append(String.format("0x%02x, ", getEventType()));
        t.append(getEventType());
        t.append(", address = ");
        t.append(getAddress());
        t.append(", handle = ");
        t.append(getHandle());
        t.append(", psm = ");
        t.append(getPsm());
        t.append(", local_cid = ");
        t.append(getLocalCid());
        t.append(", remote_cid = ");
        t.append(getRemoteCid());
        t.append(" >");
        return t.toString();
    }

}
