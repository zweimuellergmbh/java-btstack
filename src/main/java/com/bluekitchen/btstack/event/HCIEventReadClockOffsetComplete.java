package com.bluekitchen.btstack.event;

import com.bluekitchen.btstack.*;

public class HCIEventReadClockOffsetComplete extends Event {

    public HCIEventReadClockOffsetComplete(Packet packet) {
        super(packet);
    }
    
    /**
     * @return Status as int
     */
    public int getStatus(){
        return Util.readByte(data, 2);
    }

    /**
     * @return Handle as int
     */
    public int getHandle(){
        return Util.readBt16(data, 3);
    }

    /**
     * @return ClockOffset as int
     */
    public int getClockOffset(){
        return Util.readBt16(data, 5);
    }

    
    public String toString(){
        StringBuffer t = new StringBuffer();
        t.append("HCIEventReadClockOffsetComplete < type = ");
        t.append(String.format("0x%02x, ", getEventType()));
        t.append(getEventType());
        t.append(", status = ");
        t.append(getStatus());
        t.append(", handle = ");
        t.append(getHandle());
        t.append(", clock_offset = ");
        t.append(getClockOffset());
        t.append(" >");
        return t.toString();
    }

}
