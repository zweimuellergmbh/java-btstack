package com.bluekitchen.btstack.event;

import com.bluekitchen.btstack.*;

public class SMEventKeypressNotification extends Event {

    public SMEventKeypressNotification(Packet packet) {
        super(packet);
    }
    
    /**
     * @return Handle as int
     */
    public int getHandle(){
        return Util.readBt16(data, 2);
    }

    /**
     * @return Action as int
     */
    public int getAction(){
        return Util.readByte(data, 4);
    }

    
    public String toString(){
        StringBuffer t = new StringBuffer();
        t.append("SMEventKeypressNotification < type = ");
        t.append(String.format("0x%02x, ", getEventType()));
        t.append(getEventType());
        t.append(", handle = ");
        t.append(getHandle());
        t.append(", action = ");
        t.append(getAction());
        t.append(" >");
        return t.toString();
    }

}
