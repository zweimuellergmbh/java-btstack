package com.bluekitchen.btstack.event;

import com.bluekitchen.btstack.*;

public class HCIEventLinkKeyRequest extends Event {

    public HCIEventLinkKeyRequest(Packet packet) {
        super(packet);
    }
    
    /**
     * @return BdAddr as BD_ADDR
     */
    public BD_ADDR getBdAddr(){
        return Util.readBdAddr(data, 2);
    }

    
    public String toString(){
        StringBuffer t = new StringBuffer();
        t.append("HCIEventLinkKeyRequest < type = ");
        t.append(String.format("0x%02x, ", getEventType()));
        t.append(getEventType());
        t.append(", bd_addr = ");
        t.append(getBdAddr());
        t.append(" >");
        return t.toString();
    }

}
