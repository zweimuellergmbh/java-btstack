package com.bluekitchen.btstack.event;

import com.bluekitchen.btstack.*;

public class ATTEventCanSendNow extends Event {

    public ATTEventCanSendNow(Packet packet) {
        super(packet);
    }
    
    
    public String toString(){
        StringBuffer t = new StringBuffer();
        t.append("ATTEventCanSendNow < type = ");
        t.append(String.format("0x%02x, ", getEventType()));
        t.append(getEventType());
        t.append(" >");
        return t.toString();
    }

}
