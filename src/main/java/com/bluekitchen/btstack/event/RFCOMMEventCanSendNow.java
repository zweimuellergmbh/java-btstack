package com.bluekitchen.btstack.event;

import com.bluekitchen.btstack.*;

public class RFCOMMEventCanSendNow extends Event {

    public RFCOMMEventCanSendNow(Packet packet) {
        super(packet);
    }
    
    /**
     * @return RFCOMMCid as int
     */
    public int getRFCOMMCid(){
        return Util.readBt16(data, 2);
    }

    
    public String toString(){
        StringBuffer t = new StringBuffer();
        t.append("RFCOMMEventCanSendNow < type = ");
        t.append(String.format("0x%02x, ", getEventType()));
        t.append(getEventType());
        t.append(", rfcomm_cid = ");
        t.append(getRFCOMMCid());
        t.append(" >");
        return t.toString();
    }

}
