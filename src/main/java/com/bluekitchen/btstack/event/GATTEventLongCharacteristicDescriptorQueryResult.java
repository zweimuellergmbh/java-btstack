package com.bluekitchen.btstack.event;

import com.bluekitchen.btstack.*;

public class GATTEventLongCharacteristicDescriptorQueryResult extends Event {

    public GATTEventLongCharacteristicDescriptorQueryResult(Packet packet) {
        super(packet);
    }
    
    /**
     * @return Handle as int
     */
    public int getHandle(){
        return Util.readBt16(data, 2);
    }

    /**
     * @return DescriptorOffset as int
     */
    public int getDescriptorOffset(){
        return Util.readBt16(data, 4);
    }

    /**
     * @return DescriptorLength as int
     */
    public int getDescriptorLength(){
        return Util.readBt16(data, 6);
    }

    /**
     * @return Descriptor as byte []
     */
    public byte [] getDescriptor(){
        int len = getDescriptorLength();
        byte[] result = new byte[len];
        System.arraycopy(data, 8, result, 0, len);
        return result;
    }

    
    public String toString(){
        StringBuffer t = new StringBuffer();
        t.append("GATTEventLongCharacteristicDescriptorQueryResult < type = ");
        t.append(String.format("0x%02x, ", getEventType()));
        t.append(getEventType());
        t.append(", handle = ");
        t.append(getHandle());
        t.append(", descriptor_offset = ");
        t.append(getDescriptorOffset());
        t.append(", descriptor_length = ");
        t.append(getDescriptorLength());
        t.append(", descriptor = ");
        t.append(getDescriptor());
        t.append(" >");
        return t.toString();
    }

}
