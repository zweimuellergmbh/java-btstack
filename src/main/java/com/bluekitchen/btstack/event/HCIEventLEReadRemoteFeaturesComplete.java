package com.bluekitchen.btstack.event;

import com.bluekitchen.btstack.*;

public class HCIEventLEReadRemoteFeaturesComplete extends Event {

    public HCIEventLEReadRemoteFeaturesComplete(Packet packet) {
        super(packet);
    }
    
    /**
     * @return SubeventCode as int
     */
    public int getSubeventCode(){
        return Util.readByte(data, 2);
    }

    /**
     * @return ConnectionHandle as int
     */
    public int getConnectionHandle(){
        return Util.readBt16(data, 3);
    }

    /**
     * @return LEFeatures as byte []
     */
    public byte [] getLEFeatures(){
        int len = 8;
        byte[] result = new byte[len];
        System.arraycopy(data, 5, result, 0, len);
        return result;
    }

    
    public String toString(){
        StringBuffer t = new StringBuffer();
        t.append("HCIEventLEReadRemoteFeaturesComplete < type = ");
        t.append(String.format("0x%02x, ", getEventType()));
        t.append(getEventType());
        t.append(", subevent_code = ");
        t.append(getSubeventCode());
        t.append(", connection_handle = ");
        t.append(getConnectionHandle());
        t.append(", le_features = ");
        t.append(getLEFeatures());
        t.append(" >");
        return t.toString();
    }

}
