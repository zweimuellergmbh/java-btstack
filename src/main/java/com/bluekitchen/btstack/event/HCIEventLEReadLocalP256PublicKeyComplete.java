package com.bluekitchen.btstack.event;

import com.bluekitchen.btstack.*;

public class HCIEventLEReadLocalP256PublicKeyComplete extends Event {

    public HCIEventLEReadLocalP256PublicKeyComplete(Packet packet) {
        super(packet);
    }
    
    /**
     * @return SubeventCode as int
     */
    public int getSubeventCode(){
        return Util.readByte(data, 2);
    }

    /**
     * @return Status as int
     */
    public int getStatus(){
        return Util.readByte(data, 3);
    }

    /**
     * @return DhkeyX as byte []
     */
    public byte [] getDhkeyX(){
        int len = 32;
        byte[] result = new byte[len];
        System.arraycopy(data, 4, result, 0, len);
        return result;
    }

    /**
     * @return DhkeyY as byte []
     */
    public byte [] getDhkeyY(){
        int len = 32;
        byte[] result = new byte[len];
        System.arraycopy(data, 36, result, 0, len);
        return result;
    }

    
    public String toString(){
        StringBuffer t = new StringBuffer();
        t.append("HCIEventLEReadLocalP256PublicKeyComplete < type = ");
        t.append(String.format("0x%02x, ", getEventType()));
        t.append(getEventType());
        t.append(", subevent_code = ");
        t.append(getSubeventCode());
        t.append(", status = ");
        t.append(getStatus());
        t.append(", dhkey_x = ");
        t.append(getDhkeyX());
        t.append(", dhkey_y = ");
        t.append(getDhkeyY());
        t.append(" >");
        return t.toString();
    }

}
