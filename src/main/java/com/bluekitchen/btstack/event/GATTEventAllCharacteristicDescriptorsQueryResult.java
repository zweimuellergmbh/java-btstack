package com.bluekitchen.btstack.event;

import com.bluekitchen.btstack.*;

public class GATTEventAllCharacteristicDescriptorsQueryResult extends Event {

    public GATTEventAllCharacteristicDescriptorsQueryResult(Packet packet) {
        super(packet);
    }
    
    /**
     * @return Handle as int
     */
    public int getHandle(){
        return Util.readBt16(data, 2);
    }

    /**
     * @return CharacteristicDescriptor as GATTCharacteristicDescriptor
     */
    public GATTCharacteristicDescriptor getCharacteristicDescriptor(){
        return Util.readGattCharacteristicDescriptor(data, 4);
    }

    
    public String toString(){
        StringBuffer t = new StringBuffer();
        t.append("GATTEventAllCharacteristicDescriptorsQueryResult < type = ");
        t.append(String.format("0x%02x, ", getEventType()));
        t.append(getEventType());
        t.append(", handle = ");
        t.append(getHandle());
        t.append(", characteristic_descriptor = ");
        t.append(getCharacteristicDescriptor());
        t.append(" >");
        return t.toString();
    }

}
