package com.bluekitchen.btstack.event;

import com.bluekitchen.btstack.*;

public class HCIEventConnectionComplete extends Event {

    public HCIEventConnectionComplete(Packet packet) {
        super(packet);
    }
    
    /**
     * @return Status as int
     */
    public int getStatus(){
        return Util.readByte(data, 2);
    }

    /**
     * @return ConnectionHandle as int
     */
    public int getConnectionHandle(){
        return Util.readBt16(data, 3);
    }

    /**
     * @return BdAddr as BD_ADDR
     */
    public BD_ADDR getBdAddr(){
        return Util.readBdAddr(data, 5);
    }

    /**
     * @return LinkType as int
     */
    public int getLinkType(){
        return Util.readByte(data, 11);
    }

    /**
     * @return EncryptionEnabled as int
     */
    public int getEncryptionEnabled(){
        return Util.readByte(data, 12);
    }

    
    public String toString(){
        StringBuffer t = new StringBuffer();
        t.append("HCIEventConnectionComplete < type = ");
        t.append(String.format("0x%02x, ", getEventType()));
        t.append(getEventType());
        t.append(", status = ");
        t.append(getStatus());
        t.append(", connection_handle = ");
        t.append(getConnectionHandle());
        t.append(", bd_addr = ");
        t.append(getBdAddr());
        t.append(", link_type = ");
        t.append(getLinkType());
        t.append(", encryption_enabled = ");
        t.append(getEncryptionEnabled());
        t.append(" >");
        return t.toString();
    }

}
