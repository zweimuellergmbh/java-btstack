package com.bluekitchen.btstack.event;

import com.bluekitchen.btstack.*;

public class GATTEventServiceQueryResult extends Event {

    public GATTEventServiceQueryResult(Packet packet) {
        super(packet);
    }
    
    /**
     * @return Handle as int
     */
    public int getHandle(){
        return Util.readBt16(data, 2);
    }

    /**
     * @return Service as GATTService
     */
    public GATTService getService(){
        return Util.readGattService(data, 4);
    }

    
    public String toString(){
        StringBuffer t = new StringBuffer();
        t.append("GATTEventServiceQueryResult < type = ");
        t.append(String.format("0x%02x, ", getEventType()));
        t.append(getEventType());
        t.append(", handle = ");
        t.append(getHandle());
        t.append(", service = ");
        t.append(getService());
        t.append(" >");
        return t.toString();
    }

}
