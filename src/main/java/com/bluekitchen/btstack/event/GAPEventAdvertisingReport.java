package com.bluekitchen.btstack.event;

import com.bluekitchen.btstack.*;

public class GAPEventAdvertisingReport extends Event {

    public GAPEventAdvertisingReport(Packet packet) {
        super(packet);
    }
    
    /**
     * @return AdvertisingEventType as int
     */
    public int getAdvertisingEventType(){
        return Util.readByte(data, 2);
    }

    /**
     * @return AddressType as int
     */
    public int getAddressType(){
        return Util.readByte(data, 3);
    }

    /**
     * @return Address as BD_ADDR
     */
    public BD_ADDR getAddress(){
        return Util.readBdAddr(data, 4);
    }

    /**
     * @return Rssi as int
     */
    public int getRssi(){
        return Util.readByte(data, 10);
    }

    /**
     * @return DataLength as int
     */
    public int getDataLength(){
        return Util.readByte(data, 11);
    }

    /**
     * @return Data as byte []
     */
    public byte [] getData(){
        int len = getDataLength();
        byte[] result = new byte[len];
        System.arraycopy(data, 12, result, 0, len);
        return result;
    }

    
    public String toString(){
        StringBuffer t = new StringBuffer();
        t.append("GAPEventAdvertisingReport < type = ");
        t.append(String.format("0x%02x, ", getEventType()));
        t.append(getEventType());
        t.append(", advertising_event_type = ");
        t.append(getAdvertisingEventType());
        t.append(", address_type = ");
        t.append(getAddressType());
        t.append(", address = ");
        t.append(getAddress());
        t.append(", rssi = ");
        t.append(getRssi());
        t.append(", data_length = ");
        t.append(getDataLength());
        t.append(", data = ");
        t.append(getData());
        t.append(" >");
        return t.toString();
    }

}
