package com.bluekitchen.btstack.event;

import com.bluekitchen.btstack.*;

public class L2CAPEventLEIncomingConnection extends Event {

    public L2CAPEventLEIncomingConnection(Packet packet) {
        super(packet);
    }
    
    /**
     * @return AddressType as int
     */
    public int getAddressType(){
        return Util.readByte(data, 2);
    }

    /**
     * @return Address as BD_ADDR
     */
    public BD_ADDR getAddress(){
        return Util.readBdAddr(data, 3);
    }

    /**
     * @return Handle as int
     */
    public int getHandle(){
        return Util.readBt16(data, 9);
    }

    /**
     * @return Psm as int
     */
    public int getPsm(){
        return Util.readBt16(data, 11);
    }

    /**
     * @return LocalCid as int
     */
    public int getLocalCid(){
        return Util.readBt16(data, 13);
    }

    /**
     * @return RemoteCid as int
     */
    public int getRemoteCid(){
        return Util.readBt16(data, 15);
    }

    /**
     * @return RemoteMtu as int
     */
    public int getRemoteMtu(){
        return Util.readBt16(data, 17);
    }

    
    public String toString(){
        StringBuffer t = new StringBuffer();
        t.append("L2CAPEventLEIncomingConnection < type = ");
        t.append(String.format("0x%02x, ", getEventType()));
        t.append(getEventType());
        t.append(", address_type = ");
        t.append(getAddressType());
        t.append(", address = ");
        t.append(getAddress());
        t.append(", handle = ");
        t.append(getHandle());
        t.append(", psm = ");
        t.append(getPsm());
        t.append(", local_cid = ");
        t.append(getLocalCid());
        t.append(", remote_cid = ");
        t.append(getRemoteCid());
        t.append(", remote_mtu = ");
        t.append(getRemoteMtu());
        t.append(" >");
        return t.toString();
    }

}
