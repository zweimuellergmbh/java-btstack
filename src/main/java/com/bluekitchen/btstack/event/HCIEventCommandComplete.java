package com.bluekitchen.btstack.event;

import com.bluekitchen.btstack.*;

public class HCIEventCommandComplete extends Event {

    public HCIEventCommandComplete(Packet packet) {
        super(packet);
    }
    
    /**
     * @return NumHCICommandPackets as int
     */
    public int getNumHCICommandPackets(){
        return Util.readByte(data, 2);
    }

    /**
     * @return CommandOpcode as int
     */
    public int getCommandOpcode(){
        return Util.readBt16(data, 3);
    }

    /**
     * @return ReturnParameters as byte []
     */
    public byte [] getReturnParameters(){
        int len = getPayloadLen() - 5;
        byte[] result = new byte[len];
        System.arraycopy(data, 5, result, 0, len);
        return result;
    }

    
    public String toString(){
        StringBuffer t = new StringBuffer();
        t.append("HCIEventCommandComplete < type = ");
        t.append(String.format("0x%02x, ", getEventType()));
        t.append(getEventType());
        t.append(", num_hci_command_packets = ");
        t.append(getNumHCICommandPackets());
        t.append(", command_opcode = ");
        t.append(getCommandOpcode());
        t.append(", return_parameters = ");
        t.append(getReturnParameters());
        t.append(" >");
        return t.toString();
    }

}
