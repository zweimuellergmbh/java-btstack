package com.bluekitchen.btstack.event;

import com.bluekitchen.btstack.*;

public class ATTEventConnected extends Event {

    public ATTEventConnected(Packet packet) {
        super(packet);
    }
    
    /**
     * @return AddressType as int
     */
    public int getAddressType(){
        return Util.readByte(data, 2);
    }

    /**
     * @return Address as BD_ADDR
     */
    public BD_ADDR getAddress(){
        return Util.readBdAddr(data, 3);
    }

    /**
     * @return Handle as int
     */
    public int getHandle(){
        return Util.readBt16(data, 9);
    }

    
    public String toString(){
        StringBuffer t = new StringBuffer();
        t.append("ATTEventConnected < type = ");
        t.append(String.format("0x%02x, ", getEventType()));
        t.append(getEventType());
        t.append(", address_type = ");
        t.append(getAddressType());
        t.append(", address = ");
        t.append(getAddress());
        t.append(", handle = ");
        t.append(getHandle());
        t.append(" >");
        return t.toString();
    }

}
