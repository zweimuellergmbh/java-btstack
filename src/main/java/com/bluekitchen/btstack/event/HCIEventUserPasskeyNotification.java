package com.bluekitchen.btstack.event;

import com.bluekitchen.btstack.*;

public class HCIEventUserPasskeyNotification extends Event {

    public HCIEventUserPasskeyNotification(Packet packet) {
        super(packet);
    }
    
    /**
     * @return BdAddr as BD_ADDR
     */
    public BD_ADDR getBdAddr(){
        return Util.readBdAddr(data, 2);
    }

    /**
     * @return NumericValue as long
     */
    public long getNumericValue(){
        return Util.readBt32(data, 8);
    }

    
    public String toString(){
        StringBuffer t = new StringBuffer();
        t.append("HCIEventUserPasskeyNotification < type = ");
        t.append(String.format("0x%02x, ", getEventType()));
        t.append(getEventType());
        t.append(", bd_addr = ");
        t.append(getBdAddr());
        t.append(", numeric_value = ");
        t.append(getNumericValue());
        t.append(" >");
        return t.toString();
    }

}
