package com.bluekitchen.btstack.event;

import com.bluekitchen.btstack.*;

public class DaemonEventVersion extends Event {

    public DaemonEventVersion(Packet packet) {
        super(packet);
    }
    
    /**
     * @return Major as int
     */
    public int getMajor(){
        return Util.readByte(data, 2);
    }

    /**
     * @return Minor as int
     */
    public int getMinor(){
        return Util.readByte(data, 3);
    }

    /**
     * @return Revision as int
     */
    public int getRevision(){
        return Util.readBt16(data, 4);
    }

    
    public String toString(){
        StringBuffer t = new StringBuffer();
        t.append("DaemonEventVersion < type = ");
        t.append(String.format("0x%02x, ", getEventType()));
        t.append(getEventType());
        t.append(", major = ");
        t.append(getMajor());
        t.append(", minor = ");
        t.append(getMinor());
        t.append(", revision = ");
        t.append(getRevision());
        t.append(" >");
        return t.toString();
    }

}
