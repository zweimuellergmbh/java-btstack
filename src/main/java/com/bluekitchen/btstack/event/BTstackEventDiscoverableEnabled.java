package com.bluekitchen.btstack.event;

import com.bluekitchen.btstack.*;

public class BTstackEventDiscoverableEnabled extends Event {

    public BTstackEventDiscoverableEnabled(Packet packet) {
        super(packet);
    }
    
    /**
     * @return Discoverable as int
     */
    public int getDiscoverable(){
        return Util.readByte(data, 2);
    }

    
    public String toString(){
        StringBuffer t = new StringBuffer();
        t.append("BTstackEventDiscoverableEnabled < type = ");
        t.append(String.format("0x%02x, ", getEventType()));
        t.append(getEventType());
        t.append(", discoverable = ");
        t.append(getDiscoverable());
        t.append(" >");
        return t.toString();
    }

}
