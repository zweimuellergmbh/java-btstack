package com.bluekitchen.btstack.event;

import com.bluekitchen.btstack.*;

public class HCIEventHardwareError extends Event {

    public HCIEventHardwareError(Packet packet) {
        super(packet);
    }
    
    /**
     * @return HardwareCode as int
     */
    public int getHardwareCode(){
        return Util.readByte(data, 2);
    }

    
    public String toString(){
        StringBuffer t = new StringBuffer();
        t.append("HCIEventHardwareError < type = ");
        t.append(String.format("0x%02x, ", getEventType()));
        t.append(getEventType());
        t.append(", hardware_code = ");
        t.append(getHardwareCode());
        t.append(" >");
        return t.toString();
    }

}
