package com.bluekitchen.btstack.event;

import com.bluekitchen.btstack.*;

public class HCIEventConnectionRequest extends Event {

    public HCIEventConnectionRequest(Packet packet) {
        super(packet);
    }
    
    /**
     * @return BdAddr as BD_ADDR
     */
    public BD_ADDR getBdAddr(){
        return Util.readBdAddr(data, 2);
    }

    /**
     * @return ClassOfDevice as int
     */
    public int getClassOfDevice(){
        return Util.readBt24(data, 8);
    }

    /**
     * @return LinkType as int
     */
    public int getLinkType(){
        return Util.readByte(data, 11);
    }

    
    public String toString(){
        StringBuffer t = new StringBuffer();
        t.append("HCIEventConnectionRequest < type = ");
        t.append(String.format("0x%02x, ", getEventType()));
        t.append(getEventType());
        t.append(", bd_addr = ");
        t.append(getBdAddr());
        t.append(", class_of_device = ");
        t.append(getClassOfDevice());
        t.append(", link_type = ");
        t.append(getLinkType());
        t.append(" >");
        return t.toString();
    }

}
