package com.bluekitchen.btstack.event;

import com.bluekitchen.btstack.*;

public class HCIEventEncryptionChange extends Event {

    public HCIEventEncryptionChange(Packet packet) {
        super(packet);
    }
    
    /**
     * @return Status as int
     */
    public int getStatus(){
        return Util.readByte(data, 2);
    }

    /**
     * @return ConnectionHandle as int
     */
    public int getConnectionHandle(){
        return Util.readBt16(data, 3);
    }

    /**
     * @return EncryptionEnabled as int
     */
    public int getEncryptionEnabled(){
        return Util.readByte(data, 5);
    }

    
    public String toString(){
        StringBuffer t = new StringBuffer();
        t.append("HCIEventEncryptionChange < type = ");
        t.append(String.format("0x%02x, ", getEventType()));
        t.append(getEventType());
        t.append(", status = ");
        t.append(getStatus());
        t.append(", connection_handle = ");
        t.append(getConnectionHandle());
        t.append(", encryption_enabled = ");
        t.append(getEncryptionEnabled());
        t.append(" >");
        return t.toString();
    }

}
