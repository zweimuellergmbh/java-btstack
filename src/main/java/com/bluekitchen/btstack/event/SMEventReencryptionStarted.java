package com.bluekitchen.btstack.event;

import com.bluekitchen.btstack.*;

public class SMEventReencryptionStarted extends Event {

    public SMEventReencryptionStarted(Packet packet) {
        super(packet);
    }
    
    /**
     * @return Handle as int
     */
    public int getHandle(){
        return Util.readBt16(data, 2);
    }

    /**
     * @return AddrType as int
     */
    public int getAddrType(){
        return Util.readByte(data, 4);
    }

    /**
     * @return Address as BD_ADDR
     */
    public BD_ADDR getAddress(){
        return Util.readBdAddr(data, 5);
    }

    
    public String toString(){
        StringBuffer t = new StringBuffer();
        t.append("SMEventReencryptionStarted < type = ");
        t.append(String.format("0x%02x, ", getEventType()));
        t.append(getEventType());
        t.append(", handle = ");
        t.append(getHandle());
        t.append(", addr_type = ");
        t.append(getAddrType());
        t.append(", address = ");
        t.append(getAddress());
        t.append(" >");
        return t.toString();
    }

}
