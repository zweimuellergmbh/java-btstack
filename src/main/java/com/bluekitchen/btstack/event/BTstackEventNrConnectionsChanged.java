package com.bluekitchen.btstack.event;

import com.bluekitchen.btstack.*;

public class BTstackEventNrConnectionsChanged extends Event {

    public BTstackEventNrConnectionsChanged(Packet packet) {
        super(packet);
    }
    
    /**
     * @return NumberConnections as int
     */
    public int getNumberConnections(){
        return Util.readByte(data, 2);
    }

    
    public String toString(){
        StringBuffer t = new StringBuffer();
        t.append("BTstackEventNrConnectionsChanged < type = ");
        t.append(String.format("0x%02x, ", getEventType()));
        t.append(getEventType());
        t.append(", number_connections = ");
        t.append(getNumberConnections());
        t.append(" >");
        return t.toString();
    }

}
