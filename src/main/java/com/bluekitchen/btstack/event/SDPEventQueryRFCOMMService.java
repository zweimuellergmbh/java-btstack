package com.bluekitchen.btstack.event;

import com.bluekitchen.btstack.*;

public class SDPEventQueryRFCOMMService extends Event {

    public SDPEventQueryRFCOMMService(Packet packet) {
        super(packet);
    }
    
    /**
     * @return RFCOMMChannel as int
     */
    public int getRFCOMMChannel(){
        return Util.readByte(data, 2);
    }

    /**
     * @return Name as String
     */
    public String getName(){
        int offset = 3; 
        return Util.getText(data, offset, getPayloadLen()-offset);
    }

    
    public String toString(){
        StringBuffer t = new StringBuffer();
        t.append("SDPEventQueryRFCOMMService < type = ");
        t.append(String.format("0x%02x, ", getEventType()));
        t.append(getEventType());
        t.append(", rfcomm_channel = ");
        t.append(getRFCOMMChannel());
        t.append(", name = ");
        t.append(getName());
        t.append(" >");
        return t.toString();
    }

}
