package com.bluekitchen.btstack.event;

import com.bluekitchen.btstack.*;

public class L2CAPEventTriggerRun extends Event {

    public L2CAPEventTriggerRun(Packet packet) {
        super(packet);
    }
    
    
    public String toString(){
        StringBuffer t = new StringBuffer();
        t.append("L2CAPEventTriggerRun < type = ");
        t.append(String.format("0x%02x, ", getEventType()));
        t.append(getEventType());
        t.append(" >");
        return t.toString();
    }

}
