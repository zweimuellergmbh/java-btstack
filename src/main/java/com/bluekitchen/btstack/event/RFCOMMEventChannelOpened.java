package com.bluekitchen.btstack.event;

import com.bluekitchen.btstack.*;

public class RFCOMMEventChannelOpened extends Event {

    public RFCOMMEventChannelOpened(Packet packet) {
        super(packet);
    }
    
    /**
     * @return Status as int
     */
    public int getStatus(){
        return Util.readByte(data, 2);
    }

    /**
     * @return BdAddr as BD_ADDR
     */
    public BD_ADDR getBdAddr(){
        return Util.readBdAddr(data, 3);
    }

    /**
     * @return ConHandle as int
     */
    public int getConHandle(){
        return Util.readBt16(data, 9);
    }

    /**
     * @return ServerChannel as int
     */
    public int getServerChannel(){
        return Util.readByte(data, 11);
    }

    /**
     * @return RFCOMMCid as int
     */
    public int getRFCOMMCid(){
        return Util.readBt16(data, 12);
    }

    /**
     * @return MaxFrameSize as int
     */
    public int getMaxFrameSize(){
        return Util.readBt16(data, 14);
    }

    /**
     * @return Incoming as int
     */
    public int getIncoming(){
        return Util.readByte(data, 16);
    }

    
    public String toString(){
        StringBuffer t = new StringBuffer();
        t.append("RFCOMMEventChannelOpened < type = ");
        t.append(String.format("0x%02x, ", getEventType()));
        t.append(getEventType());
        t.append(", status = ");
        t.append(getStatus());
        t.append(", bd_addr = ");
        t.append(getBdAddr());
        t.append(", con_handle = ");
        t.append(getConHandle());
        t.append(", server_channel = ");
        t.append(getServerChannel());
        t.append(", rfcomm_cid = ");
        t.append(getRFCOMMCid());
        t.append(", max_frame_size = ");
        t.append(getMaxFrameSize());
        t.append(", incoming = ");
        t.append(getIncoming());
        t.append(" >");
        return t.toString();
    }

}
