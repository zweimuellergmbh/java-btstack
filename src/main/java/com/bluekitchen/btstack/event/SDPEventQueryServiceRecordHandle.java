package com.bluekitchen.btstack.event;

import com.bluekitchen.btstack.*;

public class SDPEventQueryServiceRecordHandle extends Event {

    public SDPEventQueryServiceRecordHandle(Packet packet) {
        super(packet);
    }
    
    /**
     * @return TotalCount as int
     */
    public int getTotalCount(){
        return Util.readBt16(data, 2);
    }

    /**
     * @return RecordIndex as int
     */
    public int getRecordIndex(){
        return Util.readBt16(data, 4);
    }

    /**
     * @return RecordHandle as long
     */
    public long getRecordHandle(){
        return Util.readBt32(data, 6);
    }

    
    public String toString(){
        StringBuffer t = new StringBuffer();
        t.append("SDPEventQueryServiceRecordHandle < type = ");
        t.append(String.format("0x%02x, ", getEventType()));
        t.append(getEventType());
        t.append(", total_count = ");
        t.append(getTotalCount());
        t.append(", record_index = ");
        t.append(getRecordIndex());
        t.append(", record_handle = ");
        t.append(getRecordHandle());
        t.append(" >");
        return t.toString();
    }

}
