package com.bluekitchen.btstack.event;

import com.bluekitchen.btstack.*;

public class GAPEventPairingStarted extends Event {

    public GAPEventPairingStarted(Packet packet) {
        super(packet);
    }
    
    /**
     * @return ConHandle as int
     */
    public int getConHandle(){
        return Util.readBt16(data, 2);
    }

    /**
     * @return BdAddr as BD_ADDR
     */
    public BD_ADDR getBdAddr(){
        return Util.readBdAddr(data, 4);
    }

    /**
     * @return Ssp as int
     */
    public int getSsp(){
        return Util.readByte(data, 10);
    }

    /**
     * @return Initiator as int
     */
    public int getInitiator(){
        return Util.readByte(data, 11);
    }

    
    public String toString(){
        StringBuffer t = new StringBuffer();
        t.append("GAPEventPairingStarted < type = ");
        t.append(String.format("0x%02x, ", getEventType()));
        t.append(getEventType());
        t.append(", con_handle = ");
        t.append(getConHandle());
        t.append(", bd_addr = ");
        t.append(getBdAddr());
        t.append(", ssp = ");
        t.append(getSsp());
        t.append(", initiator = ");
        t.append(getInitiator());
        t.append(" >");
        return t.toString();
    }

}
