package com.bluekitchen.btstack.event;

import com.bluekitchen.btstack.*;

public class GAPEventSecurityLevel extends Event {

    public GAPEventSecurityLevel(Packet packet) {
        super(packet);
    }
    
    /**
     * @return Handle as int
     */
    public int getHandle(){
        return Util.readBt16(data, 2);
    }

    /**
     * @return SecurityLevel as int
     */
    public int getSecurityLevel(){
        return Util.readByte(data, 4);
    }

    
    public String toString(){
        StringBuffer t = new StringBuffer();
        t.append("GAPEventSecurityLevel < type = ");
        t.append(String.format("0x%02x, ", getEventType()));
        t.append(getEventType());
        t.append(", handle = ");
        t.append(getHandle());
        t.append(", security_level = ");
        t.append(getSecurityLevel());
        t.append(" >");
        return t.toString();
    }

}
