package com.bluekitchen.btstack.event;

import com.bluekitchen.btstack.*;

public class HCIEventTransportSleepMode extends Event {

    public HCIEventTransportSleepMode(Packet packet) {
        super(packet);
    }
    
    /**
     * @return Active as int
     */
    public int getActive(){
        return Util.readByte(data, 2);
    }

    
    public String toString(){
        StringBuffer t = new StringBuffer();
        t.append("HCIEventTransportSleepMode < type = ");
        t.append(String.format("0x%02x, ", getEventType()));
        t.append(getEventType());
        t.append(", active = ");
        t.append(getActive());
        t.append(" >");
        return t.toString();
    }

}
