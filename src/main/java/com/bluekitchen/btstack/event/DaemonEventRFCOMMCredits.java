package com.bluekitchen.btstack.event;

import com.bluekitchen.btstack.*;

public class DaemonEventRFCOMMCredits extends Event {

    public DaemonEventRFCOMMCredits(Packet packet) {
        super(packet);
    }
    
    /**
     * @return RFCOMMCid as int
     */
    public int getRFCOMMCid(){
        return Util.readBt16(data, 2);
    }

    /**
     * @return Credits as int
     */
    public int getCredits(){
        return Util.readByte(data, 4);
    }

    
    public String toString(){
        StringBuffer t = new StringBuffer();
        t.append("DaemonEventRFCOMMCredits < type = ");
        t.append(String.format("0x%02x, ", getEventType()));
        t.append(getEventType());
        t.append(", rfcomm_cid = ");
        t.append(getRFCOMMCid());
        t.append(", credits = ");
        t.append(getCredits());
        t.append(" >");
        return t.toString();
    }

}
