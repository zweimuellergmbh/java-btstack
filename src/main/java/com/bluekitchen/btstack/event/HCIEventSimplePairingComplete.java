package com.bluekitchen.btstack.event;

import com.bluekitchen.btstack.*;

public class HCIEventSimplePairingComplete extends Event {

    public HCIEventSimplePairingComplete(Packet packet) {
        super(packet);
    }
    
    /**
     * @return Status as int
     */
    public int getStatus(){
        return Util.readByte(data, 2);
    }

    /**
     * @return BdAddr as BD_ADDR
     */
    public BD_ADDR getBdAddr(){
        return Util.readBdAddr(data, 3);
    }

    
    public String toString(){
        StringBuffer t = new StringBuffer();
        t.append("HCIEventSimplePairingComplete < type = ");
        t.append(String.format("0x%02x, ", getEventType()));
        t.append(getEventType());
        t.append(", status = ");
        t.append(getStatus());
        t.append(", bd_addr = ");
        t.append(getBdAddr());
        t.append(" >");
        return t.toString();
    }

}
