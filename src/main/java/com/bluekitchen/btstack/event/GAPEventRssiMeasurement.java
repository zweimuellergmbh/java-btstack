package com.bluekitchen.btstack.event;

import com.bluekitchen.btstack.*;

public class GAPEventRssiMeasurement extends Event {

    public GAPEventRssiMeasurement(Packet packet) {
        super(packet);
    }
    
    /**
     * @return ConHandle as int
     */
    public int getConHandle(){
        return Util.readBt16(data, 2);
    }

    /**
     * @return Rssi as int
     */
    public int getRssi(){
        return Util.readByte(data, 4);
    }

    
    public String toString(){
        StringBuffer t = new StringBuffer();
        t.append("GAPEventRssiMeasurement < type = ");
        t.append(String.format("0x%02x, ", getEventType()));
        t.append(getEventType());
        t.append(", con_handle = ");
        t.append(getConHandle());
        t.append(", rssi = ");
        t.append(getRssi());
        t.append(" >");
        return t.toString();
    }

}
