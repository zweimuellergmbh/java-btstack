package com.bluekitchen.btstack.event;

import com.bluekitchen.btstack.*;

public class DaemonEventRemoteNameCached extends Event {

    public DaemonEventRemoteNameCached(Packet packet) {
        super(packet);
    }
    
    /**
     * @return Status as int
     */
    public int getStatus(){
        return Util.readByte(data, 2);
    }

    /**
     * @return Address as BD_ADDR
     */
    public BD_ADDR getAddress(){
        return Util.readBdAddr(data, 3);
    }

    /**
     * @return Name as String
     */
    public String getName(){
        int offset = 9; 
        return Util.getText(data, offset, getPayloadLen()-offset);
    }

    
    public String toString(){
        StringBuffer t = new StringBuffer();
        t.append("DaemonEventRemoteNameCached < type = ");
        t.append(String.format("0x%02x, ", getEventType()));
        t.append(getEventType());
        t.append(", status = ");
        t.append(getStatus());
        t.append(", address = ");
        t.append(getAddress());
        t.append(", name = ");
        t.append(getName());
        t.append(" >");
        return t.toString();
    }

}
