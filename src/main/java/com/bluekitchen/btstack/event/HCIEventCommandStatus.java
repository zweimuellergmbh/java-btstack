package com.bluekitchen.btstack.event;

import com.bluekitchen.btstack.*;

public class HCIEventCommandStatus extends Event {

    public HCIEventCommandStatus(Packet packet) {
        super(packet);
    }
    
    /**
     * @return Status as int
     */
    public int getStatus(){
        return Util.readByte(data, 2);
    }

    /**
     * @return NumHCICommandPackets as int
     */
    public int getNumHCICommandPackets(){
        return Util.readByte(data, 3);
    }

    /**
     * @return CommandOpcode as int
     */
    public int getCommandOpcode(){
        return Util.readBt16(data, 4);
    }

    
    public String toString(){
        StringBuffer t = new StringBuffer();
        t.append("HCIEventCommandStatus < type = ");
        t.append(String.format("0x%02x, ", getEventType()));
        t.append(getEventType());
        t.append(", status = ");
        t.append(getStatus());
        t.append(", num_hci_command_packets = ");
        t.append(getNumHCICommandPackets());
        t.append(", command_opcode = ");
        t.append(getCommandOpcode());
        t.append(" >");
        return t.toString();
    }

}
