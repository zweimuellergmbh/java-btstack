package com.bluekitchen.btstack.event;

import com.bluekitchen.btstack.*;

public class RFCOMMEventIncomingConnection extends Event {

    public RFCOMMEventIncomingConnection(Packet packet) {
        super(packet);
    }
    
    /**
     * @return BdAddr as BD_ADDR
     */
    public BD_ADDR getBdAddr(){
        return Util.readBdAddr(data, 2);
    }

    /**
     * @return ServerChannel as int
     */
    public int getServerChannel(){
        return Util.readByte(data, 8);
    }

    /**
     * @return RFCOMMCid as int
     */
    public int getRFCOMMCid(){
        return Util.readBt16(data, 9);
    }

    /**
     * @return ConHandle as int
     */
    public int getConHandle(){
        return Util.readBt16(data, 11);
    }

    
    public String toString(){
        StringBuffer t = new StringBuffer();
        t.append("RFCOMMEventIncomingConnection < type = ");
        t.append(String.format("0x%02x, ", getEventType()));
        t.append(getEventType());
        t.append(", bd_addr = ");
        t.append(getBdAddr());
        t.append(", server_channel = ");
        t.append(getServerChannel());
        t.append(", rfcomm_cid = ");
        t.append(getRFCOMMCid());
        t.append(", con_handle = ");
        t.append(getConHandle());
        t.append(" >");
        return t.toString();
    }

}
