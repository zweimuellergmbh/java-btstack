package com.bluekitchen.btstack.event;

import com.bluekitchen.btstack.*;

public class HCIEventInquiryResult extends Event {

    public HCIEventInquiryResult(Packet packet) {
        super(packet);
    }
    
    /**
     * @return NumResponses as int
     */
    public int getNumResponses(){
        return Util.readByte(data, 2);
    }

    /**
     * @return BdAddr as BD_ADDR
     */
    public BD_ADDR getBdAddr(){
        return Util.readBdAddr(data, 3);
    }

    /**
     * @return PageScanRepetitionMode as int
     */
    public int getPageScanRepetitionMode(){
        return Util.readByte(data, 9);
    }

    /**
     * @return Reserved1 as int
     */
    public int getReserved1(){
        return Util.readByte(data, 10);
    }

    /**
     * @return Reserved2 as int
     */
    public int getReserved2(){
        return Util.readByte(data, 11);
    }

    /**
     * @return ClassOfDevice as int
     */
    public int getClassOfDevice(){
        return Util.readBt24(data, 12);
    }

    /**
     * @return ClockOffset as int
     */
    public int getClockOffset(){
        return Util.readBt16(data, 15);
    }

    
    public String toString(){
        StringBuffer t = new StringBuffer();
        t.append("HCIEventInquiryResult < type = ");
        t.append(String.format("0x%02x, ", getEventType()));
        t.append(getEventType());
        t.append(", num_responses = ");
        t.append(getNumResponses());
        t.append(", bd_addr = ");
        t.append(getBdAddr());
        t.append(", page_scan_repetition_mode = ");
        t.append(getPageScanRepetitionMode());
        t.append(", reserved1 = ");
        t.append(getReserved1());
        t.append(", reserved2 = ");
        t.append(getReserved2());
        t.append(", class_of_device = ");
        t.append(getClassOfDevice());
        t.append(", clock_offset = ");
        t.append(getClockOffset());
        t.append(" >");
        return t.toString();
    }

}
