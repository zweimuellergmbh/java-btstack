package com.bluekitchen.btstack.event;

import com.bluekitchen.btstack.*;

public class HCIEventKeypressNotification extends Event {

    public HCIEventKeypressNotification(Packet packet) {
        super(packet);
    }
    
    /**
     * @return BdAddr as BD_ADDR
     */
    public BD_ADDR getBdAddr(){
        return Util.readBdAddr(data, 2);
    }

    /**
     * @return NotificationType as int
     */
    public int getNotificationType(){
        return Util.readByte(data, 8);
    }

    
    public String toString(){
        StringBuffer t = new StringBuffer();
        t.append("HCIEventKeypressNotification < type = ");
        t.append(String.format("0x%02x, ", getEventType()));
        t.append(getEventType());
        t.append(", bd_addr = ");
        t.append(getBdAddr());
        t.append(", notification_type = ");
        t.append(getNotificationType());
        t.append(" >");
        return t.toString();
    }

}
