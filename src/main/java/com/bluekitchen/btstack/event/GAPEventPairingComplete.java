package com.bluekitchen.btstack.event;

import com.bluekitchen.btstack.*;

public class GAPEventPairingComplete extends Event {

    public GAPEventPairingComplete(Packet packet) {
        super(packet);
    }
    
    /**
     * @return ConHandle as int
     */
    public int getConHandle(){
        return Util.readBt16(data, 2);
    }

    /**
     * @return BdAddr as BD_ADDR
     */
    public BD_ADDR getBdAddr(){
        return Util.readBdAddr(data, 4);
    }

    /**
     * @return Status as int
     */
    public int getStatus(){
        return Util.readByte(data, 10);
    }

    
    public String toString(){
        StringBuffer t = new StringBuffer();
        t.append("GAPEventPairingComplete < type = ");
        t.append(String.format("0x%02x, ", getEventType()));
        t.append(getEventType());
        t.append(", con_handle = ");
        t.append(getConHandle());
        t.append(", bd_addr = ");
        t.append(getBdAddr());
        t.append(", status = ");
        t.append(getStatus());
        t.append(" >");
        return t.toString();
    }

}
