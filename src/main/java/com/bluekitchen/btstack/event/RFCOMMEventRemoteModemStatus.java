package com.bluekitchen.btstack.event;

import com.bluekitchen.btstack.*;

public class RFCOMMEventRemoteModemStatus extends Event {

    public RFCOMMEventRemoteModemStatus(Packet packet) {
        super(packet);
    }
    
    /**
     * @return RFCOMMCid as int
     */
    public int getRFCOMMCid(){
        return Util.readBt16(data, 2);
    }

    /**
     * @return ModemStatus as int
     */
    public int getModemStatus(){
        return Util.readByte(data, 4);
    }

    
    public String toString(){
        StringBuffer t = new StringBuffer();
        t.append("RFCOMMEventRemoteModemStatus < type = ");
        t.append(String.format("0x%02x, ", getEventType()));
        t.append(getEventType());
        t.append(", rfcomm_cid = ");
        t.append(getRFCOMMCid());
        t.append(", modem_status = ");
        t.append(getModemStatus());
        t.append(" >");
        return t.toString();
    }

}
