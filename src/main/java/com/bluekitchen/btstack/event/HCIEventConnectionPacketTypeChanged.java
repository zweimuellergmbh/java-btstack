package com.bluekitchen.btstack.event;

import com.bluekitchen.btstack.*;

public class HCIEventConnectionPacketTypeChanged extends Event {

    public HCIEventConnectionPacketTypeChanged(Packet packet) {
        super(packet);
    }
    
    /**
     * @return Status as int
     */
    public int getStatus(){
        return Util.readByte(data, 2);
    }

    /**
     * @return Handle as int
     */
    public int getHandle(){
        return Util.readBt16(data, 3);
    }

    /**
     * @return PacketTypes as int
     */
    public int getPacketTypes(){
        return Util.readBt16(data, 5);
    }

    
    public String toString(){
        StringBuffer t = new StringBuffer();
        t.append("HCIEventConnectionPacketTypeChanged < type = ");
        t.append(String.format("0x%02x, ", getEventType()));
        t.append(getEventType());
        t.append(", status = ");
        t.append(getStatus());
        t.append(", handle = ");
        t.append(getHandle());
        t.append(", packet_types = ");
        t.append(getPacketTypes());
        t.append(" >");
        return t.toString();
    }

}
