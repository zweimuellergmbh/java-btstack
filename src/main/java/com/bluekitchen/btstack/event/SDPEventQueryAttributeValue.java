package com.bluekitchen.btstack.event;

import com.bluekitchen.btstack.*;

public class SDPEventQueryAttributeValue extends Event {

    public SDPEventQueryAttributeValue(Packet packet) {
        super(packet);
    }
    
    /**
     * @return RecordId as int
     */
    public int getRecordId(){
        return Util.readBt16(data, 2);
    }

    /**
     * @return AttributeId as int
     */
    public int getAttributeId(){
        return Util.readBt16(data, 4);
    }

    /**
     * @return AttributeLength as int
     */
    public int getAttributeLength(){
        return Util.readBt16(data, 6);
    }

    /**
     * @return AttributeValue as byte []
     */
    public byte [] getAttributeValue(){
        int len = getAttributeLength();
        byte[] result = new byte[len];
        System.arraycopy(data, 8, result, 0, len);
        return result;
    }

    
    public String toString(){
        StringBuffer t = new StringBuffer();
        t.append("SDPEventQueryAttributeValue < type = ");
        t.append(String.format("0x%02x, ", getEventType()));
        t.append(getEventType());
        t.append(", record_id = ");
        t.append(getRecordId());
        t.append(", attribute_id = ");
        t.append(getAttributeId());
        t.append(", attribute_length = ");
        t.append(getAttributeLength());
        t.append(", attribute_value = ");
        t.append(getAttributeValue());
        t.append(" >");
        return t.toString();
    }

}
