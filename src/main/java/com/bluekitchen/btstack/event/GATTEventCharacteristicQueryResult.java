package com.bluekitchen.btstack.event;

import com.bluekitchen.btstack.*;

public class GATTEventCharacteristicQueryResult extends Event {

    public GATTEventCharacteristicQueryResult(Packet packet) {
        super(packet);
    }
    
    /**
     * @return Handle as int
     */
    public int getHandle(){
        return Util.readBt16(data, 2);
    }

    /**
     * @return Characteristic as GATTCharacteristic
     */
    public GATTCharacteristic getCharacteristic(){
        return Util.readGattCharacteristic(data, 4);
    }

    
    public String toString(){
        StringBuffer t = new StringBuffer();
        t.append("GATTEventCharacteristicQueryResult < type = ");
        t.append(String.format("0x%02x, ", getEventType()));
        t.append(getEventType());
        t.append(", handle = ");
        t.append(getHandle());
        t.append(", characteristic = ");
        t.append(getCharacteristic());
        t.append(" >");
        return t.toString();
    }

}
