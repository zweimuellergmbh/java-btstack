package com.bluekitchen.btstack.event;

import com.bluekitchen.btstack.*;

public class HCIEventPinCodeRequest extends Event {

    public HCIEventPinCodeRequest(Packet packet) {
        super(packet);
    }
    
    /**
     * @return BdAddr as BD_ADDR
     */
    public BD_ADDR getBdAddr(){
        return Util.readBdAddr(data, 2);
    }

    
    public String toString(){
        StringBuffer t = new StringBuffer();
        t.append("HCIEventPinCodeRequest < type = ");
        t.append(String.format("0x%02x, ", getEventType()));
        t.append(getEventType());
        t.append(", bd_addr = ");
        t.append(getBdAddr());
        t.append(" >");
        return t.toString();
    }

}
