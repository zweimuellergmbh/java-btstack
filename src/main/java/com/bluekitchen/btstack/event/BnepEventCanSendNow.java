package com.bluekitchen.btstack.event;

import com.bluekitchen.btstack.*;

public class BnepEventCanSendNow extends Event {

    public BnepEventCanSendNow(Packet packet) {
        super(packet);
    }
    
    /**
     * @return BnepCid as int
     */
    public int getBnepCid(){
        return Util.readBt16(data, 2);
    }

    /**
     * @return SourceUuid as int
     */
    public int getSourceUuid(){
        return Util.readBt16(data, 4);
    }

    /**
     * @return DestinationUuid as int
     */
    public int getDestinationUuid(){
        return Util.readBt16(data, 6);
    }

    /**
     * @return RemoteAddress as BD_ADDR
     */
    public BD_ADDR getRemoteAddress(){
        return Util.readBdAddr(data, 8);
    }

    
    public String toString(){
        StringBuffer t = new StringBuffer();
        t.append("BnepEventCanSendNow < type = ");
        t.append(String.format("0x%02x, ", getEventType()));
        t.append(getEventType());
        t.append(", bnep_cid = ");
        t.append(getBnepCid());
        t.append(", source_uuid = ");
        t.append(getSourceUuid());
        t.append(", destination_uuid = ");
        t.append(getDestinationUuid());
        t.append(", remote_address = ");
        t.append(getRemoteAddress());
        t.append(" >");
        return t.toString();
    }

}
