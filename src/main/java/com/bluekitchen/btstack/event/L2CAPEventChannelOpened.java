package com.bluekitchen.btstack.event;

import com.bluekitchen.btstack.*;

public class L2CAPEventChannelOpened extends Event {

    public L2CAPEventChannelOpened(Packet packet) {
        super(packet);
    }
    
    /**
     * @return Status as int
     */
    public int getStatus(){
        return Util.readByte(data, 2);
    }

    /**
     * @return Address as BD_ADDR
     */
    public BD_ADDR getAddress(){
        return Util.readBdAddr(data, 3);
    }

    /**
     * @return Handle as int
     */
    public int getHandle(){
        return Util.readBt16(data, 9);
    }

    /**
     * @return Psm as int
     */
    public int getPsm(){
        return Util.readBt16(data, 11);
    }

    /**
     * @return LocalCid as int
     */
    public int getLocalCid(){
        return Util.readBt16(data, 13);
    }

    /**
     * @return RemoteCid as int
     */
    public int getRemoteCid(){
        return Util.readBt16(data, 15);
    }

    /**
     * @return LocalMtu as int
     */
    public int getLocalMtu(){
        return Util.readBt16(data, 17);
    }

    /**
     * @return RemoteMtu as int
     */
    public int getRemoteMtu(){
        return Util.readBt16(data, 19);
    }

    /**
     * @return FlushTimeout as int
     */
    public int getFlushTimeout(){
        return Util.readBt16(data, 21);
    }

    /**
     * @return Incoming as int
     */
    public int getIncoming(){
        return Util.readByte(data, 23);
    }

    /**
     * @return Mode as int
     */
    public int getMode(){
        return Util.readByte(data, 24);
    }

    /**
     * @return Fcs as int
     */
    public int getFcs(){
        return Util.readByte(data, 25);
    }

    
    public String toString(){
        StringBuffer t = new StringBuffer();
        t.append("L2CAPEventChannelOpened < type = ");
        t.append(String.format("0x%02x, ", getEventType()));
        t.append(getEventType());
        t.append(", status = ");
        t.append(getStatus());
        t.append(", address = ");
        t.append(getAddress());
        t.append(", handle = ");
        t.append(getHandle());
        t.append(", psm = ");
        t.append(getPsm());
        t.append(", local_cid = ");
        t.append(getLocalCid());
        t.append(", remote_cid = ");
        t.append(getRemoteCid());
        t.append(", local_mtu = ");
        t.append(getLocalMtu());
        t.append(", remote_mtu = ");
        t.append(getRemoteMtu());
        t.append(", flush_timeout = ");
        t.append(getFlushTimeout());
        t.append(", incoming = ");
        t.append(getIncoming());
        t.append(", mode = ");
        t.append(getMode());
        t.append(", fcs = ");
        t.append(getFcs());
        t.append(" >");
        return t.toString();
    }

}
