package com.bluekitchen.btstack.event;

import com.bluekitchen.btstack.*;

public class L2CAPEventConnectionParameterUpdateRequest extends Event {

    public L2CAPEventConnectionParameterUpdateRequest(Packet packet) {
        super(packet);
    }
    
    /**
     * @return Handle as int
     */
    public int getHandle(){
        return Util.readBt16(data, 2);
    }

    /**
     * @return IntervalMin as int
     */
    public int getIntervalMin(){
        return Util.readBt16(data, 4);
    }

    /**
     * @return IntervalMax as int
     */
    public int getIntervalMax(){
        return Util.readBt16(data, 6);
    }

    /**
     * @return Latencey as int
     */
    public int getLatencey(){
        return Util.readBt16(data, 8);
    }

    /**
     * @return TimeoutMultiplier as int
     */
    public int getTimeoutMultiplier(){
        return Util.readBt16(data, 10);
    }

    
    public String toString(){
        StringBuffer t = new StringBuffer();
        t.append("L2CAPEventConnectionParameterUpdateRequest < type = ");
        t.append(String.format("0x%02x, ", getEventType()));
        t.append(getEventType());
        t.append(", handle = ");
        t.append(getHandle());
        t.append(", interval_min = ");
        t.append(getIntervalMin());
        t.append(", interval_max = ");
        t.append(getIntervalMax());
        t.append(", latencey = ");
        t.append(getLatencey());
        t.append(", timeout_multiplier = ");
        t.append(getTimeoutMultiplier());
        t.append(" >");
        return t.toString();
    }

}
