package com.bluekitchen.btstack.event;

import com.bluekitchen.btstack.*;

public class BnepEventServiceRegistered extends Event {

    public BnepEventServiceRegistered(Packet packet) {
        super(packet);
    }
    
    /**
     * @return Status as int
     */
    public int getStatus(){
        return Util.readByte(data, 2);
    }

    /**
     * @return ServiceUuid as int
     */
    public int getServiceUuid(){
        return Util.readBt16(data, 3);
    }

    
    public String toString(){
        StringBuffer t = new StringBuffer();
        t.append("BnepEventServiceRegistered < type = ");
        t.append(String.format("0x%02x, ", getEventType()));
        t.append(getEventType());
        t.append(", status = ");
        t.append(getStatus());
        t.append(", service_uuid = ");
        t.append(getServiceUuid());
        t.append(" >");
        return t.toString();
    }

}
