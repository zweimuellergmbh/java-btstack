package com.bluekitchen.btstack.event;

import com.bluekitchen.btstack.*;

public class DaemonEventSystemBluetoothEnabled extends Event {

    public DaemonEventSystemBluetoothEnabled(Packet packet) {
        super(packet);
    }
    
    
    public String toString(){
        StringBuffer t = new StringBuffer();
        t.append("DaemonEventSystemBluetoothEnabled < type = ");
        t.append(String.format("0x%02x, ", getEventType()));
        t.append(getEventType());
        t.append(" >");
        return t.toString();
    }

}
