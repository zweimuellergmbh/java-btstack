package com.bluekitchen.btstack.event;

import com.bluekitchen.btstack.*;

public class RFCOMMEventRemoteLineStatus extends Event {

    public RFCOMMEventRemoteLineStatus(Packet packet) {
        super(packet);
    }
    
    /**
     * @return RFCOMMCid as int
     */
    public int getRFCOMMCid(){
        return Util.readBt16(data, 2);
    }

    /**
     * @return LineStatus as int
     */
    public int getLineStatus(){
        return Util.readByte(data, 4);
    }

    
    public String toString(){
        StringBuffer t = new StringBuffer();
        t.append("RFCOMMEventRemoteLineStatus < type = ");
        t.append(String.format("0x%02x, ", getEventType()));
        t.append(getEventType());
        t.append(", rfcomm_cid = ");
        t.append(getRFCOMMCid());
        t.append(", line_status = ");
        t.append(getLineStatus());
        t.append(" >");
        return t.toString();
    }

}
