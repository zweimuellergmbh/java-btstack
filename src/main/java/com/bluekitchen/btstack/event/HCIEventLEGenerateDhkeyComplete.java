package com.bluekitchen.btstack.event;

import com.bluekitchen.btstack.*;

public class HCIEventLEGenerateDhkeyComplete extends Event {

    public HCIEventLEGenerateDhkeyComplete(Packet packet) {
        super(packet);
    }
    
    /**
     * @return SubeventCode as int
     */
    public int getSubeventCode(){
        return Util.readByte(data, 2);
    }

    /**
     * @return Status as int
     */
    public int getStatus(){
        return Util.readByte(data, 3);
    }

    /**
     * @return Dhkey as byte []
     */
    public byte [] getDhkey(){
        int len = 32;
        byte[] result = new byte[len];
        System.arraycopy(data, 4, result, 0, len);
        return result;
    }

    
    public String toString(){
        StringBuffer t = new StringBuffer();
        t.append("HCIEventLEGenerateDhkeyComplete < type = ");
        t.append(String.format("0x%02x, ", getEventType()));
        t.append(getEventType());
        t.append(", subevent_code = ");
        t.append(getSubeventCode());
        t.append(", status = ");
        t.append(getStatus());
        t.append(", dhkey = ");
        t.append(getDhkey());
        t.append(" >");
        return t.toString();
    }

}
