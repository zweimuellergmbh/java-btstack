package com.bluekitchen.btstack.event;

import com.bluekitchen.btstack.*;

public class HCIEventModeChange extends Event {

    public HCIEventModeChange(Packet packet) {
        super(packet);
    }
    
    /**
     * @return Status as int
     */
    public int getStatus(){
        return Util.readByte(data, 2);
    }

    /**
     * @return Handle as int
     */
    public int getHandle(){
        return Util.readBt16(data, 3);
    }

    /**
     * @return Mode as int
     */
    public int getMode(){
        return Util.readByte(data, 5);
    }

    /**
     * @return Interval as int
     */
    public int getInterval(){
        return Util.readBt16(data, 6);
    }

    
    public String toString(){
        StringBuffer t = new StringBuffer();
        t.append("HCIEventModeChange < type = ");
        t.append(String.format("0x%02x, ", getEventType()));
        t.append(getEventType());
        t.append(", status = ");
        t.append(getStatus());
        t.append(", handle = ");
        t.append(getHandle());
        t.append(", mode = ");
        t.append(getMode());
        t.append(", interval = ");
        t.append(getInterval());
        t.append(" >");
        return t.toString();
    }

}
