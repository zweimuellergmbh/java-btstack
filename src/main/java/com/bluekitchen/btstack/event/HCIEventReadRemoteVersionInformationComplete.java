package com.bluekitchen.btstack.event;

import com.bluekitchen.btstack.*;

public class HCIEventReadRemoteVersionInformationComplete extends Event {

    public HCIEventReadRemoteVersionInformationComplete(Packet packet) {
        super(packet);
    }
    
    /**
     * @return Status as int
     */
    public int getStatus(){
        return Util.readByte(data, 2);
    }

    /**
     * @return ConnectionHandle as int
     */
    public int getConnectionHandle(){
        return Util.readBt16(data, 3);
    }

    /**
     * @return Version as int
     */
    public int getVersion(){
        return Util.readByte(data, 5);
    }

    /**
     * @return ManufacturerName as int
     */
    public int getManufacturerName(){
        return Util.readBt16(data, 6);
    }

    /**
     * @return Subversion as int
     */
    public int getSubversion(){
        return Util.readBt16(data, 8);
    }

    
    public String toString(){
        StringBuffer t = new StringBuffer();
        t.append("HCIEventReadRemoteVersionInformationComplete < type = ");
        t.append(String.format("0x%02x, ", getEventType()));
        t.append(getEventType());
        t.append(", status = ");
        t.append(getStatus());
        t.append(", connection_handle = ");
        t.append(getConnectionHandle());
        t.append(", version = ");
        t.append(getVersion());
        t.append(", manufacturer_name = ");
        t.append(getManufacturerName());
        t.append(", subversion = ");
        t.append(getSubversion());
        t.append(" >");
        return t.toString();
    }

}
