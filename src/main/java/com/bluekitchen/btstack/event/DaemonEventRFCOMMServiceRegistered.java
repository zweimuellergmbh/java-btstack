package com.bluekitchen.btstack.event;

import com.bluekitchen.btstack.*;

public class DaemonEventRFCOMMServiceRegistered extends Event {

    public DaemonEventRFCOMMServiceRegistered(Packet packet) {
        super(packet);
    }
    
    /**
     * @return Status as int
     */
    public int getStatus(){
        return Util.readByte(data, 2);
    }

    /**
     * @return ChannelId as int
     */
    public int getChannelId(){
        return Util.readByte(data, 3);
    }

    
    public String toString(){
        StringBuffer t = new StringBuffer();
        t.append("DaemonEventRFCOMMServiceRegistered < type = ");
        t.append(String.format("0x%02x, ", getEventType()));
        t.append(getEventType());
        t.append(", status = ");
        t.append(getStatus());
        t.append(", channel_id = ");
        t.append(getChannelId());
        t.append(" >");
        return t.toString();
    }

}
