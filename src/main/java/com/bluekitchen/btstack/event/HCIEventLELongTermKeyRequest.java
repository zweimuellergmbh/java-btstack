package com.bluekitchen.btstack.event;

import com.bluekitchen.btstack.*;

public class HCIEventLELongTermKeyRequest extends Event {

    public HCIEventLELongTermKeyRequest(Packet packet) {
        super(packet);
    }
    
    /**
     * @return SubeventCode as int
     */
    public int getSubeventCode(){
        return Util.readByte(data, 2);
    }

    /**
     * @return ConnectionHandle as int
     */
    public int getConnectionHandle(){
        return Util.readBt16(data, 3);
    }

    /**
     * @return RandomNumber as byte []
     */
    public byte [] getRandomNumber(){
        int len = 8;
        byte[] result = new byte[len];
        System.arraycopy(data, 5, result, 0, len);
        return result;
    }

    /**
     * @return EncryptionDiversifier as int
     */
    public int getEncryptionDiversifier(){
        return Util.readBt16(data, 13);
    }

    
    public String toString(){
        StringBuffer t = new StringBuffer();
        t.append("HCIEventLELongTermKeyRequest < type = ");
        t.append(String.format("0x%02x, ", getEventType()));
        t.append(getEventType());
        t.append(", subevent_code = ");
        t.append(getSubeventCode());
        t.append(", connection_handle = ");
        t.append(getConnectionHandle());
        t.append(", random_number = ");
        t.append(getRandomNumber());
        t.append(", encryption_diversifier = ");
        t.append(getEncryptionDiversifier());
        t.append(" >");
        return t.toString();
    }

}
