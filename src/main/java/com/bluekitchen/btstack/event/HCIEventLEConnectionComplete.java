package com.bluekitchen.btstack.event;

import com.bluekitchen.btstack.*;

public class HCIEventLEConnectionComplete extends Event {

    public HCIEventLEConnectionComplete(Packet packet) {
        super(packet);
    }
    
    /**
     * @return SubeventCode as int
     */
    public int getSubeventCode(){
        return Util.readByte(data, 2);
    }

    /**
     * @return Status as int
     */
    public int getStatus(){
        return Util.readByte(data, 3);
    }

    /**
     * @return ConnectionHandle as int
     */
    public int getConnectionHandle(){
        return Util.readBt16(data, 4);
    }

    /**
     * @return Role as int
     */
    public int getRole(){
        return Util.readByte(data, 6);
    }

    /**
     * @return PeerAddressType as int
     */
    public int getPeerAddressType(){
        return Util.readByte(data, 7);
    }

    /**
     * @return PeerAddress as BD_ADDR
     */
    public BD_ADDR getPeerAddress(){
        return Util.readBdAddr(data, 8);
    }

    /**
     * @return ConnInterval as int
     */
    public int getConnInterval(){
        return Util.readBt16(data, 14);
    }

    /**
     * @return ConnLatency as int
     */
    public int getConnLatency(){
        return Util.readBt16(data, 16);
    }

    /**
     * @return SupervisionTimeout as int
     */
    public int getSupervisionTimeout(){
        return Util.readBt16(data, 18);
    }

    /**
     * @return MasterClockAccuracy as int
     */
    public int getMasterClockAccuracy(){
        return Util.readByte(data, 20);
    }

    
    public String toString(){
        StringBuffer t = new StringBuffer();
        t.append("HCIEventLEConnectionComplete < type = ");
        t.append(String.format("0x%02x, ", getEventType()));
        t.append(getEventType());
        t.append(", subevent_code = ");
        t.append(getSubeventCode());
        t.append(", status = ");
        t.append(getStatus());
        t.append(", connection_handle = ");
        t.append(getConnectionHandle());
        t.append(", role = ");
        t.append(getRole());
        t.append(", peer_address_type = ");
        t.append(getPeerAddressType());
        t.append(", peer_address = ");
        t.append(getPeerAddress());
        t.append(", conn_interval = ");
        t.append(getConnInterval());
        t.append(", conn_latency = ");
        t.append(getConnLatency());
        t.append(", supervision_timeout = ");
        t.append(getSupervisionTimeout());
        t.append(", master_clock_accuracy = ");
        t.append(getMasterClockAccuracy());
        t.append(" >");
        return t.toString();
    }

}
