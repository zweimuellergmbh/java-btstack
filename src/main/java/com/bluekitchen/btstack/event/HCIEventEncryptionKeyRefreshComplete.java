package com.bluekitchen.btstack.event;

import com.bluekitchen.btstack.*;

public class HCIEventEncryptionKeyRefreshComplete extends Event {

    public HCIEventEncryptionKeyRefreshComplete(Packet packet) {
        super(packet);
    }
    
    /**
     * @return Status as int
     */
    public int getStatus(){
        return Util.readByte(data, 2);
    }

    /**
     * @return Handle as int
     */
    public int getHandle(){
        return Util.readBt16(data, 3);
    }

    
    public String toString(){
        StringBuffer t = new StringBuffer();
        t.append("HCIEventEncryptionKeyRefreshComplete < type = ");
        t.append(String.format("0x%02x, ", getEventType()));
        t.append(getEventType());
        t.append(", status = ");
        t.append(getStatus());
        t.append(", handle = ");
        t.append(getHandle());
        t.append(" >");
        return t.toString();
    }

}
