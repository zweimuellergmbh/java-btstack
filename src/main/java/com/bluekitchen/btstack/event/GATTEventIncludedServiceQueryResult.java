package com.bluekitchen.btstack.event;

import com.bluekitchen.btstack.*;

public class GATTEventIncludedServiceQueryResult extends Event {

    public GATTEventIncludedServiceQueryResult(Packet packet) {
        super(packet);
    }
    
    /**
     * @return Handle as int
     */
    public int getHandle(){
        return Util.readBt16(data, 2);
    }

    /**
     * @return IncludeHandle as int
     */
    public int getIncludeHandle(){
        return Util.readBt16(data, 4);
    }

    /**
     * @return Service as GATTService
     */
    public GATTService getService(){
        return Util.readGattService(data, 6);
    }

    
    public String toString(){
        StringBuffer t = new StringBuffer();
        t.append("GATTEventIncludedServiceQueryResult < type = ");
        t.append(String.format("0x%02x, ", getEventType()));
        t.append(getEventType());
        t.append(", handle = ");
        t.append(getHandle());
        t.append(", include_handle = ");
        t.append(getIncludeHandle());
        t.append(", service = ");
        t.append(getService());
        t.append(" >");
        return t.toString();
    }

}
