package com.bluekitchen.btstack.event;

import com.bluekitchen.btstack.*;

public class GAPEventInquiryResult extends Event {

    public GAPEventInquiryResult(Packet packet) {
        super(packet);
    }
    
    /**
     * @return BdAddr as BD_ADDR
     */
    public BD_ADDR getBdAddr(){
        return Util.readBdAddr(data, 2);
    }

    /**
     * @return PageScanRepetitionMode as int
     */
    public int getPageScanRepetitionMode(){
        return Util.readByte(data, 8);
    }

    /**
     * @return ClassOfDevice as int
     */
    public int getClassOfDevice(){
        return Util.readBt24(data, 9);
    }

    /**
     * @return ClockOffset as int
     */
    public int getClockOffset(){
        return Util.readBt16(data, 12);
    }

    /**
     * @return RssiAvailable as int
     */
    public int getRssiAvailable(){
        return Util.readByte(data, 14);
    }

    /**
     * @return Rssi as int
     */
    public int getRssi(){
        return Util.readByte(data, 15);
    }

    /**
     * @return DeviceIdAvailable as int
     */
    public int getDeviceIdAvailable(){
        return Util.readByte(data, 16);
    }

    /**
     * @return DeviceIdVendorIdSource as int
     */
    public int getDeviceIdVendorIdSource(){
        return Util.readBt16(data, 17);
    }

    /**
     * @return DeviceIdVendorId as int
     */
    public int getDeviceIdVendorId(){
        return Util.readBt16(data, 19);
    }

    /**
     * @return DeviceIdProductId as int
     */
    public int getDeviceIdProductId(){
        return Util.readBt16(data, 21);
    }

    /**
     * @return DeviceIdVersion as int
     */
    public int getDeviceIdVersion(){
        return Util.readBt16(data, 23);
    }

    /**
     * @return NameAvailable as int
     */
    public int getNameAvailable(){
        return Util.readByte(data, 25);
    }

    /**
     * @return NameLen as int
     */
    public int getNameLen(){
        return Util.readByte(data, 26);
    }

    /**
     * @return Name as byte []
     */
    public byte [] getName(){
        int len = getNameLen();
        byte[] result = new byte[len];
        System.arraycopy(data, 27, result, 0, len);
        return result;
    }

    
    public String toString(){
        StringBuffer t = new StringBuffer();
        t.append("GAPEventInquiryResult < type = ");
        t.append(String.format("0x%02x, ", getEventType()));
        t.append(getEventType());
        t.append(", bd_addr = ");
        t.append(getBdAddr());
        t.append(", page_scan_repetition_mode = ");
        t.append(getPageScanRepetitionMode());
        t.append(", class_of_device = ");
        t.append(getClassOfDevice());
        t.append(", clock_offset = ");
        t.append(getClockOffset());
        t.append(", rssi_available = ");
        t.append(getRssiAvailable());
        t.append(", rssi = ");
        t.append(getRssi());
        t.append(", device_id_available = ");
        t.append(getDeviceIdAvailable());
        t.append(", device_id_vendor_id_source = ");
        t.append(getDeviceIdVendorIdSource());
        t.append(", device_id_vendor_id = ");
        t.append(getDeviceIdVendorId());
        t.append(", device_id_product_id = ");
        t.append(getDeviceIdProductId());
        t.append(", device_id_version = ");
        t.append(getDeviceIdVersion());
        t.append(", name_available = ");
        t.append(getNameAvailable());
        t.append(", name_len = ");
        t.append(getNameLen());
        t.append(", name = ");
        t.append(getName());
        t.append(" >");
        return t.toString();
    }

}
