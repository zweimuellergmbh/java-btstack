package com.bluekitchen.btstack.event;

import com.bluekitchen.btstack.*;

public class HCIEventScoCanSendNow extends Event {

    public HCIEventScoCanSendNow(Packet packet) {
        super(packet);
    }
    
    /**
     * @return Handle as BD_ADDR
     */
    public BD_ADDR getHandle(){
        return Util.readBdAddr(data, 2);
    }

    
    public String toString(){
        StringBuffer t = new StringBuffer();
        t.append("HCIEventScoCanSendNow < type = ");
        t.append(String.format("0x%02x, ", getEventType()));
        t.append(getEventType());
        t.append(", handle = ");
        t.append(getHandle());
        t.append(" >");
        return t.toString();
    }

}
