package com.bluekitchen.btstack.event;

import com.bluekitchen.btstack.*;

public class ATTEventHandleValueIndicationComplete extends Event {

    public ATTEventHandleValueIndicationComplete(Packet packet) {
        super(packet);
    }
    
    /**
     * @return Status as int
     */
    public int getStatus(){
        return Util.readByte(data, 2);
    }

    /**
     * @return ConnHandle as int
     */
    public int getConnHandle(){
        return Util.readBt16(data, 3);
    }

    /**
     * @return AttributeHandle as int
     */
    public int getAttributeHandle(){
        return Util.readBt16(data, 5);
    }

    
    public String toString(){
        StringBuffer t = new StringBuffer();
        t.append("ATTEventHandleValueIndicationComplete < type = ");
        t.append(String.format("0x%02x, ", getEventType()));
        t.append(getEventType());
        t.append(", status = ");
        t.append(getStatus());
        t.append(", conn_handle = ");
        t.append(getConnHandle());
        t.append(", attribute_handle = ");
        t.append(getAttributeHandle());
        t.append(" >");
        return t.toString();
    }

}
