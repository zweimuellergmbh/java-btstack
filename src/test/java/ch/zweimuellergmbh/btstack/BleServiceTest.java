package ch.zweimuellergmbh.btstack;

import com.bluekitchen.btstack.BD_ADDR;
import com.bluekitchen.btstack.BT_UUID;
import com.bluekitchen.btstack.BTstack;
import com.bluekitchen.btstack.GATTService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import static ch.zweimuellergmbh.btstack.TestHelper.STATUS_OK;
import static com.google.common.truth.Truth.assertThat;
import static org.junit.Assert.*;

public class BleServiceTest {
    private final TestHelper testHelper = new TestHelper();
    public static final int STATUS_ERROR = 1; // anything not 0

    @Before
    public void setup() {
    }

    @After
    public void teardown() {
    }

    /**
     * SYNC DISCOVER CHARACTERISTICS
     */
    @Test
    public void discoverCharacteristicsSync_throws_ifNotConnected() {
        BleDevice bleDevice = Mockito.mock(BleDevice.class);
        GATTService gattService = Mockito.mock(GATTService.class);

        Mockito.when(bleDevice.isConnected()).thenReturn(false);

        BleService bleService = new BleService(bleDevice, gattService);

        try {
            bleService.discoverCharacteristics();
            fail();
        } catch (Throwable e) {
            assertThat(e).isInstanceOf(BleGattException.class);
            assertThat(e.getMessage()).contains("GATT Request not possible. Device is not connected.");
        }
    }

    @Test
    public void whenDiscoverCharacteristicsSync_successfully() throws TimeoutException, BleGattException {
        final BleAdapter bleAdapter = new BleAdapter.BleAdapterBuilder(Mockito.mock(BTstack.class), IOCapabilities.IO_CAP_KEYBOARD_ONLY, null)
                .useExternalBtStackServer(true).build();
        final BleDevice bleDevice = Mockito.mock(BleDevice.class);

        Mockito.when(bleDevice.getBleAdapter()).thenReturn(bleAdapter);
        Mockito.when(bleDevice.isConnected()).thenReturn(true);
        Mockito.when(bleDevice.getConnectionHandle()).thenReturn(23);

        final GATTService gattService = Mockito.mock(GATTService.class);
        BleService bleService = new BleService(bleDevice, gattService);

        // set adapter to "INITIALIZED" state
        bleAdapter.handlePacket(testHelper.createBtStackEventPackage(BleAdapter.BTSTACK_WORKING));

        // first, the event with the found characteristic
        testHelper.receiveBtStackPacketsInMilliseconds(100, bleDevice.getBleAdapter(),
                testHelper.createGattCharacteristicQueryResultEvent(23));

        // second, the complete event
        testHelper.receiveBtStackPacketsInMilliseconds(500, bleDevice.getBleAdapter(),
                testHelper.createGattQueryCompleteEvent(STATUS_OK, 23));

        List<BleCharacteristic> bleCharacteristics = bleService.discoverCharacteristics();

        assertThat(bleCharacteristics.size()).isEqualTo(1);
        assertThat(bleCharacteristics.get(0).getInternalCharacteristicReference().getStartHandle()).isEqualTo(257);
    }

    @Test
    public void whenDiscoverCharacteristicsSync_fails_throws_gattException() {
        final BleAdapter bleAdapter = new BleAdapter.BleAdapterBuilder(Mockito.mock(BTstack.class), IOCapabilities.IO_CAP_KEYBOARD_ONLY, null)
                .useExternalBtStackServer(true).build();
        final BleDevice bleDevice = Mockito.mock(BleDevice.class);

        BleAddress bleAddress = new BleAddress(new BD_ADDR("AA:BB:CC:DD:EE:FF"), BleAddress.AddressType.RANDOM);
        Mockito.when(bleDevice.getBleAddress()).thenReturn(bleAddress);
        Mockito.when(bleDevice.getBleAdapter()).thenReturn(bleAdapter);
        Mockito.when(bleDevice.isConnected()).thenReturn(true);
        Mockito.when(bleDevice.getConnectionHandle()).thenReturn(23);

        final GATTService gattService = Mockito.mock(GATTService.class);
        BleService bleService = new BleService(bleDevice, gattService);

        // set adapter to "INITIALIZED" state
        bleAdapter.handlePacket(testHelper.createBtStackEventPackage(BleAdapter.BTSTACK_WORKING));

        // the complete event
        testHelper.receiveBtStackPacketsInMilliseconds(200, bleDevice.getBleAdapter(),
                testHelper.createGattQueryCompleteEvent(STATUS_ERROR, 23));

        try {
            bleService.discoverCharacteristics();
            fail();
        } catch (Throwable t) {
            assertThat(t).isInstanceOf(BleGattException.class);
        }
    }

    @Test
    public void whenDiscoverCharacteristicsSync_timesOut_throws_timeoutException() {
        final BleAdapter bleAdapter = new BleAdapter.BleAdapterBuilder(Mockito.mock(BTstack.class), IOCapabilities.IO_CAP_KEYBOARD_ONLY, null)
                .gattTimeoutMs(1000).connectTimeoutMs(1000).useExternalBtStackServer(true).build();
        final BleDevice bleDevice = Mockito.mock(BleDevice.class);

        Mockito.when(bleDevice.getBleAdapter()).thenReturn(bleAdapter);
        Mockito.when(bleDevice.isConnected()).thenReturn(true);
        Mockito.when(bleDevice.getConnectionHandle()).thenReturn(23);

        final GATTService gattService = Mockito.mock(GATTService.class);
        BleService bleService = new BleService(bleDevice, gattService);

        try {
            bleService.discoverCharacteristics();
            fail();
        } catch (TimeoutException | BleGattException e) {
            assertThat(e.getMessage()).startsWith("Timeout discover characteristics of");
        }
    }

    /**
     * ASYNC DISCOVER CHARACTERISTICS
     */
    @Test
    public void discoverCharacteristicsAsync_throws_ifNotConnected() {
        BleDevice bleDevice = Mockito.mock(BleDevice.class);
        GATTService gattService = Mockito.mock(GATTService.class);

        Mockito.when(bleDevice.isConnected()).thenReturn(false);

        BleService bleService = new BleService(bleDevice, gattService);

        try {
            bleService.discoverCharacteristicsAsync().toCompletableFuture().get();
            fail();
        } catch (Throwable e) {
            assertThat(e.getCause()).isInstanceOf(BleGattException.class);
            assertThat(e.getMessage()).contains("GATT Request not possible. Device is not connected.");
        }
    }

    @Test
    public void discoverCharacteristicsAsync_Successfully() throws ExecutionException, InterruptedException {
        final BleAdapter bleAdapter = new BleAdapter.BleAdapterBuilder(Mockito.mock(BTstack.class), IOCapabilities.IO_CAP_KEYBOARD_ONLY, null)
                .useExternalBtStackServer(true).build();
        final BleDevice bleDevice = Mockito.mock(BleDevice.class);

        Mockito.when(bleDevice.getBleAdapter()).thenReturn(bleAdapter);
        Mockito.when(bleDevice.isConnected()).thenReturn(true);
        Mockito.when(bleDevice.getConnectionHandle()).thenReturn(23);

        final GATTService gattService = Mockito.mock(GATTService.class);
        BleService bleService = new BleService(bleDevice, gattService);

        // set adapter to "INITIALIZED" state
        bleAdapter.handlePacket(testHelper.createBtStackEventPackage(BleAdapter.BTSTACK_WORKING));

        // first, the event with the found characteristic
        testHelper.receiveBtStackPacketsInMilliseconds(100, bleDevice.getBleAdapter(),
                testHelper.createGattCharacteristicQueryResultEvent(23));

        // second, the complete event
        testHelper.receiveBtStackPacketsInMilliseconds(500, bleDevice.getBleAdapter(),
                testHelper.createGattQueryCompleteEvent(STATUS_OK, 23));

        List<BleCharacteristic> bleCharacteristics = bleService.discoverCharacteristicsAsync().toCompletableFuture().get();

        assertThat(bleCharacteristics.size()).isEqualTo(1);
        assertThat(bleCharacteristics.get(0).getInternalCharacteristicReference().getStartHandle()).isEqualTo(257);
    }

    @Test
    public void whenDiscoverCharacteristicsAsync_fails_throws_gattException() {
        final BleAdapter bleAdapter = new BleAdapter.BleAdapterBuilder(Mockito.mock(BTstack.class), IOCapabilities.IO_CAP_KEYBOARD_ONLY, null)
                .useExternalBtStackServer(true).build();
        final BleDevice bleDevice = Mockito.mock(BleDevice.class);

        BleAddress bleAddress = new BleAddress(new BD_ADDR("AA:BB:CC:DD:EE:FF"), BleAddress.AddressType.RANDOM);
        Mockito.when(bleDevice.getBleAddress()).thenReturn(bleAddress);
        Mockito.when(bleDevice.getBleAdapter()).thenReturn(bleAdapter);
        Mockito.when(bleDevice.isConnected()).thenReturn(true);
        Mockito.when(bleDevice.getConnectionHandle()).thenReturn(23);

        final GATTService gattService = Mockito.mock(GATTService.class);
        BleService bleService = new BleService(bleDevice, gattService);

        // set adapter to "INITIALIZED" state
        bleAdapter.handlePacket(testHelper.createBtStackEventPackage(BleAdapter.BTSTACK_WORKING));

        // the complete event
        testHelper.receiveBtStackPacketsInMilliseconds(200, bleDevice.getBleAdapter(),
                testHelper.createGattQueryCompleteEvent(STATUS_ERROR, 23));

        try {
            bleService.discoverCharacteristicsAsync().toCompletableFuture().get();
            fail();
        } catch (Throwable t) {
            assertThat(t.getCause()).isInstanceOf(BleGattException.class);
        }
    }


    /**
     * SYNC DISCOVER CHARACTERISTIC BY UUID
     */
    @Test
    public void discoverCharacteristicByUUIDSync_throws_ifNotConnected() {
        BleDevice bleDevice = Mockito.mock(BleDevice.class);
        GATTService gattService = Mockito.mock(GATTService.class);

        Mockito.when(bleDevice.isConnected()).thenReturn(false);

        BleService bleService = new BleService(bleDevice, gattService);

        try {
            bleService.getCharacteristicByUUID(Mockito.mock(BT_UUID.class));
            fail();
        } catch (Throwable e) {
            assertThat(e).isInstanceOf(BleGattException.class);
            assertThat(e.getMessage()).contains("GATT Request not possible. Device is not connected.");
        }
    }

    @Test
    public void whenDiscoverCharacteristicByUUIDSync_successfully() throws TimeoutException, BleGattException {
        final BleAdapter bleAdapter = new BleAdapter.BleAdapterBuilder(Mockito.mock(BTstack.class), IOCapabilities.IO_CAP_KEYBOARD_ONLY, null)
                .useExternalBtStackServer(true).build();
        final BleDevice bleDevice = Mockito.mock(BleDevice.class);

        Mockito.when(bleDevice.getBleAdapter()).thenReturn(bleAdapter);
        Mockito.when(bleDevice.isConnected()).thenReturn(true);
        Mockito.when(bleDevice.getConnectionHandle()).thenReturn(23);

        final GATTService gattService = Mockito.mock(GATTService.class);
        BleService bleService = new BleService(bleDevice, gattService);

        // set adapter to "INITIALIZED" state
        bleAdapter.handlePacket(testHelper.createBtStackEventPackage(BleAdapter.BTSTACK_WORKING));

        // first, the event with the found characteristic
        testHelper.receiveBtStackPacketsInMilliseconds(100, bleDevice.getBleAdapter(),
                testHelper.createGattCharacteristicQueryResultEvent(23));

        // second, the complete event
        testHelper.receiveBtStackPacketsInMilliseconds(500, bleDevice.getBleAdapter(),
                testHelper.createGattQueryCompleteEvent(STATUS_OK, 23));

        BleCharacteristic bleCharacteristic = bleService.getCharacteristicByUUID(Mockito.mock(BT_UUID.class));

        assertThat(bleCharacteristic).isNotNull();
        assertThat(bleCharacteristic.getInternalCharacteristicReference().getStartHandle()).isEqualTo(257);
    }

    @Test
    public void whenDiscoverCharacteristicByUUIDSync_fails_throws_gattException() {
        final BleAdapter bleAdapter = new BleAdapter.BleAdapterBuilder(Mockito.mock(BTstack.class), IOCapabilities.IO_CAP_KEYBOARD_ONLY, null)
                .useExternalBtStackServer(true).build();
        final BleDevice bleDevice = Mockito.mock(BleDevice.class);

        BleAddress bleAddress = new BleAddress(new BD_ADDR("AA:BB:CC:DD:EE:FF"), BleAddress.AddressType.RANDOM);
        Mockito.when(bleDevice.getBleAddress()).thenReturn(bleAddress);
        Mockito.when(bleDevice.getBleAdapter()).thenReturn(bleAdapter);
        Mockito.when(bleDevice.isConnected()).thenReturn(true);
        Mockito.when(bleDevice.getConnectionHandle()).thenReturn(23);

        final GATTService gattService = Mockito.mock(GATTService.class);
        BleService bleService = new BleService(bleDevice, gattService);

        // set adapter to "INITIALIZED" state
        bleAdapter.handlePacket(testHelper.createBtStackEventPackage(BleAdapter.BTSTACK_WORKING));

        // second, the complete event
        testHelper.receiveBtStackPacketsInMilliseconds(500, bleDevice.getBleAdapter(),
                testHelper.createGattQueryCompleteEvent(STATUS_ERROR, 23));

        try {
            bleService.getCharacteristicByUUID(Mockito.mock(BT_UUID.class));
            fail();
        } catch (Throwable t) {
            assertThat(t).isInstanceOf(BleGattException.class);
        }
    }

    @Test
    public void whenDiscoverCharacteristicByUUIDSync_timesOut_throws_timeoutException() {
        final BleAdapter bleAdapter = new BleAdapter.BleAdapterBuilder(Mockito.mock(BTstack.class), IOCapabilities.IO_CAP_KEYBOARD_ONLY, null)
                .gattTimeoutMs(500).connectTimeoutMs(1000).useExternalBtStackServer(true).build();
        final BleDevice bleDevice = Mockito.mock(BleDevice.class);

        Mockito.when(bleDevice.getBleAdapter()).thenReturn(bleAdapter);
        Mockito.when(bleDevice.isConnected()).thenReturn(true);
        Mockito.when(bleDevice.getConnectionHandle()).thenReturn(23);

        final GATTService gattService = Mockito.mock(GATTService.class);
        final BleService bleService = new BleService(bleDevice, gattService);

        try {
            bleService.getCharacteristicByUUID(Mockito.mock(BT_UUID.class));
            fail();
        } catch (TimeoutException | BleGattException e) {
            assertThat(e.getMessage()).startsWith("Timeout get characteristic");
        }
    }

    /**
     * ASYNC DISCOVER CHARACTERISTICS BY UUID
     */
    @Test
    public void discoverCharacteristicsByUUIDAsync_throws_ifNotConnected() {
        BleDevice bleDevice = Mockito.mock(BleDevice.class);
        GATTService gattService = Mockito.mock(GATTService.class);

        Mockito.when(bleDevice.isConnected()).thenReturn(false);

        BleService bleService = new BleService(bleDevice, gattService);

        try {
            bleService.getCharacteristicByUUIDAsync(Mockito.mock(BT_UUID.class)).toCompletableFuture().get();
            fail();
        } catch (Throwable e) {
            assertThat(e.getCause()).isInstanceOf(BleGattException.class);
            assertThat(e.getMessage()).contains("GATT Request not possible. Device is not connected.");
        }
    }

    @Test
    public void discoverCharacteristicsByUUIDAsync_Successfully() throws ExecutionException, InterruptedException {
        final BleAdapter bleAdapter = new BleAdapter.BleAdapterBuilder(Mockito.mock(BTstack.class), IOCapabilities.IO_CAP_KEYBOARD_ONLY, null)
                .useExternalBtStackServer(true).build();
        final BleDevice bleDevice = Mockito.mock(BleDevice.class);

        Mockito.when(bleDevice.getBleAdapter()).thenReturn(bleAdapter);
        Mockito.when(bleDevice.isConnected()).thenReturn(true);
        Mockito.when(bleDevice.getConnectionHandle()).thenReturn(23);

        final GATTService gattService = Mockito.mock(GATTService.class);
        BleService bleService = new BleService(bleDevice, gattService);

        // set adapter to "INITIALIZED" state
        bleAdapter.handlePacket(testHelper.createBtStackEventPackage(BleAdapter.BTSTACK_WORKING));

        // first, the event with the found characteristic
        testHelper.receiveBtStackPacketsInMilliseconds(100, bleDevice.getBleAdapter(),
                testHelper.createGattCharacteristicQueryResultEvent(23));

        // second, the complete event
        testHelper.receiveBtStackPacketsInMilliseconds(500, bleDevice.getBleAdapter(),
                testHelper.createGattQueryCompleteEvent(STATUS_OK, 23));

        BleCharacteristic bleCharacteristics = bleService.getCharacteristicByUUIDAsync(Mockito.mock(BT_UUID.class)).toCompletableFuture().get();

        assertThat(bleCharacteristics).isNotNull();
        assertThat(bleCharacteristics.getInternalCharacteristicReference().getStartHandle()).isEqualTo(257);
    }

    // TODO test not found

    @Test
    public void whenDiscoverCharacteristicsByUUIDAsync_fails_throws_gattException() {
        final BleAdapter bleAdapter = new BleAdapter.BleAdapterBuilder(Mockito.mock(BTstack.class), IOCapabilities.IO_CAP_KEYBOARD_ONLY, null)
                .useExternalBtStackServer(true).build();
        final BleDevice bleDevice = Mockito.mock(BleDevice.class);

        BleAddress bleAddress = new BleAddress(new BD_ADDR("AA:BB:CC:DD:EE:FF"), BleAddress.AddressType.RANDOM);
        Mockito.when(bleDevice.getBleAddress()).thenReturn(bleAddress);
        Mockito.when(bleDevice.getBleAdapter()).thenReturn(bleAdapter);
        Mockito.when(bleDevice.isConnected()).thenReturn(true);
        Mockito.when(bleDevice.getConnectionHandle()).thenReturn(23);

        final GATTService gattService = Mockito.mock(GATTService.class);
        BleService bleService = new BleService(bleDevice, gattService);

        // set adapter to "INITIALIZED" state
        bleAdapter.handlePacket(testHelper.createBtStackEventPackage(BleAdapter.BTSTACK_WORKING));

        // second, the complete event
        testHelper.receiveBtStackPacketsInMilliseconds(500, bleDevice.getBleAdapter(),
                testHelper.createGattQueryCompleteEvent(STATUS_ERROR, 23));

        try {
            bleService.getCharacteristicByUUIDAsync(Mockito.mock(BT_UUID.class)).toCompletableFuture().get();
            fail();
        } catch (Throwable t) {
            assertThat(t.getCause()).isInstanceOf(BleGattException.class);
        }
    }
}