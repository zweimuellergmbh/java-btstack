package ch.zweimuellergmbh.btstack;

import com.bluekitchen.btstack.*;
import org.junit.*;
import org.mockito.Mockito;

import java.util.concurrent.*;

import static ch.zweimuellergmbh.btstack.TestHelper.STATUS_OK;
import static com.google.common.truth.Truth.assertThat;
import static org.junit.Assert.fail;

public class BleDeviceConnectTest {
    public static final int STATUS_ERROR = 1; // anything not 0

    private final TestHelper testHelper = new TestHelper();
    @Before
    public void setup() {
    }

    @After
    public void teardown() { }

    /**
     * SYNC REQUESTS
     */
    @Test
    public void whenConnectSync_TimesOut_throwsTimeoutException() {
        BleAddress bleAddress = Mockito.mock(BleAddress.class);
        BleAdapter bleAdapter = Mockito.mock(BleAdapter.class);

        BleDevice bleDevice = new BleDevice(bleAddress, bleAdapter);

        try {
            bleDevice.connect();
        } catch (TimeoutException | BleConnectException t) {
            assertThat(t.getMessage()).startsWith("Timeout connecting to:");
        }
    }

    @Test
    public void whenConnectSync_Fails_throwsBleConnectException() {
        final BleAddress bleAddress = new BleAddress(Mockito.mock(BD_ADDR.class), BleAddress.AddressType.RANDOM);
        final BTstack bTstack = Mockito.mock(BTstack.class);
        final BleAdapter bleAdapter = new BleAdapter.BleAdapterBuilder(bTstack, IOCapabilities.IO_CAP_KEYBOARD_ONLY, null)
                .useExternalBtStackServer(true).build();

        Event event = testHelper.createBtStackEventPackage(BleAdapter.BTSTACK_WORKING);
        bleAdapter.handlePacket(event);

        BleDevice bleDevice = new BleDevice(bleAddress, bleAdapter);

        // fail the connect request in 200 ms
        testHelper.receiveBtStackPacketsInMilliseconds(200, bleAdapter,
                testHelper.createConnectionCompleteEvent(STATUS_ERROR, -1));

        try {
            bleDevice.connect();
            fail();
        } catch (Throwable t) {
            assertThat(t).isInstanceOf(BleConnectException.class);
        }
    }

    @Test
    public void connectSync_Successfully() throws TimeoutException, BleConnectException {
        final BleAddress bleAddress = new BleAddress(Mockito.mock(BD_ADDR.class), BleAddress.AddressType.RANDOM);
        final BTstack bTstack = Mockito.mock(BTstack.class);
        final BleAdapter bleAdapter = new BleAdapter.BleAdapterBuilder(bTstack, IOCapabilities.IO_CAP_KEYBOARD_ONLY, null)
                .useExternalBtStackServer(true).build();

        Event event = testHelper.createBtStackEventPackage(BleAdapter.BTSTACK_WORKING);
        bleAdapter.handlePacket(event);

        BleDevice bleDevice = new BleDevice(bleAddress, bleAdapter);

        testHelper.receiveBtStackPacketsInMilliseconds(200, bleAdapter,
                testHelper.createConnectionCompleteEvent(STATUS_OK, 23));

        bleDevice.connect();

        assertThat(bleDevice.isConnected()).isTrue();
        assertThat(bleDevice.getConnectionHandle()).isEqualTo(23);
    }

    /**
     * ASYNC REQUESTS
     */
    @Test
    public void connectAsync_Successfully() throws ExecutionException, InterruptedException {
        final BleAddress bleAddress = new BleAddress(Mockito.mock(BD_ADDR.class), BleAddress.AddressType.RANDOM);
        final BTstack bTstack = Mockito.mock(BTstack.class);
        final BleAdapter bleAdapter = new BleAdapter.BleAdapterBuilder(bTstack, IOCapabilities.IO_CAP_KEYBOARD_ONLY, null)
                .useExternalBtStackServer(true).build();

        Event event = testHelper.createBtStackEventPackage(BleAdapter.BTSTACK_WORKING);
        bleAdapter.handlePacket(event);

        BleDevice bleDevice = new BleDevice(bleAddress, bleAdapter);

        testHelper.receiveBtStackPacketsInMilliseconds(200, bleAdapter,
                testHelper.createConnectionCompleteEvent(STATUS_OK, 23));

        bleDevice.connectAsync().toCompletableFuture().get();

        assertThat(bleDevice.isConnected()).isTrue();
        assertThat(bleDevice.getConnectionHandle()).isEqualTo(23);
    }

    @Test
    public void whenConnectAsync_Fails_throwsBleConnectException() {
        final BleAddress bleAddress = new BleAddress(Mockito.mock(BD_ADDR.class), BleAddress.AddressType.RANDOM);
        final BTstack bTstack = Mockito.mock(BTstack.class);
        final BleAdapter bleAdapter = new BleAdapter.BleAdapterBuilder(bTstack, IOCapabilities.IO_CAP_KEYBOARD_ONLY, null)
                .useExternalBtStackServer(true).build();

        Event event = testHelper.createBtStackEventPackage(BleAdapter.BTSTACK_WORKING);
        bleAdapter.handlePacket(event);

        BleDevice bleDevice = new BleDevice(bleAddress, bleAdapter);

        // fail the connect request in one second
        testHelper.receiveBtStackPacketsInMilliseconds(200, bleAdapter,
                testHelper.createConnectionCompleteEvent(STATUS_ERROR, -1));

        try {
            bleDevice.connectAsync().toCompletableFuture().get();
            fail();
        } catch (InterruptedException | ExecutionException e) {
            assertThat(e.getCause()).isInstanceOf(BleConnectException.class);
        }
    }
}