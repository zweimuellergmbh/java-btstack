package ch.zweimuellergmbh.btstack;

import ch.zweimuellergmbh.btstack.command.BleConnectCommand;
import ch.zweimuellergmbh.btstack.command.GattResponse;
import com.bluekitchen.btstack.BD_ADDR;
import com.bluekitchen.btstack.BTstack;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import static com.google.common.truth.Truth.assertThat;
import static org.junit.Assert.fail;

public class BleAdapterTest {
    private final TestHelper testHelper = new TestHelper();

    @Test
    public void adapterInitiallyNotInitialized() {
        final BleAdapter bleAdapter = new BleAdapter.BleAdapterBuilder(Mockito.mock(BTstack.class), IOCapabilities.IO_CAP_KEYBOARD_ONLY, null)
                .useExternalBtStackServer(true).build();

        assertThat(bleAdapter.isInitialized()).isFalse();
    }

    @Test
    public void adapterIsInitialized_whenBtStackPoweredUp() {
        final BleAdapter bleAdapter = new BleAdapter.BleAdapterBuilder(Mockito.mock(BTstack.class), IOCapabilities.IO_CAP_KEYBOARD_ONLY, null)
                .useExternalBtStackServer(true).build();

        bleAdapter.handlePacket(testHelper.createBtStackEventPackage(BleAdapter.BTSTACK_WORKING));

        assertThat(bleAdapter.isInitialized()).isTrue();
    }

    @Test(expected = BtStackException.class)
    public void powerUpBlocks_andThrows_ifTimeout() throws BtStackException {
        final BTstack btstack = Mockito.mock(BTstack.class);
        Mockito.when(btstack.connect()).thenReturn(true);
        final BleAdapter bleAdapter = new BleAdapter.BleAdapterBuilder(btstack, IOCapabilities.IO_CAP_KEYBOARD_ONLY, null)
                .useExternalBtStackServer(true).build();

        bleAdapter.powerOnBtStack();
    }

    @Test
    public void powerUpBlocks_untilBtStackPowered() throws BtStackException {
        final BTstack btstack = Mockito.mock(BTstack.class);
        Mockito.when(btstack.connect()).thenReturn(true);
        final BleAdapter bleAdapter = new BleAdapter.BleAdapterBuilder(btstack, IOCapabilities.IO_CAP_KEYBOARD_ONLY, null)
                .useExternalBtStackServer(true).build();

        bleAdapter.handlePacket(testHelper.createBtStackEventPackage(BleAdapter.BTSTACK_WORKING));

        bleAdapter.powerOnBtStack();
    }

    @Test
    public void canNotExecuteConnectionCommand_whenConnectionRequestIsPending() {
        final BTstack btstack = Mockito.mock(BTstack.class);
        final BleAdapter bleAdapter = new BleAdapter.BleAdapterBuilder(btstack, IOCapabilities.IO_CAP_KEYBOARD_ONLY, null)
                .useExternalBtStackServer(true).build();
        BleAddress bleAddress = new BleAddress(Mockito.mock(BD_ADDR.class), BleAddress.AddressType.RANDOM);
        BleDevice bleDevice = new BleDevice(bleAddress, bleAdapter);

        try {
            bleAdapter.executeBleConnectionCommand(new BleConnectCommand(bleAdapter, bleAddress), bleDevice);
        } catch (BleConnectException e) {
            fail();
        }

        try {
            bleAdapter.executeBleConnectionCommand(new BleConnectCommand(bleAdapter, bleAddress), bleDevice);
        } catch (BleConnectException e) {
            assertThat(e.getMessage()).isEqualTo("Connection in progress");
        }
    }

    @Test
    public void incomingConnectionEstablishedEvent_clearsPendingFlag() throws BleConnectException {
        final BTstack btstack = Mockito.mock(BTstack.class);
        final BleAdapter bleAdapter = new BleAdapter.BleAdapterBuilder(btstack, IOCapabilities.IO_CAP_KEYBOARD_ONLY, null)
                .useExternalBtStackServer(true).build();
        bleAdapter.handlePacket(testHelper.createBtStackEventPackage(BleAdapter.BTSTACK_WORKING));

        BleAddress bleAddress = new BleAddress(Mockito.mock(BD_ADDR.class), BleAddress.AddressType.RANDOM);
        BleDevice bleDevice = new BleDevice(bleAddress, bleAdapter);


        BleConnectCommand bleCommand = new BleConnectCommand(bleAdapter, bleAddress);
        bleCommand.registerResponseCallback(gattResponse -> { });

        bleAdapter.executeBleConnectionCommand(bleCommand, bleDevice);

        bleAdapter.handlePacket(testHelper.createConnectionCompleteEvent(GattResponse.ATT_ERROR_SUCCESS, 23));

        // can connect again
        bleAdapter.executeBleConnectionCommand(new BleConnectCommand(bleAdapter, bleAddress), bleDevice);
    }

    @Test
    public void canNotRunConcurrentRequestOnSameConnectionHandle() {
        final BleDevice bleDevice = testHelper.getConnectedDeviceForTest(23, 1000, 1000);

        try {
            CompletableFuture.anyOf(
                    bleDevice.discoverPrimaryServicesAsync().toCompletableFuture(),
                    bleDevice.discoverPrimaryServicesAsync().toCompletableFuture()
            ).get();
        } catch (InterruptedException | ExecutionException e) {
            assertThat(e.getCause()).isInstanceOf(BleGattException.class);
            assertThat(e.getCause().getMessage()).isEqualTo("GATT request in progress");
        }
    }
}