package ch.zweimuellergmbh.btstack;

import org.junit.Test;

import static com.google.common.truth.Truth.assertThat;

public class CharacteristicPropertiesTest {
    @Test
    public void allFalse() {
        final CharacteristicProperties properties = new CharacteristicProperties(0b0);

        assertThat(properties.isBroadcastCharacteristicValueAllowed()).isFalse();
        assertThat(properties.isRead()).isFalse();
        assertThat(properties.isWriteWithoutResponse()).isFalse();
        assertThat(properties.isWrite()).isFalse();
        assertThat(properties.isNotify()).isFalse();
        assertThat(properties.isIndicate()).isFalse();
        assertThat(properties.isSignedWrite()).isFalse();
        assertThat(properties.isQueuedWrite()).isFalse();
        assertThat(properties.isWriteableAuxiliaries()).isFalse();

        assertThat(properties.toString()).isEqualTo("[]");
    }

    @Test
    public void allPossible() {
        CharacteristicProperties properties = new CharacteristicProperties(0b111111111);
        assertThat(properties.isBroadcastCharacteristicValueAllowed()).isTrue();
        assertThat(properties.isRead()).isTrue();
        assertThat(properties.isWriteWithoutResponse()).isTrue();
        assertThat(properties.isWrite()).isTrue();
        assertThat(properties.isNotify()).isTrue();
        assertThat(properties.isIndicate()).isTrue();
        assertThat(properties.isSignedWrite()).isTrue();
        assertThat(properties.isQueuedWrite()).isTrue();
        assertThat(properties.isWriteableAuxiliaries()).isTrue();

        assertThat(properties.toString()).isEqualTo(
                "[BROADCAST_ALLOWED, READ, WRITE_W/O_RESPONSE, WRITE, NOTIFY, INDICATE, SIGNED_WRITE, QUEUED_WRITE, WRITEABLE_AUXILIARIES]"
        );
    }

    @Test
    public void readPossible() {
        CharacteristicProperties properties = new CharacteristicProperties(0b10);
        assertThat(properties.isBroadcastCharacteristicValueAllowed()).isFalse();
        assertThat(properties.isRead()).isTrue();
        assertThat(properties.isWriteWithoutResponse()).isFalse();
        assertThat(properties.isWrite()).isFalse();
        assertThat(properties.isNotify()).isFalse();
        assertThat(properties.isIndicate()).isFalse();
        assertThat(properties.isSignedWrite()).isFalse();
        assertThat(properties.isQueuedWrite()).isFalse();
        assertThat(properties.isWriteableAuxiliaries()).isFalse();

        assertThat(properties.toString()).isEqualTo(
                "[READ]"
        );
    }

    @Test
    public void read_write_indicate_queuedWrite_possible() {
        CharacteristicProperties properties = new CharacteristicProperties(0b010101010);
        assertThat(properties.isBroadcastCharacteristicValueAllowed()).isFalse();
        assertThat(properties.isRead()).isTrue();
        assertThat(properties.isWriteWithoutResponse()).isFalse();
        assertThat(properties.isWrite()).isTrue();
        assertThat(properties.isNotify()).isFalse();
        assertThat(properties.isIndicate()).isTrue();
        assertThat(properties.isSignedWrite()).isFalse();
        assertThat(properties.isQueuedWrite()).isTrue();
        assertThat(properties.isWriteableAuxiliaries()).isFalse();

        assertThat(properties.toString()).isEqualTo(
                "[READ, WRITE, INDICATE, QUEUED_WRITE]"
        );
    }
}