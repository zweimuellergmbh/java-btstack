package ch.zweimuellergmbh.btstack;

import org.junit.Test;

import java.util.function.Consumer;

import static com.google.common.truth.Truth.assertThat;

public class NotificationRegistryTest {

    public static final int CONNECTION_HANDLE_1 = 1;
    private static final int CONNECTION_HANDLE_2 = 2;

    public static final int VALUE_HANDLE_1 = 23;
    public static final int VALUE_HANDLE_2 = 24;

    final NotificationRegistry registry = new NotificationRegistry();

    @Test(expected = IllegalArgumentException.class)
    public void notificationCallbackMayNotBeNull() throws IllegalArgumentException {
        registry.registerNotificationCallback(CONNECTION_HANDLE_1, VALUE_HANDLE_1, null);
    }

    @Test
    public void getNotRegisteredCallback_returnsEmptyOptional() {
        assertThat(registry.getRegisteredCallback(CONNECTION_HANDLE_1, VALUE_HANDLE_1).isPresent()).isFalse();
    }

    @Test
    public void registerDifferentCallbackForSameConnection() throws IllegalArgumentException {
        final Consumer<byte[]> notificationCallback = byteArray -> {};
        final Consumer<byte[]> notificationCallback2 = byteArray -> {};

        registry.registerNotificationCallback(CONNECTION_HANDLE_1, VALUE_HANDLE_1, notificationCallback);
        registry.registerNotificationCallback(CONNECTION_HANDLE_1, VALUE_HANDLE_2, notificationCallback2);

        assertThat(notificationCallback).isNotEqualTo(notificationCallback2);
        assertThat(registry.getRegisteredCallback(CONNECTION_HANDLE_1, VALUE_HANDLE_1).get()).isEqualTo(notificationCallback);
        assertThat(registry.getRegisteredCallback(CONNECTION_HANDLE_1, VALUE_HANDLE_2).get()).isEqualTo(notificationCallback2);
    }

    @Test
    public void registerCallbackForDifferentConnections() throws IllegalArgumentException {
        final Consumer<byte[]> notificationCallback = byteArray -> {};
        final Consumer<byte[]> notificationCallback2 = byteArray -> {};

        registry.registerNotificationCallback(CONNECTION_HANDLE_1, VALUE_HANDLE_1, notificationCallback);
        registry.registerNotificationCallback(CONNECTION_HANDLE_2, VALUE_HANDLE_2, notificationCallback2);

        assertThat(registry.getRegisteredCallback(CONNECTION_HANDLE_1, VALUE_HANDLE_1).get()).isEqualTo(notificationCallback);
        assertThat(registry.getRegisteredCallback(CONNECTION_HANDLE_2, VALUE_HANDLE_2).get()).isEqualTo(notificationCallback2);
    }

    @Test
    public void callbackCanBeUnregistered() throws IllegalArgumentException {
        final Consumer<byte[]> notificationCallback = byteArray -> {};

        registry.registerNotificationCallback(CONNECTION_HANDLE_1, VALUE_HANDLE_1, notificationCallback);
        assertThat(registry.getRegisteredCallback(CONNECTION_HANDLE_1, VALUE_HANDLE_1).get()).isEqualTo(notificationCallback);

        // now, unregister, then it should not be found any more
        registry.unregisterNotificationCallback(CONNECTION_HANDLE_1, VALUE_HANDLE_1);
        assertThat(registry.getRegisteredCallback(CONNECTION_HANDLE_1, VALUE_HANDLE_1).isPresent()).isFalse();
    }

    @Test
    public void callbacksForOneConnectionCanBeUnregistered() throws IllegalArgumentException {
        final Consumer<byte[]> notificationCallback = byteArray -> {};
        final Consumer<byte[]> notificationCallback2 = byteArray -> {};

        // register for connection 1
        registry.registerNotificationCallback(CONNECTION_HANDLE_1, VALUE_HANDLE_1, notificationCallback);
        registry.registerNotificationCallback(CONNECTION_HANDLE_1, VALUE_HANDLE_2, notificationCallback2);
        // and register them also for connection 2
        registry.registerNotificationCallback(CONNECTION_HANDLE_2, VALUE_HANDLE_1, notificationCallback);
        registry.registerNotificationCallback(CONNECTION_HANDLE_2, VALUE_HANDLE_2, notificationCallback2);

        // they can be found on both connections
        assertThat(registry.getRegisteredCallback(CONNECTION_HANDLE_1, VALUE_HANDLE_1).get()).isEqualTo(notificationCallback);
        assertThat(registry.getRegisteredCallback(CONNECTION_HANDLE_1, VALUE_HANDLE_2).get()).isEqualTo(notificationCallback2);
        assertThat(registry.getRegisteredCallback(CONNECTION_HANDLE_2, VALUE_HANDLE_1).get()).isEqualTo(notificationCallback);
        assertThat(registry.getRegisteredCallback(CONNECTION_HANDLE_2, VALUE_HANDLE_2).get()).isEqualTo(notificationCallback2);

        // now, unregister all callback on connection 1
        registry.removeAllRegisteredCallbacksForConnection(CONNECTION_HANDLE_1);

        // now, callbacks for connection 1 not available any more; but still for connection 2
        assertThat(registry.getRegisteredCallback(CONNECTION_HANDLE_1, VALUE_HANDLE_1).isPresent()).isFalse();
        assertThat(registry.getRegisteredCallback(CONNECTION_HANDLE_1, VALUE_HANDLE_2).isPresent()).isFalse();
        assertThat(registry.getRegisteredCallback(CONNECTION_HANDLE_2, VALUE_HANDLE_1).get()).isEqualTo(notificationCallback);
        assertThat(registry.getRegisteredCallback(CONNECTION_HANDLE_2, VALUE_HANDLE_2).get()).isEqualTo(notificationCallback2);
    }
}
