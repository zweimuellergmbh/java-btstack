package ch.zweimuellergmbh.btstack;

import com.bluekitchen.btstack.BT_UUID;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import static ch.zweimuellergmbh.btstack.TestHelper.STATUS_OK;
import static com.google.common.truth.Truth.assertThat;
import static org.junit.Assert.fail;

public class BleDeviceDiscoverServiceTest {
    public static final int STATUS_ERROR = 1; // anything not 0
    private final TestHelper testHelper = new TestHelper();

    @Before
    public void setup() {
    }

    @After
    public void teardown() { }

    /**
     * SYNC DISCOVER PRIMARY SERVICES
     */
    @Test
    public void discoverPrimaryServicesSync_throws_ifNotConnected() {
        BleAddress bleAddress = Mockito.mock(BleAddress.class);
        BleAdapter bleAdapter = Mockito.mock(BleAdapter.class);

        BleDevice bleDevice = new BleDevice(bleAddress, bleAdapter);

        try {
            bleDevice.discoverPrimaryServices();
            fail();
        } catch (Throwable e) {
            assertThat(e).isInstanceOf(BleGattException.class);
            assertThat(e.getMessage()).contains("GATT Request not possible. Device is not connected.");
        }
    }

    @Test
    public void discoveryPrimaryServicesSync_Successfully() throws TimeoutException, BleGattException {
        final BleDevice bleDevice = testHelper.getConnectedDeviceForTest(23, 1000, 1000);

        // first, the event with the found service
        testHelper.receiveBtStackPacketsInMilliseconds(100, bleDevice.getBleAdapter(),
                testHelper.createGattServiceQueryResultEvent(23));

        // second, the complete event
        testHelper.receiveBtStackPacketsInMilliseconds(500, bleDevice.getBleAdapter(),
                testHelper.createGattQueryCompleteEvent(STATUS_OK, 23));

        List<BleService> bleServices = bleDevice.discoverPrimaryServices();

        assertThat(bleServices.size()).isEqualTo(1);
        assertThat(bleServices.get(0).getInternalGattServiceReference().getStartGroupHandle()).isEqualTo(256);
        assertThat(bleServices.get(0).getInternalGattServiceReference().getEndGroupHandle()).isEqualTo(2);
    }

    @Test
    public void whenDiscoverPrimaryServicesSync_Fails_throwsGattException() {
        final BleDevice bleDevice = testHelper.getConnectedDeviceForTest(23, 1000, 1000);

        // fail the request in 200 ms
        testHelper.receiveBtStackPacketsInMilliseconds(200, bleDevice.getBleAdapter(),
                testHelper.createGattQueryCompleteEvent(STATUS_ERROR, 23));

        try {
            bleDevice.discoverPrimaryServices();
            fail();
        } catch (Throwable t) {
            assertThat(t).isInstanceOf(BleGattException.class);
        }
    }

    @Test
    public void whenDiscoverPrimaryServicesSync_TimesOut_throwsTimeoutException() {
        final BleDevice bleDevice = testHelper.getConnectedDeviceForTest(23, 1000, 1000);

        try {
            bleDevice.discoverPrimaryServices();
            fail();
        } catch (TimeoutException | BleGattException e) {
            assertThat(e.getMessage()).startsWith("Timeout discover services");
        }
    }

    /**
     * ASYNC DISCOVER PRIMARY SERVICES
     */
    @Test
    public void discoverPrimaryServicesAsync_throws_ifNotConnected() {
        BleAddress bleAddress = Mockito.mock(BleAddress.class);
        BleAdapter bleAdapter = Mockito.mock(BleAdapter.class);

        BleDevice bleDevice = new BleDevice(bleAddress, bleAdapter);

        try {
            bleDevice.discoverPrimaryServicesAsync().toCompletableFuture().get();
            fail();
        } catch (Throwable e) {
            assertThat(e.getCause()).isInstanceOf(BleGattException.class);
            assertThat(e.getCause().getMessage()).contains("GATT Request not possible. Device is not connected.");
        }
    }

    @Test
    public void discoveryPrimaryServicesAsync_Successfully() throws ExecutionException, InterruptedException, BleGattException {
        final BleDevice bleDevice = testHelper.getConnectedDeviceForTest(23, 1000, 1000);

        // first, the event with the found service
        testHelper.receiveBtStackPacketsInMilliseconds(100, bleDevice.getBleAdapter(),
                testHelper.createGattServiceQueryResultEvent(23));

        // second, the complete event
        testHelper.receiveBtStackPacketsInMilliseconds(500, bleDevice.getBleAdapter(),
                testHelper.createGattQueryCompleteEvent(STATUS_OK, 23));

        List<BleService> bleServices = bleDevice.discoverPrimaryServicesAsync().toCompletableFuture().get();

        assertThat(bleServices.size()).isEqualTo(1);
        assertThat(bleServices.get(0).getInternalGattServiceReference().getStartGroupHandle()).isEqualTo(256);
        assertThat(bleServices.get(0).getInternalGattServiceReference().getEndGroupHandle()).isEqualTo(2);
    }

    @Test
    public void whenDiscoverPrimaryServicesAsync_Fails_throwsGattException() {
        final BleDevice bleDevice = testHelper.getConnectedDeviceForTest(23, 1000, 1000);

        // fail the request in 200 ms
        testHelper.receiveBtStackPacketsInMilliseconds(200, bleDevice.getBleAdapter(),
                testHelper.createGattQueryCompleteEvent(STATUS_ERROR, 23));

        try {
            bleDevice.discoverPrimaryServicesAsync().toCompletableFuture().get();
            fail();
        } catch (InterruptedException | ExecutionException e) {
            assertThat(e.getCause()).isInstanceOf(BleGattException.class);
        }
    }


    /**
     * SYNC DISCOVER PRIMARY SERVICE FOR UUID
     */
    @Test
    public void discoverPrimaryServiceByUUIDSync_throws_ifNotConnected() {
        BleAddress bleAddress = Mockito.mock(BleAddress.class);
        BleAdapter bleAdapter = Mockito.mock(BleAdapter.class);

        BleDevice bleDevice = new BleDevice(bleAddress, bleAdapter);

        try {
            bleDevice.discoverPrimaryServiceByUUID(Mockito.mock(BT_UUID.class));
            fail();
        } catch (Throwable e) {
            assertThat(e).isInstanceOf(BleGattException.class);
            assertThat(e.getMessage()).contains("GATT Request not possible. Device is not connected.");
        }
    }

    @Test
    public void discoveryPrimaryServiceByUUIDSync_successfully() throws TimeoutException, BleGattException {
        final BleDevice bleDevice = testHelper.getConnectedDeviceForTest(23, 1000, 1000);

        // first, the event with the found service
        testHelper.receiveBtStackPacketsInMilliseconds(100, bleDevice.getBleAdapter(),
                testHelper.createGattServiceQueryResultEvent(23));

        // second, the complete event
        testHelper.receiveBtStackPacketsInMilliseconds(500, bleDevice.getBleAdapter(),
                testHelper.createGattQueryCompleteEvent(STATUS_OK, 23));

        BleService bleService = bleDevice.discoverPrimaryServiceByUUID(Mockito.mock(BT_UUID.class));

        assertThat(bleService).isNotNull();
        assertThat(bleService.getInternalGattServiceReference().getStartGroupHandle()).isEqualTo(256);
        assertThat(bleService.getInternalGattServiceReference().getEndGroupHandle()).isEqualTo(2);
    }

    @Test
    public void whenDiscoverPrimarServiceByUUIDSync_fails_throwsGattException() {
        final BleDevice bleDevice = testHelper.getConnectedDeviceForTest(23, 1000, 1000);

        // fail the request in one second
        testHelper.receiveBtStackPacketsInMilliseconds(100, bleDevice.getBleAdapter(),
                testHelper.createGattQueryCompleteEvent(STATUS_ERROR, 23));

        try {
            bleDevice.discoverPrimaryServiceByUUID(Mockito.mock(BT_UUID.class));
            fail();
        } catch (Throwable t) {
            assertThat(t).isInstanceOf(BleGattException.class);
        }
    }

    @Test
    public void whenDiscoverPrimaryServiceByUUIDSync_timesOut_throwsTimeoutException() {
        final BleDevice bleDevice = testHelper.getConnectedDeviceForTest(23, 1000, 1000);

        try {
            bleDevice.discoverPrimaryServiceByUUID(Mockito.mock(BT_UUID.class));
            fail();
        } catch (TimeoutException | BleGattException e) {
            assertThat(e.getMessage()).startsWith("Timeout discover service by UUID");
        }
    }

    /**
     * ASYNC DISCOVER PRIMARY SERVICE FOR UUID
     */
    @Test
    public void discoverPrimaryServiceByUUIDAsync_throws_ifNotConnected() {
        BleAddress bleAddress = Mockito.mock(BleAddress.class);
        BleAdapter bleAdapter = Mockito.mock(BleAdapter.class);

        BleDevice bleDevice = new BleDevice(bleAddress, bleAdapter);

        try {
            bleDevice.discoverPrimaryServiceByUUIDAsync(Mockito.mock(BT_UUID.class)).toCompletableFuture().get();
            fail();
        } catch (Throwable e) {
            assertThat(e.getCause()).isInstanceOf(BleGattException.class);
            assertThat(e.getCause().getMessage()).contains("GATT Request not possible. Device is not connected.");
        }
    }

    @Test
    public void discoveryPrimarServiceByUUIDAsync_successfully() throws ExecutionException, InterruptedException, BleGattException {
        final BleDevice bleDevice = testHelper.getConnectedDeviceForTest(23, 1000, 1000);

        // first, the event with the found service
        testHelper.receiveBtStackPacketsInMilliseconds(100, bleDevice.getBleAdapter(),
                testHelper.createGattServiceQueryResultEvent(23));

        // second, the complete event
        testHelper.receiveBtStackPacketsInMilliseconds(500, bleDevice.getBleAdapter(),
                testHelper.createGattQueryCompleteEvent(STATUS_OK, 23));

        BleService bleService = bleDevice.discoverPrimaryServiceByUUIDAsync(Mockito.mock(BT_UUID.class)).toCompletableFuture().get();

        assertThat(bleService).isNotNull();
        assertThat(bleService.getInternalGattServiceReference().getStartGroupHandle()).isEqualTo(256);
        assertThat(bleService.getInternalGattServiceReference().getEndGroupHandle()).isEqualTo(2);
    }

    // TODO test not found

    @Test
    public void whenDiscoverPrimarServiceByUUIDAsync_fails_throwsGattException() {
        final BleDevice bleDevice = testHelper.getConnectedDeviceForTest(23, 1000, 1000);

        // fail the request in one second
        testHelper.receiveBtStackPacketsInMilliseconds(100, bleDevice.getBleAdapter(),
                testHelper.createGattQueryCompleteEvent(STATUS_ERROR, 23));

        try {
            bleDevice.discoverPrimaryServiceByUUIDAsync(Mockito.mock(BT_UUID.class)).toCompletableFuture().get();
            fail();
        } catch (InterruptedException | ExecutionException e) {
            assertThat(e.getCause()).isInstanceOf(BleGattException.class);
        }
    }
}