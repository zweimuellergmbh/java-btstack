package ch.zweimuellergmbh.btstack;

import com.bluekitchen.btstack.BD_ADDR;
import com.bluekitchen.btstack.BTstack;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.List;

import static com.google.common.truth.Truth.assertThat;

public class BleScannerTest {
    private final TestHelper testHelper = new TestHelper();
    private BleAdapter bleAdapter;
    private BleScanner bleScanner;

    @Before
    public void setup() {
        final BTstack btstack = Mockito.mock(BTstack.class);
        Mockito.when(btstack.connect()).thenReturn(true);
        bleAdapter = new BleAdapter.BleAdapterBuilder(btstack, IOCapabilities.IO_CAP_KEYBOARD_ONLY, null).useExternalBtStackServer(true).build();
        bleScanner = new BleScanner(bleAdapter);
    }

    @Test
    public void bleDeviceIsReturned_ifScannedNameIsFound() throws BtStackException, BleScanTimeoutException {
        testHelper.receiveBtStackPacketsInMilliseconds(100, bleAdapter,
                testHelper.createBtStackEventPackage(BleAdapter.BTSTACK_WORKING));

        bleAdapter.powerOnBtStack();

        // now initialized - send advertising in 100ms
        testHelper.receiveBtStackPacketsInMilliseconds(100, bleAdapter,
                testHelper.createGAPAdvertisingEventWithData("TEST".getBytes()));

        final BleDevice bleDevice = bleScanner.scanForDeviceWithName(1000, "TEST");

        assertThat(bleDevice).isNotNull();
    }

    @Test(expected = BleScanTimeoutException.class)
    public void scanForNameThrows_ifDeviceNameNotContainedInAdvData() throws BtStackException, BleScanTimeoutException {
        testHelper.receiveBtStackPacketsInMilliseconds(100, bleAdapter,
                testHelper.createBtStackEventPackage(BleAdapter.BTSTACK_WORKING));

        bleAdapter.powerOnBtStack();

        // now initialized - send advertising in 100ms
        testHelper.receiveBtStackPacketsInMilliseconds(100, bleAdapter,
                testHelper.createGAPAdvertisingEventWithData("TEST".getBytes()));

        bleScanner.scanForDeviceWithName(200, "***");
    }

    @Test(expected = BleScanException.class)
    public void scanForAddressThrows_ifInvalidAddressFormat() throws BtStackException, BleScanException, BleScanTimeoutException, DecoderException {
        testHelper.receiveBtStackPacketsInMilliseconds(100, bleAdapter,
                testHelper.createBtStackEventPackage(BleAdapter.BTSTACK_WORKING));

        bleAdapter.powerOnBtStack();

        // now initialized - send advertising in 100ms
        testHelper.receiveBtStackPacketsInMilliseconds(100, bleAdapter,
                testHelper.createGAPAdvertisingEventWithAddress(Hex.decodeHex("ffeeddccbbaa")));

        bleScanner.scanForDeviceWithAddress(200, "**");
    }

    @Test(expected = BleScanTimeoutException.class)
    public void scanForAddressThrows_ifScanTimesOut() throws BtStackException, BleScanException, BleScanTimeoutException, DecoderException {
        testHelper.receiveBtStackPacketsInMilliseconds(100, bleAdapter,
                testHelper.createBtStackEventPackage(BleAdapter.BTSTACK_WORKING));

        bleAdapter.powerOnBtStack();

        // now initialized - send advertising in 100ms
        testHelper.receiveBtStackPacketsInMilliseconds(100, bleAdapter,
                testHelper.createGAPAdvertisingEventWithAddress(Hex.decodeHex("ffeeddccbbaa")));

        bleScanner.scanForDeviceWithAddress(200, "42:42:42:42:42:42");
    }

    @Test
    public void scanForAddress_scanWithAddress() throws BtStackException, BleScanException, BleScanTimeoutException, DecoderException {
        testHelper.receiveBtStackPacketsInMilliseconds(100, bleAdapter,
                testHelper.createBtStackEventPackage(BleAdapter.BTSTACK_WORKING));

        bleAdapter.powerOnBtStack();

        // now initialized - send advertising in 100ms
        testHelper.receiveBtStackPacketsInMilliseconds(100, bleAdapter,
                testHelper.createGAPAdvertisingEventWithAddress(Hex.decodeHex("ffeeddccbbaa")));

        BleDevice bleDevice = bleScanner.scanForDeviceWithAddress(200, "aa:bb:cc:dd:ee:ff");

        assertThat(bleDevice.getBleAddress().getAddress()).isEqualTo(new BD_ADDR("aa:bb:cc:dd:ee:ff"));
    }

    @Test
    public void scanForDevicesReturnsEmpyList_whenNoDevicesFound() throws BtStackException, DecoderException {
        testHelper.receiveBtStackPacketsInMilliseconds(100, bleAdapter,
                testHelper.createBtStackEventPackage(BleAdapter.BTSTACK_WORKING));

        bleAdapter.powerOnBtStack();

        List<BleDevice> bleDevices = bleScanner.scanForDevices(500);
        assertThat(bleDevices).isEmpty();
    }

    @Test
    public void scanForDevicesReturnsTwoDevices_whenTwoDifferentAddressesFound() throws BtStackException, DecoderException {
        testHelper.receiveBtStackPacketsInMilliseconds(100, bleAdapter,
                testHelper.createBtStackEventPackage(BleAdapter.BTSTACK_WORKING));

        bleAdapter.powerOnBtStack();

        // now initialized - send advertising in 100ms
        testHelper.receiveBtStackPacketsInMilliseconds(100, bleAdapter,
                testHelper.createGAPAdvertisingEventWithAddress(Hex.decodeHex("ffeeddccbb11")));

        // now initialized - send advertising in 200ms
        testHelper.receiveBtStackPacketsInMilliseconds(300, bleAdapter,
                testHelper.createGAPAdvertisingEventWithAddress(Hex.decodeHex("ffeeddccbb22")));

        List<BleDevice> bleDevices = bleScanner.scanForDevices(500);

        assertThat(bleDevices).hasSize(2);
        assertThat(bleDevices.get(1).getBleAddress().getAddress()).isEqualTo(new BD_ADDR("11:bb:cc:dd:ee:ff"));
        assertThat(bleDevices.get(0).getBleAddress().getAddress()).isEqualTo(new BD_ADDR("22:bb:cc:dd:ee:ff"));
    }

    @Test
    public void scanForDevicesReturnsOneDevices_whenAdvForSameAddressReceived() throws BtStackException, DecoderException {
        testHelper.receiveBtStackPacketsInMilliseconds(100, bleAdapter,
                testHelper.createBtStackEventPackage(BleAdapter.BTSTACK_WORKING));

        bleAdapter.powerOnBtStack();

        // now initialized - send advertising in 100ms
        testHelper.receiveBtStackPacketsInMilliseconds(100, bleAdapter,
                testHelper.createGAPAdvertisingEventWithAddress(Hex.decodeHex("ffeeddccbb11")));

        // now initialized - send advertising in 200ms
        testHelper.receiveBtStackPacketsInMilliseconds(200, bleAdapter,
                testHelper.createGAPAdvertisingEventWithAddress(Hex.decodeHex("ffeeddccbb11")));

        List<BleDevice> bleDevices = bleScanner.scanForDevices(500);

        assertThat(bleDevices).hasSize(1);
        assertThat(bleDevices.get(0).getBleAddress().getAddress()).isEqualTo(new BD_ADDR("11:bb:cc:dd:ee:ff"));
    }

    @Test
    public void addressCheck_correctFormatIsOk() {
        assertThat(bleScanner.validate("11:22:33:44:55:66")).isTrue();
        // : is needed
        assertThat(bleScanner.validate("112233445566")).isFalse();
    }

    @Test
    public void addressCheck_tooShortAddressIsRejected() {
        assertThat(bleScanner.validate("11:22:33:44:55:6")).isFalse();
    }

    @Test
    public void addressCheck_tooLongAddressIsRejected() {
        assertThat(bleScanner.validate("11:22:33:44:55:66:")).isFalse();
    }

    @Test
    public void addressCheck_invalidCharactersRejected() {
        assertThat(bleScanner.validate("11:22:**:44:55:66:")).isFalse();
    }
}