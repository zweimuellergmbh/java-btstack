package ch.zweimuellergmbh.btstack;

import com.bluekitchen.btstack.BD_ADDR;
import com.bluekitchen.btstack.BTstack;
import com.bluekitchen.btstack.Event;
import com.bluekitchen.btstack.Packet;
import com.bluekitchen.btstack.event.*;
import org.mockito.Mockito;

import java.util.Arrays;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.fail;

public class TestHelper {
    public static final int STATUS_OK = 0;

    private ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();

    public BleDevice getConnectedDeviceForTest(int connectionHandle, int gattTimeout, int connectTimeout) {
        final BTstack bTstack = Mockito.mock(BTstack.class);
        final BleAddress bleAddress = new BleAddress(Mockito.mock(BD_ADDR.class), BleAddress.AddressType.RANDOM);
        final BleAdapter bleAdapter = new BleAdapter.BleAdapterBuilder(bTstack, IOCapabilities.IO_CAP_KEYBOARD_ONLY, null)
                .useExternalBtStackServer(true)
                .gattTimeoutMs(gattTimeout)
                .connectTimeoutMs(connectTimeout)
                .build();

        bleAdapter.handlePacket(createBtStackEventPackage(BleAdapter.BTSTACK_WORKING));

        BleDevice bleDevice = new BleDevice(bleAddress, bleAdapter);

        // connect ok event
        receiveBtStackPacketInMilliseconds(300, bleAdapter,
                createConnectionCompleteEvent(STATUS_OK, connectionHandle));

        try {
            bleDevice.connectAsync().toCompletableFuture().get();
        } catch (InterruptedException | ExecutionException e) {
            fail();
        }


        return bleDevice;
    }

    public void receiveBtStackPacketInMilliseconds(int millis, BleAdapter bleAdapter, Event event) {
        executor.schedule(() -> bleAdapter.handlePacket(event), millis, TimeUnit.MILLISECONDS);
    }

    public void receiveBtStackPacketsInMilliseconds(int millis, BleAdapter bleAdapter, Event ... event) {
        Arrays.stream(event).forEach(e -> executor.schedule(() -> bleAdapter.handlePacket(e), millis, TimeUnit.MILLISECONDS));
    }

    public BTstackEventState createBtStackEventPackage(int state) {
        return new BTstackEventState(
                new Packet(0, 0, new byte[]{1, 1, (byte) state}, 3)
        );
    }

    public GAPEventAdvertisingReport createGAPAdvertisingEventWithData(final byte[] data) {
        byte[] payload = new byte[12 + data.length];
        payload[11] = (byte) data.length;
        System.arraycopy(data, 0, payload, 12, data.length);

        return new GAPEventAdvertisingReport(
                new Packet(0, 0, payload, payload.length)
        );
    }

    public GAPEventAdvertisingReport createGAPAdvertisingEventWithAddress(final byte[] address) {
        byte[] payload = new byte[12 + 10]; // 10 for data
        payload[11] = 4;
        System.arraycopy("TEST".getBytes(), 0, payload, 12, 4);
        System.arraycopy(address, 0, payload, 4, address.length);

        return new GAPEventAdvertisingReport(
                new Packet(0, 0, payload, payload.length)
        );
    }

    public HCIEventLEConnectionComplete createConnectionCompleteEvent(int status, int connectionHandle) {
        byte[] payload = new byte[21];
        payload[3] = (byte) status;
        payload[4] = (byte) connectionHandle;

        return new HCIEventLEConnectionComplete(
                new Packet(0, 0, payload, payload.length)
        );
    }

    public GATTEventServiceQueryResult createGattServiceQueryResultEvent(int connectionHandle) {
        byte[] payload = new byte[25];
        payload[2] = (byte)connectionHandle;
        payload[5] = 1; // start handle = 256 (for testing only)
        payload[6] = 2; // end handle = 2 (for testing only)

        return new GATTEventServiceQueryResult(
                new Packet(0, 0, payload, payload.length)
        );
    }

    public GATTEventCharacteristicQueryResult createGattCharacteristicQueryResultEvent(int connectionHandle) {
        byte[] payload = new byte[25];
        payload[2] = (byte)connectionHandle;

        // byte 4 and 5 -> start handle: 257
        payload[4] = 1; // 256
        payload[5] = 1; // 1

        return new GATTEventCharacteristicQueryResult(
                new Packet(0, 0, payload, payload.length)
        );
    }

    // Answer of a GATT read
    public GATTEventCharacteristicValueQueryResult createGattEventCharacteristicValueQueryEvent(int connectionHandle) {
        byte[] payload = new byte[25];
        payload[2] = (byte)connectionHandle;
        payload[6] = 4; // value length
        payload[8] = 'T';
        payload[9] = 'E';
        payload[10] = 'S';
        payload[11] = 'T';

        return new GATTEventCharacteristicValueQueryResult(
                new Packet(0, 0, payload, payload.length)
        );
    }

    public GATTEventQueryComplete createGattQueryCompleteEvent(int status, int connectionHandle) {
        byte[] payload = new byte[21];
        payload[2] = (byte) connectionHandle;
        payload[4] = (byte) status;

        return new GATTEventQueryComplete(
                new Packet(0, 0, payload, payload.length)
        );
    }

    public HCIEventDisconnectionComplete createGattDisconnectCompleteEvent(int connectionHandle) {
        byte[] payload = new byte[21];
        payload[3] = (byte)connectionHandle;
        return new HCIEventDisconnectionComplete(
                new Packet(0, 0, payload, payload.length)
        );
    }

    public GATTEventAllCharacteristicDescriptorsQueryResult createGATTEventAllCharacteristicDescriptorsQueryResult(
            int handle, int connectionHandle) {
        byte[] payload = new byte[22];
        payload[2] = (byte) connectionHandle;
        payload[4] = (byte)handle;

        return new GATTEventAllCharacteristicDescriptorsQueryResult(new Packet(0, 0, payload, payload.length));
    }

//    GATTEventCharacteristicDescriptorQueryResult
}
