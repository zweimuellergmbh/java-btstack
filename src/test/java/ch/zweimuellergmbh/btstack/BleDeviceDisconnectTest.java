package ch.zweimuellergmbh.btstack;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import static com.google.common.truth.Truth.assertThat;
import static org.junit.Assert.fail;

public class BleDeviceDisconnectTest {
    public static final int STATUS_ERROR = 1; // anything not 0

    private final TestHelper testHelper = new TestHelper();
    @Before
    public void setup() {
    }

    @After
    public void teardown() { }

    /**
     * SYNC METHODS
     */
    @Test
    public void disconnectSync_throws_ifNotConnected() {
        BleAddress bleAddress = Mockito.mock(BleAddress.class);
        BleAdapter bleAdapter = Mockito.mock(BleAdapter.class);

        BleDevice bleDevice = new BleDevice(bleAddress, bleAdapter);

        try {
            bleDevice.disconnect();
            fail();
        } catch (Throwable t) {
            assertThat(t).isInstanceOf(BleGattException.class);
            assertThat(t.getMessage()).isEqualTo("GATT Request not possible. Device is not connected.");
        }
    }

    @Test
    public void disconnectSyncSuccessfully() throws TimeoutException, BleGattException {
        final BleDevice bleDevice = testHelper.getConnectedDeviceForTest(23, 1000, 1000);

        // second, the complete event
        testHelper.receiveBtStackPacketsInMilliseconds(200, bleDevice.getBleAdapter(),
                testHelper.createGattDisconnectCompleteEvent(23));

        bleDevice.disconnect();

        assertThat(bleDevice.isConnected()).isFalse();
    }

    @Test
    public void whenDisconnectSync_timesOut_throwsTimeoutException() {
        final BleDevice bleDevice = testHelper.getConnectedDeviceForTest(23, 1000, 1000);

        try {
            bleDevice.disconnect();
            fail();
        } catch (TimeoutException | BleGattException e) {
            assertThat(e.getMessage()).startsWith("Timeout disconnecting from:");
        }
    }

    /**
     * ASYNC METHODS
     */
    @Test
    public void disconnectAsync_throws_ifNotConnected() {
        BleAddress bleAddress = Mockito.mock(BleAddress.class);
        BleAdapter bleAdapter = Mockito.mock(BleAdapter.class);

        BleDevice bleDevice = new BleDevice(bleAddress, bleAdapter);

        try {
            bleDevice.disconnectAsync().toCompletableFuture().get();
            fail();
        } catch (Throwable e) {
            assertThat(e.getCause()).isInstanceOf(BleGattException.class);
            assertThat(e.getCause().getMessage()).contains("GATT Request not possible. Device is not connected.");
        }
    }

    @Test
    public void disconnectAsyncSuccessfully() throws ExecutionException, InterruptedException, BleGattException {
        final BleDevice bleDevice = testHelper.getConnectedDeviceForTest(23, 1000, 1000);

        // second, the complete event
        testHelper.receiveBtStackPacketsInMilliseconds(200, bleDevice.getBleAdapter(),
                testHelper.createGattDisconnectCompleteEvent(23));

        bleDevice.disconnectAsync().toCompletableFuture().get();

        assertThat(bleDevice.isConnected()).isFalse();
    }

    /**
     * General disconnect tests
     */
    @Test
    public void spontaneousDisconnect_changesDeviceStateTo_notConnected() throws ExecutionException, InterruptedException {
        final BleDevice bleDevice = testHelper.getConnectedDeviceForTest(42, 1000, 1000);

        // send a disconnect event
        testHelper.receiveBtStackPacketsInMilliseconds(100, bleDevice.getBleAdapter(),
                testHelper.createGattDisconnectCompleteEvent(42));

        // and sleep..to let the event arrive..
        Thread.sleep(300);

        assertThat(bleDevice.isConnected()).isFalse();
    }
}