package ch.zweimuellergmbh.btstack;

import ch.zweimuellergmbh.btstack.command.GattResponse;
import com.bluekitchen.btstack.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import static ch.zweimuellergmbh.btstack.TestHelper.STATUS_OK;
import static com.google.common.truth.Truth.assertThat;
import static org.junit.Assert.fail;

public class BleCharacteristicTest {
    private final TestHelper testHelper = new TestHelper();
    public static final int STATUS_ERROR = 1; // anything not 0

    @Before
    public void setup() {
    }

    @After
    public void teardown() {
    }

    /**
     * SYNC READ GATT
     */
    @Test
    public void readGattSync_throws_ifNotConnected() {
        BleCharacteristic bleCharacteristic = createUnconnectedCharacteristicForTest();

        try {
            bleCharacteristic.read();
            fail();
        } catch (Throwable e) {
            assertThat(e).isInstanceOf(BleGattException.class);
            assertThat(e.getMessage()).contains("GATT Request not possible. Device is not connected.");
        }
    }

    @Test
    public void readGattSync_Successfully() throws TimeoutException, BleGattException {
        BleCharacteristic bleCharacteristic = createConnectedCharacteristicForTest(1000);

        // send read gatt event and read complete event in 100ms
        testHelper.receiveBtStackPacketsInMilliseconds(100,
                bleCharacteristic.getService().getBleDevice().getBleAdapter(),
                testHelper.createGattEventCharacteristicValueQueryEvent(23),
                testHelper.createGattQueryCompleteEvent(GattResponse.ATT_ERROR_SUCCESS, 23));

        byte[] result = bleCharacteristic.read();

        assertThat(result.length).isEqualTo(4);
        assertThat(new String(result)).isEqualTo("TEST");
    }

    @Test
    public void readGattSync_fails_throwsGattException() {
        BleCharacteristic bleCharacteristic = createConnectedCharacteristicForTest(1000);

        // the erroneous complete event
        testHelper.receiveBtStackPacketsInMilliseconds(500, bleCharacteristic.getService().getBleDevice().getBleAdapter(),
                testHelper.createGattQueryCompleteEvent(STATUS_ERROR, 23));

        try {
            bleCharacteristic.read();
            fail();
        } catch (Throwable t) {
            assertThat(t).isInstanceOf(BleGattException.class);
            assertThat(t.getMessage()).startsWith("GATT read failed");
        }
    }

    @Test
    public void readGattSync_timesOut_throwsTimeoutException() {
        final BleCharacteristic bleCharacteristic = createConnectedCharacteristicForTest(1000);

        try {
            bleCharacteristic.read();
            fail();
        } catch (Throwable t) {
            assertThat(t).isInstanceOf(TimeoutException.class);
            assertThat(t.getMessage()).startsWith("Timeout reading characteristic");
        }
    }


    /**
     * ASYNC READ GATT
     */
    @Test
    public void readGattAsync_throws_ifNotConnected() {
        BleCharacteristic bleCharacteristic = createUnconnectedCharacteristicForTest();

        try {
            bleCharacteristic.readAsync().toCompletableFuture().get();
            fail();
        } catch (Throwable e) {
            assertThat(e.getCause()).isInstanceOf(BleGattException.class);
            assertThat(e.getMessage()).contains("GATT Request not possible. Device is not connected.");
        }
    }

    @Test
    public void readGattAsync_Successfully() throws ExecutionException, InterruptedException {
        BleCharacteristic bleCharacteristic = createConnectedCharacteristicForTest(1000);

        // first, the event with the read data
        // second, the complete event
        testHelper.receiveBtStackPacketsInMilliseconds(100,
                bleCharacteristic.getService().getBleDevice().getBleAdapter(),
                testHelper.createGattEventCharacteristicValueQueryEvent(23),
                testHelper.createGattQueryCompleteEvent(GattResponse.ATT_ERROR_SUCCESS, 23));

        byte[] result = bleCharacteristic.readAsync().toCompletableFuture().get();

        assertThat(result.length).isEqualTo(4);
        assertThat(new String(result)).isEqualTo("TEST");
    }

    @Test
    public void readGattAsync_fails_throwsGattException() {
        final BleCharacteristic bleCharacteristic = createConnectedCharacteristicForTest(1000);

        // the erroneous complete event
        testHelper.receiveBtStackPacketsInMilliseconds(400, bleCharacteristic.getService().getBleDevice().getBleAdapter(),
                testHelper.createGattQueryCompleteEvent(STATUS_ERROR, 23));

        try {
            bleCharacteristic.readAsync().toCompletableFuture().get();
            fail();
        } catch (Throwable t) {
            assertThat(t.getCause()).isInstanceOf(BleGattException.class);
        }
    }

    /**
     * SYNC WRITE GATT
     */
    @Test
    public void writeGattSync_throws_ifNotConnected() {
        BleCharacteristic bleCharacteristic = createUnconnectedCharacteristicForTest();

        try {
            bleCharacteristic.write(new byte[]{0x0});
            fail();
        } catch (Throwable e) {
            assertThat(e).isInstanceOf(BleGattException.class);
            assertThat(e.getMessage()).contains("GATT Request not possible. Device is not connected.");
        }
    }

    @Test
    public void writeGattSync_Successfully() throws TimeoutException, BleGattException {
        BleCharacteristic bleCharacteristic = createConnectedCharacteristicForTest(1000);

        // first, the event with the read data
        // second, the complete event
        testHelper.receiveBtStackPacketsInMilliseconds(100,
                bleCharacteristic.getService().getBleDevice().getBleAdapter(),
                testHelper.createGattEventCharacteristicValueQueryEvent(23),
                testHelper.createGattQueryCompleteEvent(GattResponse.ATT_ERROR_SUCCESS, 23));

        bleCharacteristic.write(new byte[]{0x0});
    }

    @Test
    public void writeGattSync_fails_throwsGattException() {
        BleCharacteristic bleCharacteristic = createConnectedCharacteristicForTest(1000);

        // the erroneous complete event
        testHelper.receiveBtStackPacketsInMilliseconds(400, bleCharacteristic.getService().getBleDevice().getBleAdapter(),
                testHelper.createGattQueryCompleteEvent(STATUS_ERROR, 23));

        try {
            bleCharacteristic.write(new byte[]{0x0});
            fail();
        } catch (Throwable t) {
            assertThat(t).isInstanceOf(BleGattException.class);
            assertThat(t.getMessage()).startsWith("GATT write failed");
        }
    }

    @Test
    public void writeGattSync_timesOut_throwsTimeoutException() {
        final BleCharacteristic bleCharacteristic = createConnectedCharacteristicForTest(1000);

        try {
            bleCharacteristic.write(new byte[]{0x0});
            fail();
        } catch (Throwable t) {
            assertThat(t).isInstanceOf(TimeoutException.class);
            assertThat(t.getMessage()).startsWith("Timeout writing characteristic");
        }
    }

    /**
     * ASYNC WRITE GATT
     */
    @Test
    public void writeGattAsync_throws_ifNotConnected() {
        BleCharacteristic bleCharacteristic = createUnconnectedCharacteristicForTest();

        try {
            bleCharacteristic.writeAsync(new byte[]{0x0}).toCompletableFuture().get();
            fail();
        } catch (Throwable e) {
            assertThat(e.getCause()).isInstanceOf(BleGattException.class);
            assertThat(e.getMessage()).contains("GATT Request not possible. Device is not connected.");
        }
    }

    @Test
    public void writeGattAsync_Successfully() throws ExecutionException, InterruptedException {
        BleCharacteristic bleCharacteristic = createConnectedCharacteristicForTest(1000);

        // the complete event
        testHelper.receiveBtStackPacketsInMilliseconds(500, bleCharacteristic.getService().getBleDevice().getBleAdapter(),
                testHelper.createGattQueryCompleteEvent(GattResponse.ATT_ERROR_SUCCESS, 23));

        bleCharacteristic.writeAsync(new byte[]{0x0}).toCompletableFuture().get();

    }

    @Test
    public void writeGattAsync_fails_throwsGattException() {
        final BleCharacteristic bleCharacteristic = createConnectedCharacteristicForTest(1000);

        // the erroneous complete event
        testHelper.receiveBtStackPacketsInMilliseconds(400, bleCharacteristic.getService().getBleDevice().getBleAdapter(),
                testHelper.createGattQueryCompleteEvent(STATUS_ERROR, 23));

        try {
            bleCharacteristic.writeAsync(new byte[]{0x0}).toCompletableFuture().get();
            fail();
        } catch (Throwable t) {
            assertThat(t.getCause()).isInstanceOf(BleGattException.class);
        }
    }

    /**
     * SYNC DISCOVER CHARACTERISTICS DESCRIPTORS
     */
    @Test
    public void discoverDescriptorsSync_throws_ifNotConnected() {
        final BleCharacteristic bleCharacteristic = createUnconnectedCharacteristicForTest();

        try {
            bleCharacteristic.discoverDescriptors();
            fail();
        } catch (Throwable e) {
            assertThat(e).isInstanceOf(BleGattException.class);
            assertThat(e.getMessage()).contains("GATT Request not possible. Device is not connected.");
        }
    }

    @Test
    public void discoverDescriptorsSync_successfully() throws TimeoutException, BleGattException {
        final BleCharacteristic bleCharacteristic = createConnectedCharacteristicForTest(1000);

        // first, the event with the found descriptor
        // second, another event with a found descriptor
        // third, the complete event
        testHelper.receiveBtStackPacketsInMilliseconds(
                100,
                bleCharacteristic.getService().getBleDevice().getBleAdapter(),
                testHelper.createGATTEventAllCharacteristicDescriptorsQueryResult(11, 23),
                testHelper.createGATTEventAllCharacteristicDescriptorsQueryResult(22, 23),
                testHelper.createGattQueryCompleteEvent(STATUS_OK, 23)
        );

        List<BleCharacteristicDescriptor> descriptors = bleCharacteristic.discoverDescriptors();

        // found two descriptors
        assertThat(descriptors.size()).isEqualTo(2);
        assertThat(descriptors.get(0).getInternalDescriptor().getHandle()).isEqualTo(11);
        assertThat(descriptors.get(1).getInternalDescriptor().getHandle()).isEqualTo(22);
    }

    @Test
    public void whenDiscoverDescriptorsSync_fails_throws_gattException() {
        final BleCharacteristic bleCharacteristic = createConnectedCharacteristicForTest(1000);

        // the complete event
        testHelper.receiveBtStackPacketsInMilliseconds(
                200, bleCharacteristic.getService().getBleDevice().getBleAdapter(),
                testHelper.createGattQueryCompleteEvent(STATUS_ERROR, 23));

        try {
            bleCharacteristic.discoverDescriptors();
            fail();
        } catch (Throwable t) {
            assertThat(t).isInstanceOf(BleGattException.class);
        }
    }

    @Test
    public void whenDiscoverDescriptorsSync_timesOut_throws_timeoutException() {
        final BleCharacteristic bleCharacteristic = createConnectedCharacteristicForTest(1000);

        // no incoming messages... will timeout

        try {
            bleCharacteristic.discoverDescriptors();
            fail();
        } catch (TimeoutException | BleGattException e) {
            assertThat(e.getMessage()).startsWith("Timeout discover characteristic descriptors of");
        }
    }

    /**
     * ASYNC DISCOVER CHARACTERISTICS DESCRIPTORS
     */
    @Test
    public void discoverDescriptorsAsync_throws_ifNotConnected() {
        final BleCharacteristic bleCharacteristic = createUnconnectedCharacteristicForTest();

        try {
            bleCharacteristic.discoverDescriptorsAsync().toCompletableFuture().get();
            fail();
        } catch (Throwable e) {
            assertThat(e.getCause()).isInstanceOf(BleGattException.class);
            assertThat(e.getMessage()).contains("GATT Request not possible. Device is not connected.");
        }
    }


    @Test
    public void discoverDescriptorsAsync_successfully() throws ExecutionException, InterruptedException {
        final BleCharacteristic bleCharacteristic = createConnectedCharacteristicForTest(1000);

        // first, the event with the found descriptor
        // second, another event with a found descriptor
        // third, the complete event
        testHelper.receiveBtStackPacketsInMilliseconds(
                100,
                bleCharacteristic.getService().getBleDevice().getBleAdapter(),
                testHelper.createGATTEventAllCharacteristicDescriptorsQueryResult(11, 23),
                testHelper.createGATTEventAllCharacteristicDescriptorsQueryResult(22, 23),
                testHelper.createGattQueryCompleteEvent(STATUS_OK, 23)
        );

        List<BleCharacteristicDescriptor> descriptors = bleCharacteristic.discoverDescriptorsAsync().toCompletableFuture().get();

        // found two descriptors
        assertThat(descriptors.size()).isEqualTo(2);
        assertThat(descriptors.get(0).getInternalDescriptor().getHandle()).isEqualTo(11);
        assertThat(descriptors.get(1).getInternalDescriptor().getHandle()).isEqualTo(22);
    }

    @Test
    public void whenDiscoverDescriptorsAsync_fails_throws_gattException() {
        final BleCharacteristic bleCharacteristic = createConnectedCharacteristicForTest(1000);

        // the complete event
        testHelper.receiveBtStackPacketsInMilliseconds(
                200, bleCharacteristic.getService().getBleDevice().getBleAdapter(),
                testHelper.createGattQueryCompleteEvent(STATUS_ERROR, 23));

        try {
            bleCharacteristic.discoverDescriptorsAsync().toCompletableFuture().get();
            fail();
        } catch (Throwable t) {
            assertThat(t.getCause()).isInstanceOf(BleGattException.class);
        }
    }

    /**
     * @return Returns a BleCharacteristic to be used for testing.
     *
     * The characteristic is part of a BleDevice which is connected.
     *
     * @param timeoutMillis The GATT timeout to be used.
     */
    private BleCharacteristic createConnectedCharacteristicForTest(int timeoutMillis) {
        final BTstack bTstack = Mockito.mock(BTstack.class);
        final BleAdapter bleAdapter = new BleAdapter.BleAdapterBuilder(bTstack, IOCapabilities.IO_CAP_KEYBOARD_ONLY, null)
                .useExternalBtStackServer(true)
                .connectTimeoutMs(timeoutMillis).gattTimeoutMs(timeoutMillis).build();

        final BleDevice bleDevice = Mockito.mock(BleDevice.class);
        Mockito.when(bleDevice.getBleAdapter()).thenReturn(bleAdapter);
        Mockito.when(bleDevice.isConnected()).thenReturn(true);
        Mockito.when(bleDevice.getConnectionHandle()).thenReturn(23);

        final BleService bleService = Mockito.mock(BleService.class);
        Mockito.when(bleService.getBleDevice()).thenReturn(bleDevice);

        final GATTCharacteristic gattCharacteristic = Mockito.mock(GATTCharacteristic.class);
        BleCharacteristic bleCharacteristic = new BleCharacteristic(bleService, gattCharacteristic);

        // set adapter to "INITIALIZED" state
        bleAdapter.handlePacket(testHelper.createBtStackEventPackage(BleAdapter.BTSTACK_WORKING));
        return bleCharacteristic;
    }

    /**
     * @return Returns a BleCharacteristic to be used for testing.
     * The characteristic is part of a BleDevice which is NOT connected.
     */
    private BleCharacteristic createUnconnectedCharacteristicForTest() {
        BleDevice bleDevice = Mockito.mock(BleDevice.class);
        Mockito.when(bleDevice.isConnected()).thenReturn(false);
        BleService bleService = Mockito.mock(BleService.class);
        Mockito.when(bleService.getBleDevice()).thenReturn(bleDevice);
        GATTCharacteristic gattCharacteristic = Mockito.mock(GATTCharacteristic.class);

        return new BleCharacteristic(bleService, gattCharacteristic);
    }
}