import ch.zweimuellergmbh.btstack.BleAdapter;
import ch.zweimuellergmbh.btstack.BleCharacteristic;
import ch.zweimuellergmbh.btstack.BleDevice;
import ch.zweimuellergmbh.btstack.IOCapabilities;
import com.bluekitchen.btstack.BT_UUID;
import com.bluekitchen.btstack.BTstack;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ReadFluentExample {
    private static final Logger logger = LoggerFactory.getLogger(ReadFluentExample.class);

    public static void main(String args[]) throws Exception {

        BTstack bTstack = new BTstack();
        BleAdapter bleAdapter = new BleAdapter.BleAdapterBuilder(bTstack, IOCapabilities.IO_CAP_KEYBOARD_ONLY, null)
                .useExternalBtStackServer(true)
                .build();
        bleAdapter.powerOnBtStack();

        logger.info("Waiting for LE Counter to be discovered");


        // create device
        final BleDevice bleDevice = bleAdapter.scanForDeviceWithName(10, "LE Counter");

        BT_UUID serviceUuid = new BT_UUID("0000ff10-0000-1000-8000-00805f9b34fb");
        BT_UUID characteristicUuid = new BT_UUID("0000ff11-0000-1000-8000-00805f9b34fb");

        bleDevice.connectAsync()
                .thenCompose(device -> bleDevice.discoverPrimaryServiceByUUIDAsync(serviceUuid))
                .thenCompose(s -> s.getCharacteristicByUUIDAsync(characteristicUuid))
                .thenCompose(BleCharacteristic::readAsync)
                .whenComplete((result, err) -> {
                    if (err != null) {
                        logger.error("Error: ", err);
                    } else {
                        logger.info("Read: {}", new String(result));
                    }
                })
                .thenRun(bleDevice::disconnectAsync);

        sleepSeconds(3);
    }

    private static void sleepSeconds(int seconds) {
        try {
            Thread.sleep(seconds * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
