import ch.zweimuellergmbh.btstack.*;
import com.bluekitchen.btstack.BT_UUID;
import com.bluekitchen.btstack.BTstack;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import static java.lang.Thread.sleep;

/**
 * Connect to two running instances of spp_and_gatt_counter in parallel.
 * <p>
 * The pheripherals have to run under two different addresses.
 */
public class MultiConnectExample {
    private static final Logger logger = LoggerFactory.getLogger(MultiConnectExample.class);

    public static void main(String[] args) throws ExecutionException, InterruptedException, TimeoutException, BleGattException, BtStackException {

        BTstack bTstack = new BTstack();
        BleAdapter bleAdapter = new BleAdapter.BleAdapterBuilder(bTstack, IOCapabilities.IO_CAP_KEYBOARD_ONLY, null)
                .useExternalBtStackServer(true)
                .build();

        final CountDownLatch deviceFound = new CountDownLatch(2);
        final List<BleAddress> address = new ArrayList<>();

        bleAdapter.registerRawScanCallback(advertisingData -> {
            logger.info("Found " + advertisingData.getBleAddress() + " " + new String(advertisingData.getData()));

            if (new String(advertisingData.getData()).contains("LE Counter")) {
                logger.info("Pheripheral found: " + advertisingData.getBleAddress());
                for (BleAddress bleAddress : address) {
                    if (bleAddress.getAddress().toString().equals(advertisingData.getBleAddress().getAddress().toString())) {
                        // already there
                        return;
                    }
                }
                address.add(advertisingData.getBleAddress());
                deviceFound.countDown();
            }
        });

        bleAdapter.powerOnBtStack();

        bleAdapter.startScanning();

        try {
            deviceFound.await();
            bleAdapter.stopScanning();
        } catch (InterruptedException ignored) {
        }

        final BleDevice bleDevice1 = new BleDevice(address.get(0), bleAdapter);
        final BleDevice bleDevice2 = new BleDevice(address.get(1), bleAdapter);


        // notifications
        BT_UUID serviceUuid = new BT_UUID("0000ff10-0000-1000-8000-00805f9b34fb");
        BT_UUID charUuid = new BT_UUID("0000ff11-0000-1000-8000-00805f9b34fb");


        bleDevice1.connectAsync()
                .thenCompose(d -> d.discoverPrimaryServiceByUUIDAsync(serviceUuid))
                        .thenCompose(s -> s.getCharacteristicByUUIDAsync(charUuid))
                                .thenCompose(c -> c.registerForNotificationAsync(
                                        b -> System.out.println("1: " + new String(b))));

        bleDevice2.connectAsync()
                .thenCompose(d -> d.discoverPrimaryServiceByUUIDAsync(serviceUuid))
                        .thenCompose(s -> s.getCharacteristicByUUIDAsync(charUuid))
                                .thenCompose(c -> c.registerForNotificationAsync(
                                        b -> System.out.println("2: " + new String(b))));


        // make some noise:-)
        for (int i = 0; i < 5; i++) {
            CompletableFuture<List<BleService>> cf1 = bleDevice1.discoverPrimaryServicesAsync().toCompletableFuture();
            CompletableFuture<List<BleService>> cf2 = bleDevice2.discoverPrimaryServicesAsync().toCompletableFuture();

            CompletableFuture.allOf(cf1, cf2).get();

            System.out.println("");
            System.out.println("");
            System.out.println("");
        }


        sleepSeconds(5);

        bleDevice1.disconnect();
        bleDevice2.disconnect();
    }

    private static void sleepSeconds(int seconds) {
        try {
            Thread.sleep(seconds * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
