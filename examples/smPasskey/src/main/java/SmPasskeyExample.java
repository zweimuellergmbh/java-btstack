import ch.zweimuellergmbh.btstack.*;
import ch.zweimuellergmbh.btstack.utils.SMHelper;
import com.bluekitchen.btstack.BT_UUID;
import com.bluekitchen.btstack.BTstack;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeoutException;

// NO BONDING
public class SmPasskeyExample {
    private static final Logger logger = LoggerFactory.getLogger(SmPasskeyExample.class);

    public static void main(String[] args) throws Exception {
        BTstack bTstack = new BTstack();
        SMDispachter smDispachter = new SMDispachter(bTstack);
        BleAdapter bleAdapter = new BleAdapter.BleAdapterBuilder(bTstack,
//                IOCapabilities.IO_CAP_DISPLAY_ONLY, smDispachter)
//                IOCapabilities.IO_CAP_DISPLAY_YES_NO, smDispachter)
//                IOCapabilities.IO_CAP_KEYBOARD_ONLY, smDispachter)
//                IOCapabilities.IO_CAP_NO_INPUT_NO_OUTPUT, smDispachter)
                IOCapabilities.IO_CAP_KEYBOARD_DISPLAY, smDispachter)
                .useExternalBtStackServer(true)
                .useBonding(true)
                .build();

        smDispachter.registerPasskeyRequestCallback(SMHelper.passkeySupplier);
        smDispachter.registerNumericComparisonRequestCallback(SMHelper.ncYesNoSupplier);
        smDispachter.registerJustWorksRequestCallback(SMHelper.justWorksSupplier);
        smDispachter.registerDisplayPasskeyRequestCallback(SMHelper.showPasskeyConsumer);

        bleAdapter.powerOnBtStack();

        logger.info("Waiting for SM Passkey to be discovered");

        final BleDevice bleDevice = bleAdapter.scanForDeviceWithName(10, "SM Pairing");

        // connect
        bleDevice.connect();


        // discover service
        BT_UUID serviceUuid = new BT_UUID("0000ff10-0000-1000-8000-00805f9b34fb");
        BleService service = bleDevice.discoverPrimaryServiceByUUID(serviceUuid);

        // discover characteristic
        BT_UUID characteristicUuid = new BT_UUID("0000ff12-0000-1000-8000-00805f9b34fb");
        BleCharacteristic characteristic = service.getCharacteristicByUUID(characteristicUuid);


        try {
            logger.info("Read: {}", new String(characteristic.read()));
        } catch (BleGattException | TimeoutException e) {
            logger.warn("read failed", e);
        }


        sleepSeconds(5); // detect timout is handeled correctly
        bleDevice.disconnect();


//        // when bonded - no pairing necessary (both sides need to support bonding)
//        try {
//            bleDevice.connect();
//            service = bleDevice.discoverPrimaryServiceByUUID(serviceUuid);
//            characteristic = service.getCharacteristicByUUID(characteristicUuid);
//            logger.info("Read: {}", new String(characteristic.read()));
//            bleDevice.disconnect();
//            sleepSeconds(2);
//        } catch (Exception e) {
//            logger.error("EXCEPTION:", e);
//        }

        System.exit(1);
    }

    private static void sleepSeconds(int seconds) {
        try {
            Thread.sleep(seconds * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
