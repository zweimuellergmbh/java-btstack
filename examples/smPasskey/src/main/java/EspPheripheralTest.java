import ch.zweimuellergmbh.btstack.*;
import com.bluekitchen.btstack.BTstack;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeoutException;
import java.util.function.Function;
import java.util.function.Supplier;

import static java.lang.Thread.sleep;

// NO BONDING
public class EspPheripheralTest {
    private static final Logger logger = LoggerFactory.getLogger(EspPheripheralTest.class);

    public static void main(String[] args) throws Exception {
        BTstack bTstack = new BTstack();
        SMDispachter smDispachter = new SMDispachter(bTstack);
        BleAdapter bleAdapter = new BleAdapter.BleAdapterBuilder(bTstack,
                IOCapabilities.IO_CAP_KEYBOARD_ONLY, smDispachter) // passkey
                .useExternalBtStackServer(true)
                .useBtStackTempDirectory("/Users/boris/.btstack/") // ignored, when using external btstack demon.
                .useBonding(false)
                .build();

        final Function<BleAddress, Long> passkeySupplier = (address) -> {
            JPasswordField pf = new JPasswordField();
            int okCxl = JOptionPane.showConfirmDialog(null, pf, "Enter Password", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);

            if (okCxl == JOptionPane.OK_OPTION) {
                final String password = new String(pf.getPassword());
                System.err.println("You entered: " + password);
                return Long.valueOf(password);
            }
            return (long) 0;
//            return Long.valueOf("123456");
        };


//        smDispachter.registerPasskeyRequestCallback(passkeySupplier);
        smDispachter.registerJustWorksRequestCallback((address) -> true);

        bleAdapter.powerOnBtStack();

        try {

            logger.info("Waiting for device to be discovered");

            final BleDevice bleDevice = bleAdapter.scanForDeviceWithName(10, "nimble-bleprph");

            // connect
            bleDevice.connect();

            final List<BleService> bleServices = bleDevice.discoverPrimaryServices();
            final List<BleCharacteristic> bleCharacteristics = bleServices.get(2).discoverCharacteristics();
            final BleCharacteristic characteristic = bleCharacteristics.get(1);

            int randomNum = ThreadLocalRandom.current().nextInt(0, 100);
            try {
                characteristic.write(new byte[]{(byte) randomNum});
                byte[] read = characteristic.read();
                logger.info("***********************************************************************************");
                logger.info("* Written: {}, Read: {}, Success: {}", randomNum, read, (randomNum == read[0]) ? "OK" : "FAIL");
                logger.info("***********************************************************************************");
                System.out.println("TESTRUN: is: " + ((randomNum == read[0]) ? "OK" : "FAIL"));
            } catch (BleGattException | TimeoutException e) {
                logger.warn("Pairing failed - retry", e);
            }

            bleDevice.disconnect();
            sleepSeconds(1);
        } catch (Exception e) {
            System.out.println("!*!*!*! Exception caught: " + e);
            logger.error("!*!*!*! Exception caught: ", e);
            sleepSeconds(10);
        }

        System.exit(1);
    }

    private static void sleepSeconds(int seconds) {
        try {
            Thread.sleep(seconds * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
