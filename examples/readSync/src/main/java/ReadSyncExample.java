import ch.zweimuellergmbh.btstack.*;
import com.bluekitchen.btstack.BT_UUID;
import com.bluekitchen.btstack.BTstack;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class ReadSyncExample {
    private static final Logger logger = LoggerFactory.getLogger(ReadSyncExample.class);

    public static void main(String[] args) throws Exception {
        BTstack bTstack = new BTstack();
        BleAdapter bleAdapter = new BleAdapter.BleAdapterBuilder(bTstack, IOCapabilities.IO_CAP_DISPLAY_ONLY, null)
                .useExternalBtStackServer(true)
                .useBonding(false)
                .build();

        bleAdapter.powerOnBtStack();

        logger.info("Waiting for LE Counter to be discovered");

        // wait for the LE Counter to be discovered
        List<BleDevice> bleDevices = bleAdapter.scanForDevices(5);
        bleDevices.forEach(bleDevice -> logger.info("{}", bleDevice.getBleAddress()));

        final BleDevice bleDevice = bleAdapter.scanForDeviceWithName(10, "LE Counter");
//        final BleDevice bleDevice = bleAdapter.scanForDeviceWithAddress(10, "30:AE:A4:CF:00:AE");

        // connect
        bleDevice.connect();

        // discover service
        BT_UUID serviceUuid = new BT_UUID("0000ff10-0000-1000-8000-00805f9b34fb");
        BleService service = bleDevice.discoverPrimaryServiceByUUID(serviceUuid);

        // discover characteristic
        BT_UUID characteristicUuid = new BT_UUID("0000ff11-0000-1000-8000-00805f9b34fb");
        BleCharacteristic characteristic = service.getCharacteristicByUUID(characteristicUuid);

        System.out.println(new String(characteristic.read()));
        System.out.println(characteristic.getProperties());

        // discover characteristic descriptors
        List<BleCharacteristicDescriptor> descriptors = characteristic.discoverDescriptors();
        byte[] bytes = descriptors.get(0).read();

        characteristic.registerForNotification(b -> System.out.println(new String(b)));
        sleepSeconds(3);
        characteristic.unregisterForNotification();
        sleepSeconds(1);

        bleDevice.disconnect();
        sleepSeconds(1);

    }

    private static void sleepSeconds(int seconds) {
        try {
            Thread.sleep(seconds * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
