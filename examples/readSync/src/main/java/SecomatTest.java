import ch.zweimuellergmbh.btstack.*;
import com.bluekitchen.btstack.BT_UUID;
import com.bluekitchen.btstack.BTstack;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class SecomatTest {
    private static final Logger logger = LoggerFactory.getLogger(SecomatTest.class);

    public static final int ITERATIONS = 1;
    public static final boolean DURABILITY_TESTING = false;

    //    public static final String WORKING_SSID = "FRITZ!Box 5490 ZU";
//    public static final String WORKING_PSK = "81087888083193306802";
    public static final String WORKING_SSID = "ciscosb";
    public static final String WORKING_PSK = "test123!";
    private static final String SECOMAT_ID = "SECOMAT-t0.00004";


    public static void main(String[] args) throws Exception {
        BTstack bTstack = new BTstack();
        BleAdapter bleAdapter = new BleAdapter.BleAdapterBuilder(bTstack, IOCapabilities.IO_CAP_NO_INPUT_NO_OUTPUT, null)
                .useExternalBtStackServer(true)
                .useBonding(false)
                .build();

        bleAdapter.powerOnBtStack();

        long totallyFailed = 0;

        for (int t = 0; t < ITERATIONS; t++) {
            logger.info("Waiting for Secomat to be discovered");

            // wait for the LE Counter to be discovered
//        List<BleDevice> bleDevices = bleAdapter.scanForDevices(5);
//        bleDevices.forEach(bleDevice -> logger.info("{}", bleDevice.getBleAddress()));

            final BleDevice bleDevice = bleAdapter.scanForDeviceWithName(10, SECOMAT_ID);
//            final BleDevice bleDevice = bleAdapter.scanForDeviceWithAddress(10, "C4:DD:57:7E:F0:86");

//         connect
            bleDevice.connect();

            // discover service
            final BleService wlanConfigService = bleDevice.discoverPrimaryServiceByUUID(new BT_UUID("aaaaaaaa-1234-5678-abcd-06050403ff00"));
            final BleService claimingService = bleDevice.discoverPrimaryServiceByUUID(new BT_UUID("bbbbbbbb-1234-5678-abcd-06050403ff00"));

            System.out.println("Test: Scan for SSID");
            final List<String> receivedNotifications = new ArrayList<>();
            final List<String> receivedWlanNotifications = new ArrayList<>();
            final CountDownLatch[] receivedScanEnded = new CountDownLatch[1];
            receivedScanEnded[0] = new CountDownLatch(1);
            final BleCharacteristic ssidScanCharacteristic = wlanConfigService.getCharacteristicByUUID(new BT_UUID("aaaaaaaa-1234-5678-abcd-06050403ff01"));
            final BleCharacteristic ssidScanNotification = wlanConfigService.getCharacteristicByUUID(new BT_UUID("aaaaaaaa-1234-5678-abcd-06050403ff02"));
            final BleCharacteristic scanStateNotification = wlanConfigService.getCharacteristicByUUID(new BT_UUID("aaaaaaaa-1234-5678-abcd-06050403ff05"));
            final BleCharacteristic wlanConnectionStateNotification = wlanConfigService.getCharacteristicByUUID(new BT_UUID("aaaaaaaa-1234-5678-abcd-06050403ff04"));
            final BleCharacteristic ssidPskConfigCharacteristic = wlanConfigService.getCharacteristicByUUID(new BT_UUID("aaaaaaaa-1234-5678-abcd-06050403ff03"));
            final BleCharacteristic networkConfigCharacteristic = wlanConfigService.getCharacteristicByUUID(new BT_UUID("aaaaaaaa-1234-5678-abcd-06050403ff06"));
            final BleCharacteristic serviceVersionCharacteristic = wlanConfigService.getCharacteristicByUUID(new BT_UUID("aaaaaaaa-1234-5678-abcd-06050403ffff"));

            final BleCharacteristic readClaimingTokenCharacteristic = claimingService.getCharacteristicByUUID(new BT_UUID("bbbbbbbb-1234-5678-abcd-06050403ff01"));
            final BleCharacteristic readSecomatIdCharacteristic = claimingService.getCharacteristicByUUID(new BT_UUID("bbbbbbbb-1234-5678-abcd-06050403ff02"));
            final BleCharacteristic configureTelemetryCharacteristic = claimingService.getCharacteristicByUUID(new BT_UUID("bbbbbbbb-1234-5678-abcd-06050403ff03"));
            final BleCharacteristic resetClaimingTokenCharacteristic = claimingService.getCharacteristicByUUID(new BT_UUID("bbbbbbbb-1234-5678-abcd-06050403ff04"));
            final BleCharacteristic readClaimingServiceVersionCharacteristic = claimingService.getCharacteristicByUUID(new BT_UUID("bbbbbbbb-1234-5678-abcd-06050403ffff"));

            int numTestsFailed = 0;
            
            ssidScanNotification.registerForNotification(bytes -> {
                System.out.printf("Found: %s, RSSI: %d\n",
                        new String(Arrays.copyOfRange(bytes, 2, bytes.length - 2)),
                        bytes[0]);
                receivedNotifications.add("FOUND AP");
            });
            scanStateNotification.registerForNotification(bytes -> {
                if (bytes[0] == 0) {
                    receivedNotifications.add("SCAN IDLE");
                    receivedScanEnded[0].countDown();
                } else {
                    receivedNotifications.add("SCAN RUNNING");
                }
            });
            wlanConnectionStateNotification.registerForNotification(bytes -> {
                if (bytes[0] == 0) {
                    if (bytes[1] > 0) {
                        System.out.println("WLAN NOT CONNECTED: " + ((bytes[1] == 1) ? "SSID-ERROR" : "GENERAL-ERROR"));
                        receivedWlanNotifications.add("WLAN NOT CONNECTED: " + ((bytes[1] == 1) ? "SSID-ERROR" : "GENERAL-ERROR"));
                    } else {
                        System.out.println("WLAN NOT CONNECTED");
                        receivedWlanNotifications.add("WLAN NOT CONNECTED");
                    }
                } else if (bytes[0] == 1) {
                    System.out.println("WLAN CONNECTING");
                    receivedWlanNotifications.add("WLAN CONNECTING");
                } else {
                    System.out.println("WLAN CONNECTED");
                    receivedWlanNotifications.add("WLAN CONNECTED");
                }
            });

            System.out.println("Test: Check version");
            if (serviceVersionCharacteristic.read()[0] != 0x1) {
                System.out.println("Error reading correct version number.");
                numTestsFailed++;
            } else {
                System.out.println("Success");
            }

            System.out.println("Test: Check return value of scan notification");
            if (!new String(ssidScanNotification.read()).equals("run a scan first")) {
                System.out.println("Error: scan notification did not contain 'run a scan'");
                numTestsFailed++;
            } else {
                System.out.println("Success");
            }

            System.out.println("Test: Send to many data.");
            try {
                ssidScanCharacteristic.write(new byte[]{0x1, 0x2});
                System.out.println("Fail");
                numTestsFailed++;
            } catch (BleGattException e) {
                System.out.println("Success");
            }

            System.out.println("Test: Send invalid command (not 0x1).");
            try {
                ssidScanCharacteristic.write(new byte[]{0x3});
                System.out.println("Fail");
                numTestsFailed++;
            } catch (BleGattException e) {
                System.out.println("Success");
            }

            System.out.println("Test: Send 0x1 scan command");
            try {
                ssidScanCharacteristic.write(new byte[]{0x1});
                System.out.println("Success");

                if (!receivedScanEnded[0].await(10, TimeUnit.SECONDS)) {
                    System.out.println("Error waiting for notification ended notification");
                } else {
                    // check notification list
                    System.out.println("Test: Check received notifications:");
                    receivedNotifications.forEach(System.out::println);
                    if (!receivedNotifications.get(0).equals("SCAN RUNNING")) {
                        System.out.println("Error: Expected scan pending notification");
                    }
                    if (!receivedNotifications.get(receivedNotifications.size() - 1).equals("SCAN IDLE")) {
                        System.out.println("Error: Expected scan idle notification");
                    }
                    receivedNotifications.subList(1, receivedNotifications.size() - 1).forEach(item -> {
                        if (!item.equals("FOUND AP")) {
                            System.out.println("Error: Expected ap found notification");
                        }
                    });
                }
            } catch (BleGattException e) {
                System.out.println("Fail");
                numTestsFailed++;
            }

            sleepSeconds(2);

            System.out.println("Test: Send too short SSID/PSK String");
            try {
                configureWiFi(ssidPskConfigCharacteristic, "", "test");
                System.out.println("Failed");
                numTestsFailed++;
            } catch (Exception e) {
                System.out.println("Success");
            }

            sleepSeconds(2);

            System.out.println("Test: Send too long SSID String");
            try {
                configureWiFi(ssidPskConfigCharacteristic, "0123456789012345678901234567890123456789", "test");
                System.out.println("Failed");
                numTestsFailed++;
            } catch (Exception e) {
                System.out.println("Success");
            }

            sleepSeconds(2);

            System.out.println("Test: Setting invalid SSID");
            receivedWlanNotifications.clear();
            configureWiFi(ssidPskConfigCharacteristic, "boris", "testtest");
            if ("boris".equals(new String(ssidPskConfigCharacteristic.read()))) {
                System.out.println("Read back: Success");
            }

            sleepSeconds(10);
            System.out.println("Test: Check received notifications:");
            if (receivedWlanNotifications.contains("WLAN CONNECTING")
                    && receivedWlanNotifications.contains("WLAN NOT CONNECTED: SSID-ERROR") && !receivedWlanNotifications.contains("WLAN CONNECTED")) {
                System.out.println("Success");
            } else {
                System.out.println("Failed");
                numTestsFailed++;
            }

            System.out.println("Test: Check network config Zero (not set):");
            byte[] networkConfig = networkConfigCharacteristic.read();
            if (networkConfig[0] != 1) {
                System.out.println("Failed: Invalid network config");
                numTestsFailed++;
            }
            for (int b = 1; b < networkConfig.length; b++) {
                if (networkConfig[b] != 0) {
                    System.out.println("Failed: Invalid network config (NOT ZERO)");
                    numTestsFailed++;
                }
            }

            sleepSeconds(10);

            System.out.println("Test: Setting valid SSID");
            receivedWlanNotifications.clear();
            configureWiFi(ssidPskConfigCharacteristic, WORKING_SSID, WORKING_PSK);
            if (WORKING_SSID.equals(new String(ssidPskConfigCharacteristic.read()))) {
                System.out.println("Read back: Success");
            }

            sleepSeconds(10);
            System.out.println("Test: Check received notifications:");
            if (receivedWlanNotifications.contains("WLAN CONNECTING")
                    && receivedWlanNotifications.contains("WLAN CONNECTED")) {
                System.out.println("Success");
            } else {
                System.out.println("Failed");
                numTestsFailed++;
            }
            System.out.println("Test: Read Network Config");
            networkConfig = networkConfigCharacteristic.read();
            System.out.println("DHCP: " + networkConfig[0]);
            printIp("IP:", networkConfig, 1);
            printIp("Mask: ", networkConfig, 5);
            printIp("Gateway:", networkConfig, 9);
            printIp("DNS: ", networkConfig, 13);

            System.out.println("Test: Check network config Zero (not set):");
            networkConfig = networkConfigCharacteristic.read();
            if (networkConfig[0] != 1) {
                System.out.println("Failed: Invalid network config");
                numTestsFailed++;
            }
            if (networkConfig[1] == 0) {
                System.out.println("Failed: Invalid network config at IP.");
                numTestsFailed++;
            }

            sleepSeconds(1);

            // CLAIMING
            System.out.println("Test: Read serial id");
            if (!(new String(readSecomatIdCharacteristic.read()).equals(SECOMAT_ID.substring(8)))) { // remove 'SECOMAT-'
                System.out.println("FAiled: Incorrect serial id");
                numTestsFailed++;
            } else {
                System.out.println("Success");
            }

            System.out.println("Test: get/set telemetry enable");
            System.out.println("Test: invalid value");
            try {
                configureTelemetryCharacteristic.write(new byte[]{(byte) 0x2});
                System.out.println("Error: could set invalid value");
                numTestsFailed++;
            } catch (Exception e) {
                System.out.println("Success");
            }
            System.out.println("Test: too long value");
            try {
                configureTelemetryCharacteristic.write(new byte[]{(byte)0x1, (byte) 0x2});
                System.out.println("Error: could set to loong value");
                numTestsFailed++;
            } catch (Exception e) {
                System.out.println("Success");
            }

            System.out.println("Test: valid value 1");
            configureTelemetryCharacteristic.write(new byte[]{(byte) 0x1});

            if ((int) configureTelemetryCharacteristic.read()[0] == 0x0) {
                System.out.println("Error: Wrong value");
            }
            System.out.println("Test: valid value 0");
            configureTelemetryCharacteristic.write(new byte[]{(byte) 0x0});
            if ((int) configureTelemetryCharacteristic.read()[0] == 0x1) {
                System.out.println("Error: Wrong value");
            }

            System.out.println("Test: Claiming Token");
            System.out.println("Test: invalid value");
            try {
                resetClaimingTokenCharacteristic.write(new byte[]{(byte) 0x2});
                System.out.println("Error: could set invalid value");
                numTestsFailed++;
            } catch (Exception e) {
                System.out.println("Success");
            }
            System.out.println("Test: too long value");
            try {
                resetClaimingTokenCharacteristic.write(new byte[]{(byte)0x1, (byte) 0x2});
                System.out.println("Error: could set to loong value");
                numTestsFailed++;
            } catch (Exception e) {
                System.out.println("Success");
            }

            String lastToken = "00000000000000000000000000000000";
            for (int numTokenTest = 0; numTokenTest < 10; numTokenTest++) {
                String token = new String(readClaimingTokenCharacteristic.read());
                if (token.length() != 32) {
                    System.out.println("Failed: Invalid token length");
                    numTestsFailed++;
                }
                if (token.equals(lastToken)) {
                    System.out.println("Failed: Token does not differ from last one");
                }
                lastToken = token;
                resetClaimingTokenCharacteristic.write(new byte[]{0x1});
                sleepSeconds(1);
            }
            // TODO invalid values tests

            System.out.println("################# RUN: " + t);
            System.out.println("################# FAILED: " + numTestsFailed);

            sleepSeconds(1);

            if (DURABILITY_TESTING) {
                configureWiFi(ssidPskConfigCharacteristic, "boris", "test");
                for (int i = 0; i < 10; i++) {
                    receivedScanEnded[0] = new CountDownLatch(1);
                    ssidScanCharacteristic.write(new byte[]{0x1});
                    if (!receivedScanEnded[0].await(10, TimeUnit.SECONDS)) {
                        System.out.println("Error waiting for notification ended notification");
                    }
                }

                configureWiFi(ssidPskConfigCharacteristic, WORKING_SSID, "asdfasdf");
                for (int i = 0; i < 10; i++) {
                    receivedScanEnded[0] = new CountDownLatch(1);
                    ssidScanCharacteristic.write(new byte[]{0x1});
                    if (!receivedScanEnded[0].await(10, TimeUnit.SECONDS)) {
                        System.out.println("Error waiting for notification ended notification");
                    }
                }

                configureWiFi(ssidPskConfigCharacteristic, WORKING_SSID, WORKING_PSK);
                for (int i = 0; i < 10; i++) {
                    receivedScanEnded[0] = new CountDownLatch(1);
                    ssidScanCharacteristic.write(new byte[]{0x1});
                    if (!receivedScanEnded[0].await(10, TimeUnit.SECONDS)) {
                        System.out.println("Error waiting for notification ended notification");
                    }
                }

                configureWiFi(ssidPskConfigCharacteristic, "blabla", WORKING_PSK);
                for (int i = 0; i < 10; i++) {
                    receivedScanEnded[0] = new CountDownLatch(1);
                    ssidScanCharacteristic.write(new byte[]{0x1});
                    if (!receivedScanEnded[0].await(10, TimeUnit.SECONDS)) {
                        System.out.println("Error waiting for notification ended notification");
                    }
                }

                configureWiFi(ssidPskConfigCharacteristic, WORKING_SSID, "asdfasdf");
                for (int i = 0; i < 10; i++) {
                    receivedScanEnded[0] = new CountDownLatch(1);
                    ssidScanCharacteristic.write(new byte[]{0x1});
                    if (!receivedScanEnded[0].await(10, TimeUnit.SECONDS)) {
                        System.out.println("Error waiting for notification ended notification");
                    }
                }
                configureWiFi(ssidPskConfigCharacteristic, WORKING_SSID, WORKING_PSK);
            }


            bleDevice.disconnect();
            sleepSeconds(10);

            System.out.println("FAILED TOTALLY: " + totallyFailed);
        }

        System.exit(0);
    }

    private static void printIp(String name, byte[] networkConfig, int i) {
        System.out.printf("%s: %d.%d.%d.%d\n", name,
                Byte.toUnsignedInt(networkConfig[i]),
                Byte.toUnsignedInt(networkConfig[i + 1]),
                Byte.toUnsignedInt(networkConfig[i + 2]),
                Byte.toUnsignedInt(networkConfig[i + 3]));
    }

    private static void configureWiFi(BleCharacteristic ssidPskConfigCharacteristic, String ssid, String psk) throws IOException, BleGattException, TimeoutException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        baos.write(ssid.length());
        baos.write(ssid.getBytes(StandardCharsets.UTF_8));
        baos.write(psk.length());
        baos.write(psk.getBytes(StandardCharsets.UTF_8));
        ssidPskConfigCharacteristic.write(baos.toByteArray());
    }

    private static void sleepSeconds(int seconds) {
        try {
            Thread.sleep(seconds * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}


// RANDOM STRING
//for (int i = 0; i < 1000; i++) {
//
//        int leftLimit = 97; // letter 'a'
//        int rightLimit = 122; // letter 'z'
//        Random random = new Random();
//
//        String generatedString = random.ints(leftLimit, rightLimit + 1)
//        .limit(i)
//        .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
//        .toString();
//
//        ssidPskConfigCharacteristic.write(generatedString.getBytes(StandardCharsets.UTF_8));
//        System.out.println(new String(ssidPskConfigCharacteristic.read()));
//        }